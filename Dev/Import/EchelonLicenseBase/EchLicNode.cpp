//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// File EchLicNode.cpp
//
// This file contains some utility base classes used by the Echelon 
// License to access the XML license file.  These classes are built
// using the Microsoft XML DOM interface.
//
// Copyright (c) 2009 Echelon Corporation.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "EchLicNode.h"
#include <comdef.h>

const TCHAR * EchLicNode::BOOL_TRUE_STRING = TEXT("true");
const TCHAR * EchLicNode::BOOL_FALSE_STRING = TEXT("false");

EchLicDoc::EchLicDoc()
{
    m_pXmlDoc = NULL;
    m_hasBeenLoaded = false;
}

EchLicDoc::~EchLicDoc()
{
    Set(NULL);
}

void EchLicDoc::Set(EchLic_IXMLDomDocumentPtr pXmlDoc)
{
    if (pXmlDoc == NULL)
    {
        m_hasBeenLoaded = false;
    }
    else
    {
        pXmlDoc->AddRef();
    }

    if (m_pXmlDoc != NULL)
    {
        m_pXmlDoc->Release();
    }
    m_pXmlDoc = pXmlDoc;    
}

EchLicSts EchLicDoc::Open(void)
{
    Set(NULL);
    EchLicSts sts = EchLicSts_Good; 

    EchLic_IXMLDomDocumentPtr pIXMLDOMDocument;
    if (pIXMLDOMDocument.CreateInstance(ECHLIC_DOM_UUID) == S_OK)
    {
        Set(pIXMLDOMDocument);
    } 
    else
    {
        sts = EchLicSts_OpenFailed;
    }
    return sts;
}

    /* Close the specified license file. */
EchLicSts EchLicDoc::Close(void)
{
    Set(NULL);

    return EchLicSts_Good;
}

EchLicSts EchLicDoc::GetRoot(const IN TCHAR *rootName, OUT EchLicNode &root)
{
    EchLic_IXMLDOMNodePtr pRoot;
    HRESULT hr = m_pXmlDoc->raw_selectSingleNode(bstr_t(rootName), &pRoot);
    if (hr == S_OK)
    {
        root.Set(pRoot, GetDoc());
    }
    return hr == S_OK ? EchLicSts_Good : EchLicSts_NodeNotFound;
}

EchLicSts EchLicDoc::SetProcessingInstructions(void)
{
    EchLicSts sts = EchLicSts_FailedToAddNode;
    EchLic_IXMLDOMProcessingInstructionPtr pi;
    if (m_pXmlDoc->raw_createProcessingInstruction(
         bstr_t(TEXT("xml")), 
         bstr_t(TEXT(" version='1.0' encoding='UTF-8'")), &pi) == S_OK)
    {
        EchLic_IXMLDOMNodePtr pOriginalPiNode;
        if (m_pXmlDoc->get_firstChild(&pOriginalPiNode) == S_OK)
        {
            EchLic_IXMLDOMNodePtr piNode;
            if (m_pXmlDoc->raw_replaceChild(pi, pOriginalPiNode, &piNode) == S_OK)
            {
                sts = EchLicSts_Good;
            }
        }
        else
        {
            EchLic_IXMLDOMNodePtr piNode;
            if (m_pXmlDoc->raw_appendChild(pi, &piNode) == S_OK)
            {
                sts = EchLicSts_Good;
            }
        }
    }
    return sts;
}

EchLicSts EchLicDoc::CreateRoot(const IN TCHAR *rootName, OUT EchLicNode &root)
{
    /* Processing instructions must be the first element in the document */
    EchLicSts sts = SetProcessingInstructions();

    if (sts == EchLicSts_Good)
    {
        EchLicNode docNode;

        docNode.SetDoc(m_pXmlDoc);
        docNode.Set(m_pXmlDoc, m_pXmlDoc);
        sts = docNode.AddOnlyChild(rootName, root);
        if (sts == EchLicSts_Good)
        {
            m_hasBeenLoaded = true;
        }
    }
    return sts;
}

EchLicSts EchLicDoc::OpenForRead(IN const TCHAR *schemaPath)
{
    EchLicSts sts = EchLicSts_Good;

    if (m_hasBeenLoaded)
    {
        sts = EchLicSts_AlreadyLoaded;
    }
    else
    {
        sts = Open();
    }

#if !defined USE_OLD_XML_DOM
    if (sts == EchLicSts_Good)
    {
        IXMLDOMSchemaCollection2Ptr pIXMLDOMSchemaCollection2Ptr;
        sts = EchLicSts_FailedToLoadSchema;

        // Create the Schema Collections
        if (pIXMLDOMSchemaCollection2Ptr.CreateInstance(ECHLIC_SCHEMACACHE_UUID) == S_OK)
        {
            // Add the schema to the collection
            if (pIXMLDOMSchemaCollection2Ptr->raw_add((_bstr_t)_T(""), (variant_t)schemaPath) == S_OK)
            {
                // Attach schemas
                variant_t schemas(pIXMLDOMSchemaCollection2Ptr.GetInterfacePtr());
                if (m_pXmlDoc->putref_schemas(schemas) == S_OK)
                {
                    if (m_pXmlDoc->put_async(false) == S_OK)
                    {
                        if (m_pXmlDoc->put_validateOnParse(false) == S_OK)
                        {
                            sts = EchLicSts_Good;
                        }
                    }
                }
            }
        }
    }
#endif
    return sts;
}

EchLicSts EchLicDoc::ValidateSchema(OUT EchLicString &failureExplanation)
{
    EchLicSts sts = EchLicSts_Good;
#if !defined USE_OLD_XML_DOM
    MSXML2::IXMLDOMParseErrorPtr p;
    if (SUCCEEDED(m_pXmlDoc->raw_validate(&p)) && p != NULL)
    {
        HRESULT hr;
        if (p->get_errorCode(&hr) == S_FALSE)
        {
            m_hasBeenLoaded = true;
        }
        else
        {
            sts = EchLicSts_InvalidFormat;
            bstr_t reason;
            if (p->get_reason(&reason.GetBSTR()) == S_OK)
            {
                failureExplanation = reason;
            }
        }
    }
    else
    {
        sts = EchLicSts_OpenFailed;
    }
#endif
    return sts;
}

/* Load the specified license file. */
EchLicSts EchLicDoc::Load(const TCHAR *sourceLicenseFilePath, 
                          IN const TCHAR *schemaPath,
                          EchLicString &failureExplanation)
{
    EchLicSts sts = EchLicSts_Good;

    sts = OpenForRead(schemaPath);

    if (sts == EchLicSts_Good)
    {
	    variant_t path(sourceLicenseFilePath);
        VARIANT_BOOL loadSucceeded;

        if (m_pXmlDoc->raw_load(path, &loadSucceeded) == S_OK && loadSucceeded)
        {
            sts = ValidateSchema(failureExplanation);
            if (sts == EchLicSts_Good)
            {
                m_path = sourceLicenseFilePath;
            }
        }
        else
	    {
            sts = EchLicSts_OpenFailed;
        }
    }
    return sts;
}

EchLicSts EchLicDoc::LoadXml(IN const TCHAR *xml,
                  IN const TCHAR *schemaPath,
                  OUT EchLicString &failureExplanation)
{
    EchLicSts sts = EchLicSts_Good;

    sts = OpenForRead(schemaPath);

    if (sts == EchLicSts_Good)
    {
	    bstr_t xmlData(xml);
        VARIANT_BOOL loadSucceeded;
        if (m_pXmlDoc->raw_loadXML(xmlData, &loadSucceeded) == S_OK && loadSucceeded)
        {
            sts = ValidateSchema(failureExplanation);
            if (sts == EchLicSts_Good)
            {
                // Need to replace processing instructions, the XML string will not
                // include encoding information.
                sts = SetProcessingInstructions();
            }
        }
        else
	    {
            sts = EchLicSts_OpenFailed;
        }
    }
    return sts;
}

EchLicSts EchLicDoc::GetXml(OUT EchLicString &xmlString)
{
    HRESULT hr;
    bstr_t xmlBstr;
    hr = m_pXmlDoc->get_xml(&xmlBstr.GetBSTR());
    if (hr == S_OK)
    {
        xmlString.SetW(xmlBstr, false);
    }
    return hr == S_OK ? EchLicSts_Good : EchLicSts_FailedToGetValue;
}

EchLicSts EchLicDoc::Save(IN const TCHAR *destinationPath)
{
    EchLicSts sts = EchLicSts_FailedToSaveFile;
    if (m_pXmlDoc != NULL)
    {
        bstr_t fileName = bstr_t(destinationPath);
        if (m_pXmlDoc->raw_save(variant_t(fileName.GetBSTR())) == S_OK)
        {
            sts = EchLicSts_Good;
            m_path = destinationPath;
        }
    }
    return sts;
}

/* Get the path last used to load or save the document. */
EchLicSts EchLicDoc::GetPath(OUT EchLicString &path)
{
    EchLicSts sts = EchLicSts_Good;
    if (m_path.GetLen() == 0)
    {
        sts = EchLicSts_PathNotSet;
    }
    else
    {
        path = m_path;
    }
    return sts;
}


EchLicNode::EchLicNode() : EchLicDoc()
{
    m_pNode = NULL;
}
EchLicNode::~EchLicNode()
{
    SetDoc(NULL);
}

void EchLicNode::SetDoc(EchLic_IXMLDomDocumentPtr pXmlDoc)
{
    if (pXmlDoc == NULL)
    {
        Set(NULL, NULL);
    }
    EchLicDoc::Set(pXmlDoc);
}

/* Set the node and doc.  */
void EchLicNode::Set(EchLic_IXMLDOMNodePtr pNode, EchLic_IXMLDomDocumentPtr pXmlDoc)
{
    if (pNode != NULL)
    {
        pNode->AddRef();
        SetDoc(pXmlDoc);
    }

    if (m_pNode != NULL)
    {
        m_pNode->Release();
    }    
    m_pNode = pNode;
}

/* Add a child node with the specified name. */
EchLicSts EchLicNode::AddChild(const IN TCHAR *nodeName, OUT EchLicNode &childNode)
{
    EchLicSts sts = EchLicSts_FailedToAddNode;
    EchLic_IXMLDOMElementPtr pElement;
    // Create an element.  Does an addref
    if (GetDoc()->raw_createElement(bstr_t(nodeName), &pElement) == S_OK)
    {   
        EchLic_IXMLDOMNodePtr pNode;
        // Append the child, which does an addref
        if (m_pNode->raw_appendChild(pElement, &pNode) == S_OK)
        {
            // Done with the element.
            childNode.Set(pNode, GetDoc());           
            sts = EchLicSts_Good;
        }
    }               
    return sts;
}

/* Add a child node with the specified name, or if one already exists, return it. */
EchLicSts EchLicNode::AddOnlyChild(const IN TCHAR *nodeName, OUT EchLicNode &childNode)
{
    EchLicSts sts = FindNode(nodeName, childNode);
    if (sts == EchLicSts_NodeNotFound)
    {
        sts = AddChild(nodeName, childNode);
    }
    return sts;
}

/* Find the child node. */
EchLicSts EchLicNode::FindNode(const IN TCHAR *nodeName, OUT EchLicNode &node)
{
    EchLicSts sts = EchLicSts_NodeNotFound;
    EchLic_IXMLDOMNodePtr pNode;
    HRESULT hr = m_pNode->raw_selectSingleNode(bstr_t(nodeName), &pNode);
    if (hr == S_OK)
    {
        node.Set(pNode, GetDoc());
        sts = EchLicSts_Good;
    }
    return sts;
}

/* Find the first child node. */
EchLicSts EchLicNode::GetFirstChild(OUT EchLicNode &childNode)
{
    HRESULT hr;
    EchLic_IXMLDOMNodePtr pChildNode;
    hr = m_pNode->get_firstChild(&pChildNode);
    if (hr == S_OK)
    {
        childNode.Set(pChildNode, GetDoc());
    }
    return hr == S_OK ? EchLicSts_Good : EchLicSts_NodeNotFound;
}

/* Find the next sibling. */
EchLicSts EchLicNode::GetNext(OUT EchLicNode &node)
{
    HRESULT hr;
    EchLic_IXMLDOMNodePtr pNextNode;
    hr = m_pNode->get_nextSibling(&pNextNode);
    if (hr == S_OK)
    {
        node.Set(pNextNode, GetDoc());
    }
    return hr == S_OK ? EchLicSts_Good : EchLicSts_NodeNotFound;
}

EchLicSts EchLicNode::GetNext()
{
    return GetNext(*this);
}

/* Get the name of the node */
EchLicSts EchLicNode::GetName(OUT EchLicString &nodeName)
{
    HRESULT hr;
    bstr_t name;
    hr = m_pNode->get_nodeName(&name.GetBSTR());
    if (hr == S_OK)
    {
        nodeName = name;
    }
    return hr == S_OK ? EchLicSts_Good : EchLicSts_FailedToReadNodeName;
}

/* Add a property. */
EchLicSts EchLicNode::AddProperty(unsigned int value)
{

    TCHAR valueString[100];
    _stprintf_s(valueString, sizeof(valueString)/sizeof(TCHAR), TEXT("%u"), value);

    return m_pNode->put_text(bstr_t(valueString)) == S_OK ? EchLicSts_Good : EchLicSts_FailedToSetValue;
}

EchLicSts EchLicNode::TranslateBoolToString(IN bool value, OUT EchLicString &stringValue)
{
    stringValue.Set(value ? BOOL_TRUE_STRING : BOOL_FALSE_STRING);
    return EchLicSts_Good;
}

    /* Add a property. */
EchLicSts EchLicNode::AddBoolProperty(bool value)
{
    EchLicString stringValue;
    TranslateBoolToString(value, stringValue);
    return AddProperty(stringValue);
}

    /* Add a property. */
EchLicSts EchLicNode::AddBoolProperty(const IN TCHAR *nodeName, bool value)
{
    EchLicString stringValue;
    TranslateBoolToString(value, stringValue);
    return AddProperty(nodeName, stringValue);
}

    /* Add a property. */
EchLicSts EchLicNode::AddProperty(const TCHAR *value)
{
    EchLicString valueString;
    /* Set a temporay string to normalize the value. */
    valueString.Set(value, true);
    return m_pNode->put_text(bstr_t(valueString)) == S_OK ? EchLicSts_Good : EchLicSts_FailedToSetValue;
}

EchLicSts EchLicNode::AddProperty(BSTR value)
{
    EchLicString valueString;
    /* Set a temporay string to normalize the value. Use Wide string to avoid losing any data*/
    valueString.SetW(_bstr_t(value), true);
    return m_pNode->put_text(bstr_t(valueString)) == S_OK ? EchLicSts_Good : EchLicSts_FailedToSetValue;
}

/* Get a property.  */
EchLicSts EchLicNode::GetProperty(unsigned int &value)
{
    bstr_t valueStr;
    EchLicSts sts = EchLicSts_FailedToGetValue;
    if (m_pNode->get_text(&valueStr.GetBSTR()) == S_OK)
    {
        if (sscanf_s(valueStr, "%u", &value) == 1)
        {
            sts = EchLicSts_Good;
        }
    }
    return sts;
}

EchLicSts EchLicNode::GetProperty(int &value)
{
    bstr_t valueStr;
    EchLicSts sts = EchLicSts_FailedToGetValue;
    if (m_pNode->get_text(&valueStr.GetBSTR()) == S_OK)
    {
        if (sscanf_s(valueStr, "%i", &value) == 1)
        {
            sts = EchLicSts_Good;
        }
    }
    return sts;
}

EchLicSts EchLicNode::TranslateStringToBool(const IN TCHAR *stringValue, OUT bool &value)
{
    value = _tcsicmp(stringValue, BOOL_TRUE_STRING) == 0;
    return EchLicSts_Good;
}

EchLicSts EchLicNode::GetBoolProperty(bool &value)
{
    EchLicString stringValue;
    EchLicSts sts = GetProperty(stringValue);
    if (sts == EchLicSts_Good)
    {
        sts = TranslateStringToBool(stringValue.Get(), value);
    }
    return sts;
}

EchLicSts EchLicNode::GetBoolProperty(const IN TCHAR *nodeName, bool &value)
{
    EchLicString stringValue;
    EchLicSts sts = GetProperty(nodeName, stringValue);
    if (sts == EchLicSts_Good)
    {
        sts = TranslateStringToBool(stringValue.Get(), value);
    }
    return sts;
}
    
EchLicSts EchLicNode::GetOptionalBoolProperty(const IN TCHAR *nodeName, bool &value, bool defaultValue)
{
    EchLicSts sts = GetBoolProperty(nodeName, value);
    if (sts == EchLicSts_NodeNotFound)
    {
        value = defaultValue;
        sts = EchLicSts_Good;
    }
    return sts;
}

/* Get a property.  */
EchLicSts EchLicNode::GetProperty(EchLicString &value)
{
    bstr_t valueStr;
    EchLicSts sts = EchLicSts_FailedToGetValue;
    if (m_pNode->get_text(&valueStr.GetBSTR()) == S_OK)
    {
        sts = EchLicSts_Good;
        value.Decode(valueStr);
    }
    return sts;
}

EchLicSts EchLicNode::GetXmlString(EchLicString &xmlString)
{
    HRESULT hr;
    bstr_t xmlBstr;
    hr = m_pNode->get_xml(&xmlBstr.GetBSTR());
    if (hr == S_OK)
    {
        xmlString.Set(xmlBstr, false);
    }
    return hr == S_OK ? EchLicSts_Good : EchLicSts_FailedToGetValue;
}

