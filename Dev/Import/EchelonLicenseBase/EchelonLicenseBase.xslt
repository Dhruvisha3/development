﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:template match="/">
    <html>
      <body>
        <b><xsl:text>License Type: </xsl:text></b>
        <xsl:value-of select="License/Content/LicenseType" /><br />
        <b><xsl:text>Product Edition: </xsl:text></b>
        <xsl:value-of select="License/Content/ProductEdition" /><br />
        <b><xsl:text>License Id: </xsl:text></b>
        <xsl:value-of select="License/Content/LicenseId" /><br />
        <b><xsl:text>Host Name: </xsl:text></b>
        <xsl:value-of select="License/Content/HostName" /><br />
        <b><xsl:text>User Notes: </xsl:text></b>
        <xsl:value-of select="License/Content/UserNotes" /><br />
        <br />
        <b><xsl:text>Features </xsl:text></b>
        <table border="1" frame="box">
          <tr style="background-color:lightblue;color:black">
            <th>Feature Name</th>
            <th>Feature Value</th>
          </tr>
          <xsl:for-each select="License/Content/Features/*/*">
            <tr style="background-color:lightyellow">
              <td>
                <xsl:value-of select="local-name()"/>
              </td>
              <td>
                <xsl:value-of select="@* | node()" />
              </td>
            </tr>
          </xsl:for-each>
          <tr style="background-color:lightyellow">
            <td>
              <xsl:text>ExpirationDate</xsl:text>
            </td>
            <td>
              <xsl:value-of select="License/Content/ExpirationDate" />
            </td>
          </tr>
        </table>
        <br />
        <b><xsl:text>Machine Locks </xsl:text></b>
        <table border="1" frame="box">
          <tr style="background-color:lightblue;color:black">
            <th>Lock Category</th>
            <th>Lock Name</th>
            <th>Lock Value</th>
          </tr>
          <xsl:for-each select="License/Content/MachineLock/*/*">
            <tr style="background-color:lightyellow">
              <td>
                <xsl:value-of select="local-name(parent::*)" />
              </td>
              <td>
                <xsl:value-of select="local-name()"/>
              </td>
              <td>
                <xsl:value-of select="@* | node()" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
