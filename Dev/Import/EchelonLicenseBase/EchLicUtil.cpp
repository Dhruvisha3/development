//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// File EchLicUtil.cpp
//
// This file contains a set of simple utility classes used by the 
// licensing system.
//
// Copyright (c) 2009 Echelon Corporation.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "EchLicUtil.h"
#include "assert.h"

//============================================================================
//                               EchLicString 
//============================================================================

EchLicString::EchLicString()
{
}

EchLicString::EchLicString(const TCHAR *string)
{
    Set(string);
}

EchLicString::~EchLicString()
{
    Set(NULL);
}

bool EchLicString::LegalXmlChar(WCHAR c)
{
    /*
      Illegal characters include any characters outside of the legal XML range:
        #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]

      They also include the following characters, which are used for control:
        ‘<’, ‘>’, ‘&’, ‘’’,  ‘”’

      Finally the XML documentation discourages use of the “compatibility” characters, 
      as they are “either control characters or permanently undefined Unicode characters.  
      As such, characters in the following ranges will also be considereed "illegal":
        [#x0007F-#x00084], [#x00086-#x0009F], [#x0FDD0-#x0FDDF],
        [#x1FFFE-#x1FFFF], [#x2FFFE-#x2FFFF], [#x3FFFE-#x3FFFF],
        [#x4FFFE-#x4FFFF], [#x5FFFE-#x5FFFF], [#x6FFFE-#x6FFFF],
        [#x7FFFE-#x7FFFF], [#x8FFFE-#x8FFFF], [#x9FFFE-#x9FFFF],
        [#xAFFFE-#xAFFFF], [#xBFFFE-#xBFFFF], [#xCFFFE-#xCFFFF],
        [#xDFFFE-#xDFFFF], [#xEFFFE-#xEFFFF], [#xFFFFE-#xFFFFF],
        [#x10FFFE-#x10FFFF].

      Windows encodes unicode characters using UTF-16.  A single TCHAR can hold
      all unicode characters in the "Basic Multi-Lingual Plane".  However, 
      unicode also defines ranges from 0x1000-0x10FFFF.  These are represented using
      two UTF-16 characters, called surragates.  Surragates come in pairs, with most 
      significant character in the range 0xD800-0xDBFF, and the least significant in 
      the range 0xDC00-0xDCFF.  This code treats surrogates as illegal.  As a result, both
      the high and low surrogate gets converted.  When it is read back, they will both be
      converted back.
    */
    if (c == 0x9 ||
        c == 0xa ||
        c == 0xd ||
        (c >= 0x20 && c <= 0xD7ff) ||
        (c >= 0xE000 && c <= 0xFFFD))
    {
        // Legal XML character
        if (c == '<' ||
            c == '>' ||
            c == '&' ||
            c == '\'' ||
            c == '"')
        {
            // a control character
            return false;
        }
        else if ((c >= 0x0007f && c <= 0x00084) ||
                 (c >= 0x00086 && c <= 0x0009F) ||
                 (c >= 0x0FDD0 && c <= 0x0FDDF))
        {
            // "Compatibility characters"
            return false;
        }
        else if (c >= 0x1FFFE && 
                 ((c & 0xFFFF) == 0xFFFE ||
                  (c & 0xFFFF) == 0xFFFF))
        {
            // More "Compatibility characters"
            return false;
        }
        else
        {   // Wow - a legal  character!
            return true;
        }
    }
    return false;        
}

// All of the characters below are illegal file name characters except for
// '&'.  The '&' character is included in this array because it is reserved
// by the licensing code as an escape character for the other bad characters.
static const WCHAR *badFileNameCharacters = L"<>:\"/\\|?*.&";

bool EchLicString::LegalFileNameChar(WCHAR c)
{
    return ((c > 31) && (c < 255) && (wcschr(badFileNameCharacters, c) == NULL));
}

/* If destString != NULL, copy the string, and validate against the stringSize.
 * if destString == NULL, calculate required stringSize
 * stringSize does NOT include NULL terminator.
 */
void EchLicString::SanitizeString(IN WCHAR *destString, 
                              IN const WCHAR *sourceString,
                              IN OUT size_t &stringSize,
                              bool isFileName)
{
    size_t i; 
    size_t j;
    size_t bufferSize;

    if (destString)
    {
        // stringSize does not include null terminator, but bufferSize does. 
        bufferSize = stringSize + 1;
    }

    size_t sourceStringLen = wcslen(sourceString);
    for (i = sourceStringLen - 1; i >= 0 && sourceString[i] == ' '; i--)
    {
        // Exclude trailing blank;
        sourceStringLen--;
    }
    for (i = 0, j = 0; i < sourceStringLen; i++)
    {
        if (j == 0 && sourceString[i] == ' ')
        {
            // strip leading blanks.
        }
        else if (isFileName ? !LegalFileNameChar(sourceString[i]) : !LegalXmlChar(sourceString[i]))
        {
            #define MAX_ESCAPE_SEQ 100
            WCHAR buf[MAX_ESCAPE_SEQ];
            // Create escape
            swprintf_s(buf, MAX_ESCAPE_SEQ, L"&#%u;", sourceString[i]);
            size_t escapeLen = wcslen(buf);
            if (destString)
            {
                wcscpy_s(&destString[j], bufferSize-j, buf);
                j += escapeLen;
            }
            else
            {
                stringSize += escapeLen;
                j += escapeLen;
            }                        
        }
        else 
        {
            if (destString != NULL)
            {
                destString[j++] = sourceString[i];
            }
            else
            {
                stringSize++;
                j++;
            }
        }
    }

    if (destString)
    {
        destString[stringSize] = 0;
    }
}

static const WCHAR *whitespace = L" \r\n\t";

void EchLicString::UnformatXml(void)
{
    int len = GetLen();
    WCHAR *pUnformated = new WCHAR[len+1];
    if (pUnformated != NULL)
    {
        bool inElement = FALSE;
        WCHAR *d = pUnformated;
        int i; 
        const WCHAR *s;
        for (s = GetW(), i = 0; *s != 0 && i < len; s++, i++)
        {
            const WCHAR *s2 = s;
            // See if this is white space between elements.
            while (*s2 && wcschr(whitespace, *s2) != NULL)
            {
                s2++;
            }
            if (*s2 == '<')
            {
                s = s2; // Skip the whitespace
            }
            assert(i < len);
            *d++ = *s;
        }
        assert(i <= len);
        *d++ = 0;
        SetW(pUnformated);
        delete[] pUnformated;
    }
}

void EchLicString::EncodeFileName(const TCHAR *string)
{
    Set(string, true, true);
}

void EchLicString::Set(const TCHAR *string, bool useEscapes, bool isFileName)
{
    bstr_t bstrString(string);
    SetW(bstrString, useEscapes, isFileName);
}

void EchLicString::SetW(const WCHAR *string, bool useEscapes, bool isFileName)
{
    if (string == NULL)
    {
        m_string = (WCHAR *)NULL;
    }
    else
    {
        if (useEscapes)
        {
            size_t stringSize = 0;
            /* Calculate string size.  Does NOT include NULL terminator */
            SanitizeString(NULL, string, stringSize, isFileName);

            /* Even if there are no characters, create an empty string */            
            WCHAR *pWString = new WCHAR[stringSize+1];
            SanitizeString(pWString, string, stringSize, isFileName);
            m_string = pWString;
            delete[] pWString;
        }
        else
        {
            m_string = string;
        }
    }
}

    /* Decode and set the value of the string */
void EchLicString::Decode(const WCHAR *string)
{
    /* This may be larger than necessary since the decoded string may shrink.  That's OK */
    size_t oldSize = wcslen(string)+1;

    WCHAR *pString = new WCHAR[oldSize];

    size_t i = 0;

    if (pString)
    {
        size_t newSize = 0;
        while (i < oldSize)
        {
            if (string[i] == '&' && ((i+1) < oldSize) && string[i+1] == '#')
            {
                i += 2;
                TCHAR value = 0;
                while (i < oldSize && isdigit(string[i]))
                {
                    value = value*10 + string[i++]-'0';
                }
                pString[newSize++] = value;
                // The next character should be a ';'
                // Swollow it up;
                assert(string[i] == ';');
                i++;
            }
            else 
            {
                pString[newSize++] = string[i++];
            }
        }
    }
    SetW(pString, false, false);
    delete[] pString;
}


void EchLicString::Cat(const TCHAR *string)
{
    if (m_string.length() == 0)
    {
        Set(string, false);
    }
    else
    {
        size_t newSize = GetLen() + 1;
        if (string)
        {
            newSize += _tcslen(string);
        }
        TCHAR *pString = new TCHAR[newSize];
        if (pString == NULL)
        {
            newSize = 0;
        }
        else
        {
            _tcscpy_s(pString, newSize, m_string);
            if (string)
            {
                _tcscat_s(pString, newSize, string);
            }
        }
        m_string = pString;
        delete[] pString;
    }
}

bool EchLicString::operator==(const EchLicString& other) const
{
    return m_string == other.m_string;
}

bool EchLicString::operator!=(const EchLicString& other) const
{
    return !(*this == other);
}

//============================================================================
//                          EchLic5BitEncodedString 
//============================================================================

// Map 5 bit value to a character.  Avoid characters that are easily confused.
// Omitted characters are: 0, O, 1, I.
const char EchLic5BitEncodedString::valueMap[32] = 
{
    '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
    'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
};

EchLic5BitEncodedString::EchLic5BitEncodedString() : EchLicString() 
{
    m_pDecodedData = NULL;
    m_numBits = 0;
    m_errorStartIndex = -1;
    m_errorStopIndex = -1;
    m_stringIndex = 0;
    m_checksum = 0;
    m_errorStartIndex = -1;
    m_errorStopIndex = -1;
}

EchLic5BitEncodedString::EchLic5BitEncodedString(const TCHAR *string) : EchLicString(string) 
{
    m_pDecodedData = NULL;
    m_numBits = 0;
    m_errorStartIndex = -1;
    m_errorStopIndex = -1;
    m_stringIndex = 0;
    m_checksum = 0;
}

EchLic5BitEncodedString::~EchLic5BitEncodedString() 
{
    FreeDecodeData();
}

    /* Add a digit to the encoded data. */
void EchLic5BitEncodedString::AddDigit(IN int digit)
{
    TCHAR s[2];
    m_checksum += digit;
    s[0] = valueMap[digit & 0x1f];
    s[1] = 0;
    Cat(s);
    m_stringIndex++;
}
    /* Encode 5 bit encoded data, including checksums and '-' characters. */
void EchLic5BitEncodedString::Encode(IN bool first, IN bool last,
                             IN const BYTE *pData, IN int bitLength)
{
    int i;
    
    if (first)
    {
        m_stringIndex = 0;
        m_checksum = 0;
        m_numBits = 0;
        m_value = 0;
        Set(NULL);
    }

    for (i = 0; i < bitLength; i++)
    {
        m_value = m_value << 1;
        if (pData[i/8] & (1 << (7 - (i % 8))))
        {
            m_value |= 1;
        }
        if (m_numBits % 5 == 4)
        {
            AddDigit(m_value);
            if (m_stringIndex % 8 == 7)
            {
                AddDigit(m_checksum);
                if (i != bitLength-1 || !last)
                {
                    Cat(TEXT("-"));
                }
                m_checksum = 0;
            }
            m_value = 0;
        }
        m_numBits++;
    }

    if (last) 
    {
        if (m_numBits % 5 != 0)
        {
            // Zero pad.  Presumably when this is decoded these bits will be ignored by caller.
            m_value = m_value << (5 - (m_numBits % 5)); 
            AddDigit(m_value);
        }
        if ((m_stringIndex % 8) != 0 && last)
        {
            AddDigit(m_checksum);
        }
    }
}

    /* Encode 5 bit encoded data, including checksums and '-' characters. */
void EchLic5BitEncodedString::Encode(IN const BYTE *pData, IN int numBytes)
{
    Encode(true, true, pData, numBytes*8);
}

    /* Validate and decode 5 bit encode data.  If false, data is invalid, and errorIndex 
     * contains the character index of the first detected error.  
     */
bool EchLic5BitEncodedString::Decode()
{
    bool ok = true;
    int checksum = 0;
    int stringIndex = 0;

    const TCHAR *pString = Get();
    size_t len = GetLen();
    unsigned int i;

    FreeDecodeData();
    
    m_pDecodedData = new BYTE[len*2];  // More than we need, since we don't need checksums or dashes, and each char is 5 bits worth.
    if (m_pDecodedData == NULL)
    {
        ok = FALSE;
    }
    else
    {
        memset(m_pDecodedData, 0, len);
        m_errorStartIndex = -1;
        m_errorStopIndex = -1;
        boolean checkSumError = false;
        for (i = 0; ok && i < len && ok; i++)
        {
            TCHAR c = toupper(pString[i]);
            if (i % 9 == 8)
            {
               if (c != '-')
               {
                   ok = false;
               }
            }
            else
            {
                int value;
                int j;
                ok = false;
                for (j = 0; !ok && j < sizeof(valueMap); j++)
                {
                    if (c == valueMap[j])
                    {
                        value = j;
                        ok = true;
                    }
                }

                if (ok)
                {
                    if (i % 9 == 7 || i == len-1)
                    {
                        if ((checksum & 0x1f) != value)
                        {
                            ok = false;
                            checkSumError = true;
                        }
                        checksum = 0;
                    }
                    else
                    {
                        checksum += value;
                        for (j = 0; j < 5; j++)
                        {
                            m_pDecodedData[m_numBits/8] = m_pDecodedData[m_numBits/8] << 1;
                            if (value & (1 << (4-j)))
                            {
                                m_pDecodedData[m_numBits/8] |= 1;
                            }
                            m_numBits++;
                        }
                    }
                }
            }
            if (!ok)
            {
                m_errorStopIndex = i;
                if (checkSumError)
                {
                    m_errorStartIndex = i-7;
                }
                else
                {
                    m_errorStartIndex = i;
                }
            }
        }

        if (ok && (m_numBits % 8 != 0))
        {
            m_pDecodedData[m_numBits/8] = m_pDecodedData[m_numBits/8] << (8-(m_numBits % 8));
        }
    }
    if (!ok)
    {
        FreeDecodeData();
    }
  
    return ok;
}

    /* Validate and decode 5 bit encoded data. Returns true if valid. */
bool EchLic5BitEncodedString::Decode(OUT BYTE *&pData, OUT int &numBytes)
{
    bool ok = Decode();
    pData = m_pDecodedData;
    numBytes = m_numBits/8;
    return ok;
}

    /* Validate the encoded hex data */
bool EchLic5BitEncodedString::Validate()
{
    return Decode();
}

void EchLic5BitEncodedString::GetErrorIndexes(int &start, int &stop)
{
    start = m_errorStartIndex;
    stop = m_errorStopIndex;
}

    /* Validate and normalize encoded data. Returns true if valid. */
bool EchLic5BitEncodedString::Normalize()
{
    if (Decode())
    {
        Set(NULL);
        Encode(true, true, m_pDecodedData, m_numBits);
        FreeDecodeData();
        return true;
    }
    return false;
}

    /* Free decode data */
void EchLic5BitEncodedString::FreeDecodeData()
{
    delete m_pDecodedData;
    m_pDecodedData = 0;
    m_numBits = 0;
}


//============================================================================
//                               EchLicReg
//============================================================================


EchLicReg::EchLicReg()
{
    m_hKey = NULL;
    m_var = 0;
}
EchLicReg::~EchLicReg()
{
    Close();
}

static const TCHAR *revisionName = TEXT("ID");

EchLicSts EchLicReg::Open(IN const TCHAR *pLicenseId)
{
    EchLicString nodeName;
    EchLicSts sts = EchLicSts_Good;
    GetKey(pLicenseId, nodeName);
    Close();
    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, nodeName, 0, (KEY_READ|KEY_WRITE), &m_hKey) != ERROR_SUCCESS)
    {
        DWORD   disposition;
        if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, nodeName, 0, TEXT("Application Global Data"), REG_OPTION_NON_VOLATILE,
                (KEY_READ|KEY_WRITE), NULL, &m_hKey, &disposition) != ERROR_SUCCESS)
        {
            sts = EchLicSts_RegAccessError;
        }
    }
    return sts;
}

void EchLicReg::Close()
{
    if (m_hKey != NULL)
    {
        RegCloseKey(m_hKey);
        m_hKey = NULL;
    }
}

EchLicSts EchLicReg::SetLicenseRevision(unsigned int licenseRevision)
{
    return SetValue(revisionName, licenseRevision);
}

EchLicSts EchLicReg::GetLicenseRevision(unsigned int &licenseRevision)
{
    return GetValue(revisionName, licenseRevision);
}

EchLicSts EchLicReg::SetValue(IN const TCHAR *name, unsigned int value)
{
    EchLicSts sts = MustBeOpen();
    if (sts == EchLicSts_Good)
    {
        value ^= m_maskValue;
        Encode((BYTE *)&value, sizeof(value), m_var);
        if (RegSetValueEx(m_hKey, name, 0, REG_DWORD, 
                         (BYTE *)&value, sizeof(value)) != ERROR_SUCCESS)
        {
            sts = EchLicSts_RegAccessError;
        }
    }
    return sts;
}

EchLicSts EchLicReg::GetValue(IN const TCHAR *name, unsigned int &value)
{
    EchLicSts sts = MustBeOpen();
    if (sts == EchLicSts_Good)
    {
        DWORD dataItemLen = sizeof(value);
        if (RegQueryValueEx(m_hKey, name, 0, NULL, (BYTE *)&value, &dataItemLen) == ERROR_SUCCESS)
        {
            Decode((BYTE *)&value, sizeof(value), m_var);
            value ^= m_maskValue;
        }
        else
        {
            sts = EchLicSts_RegNotFound;
        }
    }
    return sts;
}

void EchLicReg::GetKey(IN const TCHAR *pLicenseId, 
                       OUT EchLicString &key)
{
    EchLic5BitEncodedString encodedLicenseName;
    Encode(pLicenseId, 0x71, encodedLicenseName);
    key.Set(TEXT("Software\\Echelon\\Registry\\"));
    key.Cat(encodedLicenseName);

    m_var = 0;
    m_maskValue = 0;
    int shift = 0;
    for (const TCHAR *p = encodedLicenseName; *p; p++)
    {
        m_var += *p;
        m_maskValue += *p << shift;
        shift = (shift + 4) % 32;
    }
}

void EchLicReg::Encode(IN const TCHAR *pString, IN int var, OUT EchLic5BitEncodedString &encodedData)
{
    int len = (int)_tcslen(pString);
    BYTE *pData = new BYTE[len];
    if (pData != NULL)
    {
        for (int i = 0; i < len; i++)
        {
            pData[i] = (BYTE)pString[i];
        }
        Encode(pData, (int)len, var, encodedData);
        delete[] pData;
    }
}

// Hail (Caesar cipher).
// Note that this isn't really a Caeser cipher at all.  Caeser
// ciphers use a fixed alphabet shift.  This is a substition
// cipher, and includes an incrementing shift.  But I don't
// know what the name for this type of cipher is.
#define HAIL_LEN 256
static const BYTE hail[HAIL_LEN] = 
{
     /* 00 */ 0x49, 0xf6, 0xc1, 0xdb, 0x4c, 0x29, 0xaf, 0xb9,
     /* 08 */ 0xfb, 0x85, 0xa9, 0xe2, 0x75, 0x76, 0xf9, 0xcd,
     /* 10 */ 0x68, 0xc4, 0xd5, 0x24, 0x18, 0xd6, 0x6d, 0x64,
     /* 18 */ 0xbc, 0xac, 0xe9, 0x39, 0xa2, 0x0f, 0x05, 0xa8,
     /* 20 */ 0xf8, 0x4f, 0xa1, 0x1a, 0x12, 0xf3, 0x26, 0x78,
     /* 28 */ 0x6b, 0xc7, 0x48, 0x7a, 0x70, 0xe6, 0x52, 0x95,
     /* 30 */ 0xde, 0x31, 0x5f, 0x54, 0x0c, 0x19, 0xa0, 0xd2,
     /* 38 */ 0x0e, 0xb4, 0x61, 0x2f, 0x9e, 0xda, 0x89, 0x08,
     /* 40 */ 0x99, 0xaa, 0xf4, 0xfe, 0x40, 0x97, 0x69, 0xe1,
     /* 48 */ 0x62, 0xb3, 0x3d, 0xe0, 0x22, 0x10, 0x98, 0xb7,
     /* 50 */ 0x2b, 0x8c, 0x63, 0x9f, 0xbd, 0xeb, 0x1f, 0x50,
     /* 58 */ 0xb5, 0xf2, 0x28, 0x32, 0xd7, 0xd0, 0xc8, 0x91,
     /* 60 */ 0x71, 0xa6, 0x65, 0xd8, 0x7f, 0xff, 0x1d, 0xd4,
     /* 68 */ 0x8d, 0xf7, 0xe5, 0x42, 0xd1, 0x6e, 0xc5, 0x16,
     /* 70 */ 0xcb, 0x60, 0x30, 0x93, 0x4e, 0x84, 0x45, 0xc3,
     /* 78 */ 0xed, 0xea, 0x73, 0x87, 0xb2, 0x1c, 0xc9, 0x72,
     /* 80 */ 0x1b, 0x27, 0xd3, 0xe8, 0xcf, 0x58, 0x67, 0xab,
     /* 88 */ 0x15, 0x9d, 0x0b, 0x53, 0xfc, 0x03, 0x9c, 0x7e,
     /* 90 */ 0x11, 0x20, 0x38, 0x2c, 0xee, 0xdd, 0x17, 0x66,
     /* 98 */ 0x1e, 0xbf, 0x4d, 0x25, 0x3b, 0xad, 0x5e, 0x4a,
     /* a0 */ 0xef, 0xfa, 0x21, 0x4b, 0x9b, 0xfd, 0x8a, 0xa4,
     /* a8 */ 0x33, 0xf1, 0x43, 0xbb, 0x01, 0x2e, 0x5c, 0xce,
     /* b0 */ 0xcc, 0x88, 0xdc, 0xf5, 0xb0, 0x83, 0xbe, 0x86,
     /* b8 */ 0xca, 0x41, 0x79, 0xa5, 0xd9, 0x34, 0x47, 0x07,
     /* c0 */ 0x59, 0x56, 0x09, 0x77, 0xc6, 0x23, 0x37, 0x3c,
     /* c8 */ 0xb6, 0xdf, 0x96, 0x55, 0x04, 0x14, 0xa3, 0x3a,
     /* d0 */ 0xb8, 0x7b, 0xae, 0x90, 0x3f, 0x44, 0x2d, 0x82,
     /* d8 */ 0x8f, 0xe4, 0x00, 0x35, 0xe7, 0x2a, 0xba, 0x8e,
     /* e0 */ 0x46, 0x13, 0x36, 0xec, 0x57, 0xe3, 0xc0, 0x81,
     /* e8 */ 0x6c, 0x5b, 0xf0, 0x51, 0x0a, 0x02, 0x06, 0x92,
     /* f0 */ 0x5d, 0xa7, 0x8b, 0x5a, 0x80, 0x7c, 0x7d, 0x74,
     /* f8 */ 0x6f, 0xb1, 0x94, 0x9a, 0xc2, 0x0d, 0x6a, 0x3e,
};

void EchLicReg::Encode(IN BYTE *pData, IN int len, IN int var)
{
    for (int i = 0; i < len; i++)
    {
        pData[i] = hail[(pData[i] + var++) & 0xff ]; 
    }
}

void EchLicReg::Decode(IN BYTE *pData, IN int len, IN int var)
{
    for (int i = 0; i < len; i++)
    {
        for (int j = 0; j < HAIL_LEN; j++)
        {
            if (pData[i] == hail[j])
            {
                pData[i] = (BYTE)j - var++;
                break;
            }
        }
    }
}

void EchLicReg::Encode(IN BYTE *pData, IN int len, IN int var, OUT EchLic5BitEncodedString &encodedData)
{
    Encode(pData, len, var);
    encodedData.Encode(pData, len);
}


