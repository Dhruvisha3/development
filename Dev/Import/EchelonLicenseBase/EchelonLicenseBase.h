//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// File EchelonLicenseBase.h
//
// This file defines the base clases used to acces Echelon licenses. 
// This class is used by licensing clients (the software that is 
// licensed), licensing wizard (the tools used to interact with 
// the licensing server) and the license server (or any standalone
// tool that generates licenses).
//
// Specific license types are implemented by adding the following:
//   TBD...
//
// When used by a licensing server, you must define 
// INCLUDE_ECHELON_LICENSE_SERVER.  However, you must NOT define this 
// macro in any code that leave Echelon, even in binary form.
//
// Copyright (c) 2009 Echelon Corporation.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <comdef.h>
#include "EchLicNode.h"
#include <Wbemidl.h>

/* Define INCLUDE_ECHELON_LICENSE_SERVER ONLY when compiling with the Echelon
 * license server or other license generating tools.  You must not ship
 * any executable built with this defined, because that excutable will contain
 * the Echelon license private key.
*/

class EchelonLicenseBase
{
//============================================================================
//                             General Pulic Definitions
//============================================================================
public:

    /* Types of documents */
    typedef enum
    {
        DocType_Request,
        DocType_License
    } DocumentType;

    /* The list of all known license types. */
    typedef enum
    {
        LicenseType_OpenLnsActivation,
        LicenseType_OpenLnsCTActivation,
        LicenseType_BacnetRouterActivation,
		LicenseType_LumInsightLicense,
        /* Add new license types above this line */
        NumberOfLicenseTypes
    } LicenseType;

    struct LicenseDescription;
    typedef struct LicenseDescription LicenseDescription;

    /* Names of XML nodes and enumeration values. */
    static const TCHAR *NodeNameRoot;
      static const TCHAR *NodeNameContent;
        static const TCHAR *NodeNameDocumentType;
            static const TCHAR *DocumentTypeLicense;    /* A full license */
            static const TCHAR *DocumentTypeRequest;
        static const TCHAR *NodeNameLicenseType;
        static const TCHAR *NodeNameSchemaVersion;
          static const int    CurrentSchemaVersionValue;
        static const TCHAR *NodeNameHostName;
        static const TCHAR *NodeNameProductEdition;
        static const TCHAR *NodeNameLicenseId;
        static const TCHAR *NodeNameLicenseRevision;
        static const TCHAR *NodeNameUserNotes;  /* A user field containing notes for identification purposes */
        static const TCHAR *NodeNameMachineLock;
          static const TCHAR *NodeNameHardDisk;
                /* Valid node names are given by hardDiskProperties */
          static const TCHAR *NodeNameCpu;
                /* Valid node names are given by cpuProperties */
          static const TCHAR *NodeNameMotherboard;
                /* Valid node names are given by motherboardProperties */
          static const TCHAR *NodeNameBios;
                /* Valid node names are given by biosProperties */
          static const TCHAR *NodeNamePlatform;
              static const TCHAR *NodeNameVmType;
                static const TCHAR *VmTypeValueNone;
                static const TCHAR *VmTypeValueVmWare;
                static const TCHAR *VmTypeValueVirtualPc;
          static const TCHAR *NodeNameShortFormat;
            static const TCHAR *NodeNameHashCode;          
        static const TCHAR *NodeNameExpirationData;
        static const TCHAR *NodeNameFeatures;
            static const TCHAR *LicenseTypeOpenLnsActivation;
                /* LNS activation feature names */
                static const TCHAR *NodeNameProductReleaseMajor;
                static const TCHAR *NodeNameProductReleaseMinor;
                static const TCHAR *NodeNameDeviceCapacity;
                static const TCHAR *NodeNameTrialLicense;
                static const TCHAR *NodeNameRunTimeLimit;
                static const TCHAR *NodeNameOpenSystemLimit;

                /* LNS activation feature constants */
                static const unsigned int RELEASE_ALL = 0xffffffff;
                static const unsigned int UNLIMITED_CAPACITY = 0xffffffff;
                static const unsigned int NO_RUNTIME_LIMIT = 0xffffffff;
                static const unsigned int NO_OPEN_SYSTEM_LIMIT = 0xffffffff;

            static const TCHAR *LicenseTypeOpenLnsCTActivation;
                /* Open LNS CT activation feature names */
                // static const TCHAR *NodeNameProductReleaseMajor;
                // static const TCHAR *NodeNameProductReleaseMinor;
                // static const TCHAR *NodeNameDeviceCapacity;
                // static const TCHAR *NodeNameTrialLicense;
                static const TCHAR *NodeNameOpenLnsCTEditionName;
                    static const TCHAR *NodeNameOpenLnsCTEditionStandard;
                    static const TCHAR *NodeNameOpenLnsCTEditionProfessional;
                    static const TCHAR *NodeNameOpenLnsCTEditionTrial;
                static const TCHAR *NodeNameDatabaseLimit;
                // static const TCHAR *NodeNameRunTimeLimit;

                /* LM activation feature constants */
                // static const unsigned int RELEASE_ALL = 0xffffffff;
                static const unsigned int NO_DATABASE_LIMIT = 0xffffffff;
                // static const unsigned int NO_RUNTIME_LIMIT = 0xffffffff;
            static const TCHAR *LicenseTypeBacnetRouterActivation;
			static const TCHAR *LicenseTypeLumInsightLicense;
        static const TCHAR *NodeNameRequestParameters;

            // LicenseType_OpenLnsActivation;
                /* LNS Activation request parameters names */
                // static const TCHAR *NodeNameProductReleaseMajor;
                // static const TCHAR *NodeNameProductReleaseMinor;

            // LicenseType_OpenLnsCTActivation;
                /* Open LNS CT Activation request parameters names */
                // static const TCHAR *NodeNameProductReleaseMajor;
                // static const TCHAR *NodeNameProductReleaseMinor;
            
            // LicenseType_BacnetRouterActivation;
                /* Bacnet Router Activation request parameters names */
                // static const TCHAR *NodeNameProductReleaseMajor;
                // static const TCHAR *NodeNameProductReleaseMinor;
      static const TCHAR *NodeNameSignature;

      /* Registry Names */
      static const TCHAR *RegNameLicenseInfoKey; // Main key into licensing registry.
        static const TCHAR *RegNameSchemaPathKey;  // Schema pathname key.
        static const TCHAR *RegNameInstalledLicensesPath; // Path of installed license folder.
        static const TCHAR *RegNameTraceOption;           // Trace option.  0=off, 1=errors, 2=access
       
      static const TCHAR *SchemaName;

    static const TCHAR *NodeNameComment;  /* A user comment outside of the license */

        /* Manual Lock code flags */
    static const int ManualLockCodeFlag_HasHdSn          = 0x01; // Hard disk serial number is included
    static const int ManualLockCodeFlag_HasMotherboardSn = 0x02; // Mother board serial number is included
    static const int ManualLockCodeFlag_HasBiosSn        = 0x04; // Bios serial number is included.
    static const int ManualLockCodeFlag_IsVM             = 0x08; // Set if PC is a VM.

//============================================================================
//                             General Pulic Methods
//============================================================================
public:
    EchelonLicenseBase(void);
    virtual ~EchelonLicenseBase(void);

    static EchLicSts GetLicenseRootPath(EchLicString &licenseRootPath);

    static EchLicSts GetLicenseInstallPath(IN LicenseType licenseType, 
                                           OUT EchLicString &licensePath,
                                           bool sanitizeName = true);

    static EchLicSts GetFullSchemaPath(EchLicString &fullSchemaPath);

    /**************************************************************************
     *                         Access license content                             
     *************************************************************************/

        /* Get the license root node. */
    EchLicSts GetLicenseRoot(OUT EchLicNode &root);
        /* Get the license root and content nodes. */
    EchLicSts GetLicenseContent(OUT EchLicNode &root,
                                OUT EchLicNode &content);

        /* Get the schema version used by the document */
    EchLicSts GetDocumentSchemaVersion(unsigned int &schemaVersion);

    /* Override Document version.  For testing purposes only. */
    EchLicSts OverrideDocumentSchemaVersion(unsigned int schemaVersion);

        /* Get document type */
    EchLicSts GetDocumentType(DocumentType &documentType);

        /* Get the license ID. */
    EchLicSts GetLicenseId(OUT EchLicString &licenseId);

        /* Get the ID of the product that the license applies to */
    EchLicSts GetProductEdition(OUT EchLicString &productEdition);

        /* Get the host name stored in the license */
    EchLicSts GetHostName(OUT EchLicString &hostName);

        /* Get the 'UserNotes' - a user settable field to identify the license use. */
    EchLicSts GetUserNotes(OUT EchLicString &userNotes);

        /* Get the 'comment' - a user settable field outside of the signature. */
    EchLicSts GetComment(OUT EchLicString &comment);

        /* Set the 'comment' - a user settable field outside of the signature. */
    EchLicSts SetComment(IN const TCHAR *comment);

        /* Get the license revision number */
    EchLicSts GetLicenseRevision(OUT unsigned int &licenseRevision);

    /**************************************************************************
     *                         Access feature content                             
     *************************************************************************/

        /* Get the license feature node */
    EchLicSts GetLicenseFeatures(OUT EchLicNode &features);


    EchLicSts GetFeatureBool(IN const TCHAR *featureName,    OUT bool &value);
    EchLicSts GetOptionalFeatureBool(IN const TCHAR *featureName, OUT bool &value,  IN bool defaultValue);
    EchLicSts AddFeatureBool(IN const TCHAR *featureName,    IN  bool value);

    template <class T> EchLicSts GetFeature(const IN TCHAR *nodeName, OUT T &value);
    template <class T1, class T2> EchLicSts GetOptionalFeature(const IN TCHAR *nodeName, OUT T1 &value, IN T2 defaultValue);
    template <class T> EchLicSts AddFeature(const IN TCHAR *nodeName, IN T value);

//============================================================================
//                             License Client Methods
//============================================================================
public:

        /* Opens and loads the specified file without validating, using a full 
           pathname.
         */
    EchLicSts LoadLicenseFile(const TCHAR *licenseFilePath,
                              OUT EchLicString &failureExplanation);
        /* Same as above, but uses the default name, based on the license type.
         */
    EchLicSts LoadLicenseFile(LicenseType licenseType, 
                              OUT EchLicString &failureExplanation);

        /* Load from an XML string. For example, if license was passed back in
           a SOAP message. 
         */
    EchLicSts LoadLicenseFromXmlString(IN const TCHAR *xmlString,
                                      OUT EchLicString &failureExplanation);

        /* Get license as an XML string, for example, to pass in a SOAP message */
    EchLicSts GetXmlString(OUT EchLicString &xmlString);

        /* Get the license license type */
    EchLicSts GetLicenseType(LicenseType &licenseType);

        /* Validate the digital signature, machine lock, expiration, and 
         * license registry. 
         */
    virtual EchLicSts ValidateLicense(OUT EchLicString &errorExplanation, IN bool fastValidation = false);

        /* Validate the license with the license server. License file must already
         * be opened, loaded, and validated using ValidateLicenseFile.  If contact with
         * the server fails this function will return EchLicSts_Good, but contactSucceeded
         * will be FALSE.
         */
    EchLicSts ValidateWithServer(OUT bool &contactSucceeded);

        /* Get the results of an asynchronous validation */
    EchLicSts GetAsyncValidationResults(bool &serverContactSucceeded,
                                        EchLicString &failureExplanation);

        /* Get license expiration information.  If there is no expiration, expires
         * and expired is set to false and the values of all the other parameters is
         * undefined.
         */
    EchLicSts GetLicenseExpiration(bool &expires, bool &expired, 
                                   unsigned int &fullDaysLeft, unsigned int &day, 
                                   unsigned int &month, unsigned int &year,
                                   time_t &expirationTime);

        /* Kick off an asynchronous validation. When complete, the
           event handle will be set.  Use GetAsyncValidationResults() for sts.
         */
    EchLicSts StartAsynchValidation(IN HANDLE eventHandle,
                                    IN bool validateWithServer);
private:
    static void ValidationThread(void *pThis);
    
        /* Get the host name of the client PC */
    EchLicSts ReadHostName(EchLicString &hostName);

//============================================================================
//                             License Wizard Methods
//============================================================================

public:
        /* Delete the specified license */
    EchLicSts DeleteLicense(LicenseType licenseType);

        /* Delete the specified license by path name */
    EchLicSts DeleteLicense(IN const TCHAR *licensePath);

        /* Create a request document. Depending on the license type, 
         * it may be necessary to add request parameters as well
         */
    EchLicSts CreateRequest(IN LicenseType licenseType,  // May be NULL
                            IN const TCHAR *productEdition,
                            IN const TCHAR *licenseId);

        /* Create request parameters */
    EchLicSts CreateRequestParameters(OUT EchLicNode &requestParameters);

        /* Get request parameters */
    EchLicSts GetRequestParameters(OUT EchLicNode &requestParameters);

        /* Set the 'UserNotes' - a user settable field to identify the license use. */
    EchLicSts SetUserNotes(IN const TCHAR *notes);

        /* Save a previously created request to a request file */
    EchLicSts SaveRequest(IN const TCHAR *destinationPath);

        /* Install the license that has been previously loaded or received
         * in the license directory, and update the license registry. 
         */
    EchLicSts InstallLicense(IN LicenseType licenseType, 
                             EchLicString &failureExplanation,
                             bool validatePriorToInstalling = true);

//============================================================================
//                             Manual Methods
//============================================================================

    /********************* Manual License Wizard Methods *************************/

		/* Generate manual machine lock code for this PC */
	static EchLicSts GenerateManualMachineLockCode(OUT EchLic5BitEncodedString &machineLockCode);

        /* Create a manual license.  After creation, add features and signature. */
    EchLicSts CreateManualLicense(IN LicenseType licenseType,
                                  IN unsigned int schemaVersion,
		                          IN const TCHAR *productEditon,
                                  IN const TCHAR *licenseId, 
                                  IN unsigned int licenseRevision,
                                  IN const TCHAR *manualMachineLockCode);

	    /* Sign a manual license */
	EchLicSts SignManualLicense(IN const TCHAR *encodedSignature);

    /********************* Manual License Administrator Methods ******************/
		/* Create the manual Request */
	EchLicSts CreateManualRequest(IN LicenseType licenseType, 
                                  IN unsigned int schemaVersion,
                                  IN const TCHAR *productEdition,
                                  IN const TCHAR *licenseId,
                                  IN const TCHAR *machineLockCode);

		/* Get the license signature.  Typically used by the license administrator
           after receiving a manual license from the server.
          */                                        
	EchLicSts GetLicenseSignature(EchLic5BitEncodedString &encodedSignature);                                        

	/* Check to see if machine lock in the request would have generated the specified manualMachineLockCode */
    EchLicSts CompareRequestWithManualLock(const TCHAR *manualMachineLockCode);

//============================================================================
//                             License Server Methods
//============================================================================
public:
        /* Get generic request information from request.  File must be loaded using
           LoadLicenseFile or LoadLicenseFromXmlString. */
    EchLicSts GetRequestInfo(OUT LicenseType &licenseType,
                             OUT EchLicString &hostName,
                             OUT EchLicString &productEdition);

        /* Save a previously created licenes to a license file */
    EchLicSts SaveLicense(IN const TCHAR *destinationPath);

        /* Get a licenses machine lock node. */
    EchLicSts GetLicenseMachineLock(IN EchLicNode &machineLock);

#if defined(INCLUDE_ECHELON_LICENSE_SERVER)
        /* Digitally sign the license */
    EchLicSts SignLicense(const TCHAR *userName, const TCHAR *password,
                          EchLicNode &rootNode, EchLicNode &content);        
#endif
        /* Create expiration date */
    EchLicSts SetExpirationDate(unsigned int day, unsigned int month, unsigned int year);

        /* Create an expiration in approximately <numberOfDaysFromNow> days.  Note that
         * this function is generous and rounds up.  This way if the server's PC is a day
         * behind the clients, we won't short change the customer.
         */
    EchLicSts SetExpirationDateInDays(unsigned int numberOfDaysFromNow);

    /* Creates an �empty� license from a request.  Note that this method does NOT 
     * add features or the machine lock.  Typically the license server will 
     * scan the machine lock information in the request and, based on rules defined
     * in the database, selectively copy specific locks to the license.  Feature selection
     * will be driven primarily from the productEdition and request parameters.
     */
    EchLicSts CreateLicense(EchelonLicenseBase &request,
                            IN const TCHAR *licenseId, 
                            IN unsigned int licenseRevision);

    /* Compare the license on the server with the license passed in by the
     * client.  The signed portions must match exactly.  
     */
    static EchLicSts CompareLicenses(const TCHAR *serverLicenseXmlString,
                                     const TCHAR *clientLicenseXmlString);

//============================================================================
//                             Manual License Methods
//
//  These methods are used by both the license wizard and license server to 
//  create manual licenses.
//============================================================================
public:
    static EchLicSts DecodeMachineLock(IN const TCHAR *machineLockCode, 
                                      int &version, 
                                      BYTE &flags /* use ManualLockCodeFlag_* */);

        /* Set signature in manual license */
    EchLicSts SetManualLicenseSignature(IN const TCHAR *signature);

//============================================================================
//                               Signatures
//============================================================================

        /* Validate the license signature. */
    EchLicSts ValidateSignature(EchLicNode &rootNode, EchLicNode &content);

        /* Create a hash of the license contents */
    EchLicSts GetHash(EchLicNode &content, HCRYPTPROV hCryptProv, HCRYPTHASH &hHash);

        /* Open the crypto API */
    bool OpenCryptoApi(HCRYPTPROV &handle);

        /* Close the crypto API */
    void CloseCryptoApi(HCRYPTPROV &handle);


 /*============================================================================
 *----------------------------------------------------------------------------
 *                 ****  PRIVATE DEFINITION AND METHODS ****
 *----------------------------------------------------------------------------
 *==========================================================================*/

private:

//============================================================================
//                               General Utilities
//============================================================================

        /* Create a license or request framework. */
    EchLicSts CreateLicenseFramework(IN EchLicDoc &doc,
                                     IN LicenseType licenseType, 
                                     IN unsigned int schemaVersion, 
                                     IN const TCHAR *documentType, 
                                     IN const TCHAR *hostName,
									 IN const TCHAR *productEdition,
                                     IN const TCHAR *licenseId,  // May be Null for Request document  
                                     IN unsigned int licenseRevision,
                                     OUT EchLicNode &root,
                                     OUT EchLicNode &content,
                                     OUT EchLicNode &machineLock);

        /* Get the license root and content nodes. */
    EchLicSts GetContent(IN EchLicDoc &doc,
                         OUT EchLicNode &root,
                         OUT EchLicNode &content);

    
    static EchLicSts GetLicenseRegistryPath(const TCHAR *key, EchLicString &path);

    /**************************************************************************
     *                     License definition access methods
     *************************************************************************/

    EchLicSts GetLicenseDescription(const LicenseDescription * &pDesc);

    static EchLicSts GetLicenseDescription(LicenseType licenseType, 
                                           const LicenseDescription * &pDesc);

    EchLicSts ReadLicenseType(IN EchLicNode &content);

//============================================================================
//                            Hardware locking
//============================================================================

    /* Constants tables for hardware locking */

    typedef bool GetLockFuncType(EchLicString &value);
    typedef struct EchLicWbemPropertyElements
    {
        const TCHAR *propertyName;
    } EchLicWbemPropertyElements;

    typedef struct EchLicWbemProperties
    {
        // const TCHAR *collectionName;
        const TCHAR *nodeName;
        const TCHAR *queryString;
        const EchLicWbemPropertyElements *properties;
        EchLicSts    error;
    } EchLicWbemProperties;

    static const EchLicWbemProperties hardDiskLockTypesSerialNumber;
    static const EchLicWbemProperties hardDiskLockTypesSerialNumberOld; // To get Hard disk serial numbers for XP earlier
    static const EchLicWbemProperties hardDiskLockTypes;
    static const EchLicWbemProperties cpuLockTypes;
    static const EchLicWbemProperties motherboardLockTypes;
    static const EchLicWbemProperties biosLockTypes;

    static const EchLicWbemPropertyElements hardDiskPropertiesSerialNumber[];
    static const EchLicWbemPropertyElements hardDiskProperties[];
    static const EchLicWbemPropertyElements cpuProperties[];
    static const EchLicWbemPropertyElements motherboardProperties[];
    static const EchLicWbemPropertyElements biosProperties[];

    typedef struct EchLicMachineLockFlagProperties
    {
        const TCHAR *nodeName;
        const TCHAR *propertyName;
        BYTE  flagValue;
    } EchLicMachineLockFlagProperties;
    static const EchLicMachineLockFlagProperties machineLockFlagProperties[];

    /**************************************************************************
     *                             Wbem Utilities                              
     **************************************************************************
     * Wbem functions are used to gather information using the 
     * WIN32 Wbem Services to create and validate hardware locks.
     *************************************************************************/
    
        /* Get the proper hardDiskLockTypesSerialNumber for this OS. */
    const EchLicWbemProperties &GetHardDiskLockTypesSerialNumber(void);

        /* Initialize the Wbem service */
    EchLicSts InitWbem(IWbemLocator *&pLoc, IWbemServices *&pSvc);
        /* Close the Wbem service */
    void CloseWbem(IWbemLocator *&pLoc, IWbemServices *&pSvc);

        /* Create machine lock information using the EchLicWbemProperties table and
         * information gathered from the PC.  Used by license wizard to create a 
         * license request.
         */
    EchLicSts CreateFromWbem(IWbemLocator *pLoc, IWbemServices *pSvc,
                             EchLicNode &machineLock, 
                             const EchLicWbemProperties &wbem);

        /* Validate machine lock information using the EchLicWbemProperties table and
         * information gathered from the PC against the machine lock information found
         * in the machineLock node.  Used by the license client to validate the
         * license.
         */
    EchLicSts ValidateWbem(IN IWbemLocator *pLoc, IN IWbemServices *pSvc,
                           IN EchLicNode &machineLock, 
                           IN const EchLicWbemProperties &wbem,
                           OUT EchLicString &errorExplanation);

        /* Validate a single item (such as a CPU or HardDisk) found within 
         * machine lock collection against the actual hardware.
         */
    EchLicSts ValidateItemFromWbem(IN const EchLicWbemProperties &wbem,
                                   IN IWbemLocator *pLoc, IN IWbemServices *pSvc,
                                   IN IEnumWbemClassObject* pEnumerator,
                                   IN EchLicNode &node,
                                   OUT bool &matches,
                                   OUT EchLicString &errorExplanation);

        /* Concatonate full WBEM property name to string */
    void CatWbemPropertyName(IN const EchLicWbemProperties &wbem, 
                             IN const TCHAR *propertyName,
                             IN OUT EchLicString &fullName);

    void FormatMisMatch(IN const TCHAR *categoryName,
                        IN const TCHAR *propertyName, 
                        IN const TCHAR *expectedValue,
                        IN const TCHAR *actualValue,
                        OUT EchLicString &errorExplanaiton);

    void FormatMisMatch(IN const EchLicWbemProperties &wbem, 
                        IN const TCHAR *propertyName,
                        IN const TCHAR *expectedValue,
                        IN const TCHAR *actualValue,
                        OUT EchLicString &errorExplanaiton);

    /**************************************************************************
     *                                VM Utilities                              
     *************************************************************************/

        /* Add the VM type to the machine lock. */
    EchLicSts CreateVmType(EchLicNode &machineLock);

        /* Validate the VM Type in machine lock. */
    EchLicSts ValidateVmType(IN EchLicNode &machineLock, OUT EchLicString &errorExplanation);

        /* Get the VM type of the PC, and return it as a VmTypeValue* string */
    EchLicSts GetVmType(EchLicString &vmType);

        /* Test to see if the PC is running under VmWare. */
    static bool IsVMWare(void);

        /* Test to see if the PC is running under Virtual PC */
    static bool IsVirtualPC(void);

    /**************************************************************************
     *                                Manual Lock                              
     *************************************************************************/

        // The manual lock code has 3 nibbles appended to it.
        // The first two nibbles contain the version number (0)
        // The last nibble contains the manul lock code flags ManualLockCodeFlag_*.
    static const int ManualLockCode_Version = 0;

        /* Validate the manual lock contained in machineLock against the current
         * hardware. 
         */
    EchLicSts ValidateManualLockCode(IN EchLicNode &content, IN EchLicNode &machineLock,
                                     OUT EchLicString &errorExplanation);

        /* Create a hash of the machine lock */
    EchLicSts CreateMachineLockHash(EchLicNode &machineLock, BYTE *&pHashData, DWORD &hashSize);

        /* Get the manual lock code. */
    EchLicSts GetManualLockCode(EchLicNode &machineLock, OUT EchLicString &manualLockCode);

            /* Add the manual lock code. */
    EchLicSts AddManualLockCode(EchLicNode &machineLock, IN const TCHAR *manualLockCode);

	    /* Generate manual machine lock code from a request */
    static EchLicSts GenerateManualMachineLockCode(int format, EchelonLicenseBase &request, OUT EchLic5BitEncodedString &machineLockCode);

//============================================================================
//                               License Registry
//============================================================================
    EchLicSts ValidateLicenseRevision(IN EchLicNode &content, 
                                      IN bool install,
                                      OUT EchLicString &errorExplanation);

//============================================================================
//                               License Creation
//============================================================================
        /* Creates an �empty� license.  */
    EchLicSts CreateLicense(IN LicenseType licenseType,
                            IN unsigned int schemaVersion, 
					        IN const TCHAR *hostName,
					        IN const TCHAR *productEdition,
                            IN const TCHAR *licenseId, 
                            IN unsigned int licenseRevision,
                            OUT EchLicNode &root,
                            OUT EchLicNode &content,
                            OUT EchLicNode &machineLock);

#if defined(INCLUDE_ECHELON_LICENSE_SERVER)
    EchLicSts GenerateSignature(const TCHAR *userName, const TCHAR *password,
                                EchLicNode &root, EchLicNode &content, 
                                EchLic5BitEncodedString &signature);
#endif

//============================================================================
//                             Server Validation Utilities
//============================================================================

    /* Utility to load a license from an xml string, and return the xml string
     * representing the content node, and the license signature.
     */
    EchLicSts GetServerValidationInfo(const TCHAR *licenseXmlString,
                                      EchLicString &contentXmlString,
                                      EchLic5BitEncodedString &signature);

/*============================================================================
 *----------------------------------------------------------------------------
 *                         ****  PRIVATE DATA ****
 *----------------------------------------------------------------------------
 *==========================================================================*/
private:

        /* The license type */
    LicenseType m_licenseType;
      
        /* The license file path */
    EchLicString m_licensePath;

        /* The license document. */
    EchLicDoc m_licenseDoc;

        /* Asynchronous validate parameters */
    HANDLE m_asyncValidationHandle;
    bool m_asyncValidateWithServer;
    EchLicSts m_asynchLicenselicenseStatus;
    bool m_asyncServerContactSucceeded;
    EchLicString m_failureExplanation;

private:
    void logLicenseAccess(const TCHAR *title, EchLicSts sts, const TCHAR *falureExplanation);
    HANDLE m_hEventLog;
};

template <class T> EchLicSts EchelonLicenseBase::GetFeature(const IN TCHAR *nodeName, T &value)
{
    EchLicNode features;
    EchLicSts sts = GetLicenseFeatures(features);
    if (sts == EchLicSts_Good)
    {
        sts = features.GetProperty(nodeName, value);
    }
    return sts;
}

template <class T1, class T2> EchLicSts EchelonLicenseBase::GetOptionalFeature(const IN TCHAR *nodeName, T1 &value, T2 defaultValue)
{
    EchLicSts sts = GetFeature(nodeName, value);
    if (sts == EchLicSts_NodeNotFound)
    {
        value = defaultValue;
        sts = EchLicSts_Good;
    }
    return sts;
}

template <class T> EchLicSts EchelonLicenseBase::AddFeature(const IN TCHAR *nodeName, T value)
{
    EchLicNode features;
    EchLicSts sts = GetLicenseFeatures(features);
    if (sts == EchLicSts_Good)
    {
        sts = features.AddProperty(nodeName, value);
    }
    return sts;
}

// This class is used to enumerate installed licenses
class EchLicEnumerator
{
public:
    EchLicEnumerator();
    ~EchLicEnumerator();

        /* Reset enumeration from the begining. */
    EchLicSts Reset(void);

        /* Get the number of installed licenses */
    static EchLicSts GetCount(OUT int &numLicenses);

        /* Get the next installed license.  If no more licenses are installed, 
           returns EchLicSts_LicenseNotFound 
         */
    EchLicSts Next(OUT EchLicString &pathName, OUT EchelonLicenseBase::LicenseType &licenseType);

    /*************************** License Enumeration Methods *****************/
private:
    EchelonLicenseBase::LicenseType m_licenseType;
    HANDLE m_findHandle;
    WIN32_FIND_DATA m_findData;

    static EchLicSts Next(EchelonLicenseBase::LicenseType &licenseType, 
                          HANDLE &findHandle,
                          WIN32_FIND_DATA &findData);

};