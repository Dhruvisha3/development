/////////////////////////////////////////////////////////////////////////////
//
// $Header: /Source/LMPA/Dev/include/version.h 314   10/17/06 4:50p Epang $
//
// Version Information
//
// This file defines build versions.  It can be customized by different
// products to define different version numbers, if necessary.
//
/////////////////////////////////////////////////////////////////////////////


#ifndef __VERSION_H__
#define __VERSION_H__

#pragma once


//
// These version numbers should be updated before a system build.
// This can be done by a program searching for the magic markers.
// Alternatively, the values can be overridden by individual source
// files or by passing options to the compiler, e.g.
//
//      cc /DRELEASE_NUMBER_MAJOR=1 ...
//

#ifndef RELEASE_NUMBER_MAJOR
#  define RELEASE_NUMBER_MAJOR      5       // :MAJOR:   DO NOT DELETE THIS MARKER!
#endif
#ifndef RELEASE_NUMBER_MINOR1
#  define RELEASE_NUMBER_MINOR1     0       // :MINOR1:  DO NOT DELETE THIS MARKER!
#endif
#ifndef RELEASE_NUMBER_MINOR2
#  define RELEASE_NUMBER_MINOR2     0       // :MINOR2:  DO NOT DELETE THIS MARKER!
#endif
#ifndef RELEASE_NUMBER_BUILD0
#  define RELEASE_NUMBER_BUILD0     0       // :BUILD0:  DO NOT DELETE THIS MARKER!
#endif
#ifndef RELEASE_NUMBER_BUILD1
#  define RELEASE_NUMBER_BUILD1     0       // :BUILD1:  DO NOT DELETE THIS MARKER!
#endif
#ifndef RELEASE_NUMBER_BUILD2
#  define RELEASE_NUMBER_BUILD2     9       // :BUILD2:  DO NOT DELETE THIS MARKER!
#endif
#define DEVEL_BIND_NUM 255

#define VER_PRODUCT "Echelon LonScanner Protocol Analyzer"
#define COPYRIGHT_FROM             1994
#define COPYRIGHT_TO              2020
#define VER_TRADEMARKS          "Adesto, Echelon, IzoT, LON, LNS, LonMaker, LonScanner, LonTalk, LonWorks, i.LON, Neuron, OpenLDV, Digital Home, the Adesto Logo, and the Echelon logo are trademarks of Adesto Technologies Corporation that may be registered in the United States and other countries."
#define VER_FILEDESC ""

// pull-in (standard) macros for operating on these versions numbers
#include "vermacro.h"

#endif // __VERSION_H__
