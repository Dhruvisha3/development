//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// File EchLicSts.h
//
// This file defines the error codes returned by the license utilities
//
// Copyright (c) 2009 Echelon Corporation.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////

#pragma once

/* Licensing  error codes */
typedef enum 
{
    EchLicSts_Good,                             /*  0 - 0x00 */
    EchLicSts_OpenFailed,                       /*  1 - 0x01 */
    EchLicSts_InvalidFormat,                    /*  2 - 0x02 License format is invalid */
    EchLicSts_NotImplemented,                   /*  3 - 0x03 TBD */
    EchLicSts_LicenseNotFound,                  /*  4 - 0x04 */
    EchLicSts_MustBeValidated,                  /*  5 - 0x05 */
    EchLicSts_LicenseSignatureInvalid,          /*  6 - 0x06 */
    EchLicSts_MachineLockInvalid,               /*  7 - 0x07 */
    EchLicSts_FailedToAddNode,                  /*  8 - 0x08 */
    EchLicSts_FailedToSetValue,                 /*  9 - 0x09 */
    EchLicSts_NodeNotFound,                     /* 10 - 0x0A */
    EchLicSts_FailedToGetValue,                 /* 11 - 0x0B */
    EchLicSts_FailedToReadNodeName,             /* 12 - 0x0C */
    EchLicSts_MustBeLoaded,                     /* 13 - 0x0D */
    EchLicSts_CannotAccessHardwareProperties,   /* 14 - 0x0E */
    EchLicSts_FailedToSaveFile,                 /* 15 - 0x0F */
    EchLicSts_FailedToSignLicense,              /* 16 - 0x10 */
    EchLicSts_HdLockFailed,                     /* 17 - 0x11 */
    EchLicSts_CpuLockFailed,                    /* 18 - 0x12 */
    EchLicSts_MotherboardLockFailed,            /* 19 - 0x13 */
    EchLicSts_BiosLockFailed,                   /* 20 - 0x14 */
    EchLicSts_VmLockFailed,                     /* 21 - 0x15 */
    EchLicSts_ManualLockFailed,                 /* 22 - 0x16 */
    EchLicSts_FailedToGetDataHash,              /* 23 - 0x17 */
    EchLicSts_BadMachineLockCode,               /* 24 - 0x18 */
    EchLicSts_RegAccessError,                   /* 25 - 0x19 */
    EchLicSts_RegMustBeOpened,                  /* 26 - 0x1A */
    EchLicSts_RegNotFound,                      /* 27 - 0x1B */
    EchLicSts_BadRevision,                      /* 28 - 0x1C */
    EchLicSts_BadLicensePath,                   /* 29 - 0x1D */
    EchLicSts_CopyFailed,                       /* 30 - 0x1E */           
    EchLicSts_UnexpectedLicenseType,            /* 31 - 0x1F */           
    EchLicSts_ValidationInProgress,             /* 32 - 0x20 */           
    EchLicSts_InvalidExpiration,                /* 33 - 0x21 */
    EchLicSts_LicenseExpired,                   /* 34 - 0x22 */
    EchLicSts_AlreadyLoaded,                    /* 35 - 0x23 */
    EchLicSts_PersonalizationRequired,          /* 36 - 0x24 */
    EchLicSts_PathNotSet,                       /* 37 - 0x25 */
    EchLicSts_InvalidHandle,                    /* 38 - 0x26 */
    EchLicSts_KeyNotInstalled,                  /* 39 - 0x27 */
    EchLicSts_InvalidDocumentType,              /* 40 - 0x28 */
    EchLicSts_LicenseContentMismatch,           /* 41 - 0x29  The licenses being compared do not match. */
    EchLicSts_OutOfMemory,                      /* 42 - 0x2A */
    EchLicSts_ValidationComponentFailure,       /* 43 - 0x2B  Cannot call validation code */
    EchLicSts_FailedToLoadSchema,               /* 44 - 0x2C */
    EchLicSts_ServerConnectionFailed,           /* 45 - 0x2D  Cannot connet to server */
    EchLicSts_ServerValidationFailed,           /* 46 - 0x2E  Connected to server, but validation failed. */
    EchLicSts_ServerException,                  /* 47 - 0x2F  Server threw an exception.  Represents an internal error.  */

} EchLicSts;
