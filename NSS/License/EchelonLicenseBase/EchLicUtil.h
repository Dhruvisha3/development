//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// File EchLicUtil.h
//
// This file defines a set of simple utility classes used by the 
// licensing system.
//
// Copyright (c) 2009 Echelon Corporation.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <comdef.h>
#include "EchLicSts.h"

/* A class to hold a simple character string. */
class EchLicString
{
public:

    EchLicString();
    EchLicString(const TCHAR *string);
    virtual ~EchLicString();

        // Set the value of the string
    void Set(const TCHAR *string, bool useEscapes = false, bool isFileName = false);

        // Set the value of the string
    void SetW(const WCHAR *string, bool useEscapes = false, bool isFileName = false);

        /* Decode and set the value of the string */
    void Decode(const WCHAR *string);

        // Concatenation
    void Cat(const TCHAR *string);
    const TCHAR *Get(void) { return m_string; }
    size_t GetLen(void) { return m_string.length(); }
    const TCHAR * operator&() { return m_string; }
    operator const TCHAR *()  { return m_string; }
    const WCHAR *GetW(void) { return m_string; }

    EchLicString& operator=(const TCHAR *string)  
    { 
        Set(string);
        return *this;
    }

    EchLicString& operator=(EchLicString &string)  
    { 
        Set(string);
        return *this;
    }

    bool operator==(const EchLicString& other) const;
    bool operator!=(const EchLicString& other) const;
   
    void EncodeFileName(const TCHAR *string);

    void UnformatXml(void);

private:
    bool LegalXmlChar(WCHAR c);
    bool LegalFileNameChar(WCHAR c);
    void SanitizeString(IN WCHAR *destString, 
                    IN const WCHAR *sourceString,
                    IN OUT size_t &stringSize,
                    bool isPathName);

    bstr_t m_string;
};

/* A class to hold data encoded in a string using 5 bit encodeing with a checksum. 
   These are used for manual locks.
 */
class EchLic5BitEncodedString : public EchLicString
{
public:
    EchLic5BitEncodedString();
    EchLic5BitEncodedString(const TCHAR *string);
    ~EchLic5BitEncodedString();

        /* Add a digit to the encoded data. */
    void AddDigit(IN int digit);

        /* Encode 5 bit encoded data, including checksums and '-' characters. May
         * be called multiple times to create and then add to the string.
         */
    void Encode(IN bool first, IN bool last,
                IN const BYTE *pData, IN int numBits);

        /* Encode 5 bit encoded data, including checksums and '-' characters. */
    void Encode(IN const BYTE *pData, IN int numBytes);

        /* Validate and decode 5 bit encoded data.  If false, data is invalid, and errorIndex 
         * contains the character index of the first detected error.  
         */
    bool Decode();

        /* Validate and decode 5 bit encoded data. Returns true if valid. */
    bool Decode(OUT BYTE *&pData, OUT int &numBytes);

        /* Get error index after failed call to Decode or Validate. */
    void GetErrorIndexes(int &start, int &stop); 

        /* Validate the encoded hex data */
    bool Validate();

        /* Validate and normalize encoded data. Returns true if valid. */
    bool Normalize();

        /* Free decode data */
    void FreeDecodeData();
private:
    static const char valueMap[32];

    BYTE *m_pDecodedData;
    int   m_numBits;
    int   m_errorStartIndex;
    int   m_errorStopIndex;

    int m_stringIndex;
    int m_checksum;
    int m_value;
};

// This class is used to access the license registry.
// The license registry contains the currently installed license revision.
// This information is used to try to prevent users from installing old
// licenses.  The values are encode and the key names are intentionally obscure.
class EchLicReg
{
public:

    EchLicReg();
    ~EchLicReg();

    EchLicSts Open(IN const TCHAR *pLicenseId);
    void Close();

    EchLicSts SetLicenseRevision(unsigned int revision);
    EchLicSts GetLicenseRevision(unsigned int &revision);

    EchLicSts SetValue(IN const TCHAR *name, unsigned int value);
    EchLicSts GetValue(IN const TCHAR *name, unsigned int &value);

    void Encode(IN const TCHAR *pString, IN int var, OUT EchLic5BitEncodedString &encodedData);
    void Encode(IN BYTE *pData, IN int len, IN int var);
    void Decode(IN BYTE *pData, IN int len, IN int var);

    void Encode(IN BYTE *pData, IN int len, IN int var, OUT EchLic5BitEncodedString &encodedData);
private:
    EchLicSts MustBeOpen() { return (m_hKey == NULL) ? EchLicSts_RegMustBeOpened : EchLicSts_Good; }
    void GetKey(IN const TCHAR *pLicenseId, OUT EchLicString &key);

    HKEY m_hKey;
    int  m_var;
    int m_maskValue;
};