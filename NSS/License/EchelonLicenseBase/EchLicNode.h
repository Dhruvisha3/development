//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// File EchLicNode.h
//
// This file defines some utility base classes used by the Echelon 
// License to access the XML license file.  These classes are built
// using the Microsoft XML DOM interface.
//
// Copyright (c) 2009 Echelon Corporation.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////

#pragma once

/* Normally the Echelon License Utilities require MsXml4.  However, in an attempt
 * to make the (internal) "ReadMachineLock" utility work on a wide variety of PCs without the
 * aid of an installer, the license utilities can be built using MsXml3.0.  This
 * version does not support schema validation.  
 * To build using old version of XML, define USE_OLD_XML_DOM.
 *
 * The ReadMachineLock utility is used to gather information about PCs so that
 * we can determine which information is suitable for locking.
 */
#if defined USE_OLD_XML_DOM
interface IXMLDOMDocument;
#define EchLic_IXMLDomDocumentPtr IXMLDOMDocumentPtr
#define EchLic_IXMLDOMNodePtr IXMLDOMNodePtr
#define EchLic_IXMLDOMElementPtr IXMLDOMElementPtr
#define EchLic_IXMLDOMProcessingInstructionPtr MSXML2::IXMLDOMProcessingInstructionPtr
#define ECHLIC_DOM_UUID __uuidof(DOMDocument)

#define raw_createProcessingInstruction createProcessingInstruction
#define raw_replaceChild replaceChild
#define raw_appendChild appendChild
#define raw_createElement createElement
#define raw_selectSingleNode selectSingleNode
#define raw_save save
#define raw_load load
#define raw_loadXML loadXML
#else
#import "msxml6.dll"
using namespace MSXML2;

#define EchLic_IXMLDomDocumentPtr IXMLDOMDocument2Ptr
#define EchLic_IXMLDOMNodePtr MSXML2::IXMLDOMNodePtr
#define EchLic_IXMLDOMProcessingInstructionPtr MSXML2::IXMLDOMProcessingInstructionPtr
#define EchLic_IXMLDOMElementPtr MSXML2::IXMLDOMElementPtr
#define ECHLIC_DOM_UUID __uuidof(MSXML2::DOMDocument60)
#define ECHLIC_SCHEMACACHE_UUID __uuidof(MSXML2::XMLSchemaCache60)

#endif

#include <comdef.h>
#include "EchLicUtil.h"

class EchLicNode;

/* This class represents an XML license document file */
class EchLicDoc
{
public:
    EchLicDoc();
    ~EchLicDoc();

    /* Open an empty XML document */
    EchLicSts Open(void);
    EchLicSts Close(void);

    void Set(EchLic_IXMLDomDocumentPtr pXmlDoc);
    EchLicSts GetRoot(const IN TCHAR *rootName, OUT EchLicNode &root);
    EchLicSts CreateRoot(const IN TCHAR *rootName, OUT EchLicNode &root);
    EchLicSts Save(IN const TCHAR *destinationPath);
    EchLicSts LoadXml(IN const TCHAR *xmlString,
                      IN const TCHAR *schemaPath,
                      OUT EchLicString &failureExplanation);
    EchLicSts GetXml(OUT EchLicString &xmlString);

    EchLicSts Load(IN const TCHAR *sourceLicenseFilePath, 
                   IN const TCHAR *schemaPath,
                   OUT EchLicString &failureExplanation); 
    
    /* Validate that the  document has been read. */
    EchLicSts MustBeLoaded(void) { return m_hasBeenLoaded ? EchLicSts_Good : EchLicSts_MustBeLoaded; }

    EchLic_IXMLDomDocumentPtr GetDoc() { return m_pXmlDoc; }

    /* Get the path last used to load or save the document. */
    EchLicSts GetPath(OUT EchLicString &path);

private:

    EchLicSts OpenForRead(IN const TCHAR *schemaPath);
    EchLicSts ValidateSchema(OUT EchLicString &failureExplanation);
    EchLicSts SetProcessingInstructions(void);

    EchLic_IXMLDomDocumentPtr m_pXmlDoc;

    bool m_hasBeenLoaded;
    EchLicString m_path;  // Last path used to load or save the document.
};

// This class represents an individual node in an XML document.
class EchLicNode : public EchLicDoc
{
public:
    EchLicNode();
    ~EchLicNode();

    static const TCHAR * BOOL_TRUE_STRING;
    static const TCHAR * BOOL_FALSE_STRING;

    /* Add a child node with the specified name. */
    EchLicSts AddChild(const IN TCHAR *nodeName, OUT EchLicNode &childNode);

    /* Add a child node with the specified name, or if one already exists, return it. */
    EchLicSts AddOnlyChild(const IN TCHAR *nodeName, OUT EchLicNode &childNode);

    /* Find a child node. */
    EchLicSts FindNode(const IN TCHAR *nodeName, OUT EchLicNode &node);

    /* Find the first child node. */
    EchLicSts GetFirstChild(OUT EchLicNode &childNode);

    /* Find the next sibling. */
    EchLicSts GetNext(OUT EchLicNode &node);
    EchLicSts GetNext();

    /* Get the name of the node */
    EchLicSts GetName(OUT EchLicString &name);

    /* Add a property. */
    EchLicSts AddProperty(unsigned int value);
    EchLicSts AddBoolProperty(bool value);
    EchLicSts AddProperty(const TCHAR *value);
    EchLicSts AddProperty(BSTR value);

    template <class T> EchLicSts AddProperty(const IN TCHAR *nodeName, T value)
    {
        EchLicNode node;
        EchLicSts sts = AddOnlyChild(nodeName, node);
        if (sts == EchLicSts_Good)
        {
            sts = node.AddProperty(value);
        }
        return sts;
    }
    EchLicSts AddBoolProperty(const IN TCHAR *nodeName, bool value);

    /* Get a property.  */
    EchLicSts GetProperty(unsigned int &value);
    EchLicSts GetProperty(int &value);
    EchLicSts GetBoolProperty(bool &value);

    EchLicSts GetProperty(EchLicString &value);  
    template <class T> EchLicSts GetProperty(const IN TCHAR *nodeName, T &value)
    {
        EchLicNode node;
        EchLicSts sts = FindNode(nodeName, node);
        if (sts == EchLicSts_Good)
        {
            sts = node.GetProperty(value);
        }
        return sts;
    }
    EchLicSts GetBoolProperty(const IN TCHAR *nodeName, bool &value);

        /* Get a property.  */
    template <class T1, class T2> EchLicSts GetOptionalProperty(const IN TCHAR *nodeName, T1 &value, T2 defaultValue)
    {
        EchLicSts sts = GetProperty(nodeName, value);
        if (sts == EchLicSts_NodeNotFound)
        {
            value = defaultValue;
            sts = EchLicSts_Good;
        }
        return sts;
    }
    EchLicSts GetOptionalBoolProperty(const IN TCHAR *nodeName, bool &value, bool defaultValue);

    /* Get a property.  */
    // EchLicSts GetProperty(const IN TCHAR *nodeName, EchLicString &value);  

    void SetDoc(EchLic_IXMLDomDocumentPtr pXmlDoc);
    void Set(EchLic_IXMLDOMNodePtr pNode, EchLic_IXMLDomDocumentPtr pXmlDoc);
    EchLicSts GetXmlString(EchLicString &xmlString);

    bool isEmpty(void) {return m_pNode == NULL; }
private:
    EchLic_IXMLDOMNodePtr m_pNode;
    EchLicSts TranslateStringToBool(const IN TCHAR *stringValue, OUT bool &value);
    EchLicSts TranslateBoolToString(IN bool value, OUT EchLicString &stringValue);
};
