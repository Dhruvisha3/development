//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// File EchelonLicenseBase.cpp
//
// This file contains the base clases used to acces Echelon licenses. 
// This class is used by licensing clients (the software that is 
// licensed), licensing wizard (the tools used to interact with 
// the licensing server) and the licening server (or any standalone
// tool that generates licenses).  For security reasons, this code 
// should be compiled with the licensing client rather than included as
// a DLL.  License wizards may use a DLL to access the licensing class,
// if desired.
//
// When used by a licensing server, you must define 
// INCLUDE_ECHELON_LICENSE_SERVER.  However, you must NOT define this 
// macro in any code that leave Echelon, even in binary form.
//
// Copyright (c) 2009 Echelon Corporation.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "EchelonLicenseBase.h"
#if defined(INCLUDE_ECHELON_LICENSE_SERVER)
#include "EchelonLicensePrivateKey.h"
#endif
#include <comdef.h>
#include <time.h>

# pragma comment(lib, "wbemuuid.lib")
#import "Echelon.License.Client.ServerValidation.tlb" no_namespace named_guids

//============================================================================
//                               Constants
//============================================================================

/******************************************************************************
 *                      Names of XML nodes and enumeration values 
 *****************************************************************************/
const TCHAR *EchelonLicenseBase::NodeNameRoot       = TEXT("License");
  const TCHAR *EchelonLicenseBase::NodeNameContent    = TEXT("Content");
    const TCHAR *EchelonLicenseBase::NodeNameDocumentType   = TEXT("DocumentType");
        const TCHAR *EchelonLicenseBase::DocumentTypeLicense    = TEXT("License");
        const TCHAR *EchelonLicenseBase::DocumentTypeRequest    = TEXT("Request");
    const TCHAR *EchelonLicenseBase::NodeNameLicenseType = TEXT("LicenseType");
    const TCHAR *EchelonLicenseBase::NodeNameSchemaVersion   = TEXT("SchemaVersion");
      const int EchelonLicenseBase::CurrentSchemaVersionValue  = 1;
    const TCHAR *EchelonLicenseBase::NodeNameHostName = TEXT("HostName");
    const TCHAR *EchelonLicenseBase::NodeNameProductEdition = TEXT("ProductEdition");
    const TCHAR *EchelonLicenseBase::NodeNameLicenseId  = TEXT("LicenseId");
    const TCHAR *EchelonLicenseBase::NodeNameLicenseRevision   = TEXT("LicenseRevision");
    const TCHAR *EchelonLicenseBase::NodeNameUserNotes = TEXT("UserNotes");

    const TCHAR *EchelonLicenseBase::NodeNameMachineLock = TEXT("MachineLock");
      const TCHAR *EchelonLicenseBase::NodeNameHardDisk = TEXT("HardDisk");
      const TCHAR *EchelonLicenseBase::NodeNameCpu = TEXT("Cpu");
      const TCHAR *EchelonLicenseBase::NodeNameMotherboard = TEXT("Motherboard");
      const TCHAR *EchelonLicenseBase::NodeNameBios = TEXT("BIOS");

      const TCHAR *EchelonLicenseBase::NodeNamePlatform = TEXT("Platform");
          const TCHAR *EchelonLicenseBase::NodeNameVmType = TEXT("VmType");
            const TCHAR *EchelonLicenseBase::VmTypeValueNone = TEXT("None");
            const TCHAR *EchelonLicenseBase::VmTypeValueVmWare = TEXT("VmWare");
            const TCHAR *EchelonLicenseBase::VmTypeValueVirtualPc = TEXT("VirtualPc");

      const TCHAR *EchelonLicenseBase::NodeNameShortFormat = TEXT("ShortFormat");
        const TCHAR *EchelonLicenseBase::NodeNameHashCode = TEXT("HashCode");
    const TCHAR *EchelonLicenseBase::NodeNameExpirationData = TEXT("ExpirationDate");
    const TCHAR *EchelonLicenseBase::NodeNameFeatures = TEXT("Features");
        const TCHAR *EchelonLicenseBase::LicenseTypeOpenLnsActivation = TEXT("OpenLnsActivation");
                /* LNS activation feature names */
            const TCHAR *EchelonLicenseBase::NodeNameProductReleaseMajor = TEXT("MajorProductReleaseNumber");
            const TCHAR *EchelonLicenseBase::NodeNameProductReleaseMinor = TEXT("MinorProductReleaseNumber");
            const TCHAR *EchelonLicenseBase::NodeNameDeviceCapacity = TEXT("DeviceCapacity");
            const TCHAR *EchelonLicenseBase::NodeNameTrialLicense = TEXT("TrialLicense");
            const TCHAR *EchelonLicenseBase::NodeNameRunTimeLimit = TEXT("RunTimeLimit");
            const TCHAR *EchelonLicenseBase::NodeNameOpenSystemLimit = TEXT("OpenSystemLimit");

        const TCHAR *EchelonLicenseBase::LicenseTypeOpenLnsCTActivation = TEXT("OpenLnsCTActivation");
            /* Open LNS CT activation feature names */
            // static const TCHAR *NodeNameProductReleaseMajor;
            // static const TCHAR *NodeNameProductReleaseMinor;
            // static const TCHAR *NodeNameDeviceCapacity;
            // static const TCHAR *NodeNameTrialLicense;
            const TCHAR *EchelonLicenseBase::NodeNameOpenLnsCTEditionName = TEXT("EditionName");
                const TCHAR *EchelonLicenseBase::NodeNameOpenLnsCTEditionStandard = TEXT("Standard");
                const TCHAR *EchelonLicenseBase::NodeNameOpenLnsCTEditionProfessional = TEXT("Professional");
                const TCHAR *EchelonLicenseBase::NodeNameOpenLnsCTEditionTrial = TEXT("Trial");
            const TCHAR *EchelonLicenseBase::NodeNameDatabaseLimit = TEXT("DatabaseLimit");
            // static const TCHAR *NodeNameRunTimeLimit;

            /* LM activation feature constants */
            // static const unsigned int NO_RUNTIME_LIMIT = 0xffffffff;
            // static const unsigned int NO_DATABASE_LIMIT = 0xffffffff;
	    const TCHAR *EchelonLicenseBase::LicenseTypeBacnetRouterActivation = TEXT("BacnetRouterActivation");
            // static const TCHAR *NodeNameProductReleaseMajor;
            // static const TCHAR *NodeNameProductReleaseMinor;
		const TCHAR *EchelonLicenseBase::LicenseTypeLumInsightLicense = TEXT("LumInsightLicense");
    const TCHAR *EchelonLicenseBase::NodeNameRequestParameters = TEXT("RequestParameters");

        // LicenseType_OpenLnsActivation;
            /* LNS Activation request parameters names */
            // static const TCHAR *NodeNameProductReleaseMajor;
            // static const TCHAR *NodeNameProductReleaseMinor;

        // LicenseType_OpenLnsCTActivation;
            /* Open LNS CT Activation request parameters names */
            // static const TCHAR *NodeNameProductReleaseMajor;
            // static const TCHAR *NodeNameProductReleaseMinor;

    const TCHAR *EchelonLicenseBase::NodeNameSignature = TEXT("Signature");


  const TCHAR *EchelonLicenseBase::NodeNameComment = TEXT("Comment");

    /* Registry Names */
  const TCHAR *EchelonLicenseBase::RegNameLicenseInfoKey = TEXT("Software\\Echelon\\License Paths"); // Main key into licensing registry.
    const TCHAR *EchelonLicenseBase::RegNameSchemaPathKey = TEXT("Schema Path"); // Schema pathname key.
    const TCHAR *EchelonLicenseBase::RegNameInstalledLicensesPath = TEXT("Installed Licenses Path"); // Lns License path key.
    const TCHAR *EchelonLicenseBase::RegNameTraceOption = TEXT("License Access Trace Level");  // Trace option.  0=off, 1=errors, 2=access


  const TCHAR *EchelonLicenseBase::SchemaName = TEXT("EchelonLicense.xsd");

/******************************************************************************
 *                  Private definitions for license description 
 *****************************************************************************/

struct EchelonLicenseBase::LicenseDescription
{
	const TCHAR *licenseType;
	const TCHAR *licenseName;
};

/******************************************************************************
 *                      Constant tables for license description 
 *****************************************************************************/

static const EchelonLicenseBase::LicenseDescription licenseDescriptions[EchelonLicenseBase::NumberOfLicenseTypes] =
{
    {   // LicenseType_OpenLnsActivation
        EchelonLicenseBase::LicenseTypeOpenLnsActivation,
        TEXT("OpenLnsActivation"),
    },


    {   // LicenseType_OpenLnsCTActivation
        EchelonLicenseBase::LicenseTypeOpenLnsCTActivation,
        TEXT("OpenLnsCTActivation"),
    },

	{
		EchelonLicenseBase::LicenseTypeBacnetRouterActivation,
		TEXT("BacnetRouterActivation"),
	},

	{
		EchelonLicenseBase::LicenseTypeLumInsightLicense,
		TEXT("LumInsightLicense"),
	},
};
/******************************************************************************
 *                      Constant tables for hardware locking 
 *****************************************************************************/

/* Hard Disk */
const EchelonLicenseBase::EchLicWbemProperties EchelonLicenseBase::hardDiskLockTypesSerialNumber = 
{
    EchelonLicenseBase::NodeNameHardDisk,
    TEXT("SELECT * FROM Win32_DiskDrive"),  /* On VISTA and later, use Win32_DiskDrive to get serial number.
                                               Win32_PhysicalMedia is unreliable on WIN7.*/
    hardDiskPropertiesSerialNumber,
    EchLicSts_HdLockFailed
};

const EchelonLicenseBase::EchLicWbemProperties EchelonLicenseBase::hardDiskLockTypesSerialNumberOld = 
{
    EchelonLicenseBase::NodeNameHardDisk,
    TEXT("SELECT * FROM Win32_PhysicalMedia"), /* On XP and prior, use Win32_PhysicalMedia to get serial number.
                                                  because serial number support was not added to 
                                                  Win32_DiskDrive until VISTA.
                                                */
    hardDiskPropertiesSerialNumber,
    EchLicSts_HdLockFailed
};

const EchelonLicenseBase::EchLicWbemPropertyElements EchelonLicenseBase::hardDiskPropertiesSerialNumber[] =
{
    { TEXT("SerialNumber")},

    NULL,
};

const EchelonLicenseBase::EchLicWbemProperties EchelonLicenseBase::hardDiskLockTypes = 
{
    EchelonLicenseBase::NodeNameHardDisk,
    TEXT("SELECT * FROM Win32_DiskDrive"),
    hardDiskProperties,
    EchLicSts_HdLockFailed
};

const EchelonLicenseBase::EchLicWbemPropertyElements EchelonLicenseBase::hardDiskProperties[] =
{
    { TEXT("Manufacturer")},
    { TEXT("Model")},
    { TEXT("Signature")},
    { TEXT("TotalHeads")},

    NULL,
};

/* CPU */
const EchelonLicenseBase::EchLicWbemProperties EchelonLicenseBase::cpuLockTypes = 
{
    EchelonLicenseBase::NodeNameCpu,
    TEXT("SELECT * FROM Win32_Processor"),
    cpuProperties,
    EchLicSts_CpuLockFailed
};

const EchelonLicenseBase::EchLicWbemPropertyElements EchelonLicenseBase::cpuProperties[] =
{
    { TEXT("Description")},
    { TEXT("ExtClock")},
    { TEXT("Family")},
    { TEXT("Level")},
    { TEXT("Manufacturer")},
    { TEXT("MaxClockSpeed")},
    { TEXT("Name")},
    { TEXT("NumberOfCores")},
    { TEXT("NumberOfLogicalProcessors")},
    { TEXT("ProcessorId")},
    { TEXT("ProcessorType")},
    { TEXT("Revision")},
    { TEXT("Stepping")},
    { TEXT("Version")},

    NULL,
};

/* motherboard */
const EchelonLicenseBase::EchLicWbemProperties EchelonLicenseBase::motherboardLockTypes =
{
    EchelonLicenseBase::NodeNameMotherboard,
    TEXT("SELECT * FROM Win32_BaseBoard"),
    motherboardProperties,
    EchLicSts_MotherboardLockFailed
};

const EchelonLicenseBase::EchLicWbemPropertyElements EchelonLicenseBase::motherboardProperties[] =
{
    { TEXT("SerialNumber")},
    { TEXT("Manufacturer")},
    { TEXT("Name")},
    NULL,
};

/* bios */
const EchelonLicenseBase::EchLicWbemProperties EchelonLicenseBase::biosLockTypes =
{
    EchelonLicenseBase::NodeNameBios,
    TEXT("SELECT * FROM Win32_BIOS"),
    biosProperties,
    EchLicSts_BiosLockFailed
};

const EchelonLicenseBase::EchLicWbemPropertyElements EchelonLicenseBase::biosProperties[] =
{
    { TEXT("SerialNumber")},
    { TEXT("Manufacturer")},
    { TEXT("Version")},
    { TEXT("ReleaseDate")},
    { TEXT("SMBIOSBIOSVersion")},

    NULL,
};

const EchelonLicenseBase::EchLicMachineLockFlagProperties EchelonLicenseBase::machineLockFlagProperties[] =
{
    { EchelonLicenseBase::NodeNameHardDisk,    TEXT("SerialNumber"), EchelonLicenseBase::ManualLockCodeFlag_HasHdSn},
    { EchelonLicenseBase::NodeNameMotherboard, TEXT("SerialNumber"), EchelonLicenseBase::ManualLockCodeFlag_HasMotherboardSn},
    { EchelonLicenseBase::NodeNameBios,        TEXT("SerialNumber"), EchelonLicenseBase::ManualLockCodeFlag_HasBiosSn},

    NULL
};

//============================================================================
//                             General Pulic Methods
//============================================================================
EchelonLicenseBase::EchelonLicenseBase(void)
{
    m_asyncValidationHandle = NULL;
    m_asynchLicenselicenseStatus = EchLicSts_MustBeValidated;
    m_asyncServerContactSucceeded = FALSE;
    m_licenseType = NumberOfLicenseTypes;
    m_hEventLog = NULL;
}

EchelonLicenseBase::~EchelonLicenseBase(void)
{
    while (m_asyncValidationHandle != NULL)
    {   // Wait for validation thread to terminate
        Sleep(100);
    }
    m_licenseDoc.Close();

    if (m_hEventLog) 
    {
        CloseEventLog(m_hEventLog);
    }
}

EchLicSts EchelonLicenseBase::GetLicenseRegistryPath(const TCHAR *key, EchLicString &path)
{
    EchLicSts sts = EchLicSts_BadLicensePath;
    HKEY hKey;
    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, RegNameLicenseInfoKey, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
    {
        DWORD dataItemLen = 0;
        if (RegQueryValueEx(hKey, key, 0, NULL, NULL, &dataItemLen) == ERROR_SUCCESS && dataItemLen != 0)
        {
            TCHAR *pPath = new TCHAR[dataItemLen];
            if (pPath != NULL)
            {
                if (RegQueryValueEx(hKey, key, 0, NULL, (BYTE *)pPath, &dataItemLen) == ERROR_SUCCESS)
                {
                    sts = EchLicSts_Good;

                    path.Set(pPath);
                    int len = _tcslen(pPath);
                    if (len != 0 && pPath[len-1] != '\\')
                    {   // Path is supposed to have a trailing \\ - make it so.
                        path.Cat(TEXT("\\"));
                    }
                    delete[] pPath;
                }  
            }
        }
        RegCloseKey(hKey);
    }
    return sts;
}

EchLicSts EchelonLicenseBase::GetFullSchemaPath(EchLicString &fullSchemaPath)
{
    EchLicSts sts = GetLicenseRegistryPath(RegNameSchemaPathKey, fullSchemaPath);
    
    if (sts == EchLicSts_Good)
    {
        fullSchemaPath.Cat(SchemaName);
    }
    return sts;
}

EchLicSts EchelonLicenseBase::GetLicenseRootPath(EchLicString &licenseRootPath)
{
    return GetLicenseRegistryPath(RegNameInstalledLicensesPath, licenseRootPath);
}

EchLicSts EchelonLicenseBase::GetLicenseInstallPath(IN LicenseType licenseType, 
                                                    OUT EchLicString &licensePath,
                                                    bool sanitizeName)
{
    const LicenseDescription *pDesc;
    EchLicSts sts = EchLicSts_Good;
    
    sts = GetLicenseDescription(licenseType, pDesc);
    if (sts == EchLicSts_Good)
    {
        sts = GetLicenseRootPath(licensePath);
        if (sts == EchLicSts_Good)
        {
            licensePath.Cat(pDesc->licenseName);
            licensePath.Cat(TEXT(".lic"));
        }
    }
    return(sts);
}


/**************************************************************************
 *                         Access license content                             
 *************************************************************************/

    /* Get the license root node. */
EchLicSts EchelonLicenseBase::GetLicenseRoot(OUT EchLicNode &root)
{
    return m_licenseDoc.GetRoot(NodeNameRoot, root);
}


    /* Get the license root and content nodes. */
EchLicSts EchelonLicenseBase::GetLicenseContent(OUT EchLicNode &root,
                                                OUT EchLicNode &content)
{
    return GetContent(m_licenseDoc, root, content);
}

    /* Get document version */
EchLicSts EchelonLicenseBase::GetDocumentSchemaVersion(unsigned int &schemaVersion)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameSchemaVersion, schemaVersion);
    }
    return sts;
}

    /* Override Document version.  For testing purposes only. */
EchLicSts EchelonLicenseBase::OverrideDocumentSchemaVersion(unsigned int schemaVersion)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.AddProperty(NodeNameSchemaVersion, schemaVersion);
    }
    return sts;
}


    /* Get document type */
EchLicSts EchelonLicenseBase::GetDocumentType(DocumentType &documentType)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        EchLicString docTypeString;
        sts = content.GetProperty(NodeNameDocumentType, docTypeString);
        if (sts == EchLicSts_Good)
        {
            if (_tcscmp(docTypeString.Get(), DocumentTypeRequest) == 0)
            {
                documentType = DocType_Request;
            }
            else if (_tcscmp(docTypeString.Get(), DocumentTypeLicense) == 0)
            {
                documentType = DocType_License;
            }
            else
            {
                sts = EchLicSts_InvalidDocumentType;
            }
        }
    }
    return sts;
}

    /* Get the license ID. */
EchLicSts EchelonLicenseBase::GetLicenseId(OUT EchLicString &licenseId)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameLicenseId, licenseId);
    }
    return sts;
}

    /* Get the ID of the product that the license applies to */
EchLicSts EchelonLicenseBase::GetProductEdition(OUT EchLicString &productEdition)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameProductEdition, productEdition);
    }
    return sts;
}

    /* Get the host name stored in the license */
EchLicSts EchelonLicenseBase::GetHostName(OUT EchLicString &hostName)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameHostName, hostName);
    }
    return sts;
}

    /* Get the 'UserNotes' - a user settable field to identify the license use. */
EchLicSts EchelonLicenseBase::GetUserNotes(OUT EchLicString &notes)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameUserNotes, notes);
    }
    return sts;
}

    /* Get the 'comment' - a user settable field outside of the signature. */
EchLicSts EchelonLicenseBase::GetComment(OUT EchLicString &comment)
{
    EchLicNode root;
    EchLicSts sts = GetLicenseRoot(root);
    if (sts == EchLicSts_Good)
    {
        sts = root.GetOptionalProperty(NodeNameComment, comment, TEXT(""));
    }
    return sts;
}

    /* Set the 'comment' - a user settable field outside of the signature. */
EchLicSts EchelonLicenseBase::SetComment(IN const TCHAR *comment)
{
    EchLicNode root;
    EchLicSts sts = GetLicenseRoot(root);
    if (sts == EchLicSts_Good)
    {
        sts = root.AddProperty(NodeNameComment, comment);
    }
    return sts;
}

    /* Get the license revision number */
EchLicSts EchelonLicenseBase::GetLicenseRevision(OUT unsigned int &licenseRevision)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameLicenseRevision, licenseRevision);
    }
    return sts;
}

    /**************************************************************************
     *                         Access feature content                             
     *************************************************************************/

    /* Get the license feature node */
EchLicSts EchelonLicenseBase::GetLicenseFeatures(OUT EchLicNode &features)
{
    const LicenseDescription *pDesc;
    EchLicSts sts = GetLicenseDescription(pDesc);

    if (sts == EchLicSts_Good)
    {
        EchLicNode root;
        EchLicNode content;
        sts = GetLicenseContent(root, content);

        if (sts == EchLicSts_Good)
        {
            EchLicNode featureRoot;
            sts = content.FindNode(NodeNameFeatures, featureRoot);
            if (sts == EchLicSts_Good)
            {
                sts = featureRoot.FindNode(pDesc->licenseType, features);
            }
        }
    }
    return sts;
}

EchLicSts EchelonLicenseBase::GetFeatureBool(IN const TCHAR *featureName, OUT bool &value)
{
    EchLicNode features;
    EchLicSts sts = GetLicenseFeatures(features);
    if (sts == EchLicSts_Good)
    {
        sts = features.GetBoolProperty(featureName, value);
    }
    return sts;
}

EchLicSts EchelonLicenseBase::GetOptionalFeatureBool(IN const TCHAR *featureName, OUT bool &value,  IN bool defaultValue)
{
    EchLicSts sts = GetFeatureBool(featureName, value);
    if (sts == EchLicSts_NodeNotFound)
    {
        value = defaultValue;
        sts = EchLicSts_Good;
    }
    return sts;
}

EchLicSts EchelonLicenseBase::AddFeatureBool(IN const TCHAR *featureName, IN bool value)
{
    EchLicNode features;
    EchLicSts sts = GetLicenseFeatures(features);
    if (sts == EchLicSts_Good)
    {
        sts = features.AddBoolProperty(featureName, value);
    }
    return sts;
}

//============================================================================
//                             License Client Methods
//============================================================================

/* Opens and loads the specified file, and validate the digital signature, 
 * machine lock, and license registry. 
 */
EchLicSts EchelonLicenseBase::ValidateLicense(OUT EchLicString &errorExplanation,
                                              bool fastValidation)
{

    const LicenseDescription *pDesc;
    EchLicSts sts = GetLicenseDescription(pDesc);

    if (sts == EchLicSts_Good)
    {
        EchLicNode root;
        EchLicNode content;
        sts = GetLicenseContent(root, content);

        if (sts == EchLicSts_Good)
        {
            sts = ValidateSignature(root, content);
        }
        
        if (sts == EchLicSts_Good)
        {
            EchLicNode machineLock;
            sts = content.FindNode(NodeNameMachineLock, machineLock);
            if (sts == EchLicSts_Good)
            {
                IWbemLocator *pLoc; 
                IWbemServices *pSvc;
                sts = InitWbem(pLoc, pSvc);

                if (sts == EchLicSts_Good)
                {
                    sts = ValidateWbem(pLoc, pSvc, machineLock, GetHardDiskLockTypesSerialNumber(), errorExplanation);
                }
                if (sts == EchLicSts_Good)
                {
                    sts = ValidateWbem(pLoc, pSvc, machineLock, hardDiskLockTypes, errorExplanation);
                }
                if (sts == EchLicSts_Good && !fastValidation)
                {
                    sts = ValidateWbem(pLoc, pSvc, machineLock, cpuLockTypes, errorExplanation);
                }
                if (sts == EchLicSts_Good)
                {
                    sts = ValidateWbem(pLoc, pSvc, machineLock, motherboardLockTypes, errorExplanation);
                }
                if (sts == EchLicSts_Good)
                {
                    sts = ValidateWbem(pLoc, pSvc, machineLock, biosLockTypes, errorExplanation);
                }
            }
            if (sts == EchLicSts_Good)
            {
                sts = ValidateVmType(machineLock, errorExplanation);
            }
            if (sts == EchLicSts_Good)
            {
                sts = ValidateManualLockCode(content, machineLock, errorExplanation);
            }
            if (sts == EchLicSts_Good)
            {
                sts = ValidateLicenseRevision(content, false, errorExplanation);
            }
            if (sts == EchLicSts_Good)
            {
                bool expires, expired;
                unsigned int fullDaysLeft, day, month, year;
                time_t expirationTime;
                sts = GetLicenseExpiration(expires, expired, fullDaysLeft, day, month, year, expirationTime);
                if (sts == EchLicSts_Good && expired)
                {
                    #define MAX_EXPIRATION_STRING_SIZE 100
                    TCHAR expirationString[MAX_EXPIRATION_STRING_SIZE];
                    _stprintf_s(expirationString, MAX_EXPIRATION_STRING_SIZE, TEXT("License expired on %.4d-%.2d-%.2d"), year, month, day);
                    errorExplanation  = expirationString;

                    sts = EchLicSts_LicenseExpired;
                }
            }
        }
    }

    logLicenseAccess(TEXT("ValidateLicense"), sts, errorExplanation);
    return sts;
}

/* Validate the license with the license server. License file must already
 * be opened, loaded, and validated using ValidateLicenseFile.  If contact with
 * the server fails this function will return EchLicSts_Good, but contactSucceeded
 * will be FALSE.
 */
EchLicSts EchelonLicenseBase::ValidateWithServer(OUT bool &contactSucceeded)
{
    EchLicString licenseString;
    EchLicSts sts;

    contactSucceeded = FALSE;
    sts = GetXmlString(licenseString);
    if (sts == EchLicSts_Good)
    {
	    ILicenseValidation *p = NULL;

        HRESULT hr = CoCreateInstance(CLSID_LicenseValidation,
                                      NULL,
                                      CLSCTX_INPROC_SERVER,
                                      IID_ILicenseValidation,
                                      reinterpret_cast<void**>(&p));

        if (FAILED(hr))
        {
            sts = EchLicSts_ValidationComponentFailure;
        }
        else
        {
            long result = -1;
            VARIANT_BOOL deleteLicense = 0;
            hr = p->raw_ValidateLicense(bstr_t(licenseString.Get()), &deleteLicense, &result);
            if (FAILED(hr))
            {
                sts = EchLicSts_ValidationComponentFailure;
            }
            else
            {
                sts = (EchLicSts)result;
                if (sts == EchLicSts_ServerConnectionFailed)
                {
                    sts = EchLicSts_Good;
                    contactSucceeded = false;
                }
                else
                {
                    contactSucceeded = true;
                    EchLicString dummy;
                    if (deleteLicense)
                    {
                        if (ValidateLicense(dummy) == EchLicSts_Good)
                        {
                            EchelonLicenseBase tempLic;
                            tempLic.DeleteLicense(m_licenseType);
                        }
                        else
                        {
                            logLicenseAccess(TEXT("ValidateWithServer returned delete, but license file is invalid!"), sts, NULL);
                        }
                    }
                }
            }
            p->Release();
            p = NULL;
        }
    }
    logLicenseAccess(TEXT("ValidateWithServer"), sts, NULL);
    return sts;
}

    /* Kick off an asynchronous validation. When complete, the
       event handle will be set.
     */
EchLicSts EchelonLicenseBase::StartAsynchValidation(IN HANDLE eventHandle,
                                                    IN bool validateWithServer)
{
    EchLicSts sts = EchLicSts_Good;
    if (m_asyncValidationHandle)
    {
        sts = EchLicSts_ValidationInProgress;
    }
    else
    {
        m_asyncValidationHandle = eventHandle;
        m_asyncValidateWithServer = validateWithServer;
        _beginthread(ValidationThread, 0, this);
    }
    
    return sts;
}

    /* Get the results of an asynchronous validation */
EchLicSts EchelonLicenseBase::GetAsyncValidationResults(bool &serverContactSucceeded,
                                                        IN EchLicString &failureExplanation)
{
    EchLicSts sts;
    if (m_asyncValidationHandle)
    {
        sts = EchLicSts_ValidationInProgress;
    }
    else
    {
        sts = m_asynchLicenselicenseStatus;
        serverContactSucceeded = m_asyncServerContactSucceeded;
        failureExplanation = m_failureExplanation;
        logLicenseAccess(TEXT("GetAsyncValidationResults"), sts, failureExplanation);
    }
    return sts;
}

    /* Opens and loads the specified file without validating. 
     */
EchLicSts EchelonLicenseBase::LoadLicenseFile(const TCHAR *licenseFilePath, 
                                              EchLicString &failureExplanation)
{
    EchLicString schemaPath;
    EchLicSts sts = GetFullSchemaPath(schemaPath);
    
    if (sts == EchLicSts_Good)
    {
        sts = m_licenseDoc.Load(licenseFilePath, schemaPath, failureExplanation);    
        if (sts == EchLicSts_Good)
        {
            EchLicNode root;
            EchLicNode content;
            sts = GetLicenseContent(root, content);       
            if (sts == EchLicSts_Good)
            {
                sts = ReadLicenseType(content);
            }
        }
    }
    return sts;
    logLicenseAccess(TEXT("LoadLicenseFile"), sts, failureExplanation);
}

    /* Same as above, but uses the default name, based on the license type.
     */
EchLicSts EchelonLicenseBase::LoadLicenseFile(LicenseType licenseType, 
                                              OUT EchLicString &failureExplanation)
{
    EchLicString licensePath;
    EchLicSts sts;
        
    m_licenseType = licenseType;
    sts = GetLicenseInstallPath(m_licenseType, licensePath);
    
    if (sts == EchLicSts_Good)
    {
        sts = LoadLicenseFile(licensePath, failureExplanation);
    }

    logLicenseAccess(TEXT("LoadLicenseFile"), sts, failureExplanation);
    return sts;
}

        /* Load from an XML string. For example, if license was passed back in
           a SOAP message. 
         */
EchLicSts EchelonLicenseBase::LoadLicenseFromXmlString(IN const TCHAR *xmlString,
                                                       OUT EchLicString &failureExplanation)
{
    EchLicString schemaPath;
    EchLicSts sts = GetFullSchemaPath(schemaPath);
    
    if (sts == EchLicSts_Good)
    {
        sts = m_licenseDoc.LoadXml(xmlString, schemaPath, failureExplanation);    
        if (sts == EchLicSts_Good)
        {
            EchLicNode root;
            EchLicNode content;
            sts = GetLicenseContent(root, content);       
            if (sts == EchLicSts_Good)
            {
                sts = ReadLicenseType(content);
            }
        }
    }
    return sts;
}

    /* Get license as an XML string, for example, to pass in a SOAP message */
EchLicSts EchelonLicenseBase::GetXmlString(OUT EchLicString &xmlString)
{
    EchLicSts sts = m_licenseDoc.GetXml(xmlString);
    if (sts == EchLicSts_Good)
    {
        xmlString.UnformatXml();
    }
    return sts;
}

void EchelonLicenseBase::ValidationThread(void *pThis)
{
    CoInitializeEx(NULL, COINIT_MULTITHREADED);
    EchLicString failureExplanation;
    EchelonLicenseBase *p = (EchelonLicenseBase *)pThis;
    EchLicSts sts = p->ValidateLicense(failureExplanation);
    bool contactedServer = FALSE;
    if (sts == EchLicSts_Good)
    {
        if (p->m_asyncValidateWithServer)
        {
            sts = p->ValidateWithServer(contactedServer);
        }
    }
    p->m_asynchLicenselicenseStatus = sts;
    p->m_failureExplanation = failureExplanation;
    p->m_asyncServerContactSucceeded = contactedServer;
    HANDLE eventHandle = p->m_asyncValidationHandle;

    p->logLicenseAccess(TEXT("ValidationThread"), sts, failureExplanation);

    p->m_asyncValidationHandle = NULL;
    SetEvent(eventHandle);

    CoUninitialize();
}

EchLicSts EchelonLicenseBase::ReadHostName(EchLicString &hostName)
{
    DWORD len = 0;
    TCHAR *hostNameString;
    EchLicSts sts = EchLicSts_Good;
    hostName.Set(TEXT(""));

    GetComputerNameEx(ComputerNamePhysicalDnsFullyQualified, NULL, &len);
    hostNameString = new TCHAR[len];
    if (hostNameString != NULL)
    {
        if (GetComputerNameEx(ComputerNamePhysicalDnsFullyQualified, hostNameString, &len))
        {
            hostName.Set(hostNameString);
        }
        delete[] hostNameString;
    }
    else
    {
        sts = EchLicSts_OutOfMemory;
    }
    return sts;
}

    /* Get the license license type */
EchLicSts EchelonLicenseBase::GetLicenseType(LicenseType &licenseType)
{
    licenseType = m_licenseType;
    return EchLicSts_Good;
}

    /* Get license expiration information.  If there is no expiration, expires
     * and expired is set to false and the values of all the other parameters is
     * undefined.
     */
EchLicSts EchelonLicenseBase::GetLicenseExpiration(bool &expires, bool &expired, 
                                                   unsigned int &fullDaysLeft,
                                                   unsigned int &day, 
                                                   unsigned int &month, 
                                                   unsigned int &year,
                                                   time_t &expirationTime)
{
    EchLicSts sts;
    EchLicNode root;
    EchLicNode content;

    expirationTime = -1;
    sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        EchLicString expirationDate;
        sts = content.GetProperty(NodeNameExpirationData, expirationDate);
        if (sts == EchLicSts_NodeNotFound)
        {
            // No lock, OK
            sts = EchLicSts_Good;
            expires = false;
            expired = false;
        }
        else
        {
            if (_stscanf_s(expirationDate.Get(), TEXT("%d-%d-%d"), &year, &month, &day) == 3)
            {
                time_t currentTime = time(NULL);
                struct tm expirationDate;
                
                expires = true;

                memset(&expirationDate, 0, sizeof(expirationDate));
                expirationDate.tm_mday = day;   
                expirationDate.tm_mon = month-1;  // Months since January...
                expirationDate.tm_year = year - 1900;
                expirationDate.tm_hour = 0; 

                expirationTime = mktime(&expirationDate);

                // For some reason mktime seems to set the hour instead of 0.
                // This appears to be based on an adjustmet for daylight savings time?
                // In any case, subtract off any unexpected values to force the expiration
                // to be at midnight local time.
                expirationTime -= (expirationDate.tm_hour * 60 * 60) + 
                                  (expirationDate.tm_min * 60) +
                                  expirationDate.tm_sec;
                if (currentTime >= expirationTime)
                {
                    expired = true;
                    fullDaysLeft = 0;
                }
                else
                {
                    expired = false;
                    fullDaysLeft = (int)((expirationTime - currentTime)/(24*60*60));
                }
            }
            else
            {
                sts = EchLicSts_InvalidExpiration;
            }
        }
    }
    return sts;
}

//============================================================================
//                             License Wizard Methods
//============================================================================

/* Create a request document. Depending on the license type, 
 * it may be necessary to add request parameters as well
 */
EchLicSts EchelonLicenseBase::CreateRequest(IN LicenseType licenseType, // May be NULL
                                            IN const TCHAR *productEdition,
                                            IN const TCHAR *licenseId)
{
    EchLicSts sts;
    EchLicNode root;
    EchLicNode content;
    EchLicNode machineLock;
    EchLicString hostName;

    sts = ReadHostName(hostName);
    if (sts == EchLicSts_Good)
    {
        sts = CreateLicenseFramework(m_licenseDoc, licenseType, CurrentSchemaVersionValue, DocumentTypeRequest, 
                                     hostName, productEdition, licenseId, 0, root, content, machineLock);
        if (sts == EchLicSts_Good)
        {
            IWbemLocator *pLoc; 
            IWbemServices *pSvc;
            sts = InitWbem(pLoc, pSvc);
            if (sts == EchLicSts_Good)
            {    
                sts = CreateFromWbem(pLoc, pSvc, machineLock, GetHardDiskLockTypesSerialNumber());

                if (sts == EchLicSts_Good)
                {
                    sts = CreateFromWbem(pLoc, pSvc, machineLock, hardDiskLockTypes);
                }
           
                if (sts == EchLicSts_Good)
                {
                    sts = CreateFromWbem(pLoc, pSvc, machineLock, cpuLockTypes);
                }
                if (sts == EchLicSts_Good)
                {
                    sts = CreateFromWbem(pLoc, pSvc, machineLock, motherboardLockTypes);
                }
                if (sts == EchLicSts_Good)
                {
                    sts = CreateFromWbem(pLoc, pSvc, machineLock, biosLockTypes);
                }
                CloseWbem(pLoc, pSvc);
            }
            if (sts == EchLicSts_Good)
            {
                sts = CreateVmType(machineLock);
            }
        }
    }
    return sts;
}

    /* Create request parameters */
EchLicSts EchelonLicenseBase::CreateRequestParameters(OUT EchLicNode &requestParameters)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);

    if (sts == EchLicSts_Good)
    {
        const LicenseDescription *pDesc;
        sts = GetLicenseDescription(pDesc);

        if (sts == EchLicSts_Good)
        {
            EchLicNode featureRoot;
            sts = content.AddChild(NodeNameRequestParameters, featureRoot);
            if (sts == EchLicSts_Good)
            {
                sts = featureRoot.AddChild(pDesc->licenseType, requestParameters);
            }
        }
    }
    return sts;
}

    /* Get request parameters */
EchLicSts EchelonLicenseBase::GetRequestParameters(OUT EchLicNode &requestParameters)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);

    if (sts == EchLicSts_Good)
    {
        const LicenseDescription *pDesc;
        sts = GetLicenseDescription(pDesc);

        if (sts == EchLicSts_Good)
        {
            EchLicNode featureRoot;
            sts = content.FindNode(NodeNameRequestParameters, featureRoot);
            if (sts == EchLicSts_Good)
            {
                sts = featureRoot.FindNode(pDesc->licenseType, requestParameters);
            }
        }
    }
    return sts;
}

    /* Set the 'notes' - a user settable field to identify the license use. */
EchLicSts EchelonLicenseBase::SetUserNotes(IN const TCHAR *notes)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);

    if (sts == EchLicSts_Good)
    {
        sts = content.AddProperty(NodeNameUserNotes, notes);
    }
    return sts;
}


/* Save a previously created request to a request file */
EchLicSts EchelonLicenseBase::SaveRequest(IN const TCHAR *destinationPath)
{
    return m_licenseDoc.Save(destinationPath);
}



    /* Install the license that has been previously loaded or created using
     * or CreateManualRequest() 
     * in the license directory, and update the license registry. 
     */
EchLicSts EchelonLicenseBase::InstallLicense(LicenseType licenseType, 
                                             EchLicString &failureExplanation,
                                             bool validatePriorToInstalling)
{
    EchLicSts sts = EchLicSts_Good;

    if (licenseType != m_licenseType)
    {
        sts = EchLicSts_UnexpectedLicenseType;
    }
    else
    {
        EchLicNode root;
        EchLicNode content;

        sts = GetLicenseContent(root, content);
        if (sts == EchLicSts_Good)
        {
            sts = ValidateLicenseRevision(content, true, failureExplanation);
            if (sts == EchLicSts_Good)
            {
                const LicenseDescription *pDesc;
                sts = GetLicenseDescription(pDesc);

                if (sts == EchLicSts_Good)
                {
                    if (validatePriorToInstalling)
                    {
                        sts = ValidateLicense(failureExplanation);
                    }
                
                    if (sts == EchLicSts_Good)
                    {
                        EchLicString licensePath;
                        sts = GetLicenseInstallPath(licenseType, licensePath);

                        if (sts == EchLicSts_Good)
                        {
                            sts = SaveLicense(licensePath);
                        }
                    }
                }
            }
        }
    }
    logLicenseAccess(TEXT("InstallLicense"), sts, failureExplanation);

    return sts;
}

//============================================================================
//                             Manual Methods
//============================================================================

    /********************* Manual License Wizard Methods *************************/

	/* Generate manual machine lock code from a request */
EchLicSts EchelonLicenseBase::GenerateManualMachineLockCode(int version, EchelonLicenseBase &request, OUT EchLic5BitEncodedString &machineLockCode)
{
    EchelonLicenseBase temp;
    EchLicSts sts;
    BYTE flags = 0;
    EchLicNode machineLock;

    int i;
    sts = request.GetLicenseMachineLock(machineLock);
    if (sts == EchLicSts_Good)  /* Always include HD, if possible */
    {
        for (i = 0; sts == EchLicSts_Good && machineLockFlagProperties[i].nodeName; i++)
        {
            EchLicNode node;
            sts = machineLock.FindNode(machineLockFlagProperties[i].nodeName, node);
            if (sts == EchLicSts_Good)
            {
                EchLicString value;
                sts = node.GetProperty(machineLockFlagProperties[i].propertyName, value);
                if (sts == EchLicSts_Good)
                {
                    flags |= machineLockFlagProperties[i].flagValue;
                }
            }
            if (sts == EchLicSts_NodeNotFound)
            {   
                sts = EchLicSts_Good;
            }
        }
    }

    if (sts == EchLicSts_Good)
    {
        EchLicNode platform;
        sts = machineLock.FindNode(NodeNamePlatform, platform);
        if (sts == EchLicSts_Good)
        {
            EchLicString vmTypeString;
            sts = platform.GetProperty(NodeNameVmType, vmTypeString);
            if (sts == EchLicSts_Good && vmTypeString != (EchLicString)VmTypeValueNone)
            {
                flags |= ManualLockCodeFlag_IsVM;
            }
        }
    }

    if (sts == EchLicSts_Good)
    {
        BYTE *pHashData;
        DWORD hashSize;
        sts = temp.CreateMachineLockHash(machineLock, pHashData, hashSize);

        if (sts == EchLicSts_Good)
        {
            // The manual license code consists of a 3 nibble header, followed
            // by the machine lock hash data.
            BYTE extraNibbles[2];
            memset(extraNibbles, 0, sizeof(extraNibbles));
            // The byte (two nibbles )contain the version number (0)
            extraNibbles[0] = version;
            // The last nibble contains the flags
            extraNibbles[1] = flags << 4;
            machineLockCode.Encode(true, false, extraNibbles, 12);
            machineLockCode.Encode(false, true, pHashData, 8*hashSize);
            delete[] pHashData;
        }
    }

    return sts;
}

	/* Generate manual machine lock code for this PC */
EchLicSts EchelonLicenseBase::GenerateManualMachineLockCode(OUT EchLic5BitEncodedString &machineLockCode)
{
    EchelonLicenseBase temp;
    EchLicSts sts;
    BYTE flags = 0;
    EchLicNode machineLock;

    // The license type is required by CreateRequest, but the value is
    // arbitrary since it does not affect the machine lock code.
    LicenseType licenseType = EchelonLicenseBase::LicenseType_OpenLnsActivation;

    sts = temp.CreateRequest(licenseType, TEXT(""), TEXT(""));
    if (sts == EchLicSts_Good)
    {
        sts = GenerateManualMachineLockCode(ManualLockCode_Version, temp, machineLockCode);
    }

    return sts;
}

	/* Check to see if machine lock in the request would have generated the specified manualMachineLockCode */
EchLicSts EchelonLicenseBase::CompareRequestWithManualLock(const TCHAR *manualMachineLockCode)
{
    bool matches = false;
    int version; 
    BYTE flags;

    EchLicSts sts = DecodeMachineLock(manualMachineLockCode, version, flags);
    if (sts == EchLicSts_Good)
    {
        EchLic5BitEncodedString actualMachineLockCode;
        sts = GenerateManualMachineLockCode(version, *this, actualMachineLockCode);
        if (sts == EchLicSts_Good)
        {
            if (actualMachineLockCode != (EchLicString)manualMachineLockCode)
            {
                sts = EchLicSts_ManualLockFailed;
            }
        }
    }
    return sts;
}

    /* Create a manual license.  After creation, add features and signature. */
EchLicSts EchelonLicenseBase::CreateManualLicense(IN LicenseType licenseType,
                                                  IN unsigned int schemaVersion,
					                              IN const TCHAR *productEdition,
                                                  IN const TCHAR *licenseId, 
                                                  IN unsigned int licenseRevision,
                                                  IN const TCHAR *manualMachineLockCode)
{    
    EchLicNode root;
    EchLicNode content;
    EchLicNode machineLock;
    EchLicSts sts = CreateLicense(licenseType, schemaVersion, TEXT(""), /* No host name */
                                  productEdition, licenseId, licenseRevision, 
                                  root, content, machineLock);
    if (sts == EchLicSts_Good)
    {
        EchLic5BitEncodedString lockCode(manualMachineLockCode);
        if (lockCode.Normalize())
        {
            sts = AddManualLockCode(machineLock, lockCode);
        }
        else
        {
            sts = EchLicSts_BadMachineLockCode;
        }
    }
    return sts;
}
    /* Sign a manual license */
EchLicSts EchelonLicenseBase::SignManualLicense(IN const TCHAR *encodedSignature)
{
    EchLicNode root;
    EchLicSts sts = GetLicenseRoot(root);
    if (sts == EchLicSts_Good)
    {
        sts = root.AddProperty(NodeNameSignature, encodedSignature);
    }
    return sts;
}

/********************* Manual License Administrator Methods ******************/

	/* Create the manual  Request */
EchLicSts EchelonLicenseBase::CreateManualRequest(IN LicenseType licenseType, 
                                                  IN unsigned int schemaVersion,
											      IN const TCHAR *productEdition,
                                                  IN const TCHAR *licenseId,
                                                  IN const TCHAR *machineLockCode)
{
    EchLicSts sts;
    EchLicNode root;
    EchLicNode content;
    EchLicNode machineLock;
    
    sts = CreateLicenseFramework(m_licenseDoc, licenseType, schemaVersion, 
                                 DocumentTypeRequest, TEXT(""), productEdition, 
                                 licenseId, 0, root, content, machineLock);
    if (sts == EchLicSts_Good)
    {
        sts = AddManualLockCode(machineLock, machineLockCode);
    }
    return sts;
}

	/* Get Manual License Signature */                                        
EchLicSts EchelonLicenseBase::GetLicenseSignature(EchLic5BitEncodedString &encodedSignature)
{
    EchLicNode root;
    EchLicSts sts = GetLicenseRoot(root);
    if (sts == EchLicSts_Good)
    {
        sts = root.GetProperty(NodeNameSignature, encodedSignature);
    }
    return sts;
}

    /* Delete the specified license */
EchLicSts EchelonLicenseBase::DeleteLicense(IN const TCHAR *licensePath)
{
    EchLicSts sts;
    EchLicString schemaPath;

    sts = GetFullSchemaPath(schemaPath);
    
    if (sts == EchLicSts_Good)
    {
        EchLicString failureExplanation;
        sts = m_licenseDoc.Load(licensePath, schemaPath, failureExplanation);

        if (sts == EchLicSts_Good)
        {
            EchLicNode root;
            EchLicNode content;
            sts = GetLicenseContent(root, content);
            if (sts == EchLicSts_Good)
            {
                EchLicString licenseId;
                sts = content.GetProperty(NodeNameLicenseId, licenseId);
                if (sts == EchLicSts_Good)
                {
                    unsigned int licenseRevision;
                    sts = content.GetProperty(NodeNameLicenseRevision, licenseRevision);
                    if (sts == EchLicSts_Good)
                    {
                        EchLicReg reg;
                        sts = reg.Open(licenseId);
                        if (sts == EchLicSts_Good)
                        {
                            unsigned int registeredRevision;
                            bool updateRevision = true;
                            if (reg.GetLicenseRevision(registeredRevision) == EchLicSts_Good)
                            {
                                if (registeredRevision > licenseRevision)
                                {
                                    // Don't update licenseRevision - its already greater
                                    updateRevision = false;
                                }
                            }
                            if (updateRevision)
                            {
                                // Increment registered licenseRevision so this license cannot
                                // be installed again.
                                sts = reg.SetLicenseRevision(licenseRevision+1);
                            }
                        }
                    }
                }
            }
        }
        m_licenseDoc.Close();
        DeleteFile(licensePath);
    }
    logLicenseAccess(TEXT("DeleteLicense"), sts, NULL);
    return sts;
}

    /* Delete the specified license */
EchLicSts EchelonLicenseBase::DeleteLicense(LicenseType licenseType)
{
    EchLicString licensePath;
    EchLicSts sts;

    m_licenseType = licenseType;
    sts = GetLicenseInstallPath(licenseType, licensePath);

    if (sts == EchLicSts_Good)
    {
        sts = DeleteLicense(licensePath);
    }

    return sts;
}

//============================================================================
//                             License Server Methods
//============================================================================

EchLicSts EchelonLicenseBase::GetRequestInfo(OUT LicenseType &licenseType,
                                             OUT EchLicString &hostName,
                                             OUT EchLicString &productEdition)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = GetLicenseType(licenseType);
    }
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameHostName, hostName);
    }
    if (sts == EchLicSts_Good)
    {
        sts = content.GetProperty(NodeNameProductEdition, productEdition);
    }
    return sts;
}

    /* Get a licenses machine lock node. */
EchLicSts EchelonLicenseBase::GetLicenseMachineLock(IN EchLicNode &machineLock)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        sts = content.FindNode(NodeNameMachineLock, machineLock);
    }
    return sts;
}

/* Creates an �empty� license from a request.  Note that this method does NOT 
 * add features or the machine lock.  Typically the license server will 
 * scan the machine lock information in the request and, based on rules defined
 * in the database, selectively copy specific locks to the license.  The
 * license server will use the productID to determine which features must
 * be set.  Some of these feature values will be copied directly from 
 * the product database, and some (such as the database name and capacity)
 * will be copied from the request parameters.
 */
EchLicSts EchelonLicenseBase::CreateLicense(EchelonLicenseBase &request,
                                            IN const TCHAR *licenseId, 
                                            IN unsigned int licenseRevision)
{
    EchLicSts sts;
    LicenseType licenseType;
    EchLicString hostName;
    EchLicString productEdition;
    unsigned int schemaVersion;

    sts = request.GetRequestInfo(licenseType, hostName, productEdition);
    if (sts == EchLicSts_Good)
    {
        sts = request.GetDocumentSchemaVersion(schemaVersion);
    }

    if (sts == EchLicSts_Good)
    {
        EchLicNode root;
        EchLicNode content;
        EchLicNode machineLock;
        sts = CreateLicense(licenseType, schemaVersion, hostName, productEdition, licenseId, licenseRevision,
                            root, content, machineLock);
        if (sts == EchLicSts_Good)
        {
            EchLicString notes;
            sts = request.GetUserNotes(notes);
            if (sts == EchLicSts_Good)
            {
                sts = SetUserNotes(notes);
            }
        }
    }
    return sts;
}

/* Creates an �empty� license. */
EchLicSts EchelonLicenseBase::CreateLicense(IN LicenseType licenseType,
                                            IN unsigned int schemaVersion, 
											IN const TCHAR *hostName,
											IN const TCHAR *productEdition,
                                            IN const TCHAR *licenseId,                                            
                                            IN unsigned int licenseRevision,
                                            OUT EchLicNode &root,
                                            OUT EchLicNode &content,
                                            OUT EchLicNode &machineLock)
{
    EchLicSts sts;
    
    sts = CreateLicenseFramework(m_licenseDoc, licenseType, schemaVersion, DocumentTypeLicense, 
                                 hostName, productEdition, licenseId, licenseRevision,
                                 root, content, machineLock);
    if (sts == EchLicSts_Good)
    {
        EchLicNode featureRoot;
        sts = content.AddChild(NodeNameFeatures, featureRoot);

        if (sts == EchLicSts_Good)
        {
            const LicenseDescription *pDesc;
            // Need to get the description to translate the type to a type name.
            sts = GetLicenseDescription(pDesc);
            if (sts == EchLicSts_Good)
            {
                EchLicNode features;
                sts = featureRoot.AddChild(pDesc->licenseType, features);
            }
        }
    }
    return sts;
}

    /* Compare the license on the server with the license passed in by the
     * client.  The signed portions must match exactly.  
     */
EchLicSts EchelonLicenseBase::CompareLicenses(const TCHAR *serverLicenseXmlString,
                                              const TCHAR *clientLicenseXmlString)
{
    EchLicSts sts;
    EchelonLicenseBase serverLic;
    EchLicString serverContent;
    EchLic5BitEncodedString serverSignature;

    sts = serverLic.GetServerValidationInfo(serverLicenseXmlString, 
                                            serverContent, serverSignature);
    if (sts == EchLicSts_Good)
    {
        EchelonLicenseBase clientLic;
        EchLicString clientContent;
        EchLic5BitEncodedString clientSignature;

        sts = clientLic.GetServerValidationInfo(clientLicenseXmlString, 
                                                clientContent, clientSignature);
        if (serverContent != clientContent || serverSignature != clientSignature)
        {
            sts = EchLicSts_LicenseContentMismatch;
        }
    }
    return sts;
}

    /* Save a previously created licenes to a license file */
EchLicSts EchelonLicenseBase::SaveLicense(IN const TCHAR *destinationPath)
{
    return m_licenseDoc.Save(destinationPath);
}

#if defined(INCLUDE_ECHELON_LICENSE_SERVER)

    /* Digitally sign the license */
EchLicSts EchelonLicenseBase::SignLicense(const TCHAR *userName, const TCHAR *password,
                                          EchLicNode &root, EchLicNode &content)
{
    EchLic5BitEncodedString signature;
    DocumentType documentType;
    EchLicSts sts = GetDocumentType(documentType);
    if (sts == EchLicSts_Good && documentType != DocType_License)
    {
        sts = EchLicSts_InvalidDocumentType;
    }
    if (sts == EchLicSts_Good)
    {
        sts = GenerateSignature(userName, password, root, content, signature);
        if (sts == EchLicSts_Good)
        {
            sts = root.AddProperty(NodeNameSignature, signature);
        }
    }
    return sts;
}
#endif

    /* Create expiration date */
EchLicSts EchelonLicenseBase::SetExpirationDate(unsigned int day, unsigned int month, unsigned int year)
{
    EchLicSts sts;
    EchLicNode root;
    EchLicNode content;

    sts = GetLicenseContent(root, content);
    if (sts == EchLicSts_Good)
    {
        if (day < 1 || day > 31 ||
            month < 1 || month > 12 ||
            year < 1970 || year > 2038)
        {
            // 32 bit time_t expired in 2038.  
            sts = EchLicSts_InvalidExpiration;
        }
        else
        {
            TCHAR expirationString[100];
            _stprintf_s(expirationString, sizeof(expirationString)/sizeof(TCHAR), 
                        TEXT("%.4d-%.2d-%.2d"), year, month, day);

            sts = content.AddProperty(NodeNameExpirationData, expirationString);
        }
    }
    return(sts);
}

    /* Create an expiration in approximately <numberOfDaysFromNow> days.  Note that
     * this function is generous and rounds up.  This way if the server's PC is a day
     * behind the clients, we won't short change the customer.
     */
EchLicSts EchelonLicenseBase::SetExpirationDateInDays(unsigned int numberOfDaysFromNow)
{
    struct tm expirationDate;
    time_t now;
    
    // First get a time stamp for now.
    time ( &now );

    // The license will expire at midnight on the computed expiration date.  But numberOfDaysFromNow
    // means full days, so we should not count today - so increment numberOfDays to exclude today.
    numberOfDaysFromNow++;

    // But wait, the expiration occurs at midnight using the CLIENTS local time.  Since we don't know
    // the timezone of the client, be generous and add an extra day, in case they are a day ahead of
    // the license server.
    numberOfDaysFromNow++;

    // date, and we may be ahead of the client, be generous and add an extra day to the
    // calculation.
    now += numberOfDaysFromNow*24*60*60;

    localtime_s(&expirationDate, &now);

    return SetExpirationDate(expirationDate.tm_mday, expirationDate.tm_mon+1, expirationDate.tm_year + 1900);
}

//============================================================================
//                             Manual License Methods
//
//  These methods are used by both the license wizard and license server to 
//  create manual licenses.
//============================================================================

EchLicSts EchelonLicenseBase::DecodeMachineLock(IN const TCHAR *machineLockCode, 
                                                int &version, 
                                                BYTE &flags /* use ManualLicCode_* */)
{
    EchLicSts sts = EchLicSts_BadMachineLockCode;
    BYTE *pData;
    int numBytes;
    EchLic5BitEncodedString lockCode(machineLockCode);
    if (lockCode.Decode(pData, numBytes))
    {
       // The manual license code has 3 nibbles appended to it.
        // The first two form the version number (0)
        version = pData[0];

        if (version == ManualLockCode_Version)
        {
            // The only version currently supported...

            // The last nibble contains the ManualLicCode_* flags
            flags = (pData[1] >> 4);

            sts = EchLicSts_Good;
        }
    }
    return sts;
}

    /* Set signature in manual license */
EchLicSts EchelonLicenseBase::SetManualLicenseSignature(IN const TCHAR *signature)
{
    EchLicNode root;
    EchLicNode content;
    EchLicSts sts;
    EchLic5BitEncodedString signatureString(signature);
    if (signatureString.Normalize())
    {
        sts = GetLicenseContent(root, content);
        if (sts == EchLicSts_Good)
        {
            sts = root.AddProperty(NodeNameSignature, signatureString);
            if (sts == EchLicSts_Good)
            {
                sts = ValidateSignature(root, content);
            }
        }
    }
    else
    {
        sts = EchLicSts_LicenseSignatureInvalid;
    }
    return sts;
}

/*============================================================================
 *----------------------------------------------------------------------------
 *                 ****  PRIVATE DEFINITION AND METHODS ****
 *----------------------------------------------------------------------------
 *==========================================================================*/

//============================================================================
//                               General Utilities
//============================================================================

    /* Create a license or request framework. */
EchLicSts EchelonLicenseBase::CreateLicenseFramework(IN EchLicDoc &doc,
                                                     IN LicenseType licenseType,
                                                     IN unsigned int schemaVersion, 
                                                     IN const TCHAR *documentType,
                                                     IN const TCHAR *hostName,
                                                     IN const TCHAR *productEdition,
                                                     IN const TCHAR *licenseId,  // May be NULL for request documents
                                                     IN unsigned int licenseRevision,
                                                     OUT EchLicNode &root,
                                                     OUT EchLicNode &content,
                                                     OUT EchLicNode &machineLock)
{
    const LicenseDescription *pDesc;
    EchLicSts sts;
    
    m_licenseType = licenseType;
    sts = GetLicenseDescription(pDesc);
    
    if (sts == EchLicSts_Good)
    {
        sts = doc.Open();
        if (sts == EchLicSts_Good)
        {
            sts = doc.CreateRoot(NodeNameRoot, root);

            if (sts == EchLicSts_Good)
            {
                sts = root.AddOnlyChild(NodeNameContent, content);
                
                if (sts == EchLicSts_Good)
                {
                    sts = content.AddProperty(NodeNameDocumentType, documentType);
                    if (sts == EchLicSts_Good)
                    {
                        sts = content.AddProperty(NodeNameLicenseType, pDesc->licenseType);
                    }
                    if (sts == EchLicSts_Good)
                    {
                        sts = content.AddProperty(NodeNameSchemaVersion, schemaVersion);
                    }
                    if (sts == EchLicSts_Good)
                    {
                        sts = content.AddProperty(NodeNameHostName, hostName);
                    }
                    if (sts == EchLicSts_Good)
                    {
                        sts = content.AddProperty(NodeNameProductEdition, productEdition);
                    }
                    if (sts == EchLicSts_Good && licenseId != NULL)
                    {
                        // License ID is optional for request documents
                        sts = content.AddProperty(NodeNameLicenseId, licenseId);
                    }
                    if (sts == EchLicSts_Good)
                    {
                        sts = content.AddProperty(NodeNameLicenseRevision, licenseRevision);
                    }

                    if (sts == EchLicSts_Good)
                    {
                        // A place holder...
                        sts = content.AddProperty(NodeNameUserNotes, TEXT("")); 
                    }

                    if (sts == EchLicSts_Good)
                    {
                        sts = content.AddOnlyChild(NodeNameMachineLock, machineLock);
                    }
                }
            }
        }
    }
    return sts;
}

    /* Get the license root and content nodes. */
EchLicSts EchelonLicenseBase::GetContent(IN EchLicDoc &doc,
                                         OUT EchLicNode &root,
                                         OUT EchLicNode &content)
{
    EchLicSts sts = doc.MustBeLoaded();
    if (sts == EchLicSts_Good)
    {
        sts = doc.GetRoot(NodeNameRoot, root);
        if (sts == EchLicSts_Good)
        {
            sts = root.FindNode(NodeNameContent, content);
        }
    }
    return sts;
}

/**************************************************************************
 *                     License definition access methods
 *************************************************************************/

EchLicSts EchelonLicenseBase::GetLicenseDescription(LicenseType licenseType, 
                                                    const LicenseDescription * &pDesc)
{
    EchLicSts sts = EchLicSts_Good;
    if (licenseType < NumberOfLicenseTypes)
    {
        pDesc = &licenseDescriptions[licenseType];
    }
    else
    {
        sts = EchLicSts_UnexpectedLicenseType;
    }
    return sts;
}

EchLicSts EchelonLicenseBase::GetLicenseDescription(const LicenseDescription * &pDesc)
{
    return GetLicenseDescription(m_licenseType, pDesc);
}

EchLicSts EchelonLicenseBase::ReadLicenseType(IN EchLicNode &content)
{
    EchLicString licenseTypeName;
    EchLicSts sts = content.GetProperty(NodeNameLicenseType, licenseTypeName);
    if (sts == EchLicSts_Good)
    {
        int i;
        LicenseType licenseType = NumberOfLicenseTypes;
        for (i = 0; i < (int)NumberOfLicenseTypes && licenseType == NumberOfLicenseTypes; i++)
        {
            if (_tcscmp(licenseTypeName, licenseDescriptions[i].licenseType) == 0)
            {
                licenseType = (LicenseType)i;
            }
        }

        if (licenseType == NumberOfLicenseTypes)
        {   // Don't recognize this license type.
            sts = EchLicSts_UnexpectedLicenseType;
        }
        else if (m_licenseType != licenseType &&
                 m_licenseType != NumberOfLicenseTypes)
        {   // License type has already been set, but does not match the expected type
            sts = EchLicSts_UnexpectedLicenseType;
        }
        else
        {
            m_licenseType = licenseType;
            sts = EchLicSts_Good;
        }
    }
    return sts;
}

//============================================================================
//                               Signatures
//============================================================================
const BYTE publicKey[] =
{
    0x06, 0x02, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00, 0x52, 0x53, 0x41, 0x31, 0x80, 0x01, 0x00, 0x00, 
    0x01, 0x00, 0x01, 0x00, 0xfd, 0xe0, 0xed, 0x07, 0x59, 0x1c, 0x22, 0x2f, 0x5f, 0xee, 0xd0, 0xf7, 
    0x93, 0xf8, 0xee, 0xbf, 0xac, 0xe7, 0xe2, 0x2e, 0x88, 0x2b, 0x1b, 0x1b, 0x31, 0x4c, 0x62, 0xb7, 
    0x25, 0xa6, 0xa2, 0x75, 0x7e, 0xe1, 0xd0, 0xb6, 0x4f, 0x89, 0x5f, 0x26, 0xf3, 0xd7, 0x18, 0xd4, 
    0x13, 0x39, 0x5a, 0xca
};

    /* Validate the license signature. */
EchLicSts EchelonLicenseBase::ValidateSignature(EchLicNode &rootNode, EchLicNode &content)
{
    EchLicSts sts = EchLicSts_Good;
    bool signatureValid = false;
    HCRYPTPROV hCryptProv;        // handle for the cryptographic provider context

    if (OpenCryptoApi(hCryptProv))
    {
        HCRYPTKEY hKey;               // public/private key handle
        if (CryptImportKey(hCryptProv, publicKey, sizeof(publicKey), NULL, 0, &hKey))
        {
            HCRYPTHASH hHash;
            sts = GetHash(content, hCryptProv, hHash);
            if (sts == EchLicSts_Good)
            {
                EchLic5BitEncodedString signature;
                sts = rootNode.GetProperty(NodeNameSignature, signature);
                if (sts == EchLicSts_Good)
                {
                    int numBytes;
                    BYTE *pSig;
                    if (signature.Decode(pSig, numBytes))
                    {
                        if (CryptVerifySignature(hHash, pSig, numBytes, hKey, NULL, 0))
                        {
                            signatureValid = true;
                        }
                    }
                }
                CryptDestroyHash(hHash);
            }
        }
        CloseCryptoApi(hCryptProv);
    }
    if (!signatureValid)
    {
        sts = EchLicSts_LicenseSignatureInvalid;
    }
    return sts;
}

        /* Create a hash of the license contents */
EchLicSts EchelonLicenseBase::GetHash(EchLicNode &content, HCRYPTPROV hCryptProv, HCRYPTHASH &hHash)
{
    EchLicSts sts = EchLicSts_LicenseSignatureInvalid;

    EchLicString xmlString;
    sts = content.GetXmlString(xmlString);

    if (sts == EchLicSts_Good)
    {
        /* The default character type of the licensing software may be either unicode or multi-byte, 
         * but the signature is always generated based on hash of the unicode version.
         */
        const WCHAR *xmlWString = xmlString.GetW();
        WCHAR *sanitizedXmlString = new WCHAR[wcslen(xmlWString)+1];
        if (sanitizedXmlString == NULL)
        {
            sts = EchLicSts_OutOfMemory;
        }
        else
        {
            DWORD len = 0;
            for (len = 0; *xmlWString; xmlWString++)
            {
                // Remove non-printable charactors
                if (*xmlWString > 0x20)
                {
                    sanitizedXmlString[len++] = *xmlWString;
                }
            }
            sanitizedXmlString[len] = (WCHAR)0;
            if (!CryptCreateHash(hCryptProv, CALG_MD5, 0, 0, &hHash) ||
                !CryptHashData(hHash, (BYTE *)sanitizedXmlString, sizeof(WCHAR)*len, CRYPT_USERDATA))
            {
                sts = EchLicSts_FailedToGetDataHash;
            }
            delete[] sanitizedXmlString;
        }
    }
    return sts;
}

#if defined(INCLUDE_ECHELON_LICENSE_SERVER)

    /* Digitally sign the license */
EchLicSts EchelonLicenseBase::GenerateSignature(const TCHAR *userName, const TCHAR *password,
                                                EchLicNode &root, EchLicNode &content, 
                                                EchLic5BitEncodedString &signature)
{
    bool signedLicense = false;
    EchLicSts sts = EchLicSts_Good;
    BYTE privateKey[ECHELON_LICENSE_PRIVATE_KEY_SIZE];
    if (EchLicGetPrivateKey(userName, password, privateKey, sizeof(privateKey)))
    {
        HCRYPTPROV hCryptProv;        // handle for the cryptographic provider context
        if (OpenCryptoApi(hCryptProv))
        {
            HCRYPTKEY hKey;  
            if (CryptImportKey(hCryptProv, privateKey, sizeof(privateKey), NULL, 0, &hKey))
            {
                HCRYPTHASH hHash;
                sts = GetHash(content, hCryptProv, hHash);
                if (sts == S_OK)
                {
                    DWORD sigLen;
                    if (CryptSignHash(hHash, AT_SIGNATURE, NULL, 0, NULL, &sigLen))
                    {
                        BYTE *pSig = (BYTE *)malloc(sigLen);
                        if (CryptSignHash(hHash, AT_SIGNATURE, NULL, 0, pSig, &sigLen))
                        {
                            signedLicense = true;
                            signature.Encode(pSig, sigLen);
                        }
                        free(pSig);
                    }
                    CryptDestroyHash(hHash);
                }
            }
            CloseCryptoApi(hCryptProv);
        }
    }
    else
    {
        sts = EchLicSts_KeyNotInstalled;
    }

    if (sts == EchLicSts_Good && !signedLicense)
    {
        sts = EchLicSts_FailedToSignLicense;
    }
    return sts;
}
#endif

//============================================================================
//                             Server Validation Utilities
//============================================================================

/* Utility to load a license from an xml string, and return the xml string
 * representing the content node and the license signature.
 */
EchLicSts EchelonLicenseBase::GetServerValidationInfo(const TCHAR *licenseXmlString,
                                                      EchLicString &contentXmlString,
                                                      EchLic5BitEncodedString &signature)
{
    EchLicString errString;
    EchLicSts sts;

    sts = LoadLicenseFromXmlString(licenseXmlString, errString);
    if (sts == EchLicSts_Good)
    {
        EchLicNode root;
        EchLicNode content;
        sts = GetLicenseContent(root, content);
        if (sts == EchLicSts_Good)
        {
            sts = content.GetXmlString(contentXmlString);
            if (sts == EchLicSts_Good)
            {
                sts = GetLicenseSignature(signature);
            }
        }
    }
    return sts;
}

    /* Open the crypto API */
bool EchelonLicenseBase::OpenCryptoApi(HCRYPTPROV &handle)
{
    handle = 0;
    if (CryptAcquireContext(&handle,               // handle to the CSP
                            NULL,                  // container name 
                            NULL,                  // use the default provider
                            PROV_RSA_FULL,         // provider type
                            CRYPT_VERIFYCONTEXT))  // flag values
    {
        return true;
    }
    return false;
}

    /* Close the crypto API */
void EchelonLicenseBase::CloseCryptoApi(HCRYPTPROV &handle)
{
    if (handle != 0)
    {
        CryptReleaseContext(handle, 0);
        handle = 0;
    }
}

//============================================================================
//                            Hardware locking
//============================================================================

/**************************************************************************
 *                             Wbem Utilities                              
 *************************************************************************/

    /* Get the proper hardDiskLockTypesSerialNumber for this OS. */
const EchelonLicenseBase::EchLicWbemProperties &EchelonLicenseBase::GetHardDiskLockTypesSerialNumber(void)
{
    OSVERSIONINFO osvi;

    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    GetVersionEx(&osvi);

    if (osvi.dwMajorVersion >= 6)
    {
        // Vista (6.0) or later.
        return hardDiskLockTypesSerialNumber;
    }
    else
    {
        return hardDiskLockTypesSerialNumberOld;
    }
}

    /* Initialize the Wbem service */
EchLicSts EchelonLicenseBase::InitWbem(IWbemLocator *&pLoc, IWbemServices *&pSvc)
{
    HRESULT sts = S_OK;
    pLoc = NULL;
    pSvc = NULL;

    if (SUCCEEDED(sts))
    {
        // Obtain the initial locator to WMI -------------------------

        sts = CoCreateInstance(
            CLSID_WbemLocator,             
            0, 
            CLSCTX_INPROC_SERVER, 
            IID_IWbemLocator, (LPVOID *) &pLoc);

        if (SUCCEEDED(sts))
        {
            // Connect to WMI through the IWbemLocator::ConnectServer method
	
            // Connect to the root\cimv2 namespace with
            // the current user and obtain pointer pSvc
            // to make IWbemServices calls.
            sts = pLoc->ConnectServer(
                 _bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
                 NULL,                    // User name. NULL = current user
                 NULL,                    // User password. NULL = current
                 0,                       // Locale. NULL indicates current
                 NULL,                    // Security flags.
                 0,                       // Authority (e.g. Kerberos)
                 0,                       // Context object 
                 &pSvc                    // pointer to IWbemServices proxy
                 );
        }

        if (SUCCEEDED(sts))
        {
            // Set security levels on the proxy -------------------------

            sts = CoSetProxyBlanket(
               pSvc,                        // Indicates the proxy to set
               RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
               RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
               NULL,                        // Server principal name 
               RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
               RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
               NULL,                        // client identity
               EOAC_NONE                    // proxy capabilities 
            );
        }
    }

    if (!SUCCEEDED(sts))
    {
        CloseWbem(pLoc, pSvc);
    }
    return SUCCEEDED(sts) ? EchLicSts_Good : EchLicSts_CannotAccessHardwareProperties;
}

    /* Close the Wbem service */
void EchelonLicenseBase::CloseWbem(IWbemLocator *&pLoc, IWbemServices *&pSvc)
{
    if (pSvc)
    {
        pSvc->Release();
        pSvc = NULL;
    }
    if (pLoc)
    {
        pLoc->Release();
        pLoc = NULL;
    }
}

/* Create machine lock information using the EchLicWbemProperties table and
 * information gathered from the PC.  Used by license wizard to create a 
 * license request.
 */
EchLicSts EchelonLicenseBase::CreateFromWbem(IWbemLocator *pLoc, IWbemServices *pSvc,
                                       EchLicNode &machineLock, 
                                       const EchLicWbemProperties &wbem)
{
    EchLicSts sts = EchLicSts_Good;
    IEnumWbemClassObject* pEnumerator = NULL;
    HRESULT hr = pSvc->ExecQuery(bstr_t("WQL"), 
                                 bstr_t(wbem.queryString),
                                 WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
                                 NULL,
                                 &pEnumerator);

    if (SUCCEEDED(hr))
    {
        IWbemClassObject *pclsObj;
        ULONG uReturn = 0;
        if (pEnumerator)
        {
            hr = pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &uReturn);
        }

        if (0 != uReturn)
        {
            EchLicNode node;

            if (SUCCEEDED(hr))
            {
                int i;
                for(i = 0; wbem.properties[i].propertyName; i++)
                {
                    VARIANT vtProp;

                    VariantInit(&vtProp);

                    CIMTYPE cimType;
                    // Get the value of the Name property
                    hr = pclsObj->Get(_bstr_t(wbem.properties[i].propertyName), 0, &vtProp, &cimType, 0);
                    if (SUCCEEDED(hr) && 
                        (V_VT(&vtProp) == VT_BSTR || V_VT(&vtProp) == VT_I4))
                    {        
                        if (node.isEmpty())
                        {
                            sts = machineLock.FindNode(wbem.nodeName, node);
                            if (sts == EchLicSts_NodeNotFound)
                            {
                                sts = machineLock.AddChild(wbem.nodeName, node);
                            }
                        }
                        if (sts == EchLicSts_Good)
                        {
                            if (V_VT(&vtProp) == VT_BSTR)
                            { 
                                sts = node.AddProperty(wbem.properties[i].propertyName, vtProp.bstrVal);
                            }              
                            else if (V_VT(&vtProp) == VT_I4)
                            {
                                sts = node.AddProperty(wbem.properties[i].propertyName, V_I4(&vtProp));
                            }
                        }
                        VariantClear(&vtProp);
                    }
                }

                pclsObj->Release();
            }
        }
    }

    // Cleanup
    // ========
    
    if (pEnumerator)
    {
        pEnumerator->Release();
    }
    return sts;
}

/* Validate machine lock information using the EchLicWbemProperties table and
 * information gathered from the PC against the machine lock information found
 * in the machineLock node.  Used by the license client to validate the
 * license.
 */
EchLicSts EchelonLicenseBase::ValidateWbem(IN IWbemLocator *pLoc, IN IWbemServices *pSvc,
                                           IN EchLicNode &machineLock, 
                                           IN const EchLicWbemProperties &wbem,
                                           OUT EchLicString &errorExplanation)
{
    EchLicNode node;
    EchLicSts sts = machineLock.FindNode(wbem.nodeName, node);

    if (sts == EchLicSts_NodeNotFound)
    {
        // No lock, OK
        sts = EchLicSts_Good;
    }
    else if (sts == EchLicSts_Good)
    {
        bool matches;
        HRESULT result;
        matches = true;
        IEnumWbemClassObject* pEnumerator = NULL;
        result = pSvc->ExecQuery(
            bstr_t("WQL"), 
            bstr_t(wbem.queryString),
            WBEM_FLAG_RETURN_IMMEDIATELY, 
            NULL,
            &pEnumerator);
    
        if (SUCCEEDED(result))
        {
            sts = ValidateItemFromWbem(wbem, pLoc, pSvc, pEnumerator, node, matches, errorExplanation);
        }
        if (sts == EchLicSts_Good && !matches)
        {
            sts = wbem.error;
        }

        pEnumerator->Release();
    }

    // Cleanup
    // ========
    
    return (sts);
}

/* Validate a single item (such as a CPU or HardDisk) found within 
 * machine lock collection against the actual hardware.
 */
EchLicSts EchelonLicenseBase::ValidateItemFromWbem(IN const EchLicWbemProperties &wbem,
                                                   IN IWbemLocator *pLoc, IN IWbemServices *pSvc,
                                                   IN IEnumWbemClassObject* pEnumerator,
                                                   IN EchLicNode &node,                                                 
                                                   OUT bool &matches,
                                                   OUT EchLicString &errorExplanation)
{
    EchLicSts sts = EchLicSts_Good;
    HRESULT result = S_OK;
    matches = false;

    result = pEnumerator->Reset();

    if (SUCCEEDED(result))
    {
        IWbemClassObject *pclsObj;
        ULONG uReturn = 0;
        
        while (pEnumerator && !matches)
        {
            HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &uReturn);

            if (0 == uReturn)
            {
                break;
            }

            if (SUCCEEDED(hr))
            {
                matches = true;
                EchLicNode propertyNode;
                for(sts = node.GetFirstChild(propertyNode); 
                    sts == EchLicSts_Good && matches;)
                {
                    EchLicString propertyName;
                    bool validateProperty = false;

                    /* If anything goes wrong, its not a match... */
                    matches = false;

                    sts = propertyNode.GetName(propertyName);
                    if (sts == EchLicSts_Good)
                    {
                        int i;

                        // See if  we need to validate this property right now.
                        // For example, the hard disk lock comes from two separate 
                        // wbem's - need to validate only if we are currently processing the
                        // right one.
                        for (i = 0; !validateProperty && wbem.properties[i].propertyName; i++)
                        {
                            if (_tcscmp(wbem.properties[i].propertyName, propertyName) == 0)
                            {
                                validateProperty = true;
                            }
                        }
                        if (!validateProperty)
                        {
                            // This property doesn't come from this lock - so its not a mis-match after all.
                            matches = true;  
                        }
                    }

                    if (sts == EchLicSts_Good && validateProperty)
                    {
                        VARIANT vtProp;
                    
                        VariantInit(&vtProp);
                        hr = pclsObj->Get(bstr_t(propertyName), 0, &vtProp, NULL, 0);
                        if (SUCCEEDED(hr))
                        {        
                            if (V_VT(&vtProp) == VT_BSTR)
                            { 
                                EchLicString expectedValue;
                                sts = propertyNode.GetProperty(expectedValue);
                                if (sts == EchLicSts_Good)
                                {
                                    EchLicString encodedValue;   
                                    /* In order to make a valid comparison, encode the value
                                     * which also strips certain attributes, and then decode the value
                                     */
                                    encodedValue.Set(bstr_t(vtProp),true);
                                    EchLicString actualValue;
                                    actualValue.Decode(encodedValue.GetW());
                                    if (expectedValue == actualValue)
                                    {
                                        matches = true;
                                    }
                                    else
                                    {
                                        FormatMisMatch(wbem, propertyName, expectedValue, actualValue, 
                                                        errorExplanation);
                                    }
                                }
                            }              
                            else if (V_VT(&vtProp) == VT_I4)
                            {
                                unsigned int expectedValue;
                                sts = propertyNode.GetProperty(expectedValue);
                                if (sts == EchLicSts_Good)
                                {
                                    unsigned int actualValue = V_I4(&vtProp);
                                    if (expectedValue == actualValue)
                                    {
                                        matches = true;
                                    }
                                    else
                                    {
                                        #define MAX_NUMERIC_BUF 20
                                        TCHAR szActual[MAX_NUMERIC_BUF];
                                        TCHAR szExpected[MAX_NUMERIC_BUF];
                                        // Create strings for expected and actual
                                        _stprintf_s(szActual, MAX_NUMERIC_BUF, TEXT("%u"), actualValue);
                                        _stprintf_s(szExpected, MAX_NUMERIC_BUF, TEXT("%u"), expectedValue);
                                        FormatMisMatch(wbem, propertyName, szExpected, szActual,
                                                       errorExplanation);
                                    }
                                }
                            }
                            else
                            {
                                errorExplanation.Set(TEXT("Unexpected data type for machine lock property "), false);
                                CatWbemPropertyName(wbem, propertyName, errorExplanation);
                            }
                        }
                        else
                        {
                            errorExplanation.Set(TEXT("Cannot get required machine lock property "), false);
                            CatWbemPropertyName(wbem, propertyName, errorExplanation);
                            errorExplanation.Cat(TEXT(" from system"));
                        }
                        VariantClear(&vtProp);
                    }

                    if (sts == S_OK && matches)
                    {
                        errorExplanation.Set(NULL);
                        sts = propertyNode.GetNext();
                        if (sts == EchLicSts_NodeNotFound)
                        {
                            sts = EchLicSts_Good;
                            break;
                        }
                    }
                }
                pclsObj->Release();
            }
        }
    }
    return sts;
}

void EchelonLicenseBase::CatWbemPropertyName(IN const EchLicWbemProperties &wbem, 
                                             IN const TCHAR *propertyName,
                                             IN OUT EchLicString &fullName)
{
    fullName.Cat(wbem.nodeName);
    fullName.Cat(TEXT("."));
    fullName.Cat(propertyName);
}

void EchelonLicenseBase::FormatMisMatch(IN const TCHAR *categoryName, 
                                        IN const TCHAR *propertyName, 
                                        IN const TCHAR *expectedValue,
                                        IN const TCHAR *actualValue,
                                        OUT EchLicString &errorExplanation)
{
    errorExplanation.Set(TEXT("Machine lock mis-match for property "), false);
    errorExplanation.Cat(categoryName);
    errorExplanation.Cat(TEXT("."));
    errorExplanation.Cat(propertyName);
    errorExplanation.Cat(TEXT(". Expected '"));
    errorExplanation.Cat(expectedValue);
    errorExplanation.Cat(TEXT("', got '"));
    errorExplanation.Cat(actualValue);
    errorExplanation.Cat(TEXT("'."));
}

void EchelonLicenseBase::FormatMisMatch(IN const EchLicWbemProperties &wbem, 
                                        IN const TCHAR *propertyName,
                                        IN const TCHAR *expectedValue,
                                        IN const TCHAR *actualValue,
                                        OUT EchLicString &errorExplanation)
{
    FormatMisMatch(wbem.nodeName, propertyName, expectedValue, actualValue, errorExplanation);
}

/**************************************************************************
 *                                VM Utilities                              
 *************************************************************************/

    /* Add the VM type to the machine lock. */
EchLicSts EchelonLicenseBase::CreateVmType(EchLicNode &machineLock)
{
    EchLicString vmType;
    EchLicSts sts = GetVmType(vmType);
    if (sts == EchLicSts_Good)
    {
        EchLicNode platform;
        sts = machineLock.AddChild(NodeNamePlatform, platform);
        if (sts == EchLicSts_Good)
        {
            sts = platform.AddProperty(NodeNameVmType, vmType);
        }
    }
    return sts;
}

    /* Validate the VM Type in machine lock. */
EchLicSts EchelonLicenseBase::ValidateVmType(IN EchLicNode &machineLock,
                                             OUT EchLicString &errorExplanation)
{
    EchLicSts sts;
    EchLicString expectedVmType;
    EchLicNode platform;

    sts = machineLock.FindNode(NodeNamePlatform, platform);
    if (sts == EchLicSts_NodeNotFound)
    {
        // No platform - that's OK.
        sts = EchLicSts_Good;
    }
    else if (sts == EchLicSts_Good)
    {
        sts = platform.GetProperty(NodeNameVmType, expectedVmType);
        if (sts == EchLicSts_NodeNotFound)
        {
            // No VM Type - that's OK.
            sts = EchLicSts_Good;
        }
        else if (sts == EchLicSts_Good)
        {
            EchLicString actualVmType;
            sts = GetVmType(actualVmType);
            if (sts == EchLicSts_Good)
            {
                if (actualVmType != expectedVmType)
                {
                    sts = EchLicSts_VmLockFailed;
                    FormatMisMatch(NodeNamePlatform, NodeNameVmType, expectedVmType, actualVmType, errorExplanation);
                }
            }
        }
    }
    return sts;
}

    /* Get the VM type of the PC, and return it as a VmTypeValue* string */
EchLicSts EchelonLicenseBase::GetVmType(EchLicString &vmType)
{
    EchLicSts sts = EchLicSts_Good;
    if (IsVMWare())
    {
        vmType.Set(VmTypeValueVmWare);
    }
    else if (IsVirtualPC())
    {
        vmType.Set(VmTypeValueVirtualPc);
    }
    else
    {
        vmType.Set(VmTypeValueNone);
    }
    return sts;
}

    /* Test to see if the PC is running under VmWare. */
bool EchelonLicenseBase::IsVMWare(void)
{
  unsigned long _EBX;
  __try 
  {
    __asm
    {
      // Run the magic code sequence
      push ebx
      mov eax, 0x564D5868
      mov ebx, 0x8685D465 // Ensure EBX doesn't contain 0x564D5868 :)
      mov ecx, 10 // The command for obtaining VMWare version information
      mov dx, 0x5658
      in eax, dx
      mov _EBX, ebx
      pop ebx
    };
  }
  __except(1)
  {
    // An exception occured, we ain't in VMWare
    return false;
  }
  // The code was executed successfuly, check for the magic value
  return _EBX == 0x564D5868;
}

    /* Test to see if the PC is running under Virtual PC */
bool EchelonLicenseBase::IsVirtualPC(void)
{
  __try 
  {
    __asm
    {
      // Execute the magic code sequence
      // Taken from Microsoft's vmcheck.dll
      mov eax, 1
      _emit 0x0F // db 0fh
      _emit 0x3F // aas
      _emit 0x07 // pop es
      _emit 0x0B // or eax, edi
      _emit 0xC7
      _emit 0x45 // inc ebp
      _emit 0xFC // cld
      _emit 0xFF // dd 0ffffffffh
      _emit 0xFF
      _emit 0xFF
      _emit 0xFF
    };
  }
  __except(1)
  {
    // An exception occured, we ain't in Virtual PC
    return false;
  }
  // We succeeded, we're Virtual PC emulated
  return true;
}

/**************************************************************************
 *                                Manual Lock                              
 *************************************************************************/

    /* Validate the manual lock contained in machineLock against the current
     * hardware. 
     */
EchLicSts EchelonLicenseBase::ValidateManualLockCode(IN EchLicNode &content, IN EchLicNode &machineLock,
                                                     OUT EchLicString &errorExplanation)
{
    EchLicSts sts;
    EchLicString expectedManualLockCode;

    sts = GetManualLockCode(machineLock, expectedManualLockCode);
    if (sts == EchLicSts_NodeNotFound)
    {
        // No manual lock code - that's OK.
        sts = EchLicSts_Good;
    }
    else if (sts == EchLicSts_Good)
    {
        int version;
        BYTE flags;
        sts = DecodeMachineLock(expectedManualLockCode, version, flags);
        if (sts == EchLicSts_Good)
        {
            EchLic5BitEncodedString actualManualLockCode;
            // Generate the actual machine lock code for this PC.
            // NOTE - in the future if we ever support multiple versions, we will need
            // to make sure we construct a compatible version, or know how to compare the
            // two versions.
            sts = GenerateManualMachineLockCode(actualManualLockCode);
            if (sts == EchLicSts_Good)
            {
                if (actualManualLockCode != expectedManualLockCode)
                {
                    sts = EchLicSts_ManualLockFailed;
                    FormatMisMatch(NodeNameShortFormat, NodeNameHashCode, expectedManualLockCode, 
                                   actualManualLockCode, 
                                   errorExplanation);
                }
            }
        }
    }
    return sts;
}

    /* Create a hash of the machine lock */
EchLicSts EchelonLicenseBase::CreateMachineLockHash(EchLicNode &machineLock, BYTE *&pHashData, DWORD &hashSize)
{
    EchLicSts sts = EchLicSts_FailedToGetDataHash;
    HCRYPTPROV hCryptProv;        // handle for the cryptographic provider context
    pHashData = NULL;
    if (OpenCryptoApi(hCryptProv))
    {
        HCRYPTHASH hHash;
        if (GetHash(machineLock, hCryptProv, hHash) == EchLicSts_Good)
        {
            hashSize = 0;
            DWORD hashSizeSize = sizeof(hashSize);
            if (CryptGetHashParam(hHash, HP_HASHSIZE, (BYTE *)&hashSize, &hashSizeSize, 0))
            {
                pHashData = new BYTE[hashSize];
                if (pHashData == NULL)
                {
                    sts = EchLicSts_OutOfMemory;
                }
                else if (CryptGetHashParam(hHash, HP_HASHVAL, pHashData, &hashSize, 0))
                {
                    sts = EchLicSts_Good;
                }
                else
                {
                    delete[] pHashData;
                    pHashData = NULL;
                }
            }
            CryptDestroyHash(hHash);
        }
        CloseCryptoApi(hCryptProv);
    }
    return sts;
}

    /* Get the manual lock code. */
EchLicSts EchelonLicenseBase::GetManualLockCode(EchLicNode &machineLock, OUT EchLicString &manualLockCode)
{
    EchLicNode shortFormat;
    EchLicSts sts = machineLock.FindNode(NodeNameShortFormat, shortFormat);
    if (sts == EchLicSts_Good)
    {
        sts = shortFormat.GetProperty(NodeNameHashCode, manualLockCode);
    }
    return sts;
}

        /* Add the manual lock code. */
EchLicSts EchelonLicenseBase::AddManualLockCode(EchLicNode &machineLock, IN const TCHAR *manualLockCode)
{
    EchLicNode shortFormat;
    EchLicSts sts = machineLock.AddOnlyChild(NodeNameShortFormat, shortFormat);
    if (sts == EchLicSts_Good)
    {
        sts = shortFormat.AddProperty(NodeNameHashCode, manualLockCode);
    }
    return sts;
}   

//============================================================================
//                               License Registry
//============================================================================
EchLicSts EchelonLicenseBase::ValidateLicenseRevision(IN EchLicNode &content,
                                                      IN bool install,
                                                      OUT EchLicString &errorExplanation)
{
    EchLicSts sts = EchLicSts_Good;

    EchLicString licenseId;
    sts = content.GetProperty(NodeNameLicenseId, licenseId);
    if (sts == EchLicSts_Good)
    {
        unsigned int licenseRevision;
        sts = content.GetProperty(NodeNameLicenseRevision, licenseRevision);
        if (sts == EchLicSts_Good)
        {
            EchLicReg reg;
            sts = reg.Open(licenseId);
            if (sts == EchLicSts_Good)
            {
                unsigned int registeredRevision;
                sts = reg.GetLicenseRevision(registeredRevision);
                if (sts == EchLicSts_Good)
                {
                    if (registeredRevision > licenseRevision)
                    {
                        sts = EchLicSts_BadRevision;
                    }
                    else if (registeredRevision < licenseRevision)
                    {
                        if (install)
                        {
                            sts = reg.SetLicenseRevision(licenseRevision);
                        }
                        else
                        {
                            sts = EchLicSts_BadRevision;
                        }
                    }
                    if (sts == EchLicSts_BadRevision)
                    {
                        #define MAX_ERROR_STRING_SIZE 100
                        TCHAR errorString[MAX_ERROR_STRING_SIZE];
                        // Create strings for expected and actual
                        _stprintf_s(errorString, MAX_ERROR_STRING_SIZE, 
                                    TEXT("Bad licenseRevision number, expected '%u' got '%u'."), 
                                    registeredRevision, licenseRevision);
                        errorExplanation.Set(errorString, false);
                    }
                }
                else if (sts == EchLicSts_RegNotFound)
                {
                    if (install)
                    {
                        sts = reg.SetLicenseRevision(licenseRevision);
                    }
                    else
                    {
                        sts = EchLicSts_BadRevision;
                        errorExplanation.Set(TEXT("Revision number not found"));
                    }
                }
            }
        }
    }
    return sts;
}

void EchelonLicenseBase::logLicenseAccess(const TCHAR *title, EchLicSts sts, const TCHAR *falureExplanation)
{
    DWORD traceOption = 1;
    HKEY hKey;
    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, RegNameLicenseInfoKey, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
    {
        DWORD dataItemLen = sizeof(traceOption);
        RegQueryValueEx(hKey, RegNameTraceOption, 0, NULL, (BYTE *)&traceOption, &dataItemLen);
        
        RegCloseKey(hKey);
    }
    if (traceOption >= 2 || (traceOption >= 1 && sts != EchLicSts_Good))
    {

        if (m_hEventLog == NULL)
        {
            m_hEventLog = OpenEventLog(NULL, TEXT("Echelon License Validation"));
        }
        if (m_hEventLog != NULL)
        {
            TCHAR buf[1000];
            const LicenseDescription *pDesc = NULL;
    
            GetLicenseDescription(m_licenseType, pDesc);

            _stprintf_s(buf, sizeof(buf)/sizeof(TCHAR), TEXT("\n\n***\nLicense Trace (%s): %s returned EchLicSts = %d. %s"), 
                pDesc ? pDesc->licenseName : TEXT("???"), title, sts, 
                falureExplanation==NULL ? TEXT(""): falureExplanation);

            const TCHAR *strings[1];
            strings[0] = buf;
            ReportEvent(m_hEventLog, sts == EchLicSts_Good ? EVENTLOG_INFORMATION_TYPE : EVENTLOG_ERROR_TYPE,
                0, 0, NULL, 1, 0, strings, NULL);
        }
    }
}

//============================================================================
//                               LicenseEnumerator
//============================================================================
EchLicEnumerator::EchLicEnumerator() 
{ 
    m_findHandle = NULL;
    Reset(); 
};

EchLicEnumerator::~EchLicEnumerator() 
{
    Reset();
};

    /* Reset enumeration from the begining. */
EchLicSts EchLicEnumerator::Reset(void)
{
    if (m_findHandle != NULL)
    {
        FindClose(m_findHandle);
        m_findHandle = NULL;
    }
    m_licenseType = (EchelonLicenseBase::LicenseType)0;
    return EchLicSts_Good;
}

EchLicSts EchLicEnumerator::Next(EchelonLicenseBase::LicenseType &licenseType, 
                                 HANDLE &findHandle,
                                 WIN32_FIND_DATA &findData)
{
    EchLicString path;
    EchLicSts sts = EchLicSts_Good;
    bool found = false;
    while (sts == EchLicSts_Good && !found)
    {
        if (findHandle == NULL)
        {
            EchLicString pathNameTemplate;

            sts = EchelonLicenseBase::GetLicenseInstallPath(licenseType, 
                                                            pathNameTemplate,
                                                            false);
            if (sts == EchLicSts_Good)
            {
                findHandle = FindFirstFile(pathNameTemplate, &findData);
                if (findHandle != INVALID_HANDLE_VALUE)
                {
                    found = true;
                }
            }
        }
        else
        {
            found = (FindNextFile(findHandle, &findData) == TRUE);
        }

        if (sts == EchLicSts_Good)
        {
            if (!found)
            {                 
                licenseType = (EchelonLicenseBase::LicenseType)((int)licenseType + 1);
                FindClose(findHandle);
                findHandle = NULL;
                if (licenseType >= EchelonLicenseBase::NumberOfLicenseTypes)
                {
                    // Done...
                    licenseType = (EchelonLicenseBase::LicenseType)0;
                    sts = EchLicSts_LicenseNotFound;
                }
            }
        }
    }
    return sts;
}

    /* Get the next installed license.  If no more licenses are installed, 
       returns EchLicSts_LicenseNotFound 
     */
EchLicSts EchLicEnumerator::Next(OUT EchLicString &pathName, OUT EchelonLicenseBase::LicenseType &licenseType)
{
    EchLicSts sts = EchLicSts_Good;
    sts = Next(m_licenseType, m_findHandle, m_findData);
    if (sts == EchLicSts_Good)
    {
        sts = EchelonLicenseBase::GetLicenseRootPath(pathName);
        if (sts == EchLicSts_Good)
        {
            pathName.Cat(m_findData.cFileName);
            licenseType = m_licenseType;
        }
    }
    return sts;
}

    /* Get the number of installed licenses */
EchLicSts EchLicEnumerator::GetCount(OUT int &numLicenses)
{
    EchelonLicenseBase::LicenseType licenseType = (EchelonLicenseBase::LicenseType)0;
    HANDLE findHandle = NULL;
    WIN32_FIND_DATA findData;
    numLicenses = 0;
    EchLicSts sts = EchLicSts_Good;
    while (sts == EchLicSts_Good)
    {
        sts = Next(licenseType, findHandle, findData);
        if (sts == EchLicSts_Good)
        {
            numLicenses++;
        }
        else if (sts == EchLicSts_LicenseNotFound)
        {
            sts = EchLicSts_Good;
            break;
        }
    }
    return sts;
}