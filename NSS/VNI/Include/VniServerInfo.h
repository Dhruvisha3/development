//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniServerInfo.h: interface for the VniServerControlBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VNISERVERCONTROLBASE_H__4A364AA2_12BC_11D3_80B2_00105A202B29__INCLUDED_)
#define AFX_VNISERVERCONTROLBASE_H__4A364AA2_12BC_11D3_80B2_00105A202B29__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "VniDefs.h"
#include "RmoControl.h"

class VNIBASE_DECL VniServerInfo 
{
public:
	VniServerInfo();
	virtual ~VniServerInfo();

protected:
    VniSts lock(void);
    VniSts unlock(void);
	BOOLEAN isStartupPending();
	void startupPending();
	void clearServerInfo();

	RmoClientId  getServerClientId();
	void setServerClientId(RmoClientId clientId);
    DWORD        getServerProcessId();
	void		 setServerProcessId(DWORD processId);
    VniSts       getFatalError();
	void		 setFatalError(VniSts sts);
    const RmoObjectId&  getServerObjectControlId();
    void		 setServerObjectControlId(const RmoObjectId& objectId);
	BOOLEAN		 isServerProcessAlive();


private:
	int			m_lockCount;
    static void init(void *p, int size);
	SharedMemoryHandle    m_VniServerInfoHandle;
    struct VniServerDirectory *m_pVniServerInfo;
	struct VniServerDirectory *getDir();
};

#endif // !defined(AFX_VNISERVERCONTROLBASE_H__4A364AA2_12BC_11D3_80B2_00105A202B29__INCLUDED_)
