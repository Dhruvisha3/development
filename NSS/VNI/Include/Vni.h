//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// Vni.h main interface include for VNI clients. 
//
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VNI_H__DA69AF4C_14E2_11D3_80B2_00105A202B29__INCLUDED_)
#define AFX_VNI_H__DA69AF4C_14E2_11D3_80B2_00105A202B29__INCLUDED_
#include "VniInterfaces.h"
#include "VniMiscLt.h"
#include "VniServerControl.h"
#include "VniStack.h"
#include "VniMsg.h"
#include "VniMonitorSet.h"
#include "VniNvPoint.h"
#include "VniMsgPoint.h"
#endif


