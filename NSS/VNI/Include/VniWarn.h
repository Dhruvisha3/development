//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniWarn.h: Some macros used to produce warnings, reminders, etc.
//
//
//////////////////////////////////////////////////////////////////////

#ifndef __HERE__
// macros to aid message displays, for example:
// #pragma message(__HERE__ "Remove sleep after debugging")
#define _QUOTE(x) # x
#define QUOTE(x) _QUOTE(x)
#define __FILE__LINE__ __FILE__ "(" QUOTE(__LINE__) ") : reminder: "
#define __HERE__ __FILE__LINE__

#endif

#ifndef __TBD__
#define TBD __FILE__ "(" QUOTE(__LINE__) ") : TBD: "
#define __HERE__ __FILE__LINE__
#endif
