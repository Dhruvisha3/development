//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 2001, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniProtocolAnalyzerControl.h:  Definitions used to open a protocol analyzer
//				   This object is designed to be "stand-alone", in that it requires 
//				   minimal VNI include files, and manages the VniServerControl
//				   itself.  
//////////////////////////////////////////////////////////////////////

#if !defined(VniProtocolAnalyzerControl__INCLUDED_)
#define VniProtocolAnalyzerControl__INCLUDED_
#include  <sys\timeb.h>

#ifdef VNICLIENT_EXPORTS
#define VNI_PA_CLIENT_DECL __declspec(dllexport) 
#else
#define VNI_PA_CLIENT_DECL __declspec(dllimport) 
#endif

#ifdef _MSC_VER
#pragma pack(push,8)
#endif

#define MAX_TIME_STAMPED_PACKET_DATA_LEN 2048
typedef struct _tagTimeStampedPkt {
	DWORD 	highResTimeStamp;
	_timeb 	currentTime;
	BYTE 	packetData[MAX_TIME_STAMPED_PACKET_DATA_LEN+10 /*allow for extra control data */];
} TimeStampedPkt;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

typedef enum
{
    VNIPA_VERSION_INITIAL,
    VNIPA_VERSION_BAD_PACKET_SUPPORT,
    VNIPA_VERSION_NI_EVENTS,

    // Add new versions above this line, and update current version
    VNIPA_VERSION_CURRENT = VNIPA_VERSION_NI_EVENTS
} VniPaVersion;

class VNI_PA_CLIENT_DECL VniProtocolAnalyzerControl
{
public:

    //*************************************************************************************
    // VNIPA_VERSION_INITIAL
    //*************************************************************************************

	VniProtocolAnalyzerControl();
	virtual ~VniProtocolAnalyzerControl();

    // This open method is obsolete.  Calling this method is identical to calling the
    // openEx method with useTimeStampedPkt set to FALSE.
    HRESULT open(const char *deviceName, int password = 0x0854abc93);
    HRESULT close();

	void releasePacket(BYTE *pData);

    // Implement this callback to recieve incomming packet data, excluding the 2 byte
    // layer 2 header.  Bad packets will not be recieved.
    // You must call open(FALSE, ....) or use the deprecated open method above 
    // to recieve packets with this callback.
    // Your method must call releasePacket to release the packet when you are done with it.
	virtual void packetArrived(DWORD timeStamp, int packetNumber, int length, BYTE *pData) 
	{
		releasePacket(pData);
	};

    void sendPacket(BYTE *pData, int len);


	//*************************************************************************************
    // VNIPA_VERSION_BAD_PACKET_SUPPORT
    // 
    // The interfaces below this line are new for LNS 3.20.60. They support an extended 
    // packet format (TimeStampedPkt) which includes both a time stamp and the full layer 2
    // SICB.  The first two bytes of the SICB the packet type and the packet length. When the
    // TimeStampedPkt format is used, bad packets will be reported, whereas they will be ommitted
    // if the old format is used (since the old format does not include the packet type information.
	//*************************************************************************************

    // This open method is obsolete.  Use openEx.
    HRESULT open(boolean useTimeStampedPkt, const char *deviceName, int password = 0x0854abc93);

	void releasePacket(TimeStampedPkt *pPacket);

    // Implement this callback to recieve incomming packets with the TimeStampedPkt format.
    // You must call openEx(TRUE, ....) to recieve packets with this callback.
    // Your method must call releasePacket to release the packet when you are done with it.
	virtual void packetArrivedEx(TimeStampedPkt *pPacket, int length, int packetNumber)
	{
		releasePacket(pPacket);
	}

    //*************************************************************************************
    // VNIPA_VERSION_NI_EVENTS
    //
    // The interfaces below this line are new for LNS 3.30. They support
    // 1) networkInterfaceEventArrived - Connection event callbacks.
    // 2) getXcvrId - retreieve the transcever ID
    // This version also introduces a new open method.  All other open methods are obsolete.
    // The version number is used to track which version of the interface the client was
    // compiled with.
	//*************************************************************************************
    
    // If useTimeStampedPkt is TRUE, the packetArrivedEx method will be called as new packets arrive.
    // If useTimeStampedPkt is FALSE, the old packetArrived method will be called as new packets arrive,
    // and bad packets will be discarded.   
    HRESULT openEx(const char *deviceName, boolean useTimeStampedPkt, 
                   VniPaVersion version = VNIPA_VERSION_CURRENT, 
                   int password = 0x0854abc93); 
    /**
     * This method is called when a network interface event occurs (Xdriver events).
     * See ldv32.h for valid event types.  Note that this value is relative to 
     * LDVX_APP (thus eventType = 0 indicates that LDV genterated an LDVX_WM_CLOSED event.
     *
     */
    virtual void networkInterfaceEventArrived(int eventType) {};

    // Get the transceiver ID of the current channel from the PA.
    HRESULT getXcvrId(int &xcvrId);

private:
	class VniServerControl	  *m_pControl;
	class VniProtocolAnalyzer *m_pProtocolAnalyzer;
};
// Returns temporary error message string.  String will be overwritten
// on each call, so client should copy it if needed longer term.
VNI_PA_CLIENT_DECL LPCSTR VniErrorMessage(HRESULT sts);

// These are some of the valid values that appear as the first byte of a packet when using the
// new interface packetArrivedEx.  This corresponds to the SICB command byte.
#define L2_PKT_TYPE_TIMEOUT				0x30
#define L2_PKT_TYPE_CRC					0x31
#define L2_PKT_TYPE_PACKET_TOO_LONG		0x32
#define L2_PKT_TYPE_PREAMBLE_TOO_LONG	0x33
#define L2_PKT_TYPE_PREAMBLE_TOO_SHORT	0x34
#define L2_PKT_TYPE_PACKET_TOO_SHORT	0x35
#define L2_PKT_TYPE_LOCAL_NM_RESP		0x16	// Response to local NM command
#define L2_PKT_TYPE_INCOMMING			0x1a	// traditional incoming L2 packets
#define L2_PKT_TYPE_MODE1_INCOMMING		0x1b	// mode 1 incoming L2 packets.  
#define L2_PKT_TYPE_FREQUENCY_REPORT	0x40	// Frequency report
#define L2_PKT_TYPE_RESET				0x50	// Reset

#undef VNI_PA_CLIENT_DECL
#endif
