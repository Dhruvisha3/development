//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 2000, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniIpAuthKey.h:  Definitions used to set IP authentication
//////////////////////////////////////////////////////////////////////

#if !defined(VniIpAuthKey__INCLUDED_)
#define VniIpAuthKey__INCLUDED_
#include "VniSts.h"
typedef unsigned char VniIpChannelSecretKey[16];

#ifdef VNICLIENT_EXPORTS
#define VniIpDecl __declspec(dllexport) 
#else
#define VniIpDecl __declspec(dllimport) 
#endif
VniSts VniIpDecl vniSetIpAuthKey(const char *ipDeviceName, const VniIpChannelSecretKey key);
VniSts VniIpDecl vniSetIpAthentication(const char *ipDeviceName, boolean enabled, const VniIpChannelSecretKey key);
VniSts VniIpDecl vniGetIpAuthentication(const char *ipDeviceName, boolean &enabled, VniIpChannelSecretKey &key);

#endif
