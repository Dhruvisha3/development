//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 2002, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniInterfaces.h Common definitions VNI clients interfaces. 
//
//
//////////////////////////////////////////////////////////////////////

#if !defined(_VniInterfaces__INCLUDED__)
#define _VniInterfaces__INCLUDED__
typedef unsigned char		boolean;

#include "RmoUtil.h"

//****************************************************************************
// All exposed VNI client objects are based on VniObjBase or VniSimpleObjBase.  
// All of them are empty interfaces, containin a poiter to the "real" object.
///
// VniObjBase is a reference counted object. If you wish to derive an object 
// using one of these as a base class, you may allocate your derived object, 
// and you must override the "releaseObj" function. This way the object will be 
// both allocated and deleted at the same level.  
// If you wish to use one of these objects directly, you cannot allocate it directly,
// rather you must call the appropriate "newObj" routine and the object will
// be allocated and released under the VniClient DLL.  Note that some objects
// can only be used as base classes, as they have pure virtual functions.  These
// classes have no "newObj" methods.
//     
//****************************************************************************

template <class TObjType> class VniSimpleObjBase 
{
public:
    VniSimpleObjBase<TObjType>() { m_pImplementor = NULL; }
    virtual ~VniSimpleObjBase<TObjType>() {};
    TObjType const *getImplementor() const { return m_pImplementor; }
    TObjType *getImplementor() { return m_pImplementor; }

protected:
    void setImplementor(TObjType *pImplementor) 
    { 
        m_pImplementor = pImplementor; 
    }

private:
    TObjType *m_pImplementor;
};


template <class TObjType> class VniObjBase : public RmoRefBaseClass
{
public:
    VniObjBase<TObjType>() { m_pImplementor = NULL; }
    virtual ~VniObjBase<TObjType>() {};
    TObjType const *getImplementor() const { return m_pImplementor; }
    TObjType *getImplementor() { return m_pImplementor; }

protected:
    void setImplementor(TObjType *pImplementor) 
    { 
        m_pImplementor = pImplementor; 
    }

private:
    TObjType *m_pImplementor;
};


#endif
