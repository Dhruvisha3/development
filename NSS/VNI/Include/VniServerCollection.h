//////////////////////////////////////////////////////////////////////
//
// $Header $
//
// � Copyright 2000, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniServerCollection.h interface for the VniServerCollection class.
//
// This (optional) class can be used to maintain a collection of 
// VniServerControl objects.  When used, it provides a single thread to
// process messages sent to all VniServerControl objects registered with it.
//
// To use this object:
//   Allocate an instance and add a reference to it.
//
//   For each VniServerControl object to be managed by this service, call
//      VniServerControl::registerWithServerCollection passing a pointer to this object.
//          This is done instead of calling "VniServerControl::createReaderThread".
//      Before closing the VniServerControl, call 
//      VniServerControl::deregisterWithServerCollection
//          This is done instead of calling "VniServerControl::waitForReaderThreadToQuit".
//
//   When done release the reference to it.
//  
// Note, the VniServerControl::registerWithServerCollection and VniServerControl::deregisterWithServerCollection
// methods must not be called from the VniServerCollection reader thread.
//
// Note that when a VniServerCollection is used, the throttle parameters come from it,
// and the parameters from the VniServerControl are ignored.
//
//////////////////////////////////////////////////////////////////////

#if !defined(VNISERVERCOLLECTION_H__INCLUDED_)
#define VNISERVERCOLLECTION_H__INCLUDED_
#include "vnidefs.h"
#include "vniInterfaces.h"

class VniServerControl;
class VniClientServerCollection;

template class VNICLIENT_DECL VniObjBase<VniClientServerCollection>;

class VNICLIENT_DECL VniServerCollection : public VniObjBase<VniClientServerCollection>
{
public:
    static VniServerCollection* newObj(int maxEvents = 0, Msec maxEventPeriod = 100);
    void setThrottle(int maxEvents, Msec maxEventPeriod);
    void getThrottle(int &maxEvents, Msec &maxEventPeriod);
    class VniServerControl *getFirstServerControl(void) const;


protected:
	VniServerCollection(int maxEvents = 0, Msec maxEventPeriod = 100);
	virtual ~VniServerCollection();
    virtual void releaseObj();
};

#endif
