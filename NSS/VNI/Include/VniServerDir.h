//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniServerDir.h:  
//////////////////////////////////////////////////////////////////////

#if !defined(VniServerDir__INCLUDED_)
#define VniServerDir__INCLUDED_


#define VNI_SERVER_DIRECTORY_NAME "ECHELON_VNI_SERVER_DIRECTORY"

#define VNI_SERVER_PROCESS_STARTUP_TIMEOUT 10000
#define VNI_SERVER_PROCESS_STARTUP_CHECK_INTERVAL 50
#define VNI_PROCESS_NAME "VniServer.exe"

#endif
