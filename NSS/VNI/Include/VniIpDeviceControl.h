//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 2000, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniIpDeviceControl.h:  Definitions used to manipulate an IP device to
//                 allow the ConfigServer to commission it, used by the
//                 LNS IP Configuration Utility.
//				   This object is designed to be "stand-alone", in requires 
//				   minimal VNI include files, and manages the VniServerControl
//				   itself.  
//////////////////////////////////////////////////////////////////////

#if !defined(VniIpDeviceControl__INCLUDED_)
#define VniIpDeviceControl__INCLUDED_
#include "VniSts.h"
#ifdef VNICLIENT_EXPORTS
#define VNI_IP_DEVICE_CLIENT_DECL __declspec(dllexport) 
#else
#define VNI_IP_DEVICE_CLIENT_DECL __declspec(dllimport) 
#endif

class VNI_IP_DEVICE_CLIENT_DECL VniIpDeviceControl
{
public:
	VniIpDeviceControl();
	virtual ~VniIpDeviceControl();

    VniSts startConfigServerCheck(const char *deviceName, ULONG csIpAddr = 0, WORD csIpPort = 0);
    VniSts stopConfigServerCheck();
	virtual void configServerTestPassed(ULONG csIpAddr, WORD csPort) {};
    VniSts dumpVniIpChannelXmlConfig(const char *ipDeviceName);

	VniSts startConfigServerCheckEx(const char *deviceName, ULONG csIpAddr = 0, WORD csIpPort = 0);
	virtual void configServerTestExStatus(VniSts sts, ULONG csIpAddr, WORD csPort) {};

private:
	class VniServerControl	*m_pControl;
	class VniIpDevice		*m_pIpDevice;
};

VniSts VNI_IP_DEVICE_CLIENT_DECL refreshSntp(void);

// Returns temporary error message string.  String will be overwritten
// on each call, so client should copy it if needed longer term.
VNI_IP_DEVICE_CLIENT_DECL LPCSTR VniErrorMessage(HRESULT sts);

#undef VNI_IP_DEVICE_CLIENT_DECL
#endif
