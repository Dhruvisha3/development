// NodeSimControl.h: Definition of the NodeSimControl class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NODESIMCONTROL_H__F0B860AE_C0E4_11D6_99DE_00A0CC32740E__INCLUDED_)
#define AFX_NODESIMCONTROL_H__F0B860AE_C0E4_11D6_99DE_00A0CC32740E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef NODESIMSUPPORT_EXPORTS
#define NODESIMSUPPORT_DECL __declspec(dllexport) 
#else
#define NODESIMSUPPORT_DECL __declspec(dllimport) 
#endif

/////////////////////////////////////////////////////////////////////////////
// NodeSimControl

class PlOlcSimControl;

class NODESIMSUPPORT_DECL NodeSimControl 
{
public:
	NodeSimControl(const char *caption = NULL);
    ~NodeSimControl();

public:
        // Start the node simulator.  
        // Parameters:
        //  startupScript   - a full pathname to a node simulator script, or NULL.  
        //  visible         - true if node simulator should be displayed.
        //  modal           - true if tbe start command should wait for the node 
        //                    simulator to terminate before returning.
        //
	void start(const char *startupScript, BOOL visible, BOOL modal, BOOL bIsLPG = false, LRESULT (*func)(void) = NULL);

        // Stop the node simulator
    void stop();

        // Send a command to the node simulator.  Commands are in the same
        // syntax as script lines.
	void sendCommand(const char *command);

        // Create a nodeSimStack.  Override to use a derived class
    virtual class NodeSimStack *newNodeSimStack(IN class NodeSimServerControl &serverControl);

        // Create a NodeSimMonitorSet. Override to use a derived class
    virtual class NodeSimMonitorSet *newNodeSimMonitorSet(IN class NodeSimStack &nodeSimStack);


//////////////////////////////////////////////////////////////////////////////////
//    Callbacks
//////////////////////////////////////////////////////////////////////////////////

        // Dialog is exiting.
    virtual void dialogComplete() {}

        // DisplayMessages - this gets called whenever the node simulator would like to
        // put up an error or informational message.  Since these messages are invisible
        // if the dialog is invisible, this callback can be used to route error messages.
        // Return TRUE if you wish NodeSim to display a message box, false otherwise. Note
        // that nodeSime never displays a message box if it is not visible.
    virtual boolean DisplayMessage(const char *description, const char *pTitle = NULL) 
        { return TRUE; }

private:
    class CMainDlg *m_pDlg;
};

#endif // !defined(AFX_NODESIMCONTROL_H__F0B860AE_C0E4_11D6_99DE_00A0CC32740E__INCLUDED_)
