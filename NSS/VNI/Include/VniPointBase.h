//////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999, Bob Walker, Echelon Corp.  All Rights Reserved.
//
// VniPointBase.h interface for the VniPointBase class, encapsulating
// common properties of the VniNvPoint and VniMsgPoint classes.
//
// Encapsulates common properties of the VniNvPoint and VniMsgPoint 
// classes.  
//
//////////////////////////////////////////////////////////////////////

#if !defined(VNIPOINTBASE__INCLUDED_)
#define VNIPOINTBASE__INCLUDED_
#include "vnidefs.h"
#include "vniInterfaces.h"
class VNICLIENT_DECL VniPointBase : public VniDescription
{

public:
    virtual LtMsIndex msIndex() const = 0;
    virtual LtMpIndex mpIndex() const = 0;
    virtual VniMonitorSet *getMonitorSet(void) const = 0;
    virtual VniMonitorEnableType getMonitorEnableType() const = 0;

    virtual VniSts setMsgOverride(VniMsgOverride &ltMsgOverride) = 0;
    virtual VniSts getMsgOverride(VniMsgOverride &ltMsgOverride) = 0;

};

#endif
