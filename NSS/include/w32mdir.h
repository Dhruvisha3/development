//
// $Header$
//

/***************************************************************************
 *  Filename:       w32mdir.h
 *  Author:         Bob Walker
 *  Copyright (c) 1995-1997 Echelon Corporation.  All Rights Reserved.
 *
 *  Description:
 *      This file contains definitions for the WIN32 specific code that
 *      manages the global registry of NSS-20 managers.
 *
 *  Modification History:
 *      Name      Date        Description
 *
 ***************************************************************************/
#ifndef _w32mdir_h
#define _w32mdir_h


#include "w32ipc.h"
#ifdef NSS_ENGINE
#include "msgmgmt.h"
#endif
#include "ni.h"
#include "ns_plat.h"
#include "ns_srsts.h"

// max number of  manager handles allocated by NSS (range 0..MAX_NSS20_MANAGERS-1)
#define MAX_NSS20_MANAGERS 5000

#define NO_NET_ADDR         0   // Value of subnet ID when no network address is defined.

typedef HANDLE NsGlobalMgrDirectoryHandle;

SrSts AccessManagerDirectory(const char *nssName, NsGlobalMgrDirectoryHandle *pDirHandle);
SrSts AllocateManagerHandle(NsGlobalMgrDirectoryHandle dirHandle, 
                            Byte managerTypeId, ManagerHandle managerHandle,
                            IPCAddr ipcAddr, SubnetNodeAddr *pNetAddr,
                            ManagerHandle packageManagerHandle);
SrSts FreeManagerHandle(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle);
SrSts FreeAllRemoteNetworkMgrEntries(NsGlobalMgrDirectoryHandle dirHandle);
SrSts GetManagerAddress(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle, IPCAddr *pIpcAddr, SubnetNodeAddr *pNetAddr);
SrSts GetManagerNetAddress(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle, SubnetNodeAddr *pNetAddr);
SrSts GetManagerIpcAddress(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle, IPCAddr *pIpcAddr);
SrSts GetPackageManagerHandle(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle,
                              ManagerHandle *pPackageManagerHandle);
// Note there may be more than one manager for this ipc...
SrSts GetManagerHandleForIpcAddress(NsGlobalMgrDirectoryHandle dirHandle, IPCAddr ipcAddr,
                                    ManagerHandle *pManagerHandle);

SrSts GetManagerTypeInfo(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle, Byte *pManagerType);

void GetNsApiOpened(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle,
                              Bool *pNsApiOpened);
void SetNsApiOpened(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle,
                              Bool nsApiOpened);

// Get the IPC address of the NEXT manager.  Initially *pManagerHandle should
// be 0.  On next iteration, pass previous manager handle
SrSts GetNextManagerIpcAddr(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle *pManagerHandle, IPCAddr *pIpcAddr);
SrSts GetNextManagerNetAddr(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle *pManagerHandle, SubnetNodeAddr *pNetAddr);
SrSts GetNextManagerAddr(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle *pManagerHandle, IPCAddr *pIpcAddr, SubnetNodeAddr *pNetAddr);


SrSts GetNextManagerHandle(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle *pManagerHandle);
SrSts GetNextManagerHandleForType(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle *pManagerHandle, Byte managerType);

SrSts GetManagerProcessId(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandle, DWORD *pProcessId);

// Get the IPC address for the specified NSI
SrSts GetNsiIpcAddress(NsGlobalMgrDirectoryHandle dirHandle, const char *nsiName, IPCAddr *pIpcAddr, Bool *pFatal);

SrSts UpdateGlobalManagerHandle(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle oldManagerHandle, ManagerHandle newManagerHandle);

// Indicate whether or not the system manager is ready
SrSts SetNsiReady(NsGlobalMgrDirectoryHandle dirHandle, ManagerHandle managerHandler, Bool ready);
SrSts SetEngineProcessId(NsGlobalMgrDirectoryHandle dirHandle, DWORD processId);
Bool IsEngineProcessAlive(NsGlobalMgrDirectoryHandle dirHandle);

// Indicate that there has been a fatal error that will prevent the NSI from
// comming up.
void SetNsiFatalError(NsGlobalMgrDirectoryHandle dirHandle, SrSts sts);

void CloseNssGlobalMgrDir(NsGlobalMgrDirectoryHandle dirHandle);

Bool ValidateProcess(DWORD processId);

BOOL WINAPI w32mdir_DllEntryPoint(HINSTANCE  hinstDLL, DWORD  fdwReason,
                                  LPVOID  lpvReserved);

#endif
