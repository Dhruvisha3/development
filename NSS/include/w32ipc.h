//
// $Header$
//

#ifndef _W32IPC_H
#define _W32IPC_H
/***************************************************************************
 *  Filename:       W32IPC.H
 *  Author:         Bob Walker
 *  Copyright (c) 1995-1997 Echelon Corporation.  All Rights Reserved.
 *
 *  Description:
 *      This file contains definitions for Win32 interprocessor communication
 *      used by the NSS-20.  To recieve a message a process must allocate an
 *      input port.  Messages from many processes may be recieved on this port.
 *      to send a message, a process must open an output port to the destination
 *      process.  One such port is opened by each sending process for each
 *      recieving process.  Prior to any communication, an IPC address must
 *      be obtained.  This is used as a method of identification.
 *
 *  Modification History:
 *      Name      Date        Description
 ***************************************************************************/

#include "w32.h"

#ifdef _MSC_VER
    // new version of STLport combined with this project's default packing
    // option (/Zp1) causes the following warning to be emitted
    #pragma warning(disable: 4103)   // used #pragma pack to change alignment
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <windows.h>

#ifdef _MSC_VER
/* Default packing alignment for IPC structures is single byte boundaries */
#pragma pack(push,_IPC_PACKING)
#endif

typedef UINT IPCAddr;
/* An IPC address is split into two parts, the "IPC space index" and "IPC index" within 
   that space.  Each space is managed independently.  Each space is defined by
   a base name - an NSS name for example.  IPC ports in one space cannot access 
   IPC ports in another space. 
   The IPC space is an index into the IPC SPACE directory, which contains the base name
   of the IPC directory. */

#define NUM_IPC_INDEX_BITS 16
#define IPC_INDEX_MASK 0xffff
#define IPC_SPACE_INDEX(ipcAddr) ((ipcAddr)>>NUM_IPC_INDEX_BITS)
#define IPC_INDEX(ipcAddr) ((ipcAddr) & IPC_INDEX_MASK)
#define MAKE_IPC_ADDR(spaceIndex, ipcIndex) \
    ((IPCAddr)(((spaceIndex) << NUM_IPC_INDEX_BITS) | (ipcIndex)) )

#define MAX_IPC_SPACE 256

#define NO_IPC_ADDR 0xFFFFFFFFU
#define IPC_WAIT_TIME 30*1000            // 30 seconds.
#define MAX_IPC_ADDRESSES 3000

typedef struct {
    WORD                msgSize;
    IPCAddr             sourceAddr;
    IPCMessagePriority  priority;
    // BYTE    data[0];   Variable length
} IPCHeader;
#define IPC_HEADER_LEN (sizeof(IPCHeader))

typedef enum {
    IPC_PROC_HOST_APPL,
    IPC_PROC_NSS_MANAGER,
    IPC_PROC_DEAD_APPL,
    IPC_PROC_RMO,

    IPC_PROC_NUM_TYPES,
    IPC_PROC_ANY_TYPE = IPC_PROC_NUM_TYPES
} IpcProcType;

#define MAX_IPC_BASE_NAME_SIZE   32


// Obtain an IPC address.  This is the first step before sending or receiveing
// messages.
ECH_IPC_DECL IpcSts RegisterIpcAddr(char *name, IPCAddr *pMyIPCAddr, IpcProcType processType);
// Free the IPC address
ECH_IPC_DECL void  DeRegisterIpcAddr(IPCAddr *pMyIPCAddr);


// Initialize my input port.  Any process that wishes to recieve a message must
// create an input port.  The handle to the port is returned via ppInputPort.
ECH_IPC_DECL IpcSts InitIPCInputPort(char *name, int bufferSize, IPCAddr myIPCAddr,
                       void **ppInputPort);
// Free the input port.  Will be set to NULL.
ECH_IPC_DECL void  FreeIPCInputPort(void **pInputPort);

// Read a message from the specified input port (usually only one per process).
// Returned message includes header.
ECH_IPC_DECL IpcSts ReadIPCMsg(void *pMsg, void *pInputPort, DWORD waitTime, BOOLEAN *pThrottleIt);
// Same as above, but allocate a message buffer, and only pass back data portion in the
// buffer.  The headerSize parameter specifies how much room the app wants to have reserved
// before the message start (to link msgs for example).
ECH_IPC_DECL IpcSts AllocAndReadIPCMsg(void *pInputPort, void **ppMsg, 
                                   WORD *pMsgSize, IPCAddr *pSourceAddr, 
                                   DWORD waitTime, int headerSize,
                                   BOOLEAN *pThrottleIt);

ECH_IPC_DECL void FreeIpcMsg(void **ppMsg);
// Returns true if a message is avail, false otherwise.  Also fills in the
// handle used to wait for messages.  This way a thread can check to see if
// any messages are available, and if not, wait on that handle as well as other
// handles.
ECH_IPC_DECL BOOL CheckForIPCMsgAvail(void *pInputPort, HANDLE *pHandle);


// Open an output port.  Pointer to port is returned.
ECH_IPC_DECL IpcSts OpenIpcSlot(char *name, IPCAddr ipcAddr, void **ppIPCPort);
// Free output port.
ECH_IPC_DECL void FreeIPCEntry(void **pIPCPort);
// Send a message using an open output port.
ECH_IPC_DECL IpcSts IPCSend(void *pIPCPort, void *pMsg, UINT size, IPCAddr myIPCAddr, 
                        IPCMessagePriority priority, DWORD waitTime);

ECH_IPC_DECL void IpcDeRegisterProcess(void);

ECH_IPC_DECL const char *IpcGetVersion(void);

/* Return IPC address for next IPC port of the specified type of application.
*/
ECH_IPC_DECL IpcSts IpcGetNextAddr(IPCAddr myIpcAddr, int *pFollowing, IpcProcType procType, IPCAddr *pIPCAddr);
/* Return IPC base name for next active IPC base.
*/
ECH_IPC_DECL IpcSts IpcGetNextBaseName(int *pFollowing, char *baseName);

/* Return TRUE if process associated with pIPCPort is alive. */
ECH_IPC_DECL BOOL IpcProcessIsAlive(void *pIPCPort);

/* Return TRUE if the IPC port and process are currently alive. */
ECH_IPC_DECL BOOL ValidIpcAddress(void *pIPCPort);


/* Set IPC process type to that specified. */
ECH_IPC_DECL void SetIpcProcessType(IPCAddr ipcAddr, IpcProcType procType);

ECH_IPC_DECL IpcSts GetIpcProcessIdAndType(IPCAddr ipcAddr, DWORD *pProcessId, IpcProcType *pProcessType);

ECH_IPC_DECL IpcSts SetIpcThresholds(void *pIPCPort, const IPCThresholds *pThresholds);
ECH_IPC_DECL IpcSts GetIpcThresholds(void *pIPCPort, IPCThresholds *pThresholds);
ECH_IPC_DECL IpcSts GetIpcStatistics(void *pIPCPort, IPCStatistics *pStats, unsigned long *pCapacity, unsigned long *pCurrentSize);
ECH_IPC_DECL IpcSts ClearIpcStatistics(void *pIPCPort);

#ifdef _MSC_VER
#pragma pack(pop)
#endif

#endif



