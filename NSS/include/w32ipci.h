//
// $Header$
//

#ifndef _W32IPCI_H
#define _W32IPCI_H
/***************************************************************************
 *  Filename:       W32IPCI.H
 *  Author:         Bob Walker
 *  Copyright (c) 1995-2000 Echelon Corporation.  All Rights Reserved.
 *
 *  Description:
 *      This file contains internal definitions for Win32 interprocessor communication
 *      used by the NSS-20.  
 *  Modification History:
 *      Name      Date        Description
 ***************************************************************************/

#include "w32ipc.h"

#ifdef _MSC_VER
/* Default packing alignment for IPC structures is single byte boundaries */
#pragma pack(push,_IPC_PACKING)
#endif

#define MAX_IPC_NAME_SIZE   500
// Buffer is empty if bufferIn == bufferOut.  Buffer is full if
// bufferIn+1 (mod size) == buffer out.
typedef struct IPCRingBuffer {
    BOOL          alive;
    IPCAddr       ipcAddr;
    UINT          maxRingBufferSize;
    UINT          bufferIn;           // Start index to write data
    UINT          bufferOut;          // Sart index to read data.
    BOOL          waitingForSpace;
    IPCStatistics ipcStats;
    IPCThresholds ipcThresholds;
    BYTE          data[0];    // Variable length
} IPCRingBuffer;

#define IPC_RNG_BFR_HEADER_LEN (offsetof(IPCRingBuffer, data))
#define IPC_DATA_AVAIL(p) \
    (!((((p)->bufferIn + 1) % (p)->maxRingBufferSize) == (p)->bufferOut))

typedef struct IPCPort {
    IPCRingBuffer   *pMsgInRingBuffer;
    HANDLE           hWriteMutex;       // Writes allowed
    HANDLE           hMsgInEvent;       // Msg read to read
    HANDLE           hSpaceAvailEvent;  // Space is available to write
    HANDLE           hMemoryHandle;
    HANDLE           hProcessHandle;
    DWORD            processId;
} IPCPort;

typedef struct IPCAddrEntry {
    BOOL        allocated;
    DWORD       processId;
    IPCPort     *pInputPort;
    IpcProcType processType;
} IPCAddrEntry;

typedef struct IPCSpaceDirectoryEntry
{
    char baseName[MAX_IPC_BASE_NAME_SIZE + 1];
} IPCSpaceDirectoryEntry;

#define IPC_SPACE_DIRECTORY_SIZE (sizeof(IPCSpaceDirectoryEntry)*MAX_IPC_SPACE)

typedef struct IPCDirectory
{
    IPCAddr         ipcAddrBase;
    int             useCount;
    IPCAddrEntry    ipcEntries[MAX_IPC_ADDRESSES];
} IPCDirectory;

// Directory information

#define IPC_ADDR_TABLE_SIZE (sizeof(IPCDirectory))

#ifdef _MSC_VER
#pragma pack(pop)
#endif

#endif
