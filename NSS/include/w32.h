//
// $Header$
//

#ifndef _W32_H
#define _W32_H
#define MEMORY_SPACE        (HANDLE)0xFFFFFFFF

#define NEARLY_INFINITE  1000*60*10 // 10 minutes
#define GET_MUTEX(mutex) GetNssMutex(mutex, __FILE__, __LINE__)
#define RELEASE_MUTEX(mutex) ReleaseNssMutex(mutex, __FILE__, __LINE__)

#define _IPC_PACKING        1
#include "ns_srsts.h"

typedef enum
{
    IPCSTS_GOOD             = SRSTS_GOOD,    
    IPCSTS_MSG_ERROR        = SRSTS_MSG_ERROR,
    IPCSTS_RESOURCE_PROBLEM = SRSTS_RESOURCE_PROBLEM,
    IPCSTS_OUT_OF_MEMORY    = SRSTS_OUT_OF_MEMORY,
    IPCSTS_OUT_OF_RANGE     = SRSTS_OUT_OF_RANGE,
    IPCSTS_CANT_FIND_OBJECT = SRSTS_CANT_FIND_OBJECT,
    IPCSTS_LOCK_FAILURE     = SRSTS_MSG_ERROR,
} IpcSts;

#define NUM_IPC_PRIORITIES 5
// Priority 0 is the highest priority, and is the only one that will wait
typedef struct IPCStatistics
{
    unsigned long messagesSent[NUM_IPC_PRIORITIES];
    unsigned long messagesRead[NUM_IPC_PRIORITIES];
    unsigned long messagesTossedBySender[NUM_IPC_PRIORITIES];
    unsigned long messagesTossedByReader[NUM_IPC_PRIORITIES];
    unsigned long bytesSent;
    unsigned long bytesRead;
    unsigned long maxBytesInQueue;
    unsigned long lastCleared;
} IPCStatistics;

typedef struct IPCThresholds
{
      // Toss message at a given prioity if buffer is > %full.  0 means not used.
    unsigned char tossAtPercentFull[NUM_IPC_PRIORITIES];
} IPCThresholds;

typedef unsigned char IPCMessagePriority;
#if defined(NSSIPC_LIB)
#define NS_IPC_DECL
#elif defined(NSSIPC_DLL)
#define NS_IPC_DECL __declspec(dllexport)
#else
#define NS_IPC_DECL __declspec(dllimport)
#endif

#if defined(ECHELONIPC_EXPORTS)
#define ECH_IPC_DECL __declspec(dllexport)
#else
#define ECH_IPC_DECL __declspec(dllimport)
#endif

#endif
