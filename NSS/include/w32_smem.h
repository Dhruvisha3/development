//
// $Header$
//

/***************************************************************
 *  Filename:     w32_smem.h
 *
 *  Description:  This file contains definitions for allocating and
 *                controlling access to shared memory by multiple processes.
 *
 *
 *  Copyright (c) 1995-1997 by Echelon Corporation.  All Rights Reserved.
 *
 *
 *  Modification History:
 *    Name      Date        Description
 *
 *    DWD       5-18-95     Added read/write support (multiple readers xor 1 writer).
 *
 ****************************************************************/

#ifndef _W32_SMEM_H
#define _W32_SMEM_H

#ifdef _MSC_VER
    // new version of STLport combined with this project's default packing
    // option (/Zp1) causes the following warning to be emitted
    #pragma warning(disable: 4103)   // used #pragma pack to change alignment
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <windows.h>
#include "w32.h"

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

#define WRITE_LOCK(x)    GetSharedMemWriteLock(x)
#define WRITE_UNLOCK(x)  ReleaseSharedMemWriteLock(x)
#define READ_LOCK(x)     GetSharedMemReadLock(x)
#define READ_UNLOCK(x)   ReleaseSharedMemReadLock(x)

/* The SharedMemoryHandle is opaque to the outside world. */
typedef struct SharedMemoryControlBlock* SharedMemoryHandle;

typedef void MemInitFunc(void *p, int size);

ECH_IPC_DECL IpcSts OpenSharedMemoryAccess(char *name, int fileSize, BOOL readWrite,
                                           SharedMemoryHandle *pHandle, void **ppData,
                                           MemInitFunc *pInit);

ECH_IPC_DECL void CloseSharedMemoryAccess(SharedMemoryHandle *pHandle);

ECH_IPC_DECL BOOL GetSharedMemReadLock(SharedMemoryHandle handle);
ECH_IPC_DECL BOOL ReleaseSharedMemReadLock(SharedMemoryHandle handle);
ECH_IPC_DECL BOOL GetSharedMemWriteLock(SharedMemoryHandle handle);
ECH_IPC_DECL BOOL ReleaseSharedMemWriteLock(SharedMemoryHandle handle);

ECH_IPC_DECL LPSECURITY_ATTRIBUTES GetNullSecurityAttributes(void);
ECH_IPC_DECL BOOL GetNssMutex(HANDLE mutex, const char *file, int line);
ECH_IPC_DECL void ReleaseNssMutex(HANDLE mutex, const char *file, int line);

void InitNullSecurityAttributes(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif

