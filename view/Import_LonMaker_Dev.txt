project: export_repo development_repo
Ldrf\Dev\Include\*.* Dev\Import\Ldrf\Include\*.*
Ldrf\Dev\Lib\*.* Dev\Import\Ldrf\Lib\*.*
Ldrf\Dev\Debug\*.* Dev\Import\Ldrf\Debug\*.*
Ldrf\Dev\Release\*.* Dev\Import\Ldrf\Release\*.*
Win32Util\V4.0x\Install\LonWorksXML3.msm Dev\Import\MergeModules\LonWorksXML3.msm
Win32Util\Dev\Install\Win32Util_Data.msm Dev\Import\MergeModules\Win32Util_Data.msm
LnsCtXML\v4.0x\Install\Echelon?OpenLNS?CT?XML?Utility.msi Dev\Import\MergeModules\Echelon?OpenLNS?CT?XML?Utility.msi
LNS\Dev\Install\Ip852ConfigServer401.msi Dev\Import\Installers\Ip852ConfigServer401.msi
Licensing\LicenseTools\dev\Install\LicenseWizard100.exe Dev\Import\Installers\LicenseWizard100.exe
Licensing\LicenseTools\Dev\Install\LicenseWizard100.msi Dev\Import\Installers\LicenseWizard100.msi
Licensing\LicenseFileAccess\Dev\EchelonLicenseBase\*.* Dev\Import\EchelonLicenseBase\*.*
LNS\Dev\Install\IzoTNetServer440.msi Dev\Import\Installers\IzoTNetServer440.msi
LNS\Dev\Install\IzoTNetServerSilent440.msi Dev\Import\Installers\IzoTNetServerSilent440.msi
LNS\Dev\Install\_SetupOpenLNS.dll Dev\Import\Installers\_SetupOpenLNS.dll
LNS\Dev\Install\msxml6.msi Dev\Import\Installers\msxml6.msi
LonMark?Resource?Files\Dev\Install\LonMarkResourceFiles1600.msi Dev\Import\Installers\LonMarkResourceFiles1600.msi
OpenLDV\Dev\Install\Embedded\OpenLDV510.msi Dev\Import\Installers\OpenLDV510.msi

project: component_Repo development_repo
Others\InstallShield\V12\Microsoft_VC90_CRT_x86.msm Dev\Import\MergeModules\Microsoft_VC90_CRT_x86.msm
Others\InstallShield\V12\Microsoft_VC90_MFC_x86.msm Dev\Import\MergeModules\Microsoft_VC90_MFC_x86.msm
Others\InstallShield\V12\policy_9_0_Microsoft_VC90_CRT_x86.msm Dev\Import\MergeModules\policy_9_0_Microsoft_VC90_CRT_x86.msm
Others\InstallShield\V12\policy_9_0_Microsoft_VC90_MFC_x86.msm Dev\Import\MergeModules\policy_9_0_Microsoft_VC90_MFC_x86.msm
Others\InstallShield\V12\Microsoft_VC90_DebugCRT_x86.msm Dev\Import\MergeModules\Microsoft_VC90_DebugCRT_x86.msm
Others\InstallShield\V12\Microsoft_VC90_DebugMFC_x86.msm Dev\Import\MergeModules\Microsoft_VC90_DebugMFC_x86.msm
Others\InstallShield\V12\policy_9_0_Microsoft_VC90_DebugMFC_x86.msm Dev\Import\MergeModules\policy_9_0_Microsoft_VC90_DebugMFC_x86.msm
Others\InstallShield\V12\policy_9_0_Microsoft_VC90_DebugCRT_x86.msm Dev\Import\MergeModules\policy_9_0_Microsoft_VC90_DebugCRT_x86.msm
Others\STLport\V5.2\bin\vc12\stlportd_vc9_echelon.5.2.dll Dev\Import\Bin\Debug\stlportd_vc9_echelon.5.2.dll
Others\STLport\V5.2\bin\vc12\stlport_vc12_echelon.5.2.dll Dev\Import\Bin\Release\stlport_vc12_echelon.5.2.dll
Echelon\Install\V2.0\bin\_SetupXsoMinXP.dll Dev\Import\Installers\_SetupXsoMinXP.dll
Others\Adobe\Reader\AdbeRdr920_en_US.exe Dev\Import\Installers\AdbeRdr920_en_US.exe
Others\InstallShield\V9\msxml3.msm Dev\Import\MergeModules\msxml3.msm