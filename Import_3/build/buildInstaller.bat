@Echo Off
Rem This batch file builds the LonScanner installer.  It can only be run
Rem in the build root folder.  For example: \LonScanner\Dev or LonScanner\V4.0x
Rem 
Rem The product files must have previously been built
Rem Notes: 
Rem 	1. The build machine must have the installShield command-line builder 
Rem 	on the path:  This will be like: 
Rem 	"C:\Program Files\InstallShield\Professional - Windows Installer Edition\System\
Rem 	The executable is IsCmdBld.exe
Rem	2. The environment variable, NBBASELINE must be set to Dev or V3.0x
Rem

Set _PATH=%PATH%
Set PATH=%PATH%;%ISDir12%
rem @echo PATH="%PATH%"

Set ProductVersion=%VerMajor%.%VerMinor%.%VerBuild%

if "%ECHO%" == "1" Echo On 
Echo Start the LonScanner installer build

ECHO - Remove Data directory
if Exist .\Install\Data rmdir /s /q .\Install\Data

mkdir .\Install\Data
mkdir .\Install\Data\Bin
mkdir .\Install\Data\LonScanner
mkdir ".\Install\Data\LonScanner\Example Custom Filter"
mkdir ".\Install\Data\LonScanner\Example Logs"
mkdir ".\Install\Data\LonMark Resource Files"
mkdir ".\Install\Data\OpenLDV510"
mkdir .\Install\Data\MergeModules
mkdir .\Install\Data\Win
mkdir ".\Install\Data\Win\LNS Licenses"
mkdir ".\Install\Data\MergeModules"

ECHO - Copy LonScanner folder
xcopy  /y /q ".\doc\license.txt"			".\Install\Data\LonScanner\."  /r   
xcopy  /y /q ".\mak\release\lonscanner.exe"		".\Install\Data\LonScanner\."  /r   
xcopy  /y /q ".\Lmpa\Help\lonscanner.chm" 		".\Install\Data\LonScanner\."  /r 
xcopy  /y /q ".\Lmpa\Help\LonScanner.pdf" 		".\Install\Data\LonScanner\."  /r 
rem xcopy  /y /q ".\mak\release\LspaLicXfr.exe"		".\Install\Data\LonScanner\."  /r   
xcopy  /y /q ".\Lmpa\Help\lspaxfer.chm" 		".\Install\Data\LonScanner\."  /r 
xcopy  /y /q ".\mak\release\NTGraph.ocx"		".\Install\Data\LonScanner\."  /r   
xcopy  /y /q ".\mak\release\OrderForm.txt"		".\Install\Data\LonScanner\."  /r   
xcopy  /y /q ".\doc\readme.htm"				".\Install\Data\LonScanner\."  /r   

Echo - Copy bin folder
rem xcopy  /y /q ".\import\lns\checklic130.exe"		".\Install\Data\Bin\."  /r   
rem xcopy  /y /q ".\import\lns\V3.2x\bin\ckx.dll"		".\Install\Data\Bin\."  /r   
rem xcopy  /y /q ".\import\crypkey\v5\Bin\CRP32DLL.dll"	".\Install\Data\Bin\."  /r   
rem xcopy  /y /q ".\import\lns\V3.2x\bin\LicXfer.ocx"		".\Install\Data\Bin\."  /r   

ECHO - Copy Example Custom Filter folder
xcopy  /y /q ".\LsCustomFilter\CmTime.h"		".\Install\Data\LonScanner\Example Custom Filter\."  /r   
xcopy  /y /q ".\LsCustomFilter\LsCustomFilter.cpp"	".\Install\Data\LonScanner\Example Custom Filter\."  /r   
xcopy  /y /q ".\LsCustomFilter\LsCustomFilter.h"	".\Install\Data\LonScanner\Example Custom Filter\."  /r   
xcopy  /y /q ".\LsCustomFilter\LsCustomFilter.sln"	".\Install\Data\LonScanner\Example Custom Filter\."  /r   
xcopy  /y /q ".\LsCustomFilter\LsCustomFilter.vcproj"	".\Install\Data\LonScanner\Example Custom Filter\."  /r   
xcopy  /y /q ".\LsCustomFilter\ParsedPacket.h"		".\Install\Data\LonScanner\Example Custom Filter\."  /r   
xcopy  /y /q ".\LsCustomFilter\StdAfx.cpp"		".\Install\Data\LonScanner\Example Custom Filter\."  /r   
xcopy  /y /q ".\LsCustomFilter\StdAfx.h"		".\Install\Data\LonScanner\Example Custom Filter\."  /r   

ECHO - Copy Example Logs folder
xcopy  /y /q ".\example_logs\Sample1.lsl"		".\Install\Data\LonScanner\Example Logs\."  /r   
xcopy  /y /q ".\example_logs\Sample2.lsl"		".\Install\Data\LonScanner\Example Logs\."  /r   
xcopy  /y /q ".\example_logs\Sample2.lsn"		".\Install\Data\LonScanner\Example Logs\."  /r   

ECHO - Copy LNS License folder
rem xcopy  /y /q ".\import\crypkey\v7\Bin\CKS.exe"		".\Install\Data\Win\LNS Licenses\."  /r   
rem xcopy  /y /q ".\import\crypkey\v7\Bin\crp32002.ngn"	".\Install\Data\Win\LNS Licenses\."  /r   
rem xcopy  /y /q ".\import\crypkey\v7\Bin\CRP32DLL.dll"	".\Install\Data\Win\LNS Licenses\."  /r   
rem xcopy  /y /q ".\import\crypkey\v7\Bin\SetupEx.exe"	".\Install\Data\Win\LNS Licenses\."  /r   
xcopy  /y /q ".\mak\release\lpa.exe"			".\Install\Data\Win\LNS Licenses\."  /r   
rem xcopy  /y /q ".\import\crypkey\v5\Bin\CRP9516E.dll"	".\Install\Data\Win\LNS Licenses\."  /r   
rem xcopy  /y /q ".\import\crypkey\v5\Bin\CRYP95E.dll"	".\Install\Data\Win\LNS Licenses\."  /r   

ECHO - Copy installer files
xcopy  /y /q ".\import\install\bin\*"		".\Install\Data\."  /r   
xcopy  /y /q ".\Lmpa\Res\lspa.ico"		".\Install\Data\."  /r   

ECHO - Copy Merge Modules
xcopy  /y /q ".\import\mergemodules\*"		".\Install\Data\MergeModules\."  /r    

ECHO - Copy LonMark Resource Files
xcopy  /y /q ".\import\LonMark Resource Files\*"		".\Install\Data\LonMark Resource Files\."  /r   
xcopy  /y /q ".\import\OpenLDV510\*"		".\Install\Data\OpenLDV510\."  /r   

Echo **** Setting timestamps for all files ****
setfdate * %VerMajor%:%VerMinor% .\Install\Data\Bin\*.* > NUL
setfdate * %VerMajor%:%VerMinor% .\Install\Data\LonScanner\*.* > NUL
setfdate * %VerMajor%:%VerMinor% .\Install\Data\NodeBuilder\*.* > NUL
setfdate * %VerMajor%:%VerMinor% .\Install\Data\LonScanner\Example Custom Filter\*.* > NUL
setfdate * %VerMajor%:%VerMinor% .\Install\Data\LonScanner\Example Logs\*.* > NUL
setfdate * %VerMajor%:%VerMinor% .\Install\Data\MergeModules\*.* > NUL
setfdate * %VerMajor%:%VerMinor% .\Install\Data\Win\*.* > NUL
setfdate * %VerMajor%:%VerMinor% .\Install\Data\Win\LNS Licenses\*.* > NUL

Echo - InstallShield command-line build
attrib -r ".\Install\LSPA Installer.ism"
Echo - Set the Product Version number in the InstallShield project
C:\Windows\SysWOW64\cscript //nologo .\build\UpdateIsProductVersionIs12.vbs ".\Install\LSPA Installer.ism" %ProductVersion%
attrib +r ".\Install\LSPA Installer.ism"
If ErrorLevel 1 Goto done_error
rem Delete any existing installer
if Exist ".\install\LSPA Installer\PROJECT_ASSISTANT" rmdir /q /s ".\install\LSPA Installer\PROJECT_ASSISTANT"

IsCmdBld.exe  -p ".\install\LSPA Installer.ism" -s -o .\Import\MergeModules -a "PROJECT_ASSISTANT" -r "CDROM_IMAGE" 
if errorlevel 1 goto errorExit

Rem Digitally sign the installer
Call Sign ".\Install\LSPA Installer\PROJECT_ASSISTANT\CDROM_IMAGE\DiskImages\DISK1\setup.exe"
REM if errorlevel 1 goto errorExit

IsCmdBld.exe  -p ".\install\LSPA Installer.ism" -s -o .\Import\MergeModules -a "PROJECT_ASSISTANT" -r "Web" 
if errorlevel 1 goto errorExit

Rem Digitally sign the installer
Call Sign ".\Install\LSPA Installer\PROJECT_ASSISTANT\CDROM_IMAGE\DiskImages\DISK1\setup.exe"
REM if errorlevel 1 goto errorExit

:successExit
ECHO Successfully built installation 
goto done

:errorExit
ECHO Error building installation 
SetErrorlevel 1
goto reallyDone

:done
rem Set path back to initial value
Set PATH=%_PATH%

:reallyDone
