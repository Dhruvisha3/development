' UpdateIsProjectVersion.vbs - A VBScript file to update an InstallShield Project
'                              number.  This allows a build script to update the
'                              version number with every build, ensuring that the
'                              Installer will overwrite the build before it.
'
Option Explicit

Dim argNum, argCount:argCount = Wscript.Arguments.Count
If (argCount < 2) Then
	Wscript.Echo "UpdateIsProjectVersion called without the required two arguments, project path" &_
		vbLf & " string and new version number string."
	Wscript.Quit 1
End If

Dim ProjectPath : ProjectPath = Wscript.Arguments.Item(0)
Dim VersionNum  : VersionNum  = Wscript.Arguments.Item(1)
	
Wscript.Echo ProjectPath
Wscript.Echo VersionNum

' Get the InstallShield 12 Standalone Windows Automation object
Dim m_ISWiProject : Set m_ISWiProject = Nothing
Set m_ISWiProject = CreateObject("IswiAuto20.ISWiProject") : CheckError

' Open the InstallShield project
m_ISWiProject.OpenProject ProjectPath

' Set the new version number
m_ISWiProject.ProductVersion = VersionNum

' Save the modified project
m_ISWiProject.SaveProject
m_ISWiProject.CloseProject

' Done...
Wscript.Quit 0


'
'  Subroutines
'
Sub CheckError
	Dim message, errRec
	If Err = 0 Then Exit Sub
	message = Err.Source & " " & Hex(Err) & ": " & Err.Description
	If Not m_ISWiProject Is Nothing Then
		Set errRec = m_ISWiProject.LastErrorRecord
		If Not errRec Is Nothing Then message = message & vbLf & errRec.FormatText
	End If
	Fail message
End Sub

Sub Fail(message)
	Wscript.Echo message
	Wscript.Quit 2
End Sub
