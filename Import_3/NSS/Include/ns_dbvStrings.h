//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ns_dbvStrings.h#8 $
//
#ifndef _NS_DBV_STRINGS_H
#define _NS_DBV_STRINGS_H
/***************************************************************
 *  Filename: ns_dbvStrings.h
 *
 *  Description:  This header file contains IDs for database validation.
 *                phase and step names.
 *
 *  Copyright (c) 2003 Echelon Corporation.  All Rights Reserved.
 *
 ****************************************************************/

/*===================================================================
			DB Validation phase names
 *====================================================================*/

#define DbvPhase_MAL_SCHEMA	0x01	// "Application Layer Schema"
#define DbvPhase_MSM_SCHEMA	0x02	// "System Schema"
#define DbvPhase_MPL_SCHEMA	0x03	// "Physical Schema"
#define DbvPhase_MDM_SCHEMA	0x04	// "Addressing Schema Schema"
#define DbvPhase_MCH_SCHEMA	0x05	// "Channel Schema"
#define DbvPhase_CONNECTIONS 0x06   // Connection diagnostics

#define DBV_CAT_RECORD 0x01
#define DBV_CAT_FIELD  0x02

#define DBV_STEP_ID(phase, cat, subphase)   ( (phase)<<24 | (cat) << 16 | (subphase) )
#define DBV_RECORD_ID(phase, subphase)      (DBV_STEP_ID(phase, DBV_CAT_RECORD, subphase))
#define DBV_FIELD_ID(phase, subphase)       (DBV_STEP_ID(phase, DBV_CAT_FIELD, subphase))
#define DBV_STEP_ID_STD(phase, subphase)    (DBV_STEP_ID(phase, 0, subphase))
/*===================================================================
			DB Validation sub-phase names
 *====================================================================*/
#define DbvStep_START          0   // "Start

///////////////////////////////
// DbvPhase_MAL_SCHEMA
///////////////////////////////
#define DbvStep_ALREC_MANAGER                      DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0000) // 0x01010000 
#define DbvStep_ALREC_PACKAGE_MANAGER              DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0001) // 0x01010001 
#define DbvStep_ALREC_MANAGER_ADDRESS              DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0002) // 0x01010002  
#define DbvStep_ALREC_MANAGER_LABEL                DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0003) // 0x01010003 
#define DbvStep_ALREC_SERVICE                      DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0004) // 0x01010004 
#define DbvStep_ALREC_PROPERTY                     DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0005) // 0x01010005 
#define DbvStep_ALREC_XINFO                        DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0006) // 0x01010006 
#define DbvStep_ALREC_CUSTOMER                     DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0007) // 0x01010007 - OBSOLETE
#define DbvStep_ALREC_BITMAP                       DBV_RECORD_ID(DbvPhase_MAL_SCHEMA, 0x0008) // 0x01010008

#define DbvStep_ALIDX_MANAGER                      DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0000) // 0x01020000
#define DbvStep_ALKEY_MANAGER_HANDLE               DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0001) // 0x01020001       
#define DbvStep_ALKEY_MANAGER_TYPE                 DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0002) // 0x01020002       
#define DbvStep_ALKEY_MANAGER_NAME                 DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0003) // 0x01020003       
#define DbvStep_ALLNK_PACKAGE_MANAGER              DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0004) // 0x01020004       
#define DbvStep_ALIDX_PACKAGE_MANAGER              DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0005) // 0x01020005       
#define DbvStep_ALKEY_PACKAGE_MANAGER_HANDLE       DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0006) // 0x01020006       
#define DbvStep_ALLNK_PACKAGE_ADDRESS              DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0007) // 0x01020007       
#define DbvStep_ALIDX_MANAGER_ADDRESS              DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0008) // 0x01020008       
#define DbvStep_ALKEY_MANAGER_NODE_HANDLE          DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0009) // 0x01020009       
#define DbvStep_ALIDX_MANAGER_LABEL_REC            DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x000a) // 0x0102000a       
#define DbvStep_ALKEY_MANAGER_LABEL                DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x000b) // 0x0102000b       
#define DbvStep_ALKEY_MANAGER_LABEL_SORTED         DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x000c) // 0x0102000c       
#define DbvStep_ALLNK_MANAGER_LABEL                DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x000d) // 0x0102000d       
#define DbvStep_ALIDX_SERVICE                      DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x000e) // 0x0102000e       
#define DbvStep_ALKEY_SERVICE_KEY                  DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x000f) // 0x0102000f       
#define DbvStep_ALLNK_SERVICE_MANAGER              DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0010) // 0x01020010       
#define DbvStep_ALIDX_PROPERTY                     DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0011) // 0x01020011       
#define DbvStep_ALKEY_PROPERTY_KEY                 DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0012) // 0x01020012       
#define DbvStep_ALLNK_PROPERTY_MANAGER             DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0013) // 0x01020013       
#define DbvStep_ALIDX_XINFO_REC                    DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0014) // 0x01020014       
#define DbvStep_ALKEY_XINFO_BY_TYPE                DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0015) // 0x01020015       
#define DbvStep_ALKEY_XINFO_BY_TYPE_AND_OWNER      DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0016) // 0x01020016       
#define DbvStep_ALKEY_XINFO_BY_OWNER               DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0017) // 0x01020017       
#define DbvStep_ALIDX_CUSTOMER_REC                 DBV_FIELD_ID(DbvPhase_MAL_SCHEMA, 0x0018) // 0x01020018  - OBSOLETE

///////////////////////////////
// DbvPhase_MSM_SCHEMA
///////////////////////////////
#define DbvStep_SMREC_PROPS                         DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0000) // 0x02010000
#define DbvStep_SMREC_NODE                          DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0001) // 0x02010001
#define DbvStep_SMREC_PROGRAM                       DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0002) // 0x02010002
#define DbvStep_SMREC_PROGNV                        DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0003) // 0x02010003
#define DbvStep_SMREC_CONN                          DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0004) // 0x02010004
#define DbvStep_SMREC_CONNMEMBER                    DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0005) // 0x02010005
#define DbvStep_SMREC_CONNPROP                      DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0006) // 0x02010006
#define DbvStep_SMREC_HOST                          DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0007) // 0x02010007
#define DbvStep_SMREC_LOCKOWNER                     DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0008) // 0x02010008
#define DbvStep_SMREC_LOCK                          DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0009) // 0x02010009
#define DbvStep_SMREC_LMOBJ                         DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x000a) // 0x0201000a
#define DbvStep_SMREC_CP                            DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x000b) // 0x0201000b
#define DbvStep_SMREC_CPRANGE                       DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x000c) // 0x0201000c
#define DbvStep_SMREC_CPVALUE                       DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x000d) // 0x0201000d
#define DbvStep_EVEREC_ESTEntry                     DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x000e) // 0x0201000e
#define DbvStep_EVEREC_Recipient                    DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x000f) // 0x0201000f
#define DbvStep_EVEREC_SndProps                     DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0010) // 0x02010010
#define DbvStep_SMREC_DB_RCVRY_CONTEXT              DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0011) // 0x02010011
#define DbvStep_SMREC_DB_RCVRY_SUBGRP_INFO          DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0012) // 0x02010012
#define DbvStep_SMREC_DB_RCVRY_CONNPROP_INFO        DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0013) // 0x02010013
#define DbvStep_SMREC_ALT_PROGNV                    DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0014) // 0x02010014
#define DbvStep_SMREC_XINFO                         DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0015) // 0x02010015
#define DbvStep_SMREC_FREE_RES                      DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0016) // 0x02010016
#define DbvStep_SMREC_MCS                           DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0017) // 0x02010017
#define DbvStep_SMREC_MCP                           DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0018) // 0x02010018
#define DbvStep_SMREC_SELECTOR_POOL_MAP             DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x0019) // 0x02010019
#define DbvStep_SMREC_NVDYNTYPE                     DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x001a) // 0x0201001a
#define DbvStep_SMREC_USER_DMT                      DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x001b) // 0x0201001b
#define DbvStep_SMREC_BITMAP                        DBV_RECORD_ID(DbvPhase_MSM_SCHEMA, 0x001c) // 0x0201001c

#define DbvStep_SMIDX_PROPS_REC                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0000) // 0x02020000
#define DbvStep_SMIDX_NODE_REC                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0001) // 0x02020001
#define DbvStep_SMKEY_NODE_HANDLE                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0002) // 0x02020002
#define DbvStep_SMLNK_NODE_PROGRAM                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0003) // 0x02020003
#define DbvStep_SMIDX_PROGRAM_REC                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0004) // 0x02020004
#define DbvStep_SMKEY_PROGRAM_ID                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0005) // 0x02020005
#define DbvStep_SMIDX_PROGNV_REC                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0006) // 0x02020006
#define DbvStep_SMKEY_PROGNV_ID                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0007) // 0x02020007
#define DbvStep_SMLNK_PROGNV_PROG                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0008) // 0x02020008
#define DbvStep_SMKEY_PROGNV_DYN_NV_CP_USER         DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0009) // 0x02020009
#define DbvStep_SMIDX_CONN_REC                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x000a) // 0x0202000a
#define DbvStep_SMKEY_CONN_HUB                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x000b) // 0x0202000b
#define DbvStep_SMKEY_CONN_NODE                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x000c) // 0x0202000c
#define DbvStep_SMKEY_CONN_SELECTOR                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x000d) // 0x0202000d
#define DbvStep_SMLNK_CONN_MEMBER0                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x000e) // 0x0202000e
#define DbvStep_SMLNK_CONN_DESC                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x000f) // 0x0202000f
#define DbvStep_SMLNK_CONN_ROOT                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0010) // 0x02020010
#define DbvStep_SMIDX_CONNMEMBER_REC                DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0011) // 0x02020011
#define DbvStep_SMKEY_CONNMEMBER_ID                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0012) // 0x02020012
#define DbvStep_SMLNK_CONNMEMBER_CONN               DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0013) // 0x02020013
#define DbvStep_SMIDX_CONNPROP_REC                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0014) // 0x02020014
#define DbvStep_SMKEY_CONNPROP_ID                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0015) // 0x02020015
#define DbvStep_SMIDX_HOST_REC                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0016) // 0x02020016
#define DbvStep_SMKEY_HOST_ID                       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0017) // 0x02020017
#define DbvStep_SMIDX_LOCKOWNER_REC                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0018) // 0x02020018
#define DbvStep_SMKEY_LOCKOWNER_HANDLE              DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0019) // 0x02020019
#define DbvStep_SMLNK_LOCKOWNER_LOCK                DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x001a) // 0x0202001a
#define DbvStep_SMIDX_LOCK_REC                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x001b) // 0x0202001b
#define DbvStep_SMKEY_LOCK_ID                       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x001c) // 0x0202001c
#define DbvStep_SMIDX_LMOBJ_REC                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x001d) // 0x0202001d
#define DbvStep_SMKEY_LMOBJ_ID                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x001e) // 0x0202001e
#define DbvStep_SMLNK_LMOBJ_PROG                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x001f) // 0x0202001f
#define DbvStep_SMKEY_LMOBJ_MEM_ID                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0020) // 0x02020020
#define DbvStep_SMIDX_CP_REC                        DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0021) // 0x02020021
#define DbvStep_SMKEY_CP_ID                         DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0022) // 0x02020022
#define DbvStep_SMFLD_CP_ORDINAL                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0023) // 0x02020023
#define DbvStep_SMLNK_CP_PROG                       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0024) // 0x02020024
#define DbvStep_SMLNK_CP_OWNER                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0025) // 0x02020025
#define DbvStep_SMIDX_CPRANGE_REC                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0026) // 0x02020026
#define DbvStep_SMKEY_CPRANGE_ID                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0027) // 0x02020027
#define DbvStep_SMLNK_CPRANGE_CP                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0028) // 0x02020028
#define DbvStep_SMIDX_CPVALUE_REC                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0029) // 0x02020029
#define DbvStep_SMKEY_CPVALUE_ID                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x002a) // 0x0202002a
#define DbvStep_SMLNK_CPVALUE_PROG                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x002b) // 0x0202002b
#define DbvStep_EVEIDX_ESTEntry_rec                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x002c) // 0x0202002c
#define DbvStep_EVEKEY_ESTEntry_key                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x002d) // 0x0202002d
#define DbvStep_EVEIDX_Recipient_idx                DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x002e) // 0x0202002e
#define DbvStep_EVELNK_Recipient_ESTEntry           DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x002f) // 0x0202002f
#define DbvStep_EVEIDX_SndProps                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0030) // 0x02020030
#define DbvStep_SMIDX_DB_RCVRY_REC                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0031) // 0x02020031
#define DbvStep_SMIDX_DBR_SUBGRP_INFO               DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0032) // 0x02020032
#define DbvStep_SMKEY_DBR_SUBGRP_SCAN_OWNERS        DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0033) // 0x02020033
#define DbvStep_SMKEY_DBR_SUBGRP_SCAN_SUBGRP        DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0034) // 0x02020034
#define DbvStep_SMKEY_DBR_SUBGRP_FIND_SUBGRP        DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0035) // 0x02020035
#define DbvStep_SMKEY_DBR_SUBGRP_FIND               DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0036) // 0x02020036
#define DbvStep_SMKEY_DBR_SUBGRP_BY_PRIMARY         DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0037) // 0x02020037
#define DbvStep_SMIDX_DBR_CONNPROP_INFO             DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0038) // 0x02020038
#define DbvStep_SMKEY_DBR_CONNPROP_PROPS            DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0039) // 0x02020039
#define DbvStep_SMKEY_DBR_CONNPROP_ADDR             DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x003a) // 0x0202003a
#define DbvStep_SMIDX_ALT_PROGNV_REC                DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x003b) // 0x0202003b
#define DbvStep_SMKEY_ALT_PROGNV_ID                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x003c) // 0x0202003c
#define DbvStep_SMLNK_ALT_PROGNV_NODE               DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x003d) // 0x0202003d
#define DbvStep_SMIDX_XINFO_REC                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x003e) // 0x0202003e
#define DbvStep_SMKEY_XINFO_BY_TYPE                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x003f) // 0x0202003f
#define DbvStep_SMKEY_XINFO_BY_TYPE_AND_OWNER       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0040) // 0x02020040
#define DbvStep_SMKEY_XINFO_BY_OWNER                DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0041) // 0x02020041
#define DbvStep_SMIDX_FREE_RES_REC                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0042) // 0x02020042
#define DbvStep_SMKEY_FREE_RES_ID                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0043) // 0x02020043
#define DbvStep_SMLNK_FREE_RES_NODE                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0044) // 0x02020044
#define DbvStep_SMIDX_MCS_REC                       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0045) // 0x02020045
#define DbvStep_SMKEY_MCS_ID                        DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0046) // 0x02020046
#define DbvStep_SMLNK_MCS_NODE                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0047) // 0x02020047
#define DbvStep_SMIDX_MCP_REC                       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0048) // 0x02020048
#define DbvStep_SMKEY_MCP_ID                        DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0049) // 0x02020049
#define DbvStep_SMLNK_MCP_NODE                      DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x004a) // 0x0202004a
#define DbvStep_SMKEY_MCP_MCS                       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x004b) // 0x0202004b
#define DbvStep_SMLNK_MCP_MCS                       DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x004c) // 0x0202004c
#define DbvStep_SMKEY_MCP_MONITOR_NVMT              DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x004d) // 0x0202004d
#define DbvStep_SMKEY_MCP_TARGET                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x004e) // 0x0202004e
#define DbvStep_SMIDX_SELECTOR_POOL_REC             DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x004f) // 0x0202004f
#define DbvStep_SMKEY_SELECTOR_POOL_HANDLE          DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0050) // 0x02020050
#define DbvStep_SMKEY_CONNMEMBER_NODE               DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0051) // 0x02020051
#define DbvStep_SMIDX_NVDYNTYPE                     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0052) // 0x02020052
#define DbvStep_SMLNK_NVDYNTYPE_ALT_PROG_NV         DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0053) // 0x02020053
#define DbvStep_SMIDX_USER_DMT_REC                  DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0054) // 0x02020054
#define DbvStep_SMKEY_USER_DMT_ID                   DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0055) // 0x02020055
#define DbvStep_SMLNK_USER_DMT_NODE                 DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0056) // 0x02020056
#define DbvStep_SMLNK_FREE_RES_LMOBJ                DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0057) // 0x02020057
#define DbvStep_SMKEY_ALT_NV_LMOBJ_MEM_ID           DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0058) // 0x02020058
#define DbvStep_SMKEY_ALT_NV_LMOBJ_SCAN_MEMBERS     DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x0059) // 0x02020059
#define DbvStep_SMLNK_LMOBJ_NODE                    DBV_FIELD_ID(DbvPhase_MSM_SCHEMA, 0x005a) // 0x0202005a

///////////////////////////////
// DbvPhase_MPL_SCHEMA
///////////////////////////////

#define DbvStep_PLREC_PROPS                         DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0000) // 0x03010000
#define DbvStep_PLREC_CHANNEL                       DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0001) // 0x03010001
#define DbvStep_PLREC_NODE                          DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0002) // 0x03010002
#define DbvStep_PLREC_SPIN                          DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0003) // 0x03010003
#define DbvStep_PLREC_ROUTER                        DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0004) // 0x03010004
#define DbvStep_PLREC_NODERTR                       DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0005) // 0x03010005
#define DbvStep_PLREC_CHANNEL_MANAGER               DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0006) // 0x03010006
#define DbvStep_PLREC_DB_RCVRY_CONTEXT              DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0007) // 0x03010007
#define DbvStep_PLREC_ROUTER_LABEL                  DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0008) // 0x03010008
#define DbvStep_PLREC_CHANNEL_LABEL                 DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x0009) // 0x03010009
#define DbvStep_PLREC_BITMAP                        DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x000a) // 0x0301000a
#define DbvStep_PLREC_XINFO                         DBV_RECORD_ID(DbvPhase_MPL_SCHEMA, 0x000b) // 0x0301000b

#define DbvStep_PLIDX_PROPS_REC                     DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0000)  // 0x03020000
#define DbvStep_PLIDX_CHANNEL_REC                   DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0001)  // 0x03020001
#define DbvStep_PLKEY_CHANNEL_HANDLE                DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0002)  // 0x03020002
#define DbvStep_PLKEY_CHANNEL_ID                    DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0003)  // 0x03020003
#define DbvStep_PLLNK_CHANNEL_CHANNEL               DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0004)  // 0x03020004
#define DbvStep_PLIDX_NODE_REC                      DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0005)  // 0x03020005
#define DbvStep_PLKEY_NODE_HANDLE                   DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0006)  // 0x03020006
#define DbvStep_PLKEY_NODE_NEURONID                 DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0007)  // 0x03020007
#define DbvStep_PLLNK_NODE_CHANNEL                  DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0008)  // 0x03020008
#define DbvStep_PLIDX_SPIN_REC                      DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0009)  // 0x03020009
#define DbvStep_PLFLD_SPIN_CLIENTHANDLE             DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x000a)  // 0x0302000a
#define DbvStep_PLIDX_ROUTER_REC                    DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x000b)  // 0x0302000b
#define DbvStep_PLKEY_ROUTER_HANDLE                 DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x000c)  // 0x0302000c
#define DbvStep_PLLNK_ROUTER_CHANNEL                DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x000d)  // 0x0302000d
#define DbvStep_PLIDX_NODERTR_REC                   DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x000e)  // 0x0302000e
#define DbvStep_PLKEY_NODERTR_HANDLE                DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x000f)  // 0x0302000f
#define DbvStep_PLIDX_CHANNEL_MANAGER_REC           DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0010)  // 0x03020010
#define DbvStep_PLKEY_CHANNEL_MANAGER_HANDLE        DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0011)  // 0x03020011
#define DbvStep_PLIDX_DB_RCVRY_REC                  DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0012)  // 0x03020012
#define DbvStep_PLIDX_CHANNEL_LABEL_REC             DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0013)  // 0x03020013
#define DbvStep_PLKEY_CHANNEL_LABEL                 DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0014)  // 0x03020014
#define DbvStep_PLKEY_CHANNEL_LABEL_SORTED          DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0015)  // 0x03020015
#define DbvStep_PLLNK_CHANNEL_LABEL                 DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0016)  // 0x03020016
#define DbvStep_PLIDX_ROUTER_LABEL_REC              DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0017)  // 0x03020017
#define DbvStep_PLKEY_ROUTER_LABEL                  DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0018)  // 0x03020018
#define DbvStep_PLKEY_ROUTER_LABEL_SORTED           DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x0019)  // 0x03020019
#define DbvStep_PLLNK_ROUTER_LABEL                  DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x001a)  // 0x0302001a
#define DbvStep_PLIDX_XINFO_REC                     DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x001b)  // 0x0302001b
#define DbvStep_PLKEY_XINFO_BY_OWNER                DBV_FIELD_ID(DbvPhase_MPL_SCHEMA, 0x001c)  // 0x0302001c

///////////////////////////////
// DbvPhase_MDM_SCHEMA
///////////////////////////////
#define DbvStep_DMREC_DOMAIN                        DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0000) // 0x04010000
#define DbvStep_DMREC_SUBNET                        DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0001) // 0x04010001
#define DbvStep_DMREC_NODE                          DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0002) // 0x04010002
#define DbvStep_DMREC_TGTD_NODE                     DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0003) // 0x04010003
#define DbvStep_DMREC_SYSTEM                        DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0004) // 0x04010004
#define DbvStep_DMREC_GROUP                         DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0005) // 0x04010005
#define DbvStep_DMREC_GROUP_XREF                    DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0006) // 0x04010006
#define DbvStep_DMREC_SUBGROUP                      DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0007) // 0x04010007
#define DbvStep_DMREC_CHGD_SUBGROUP                 DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0008) // 0x04010008
#define DbvStep_DMREC_SUBGROUP_MEMBER               DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0009) // 0x04010009
#define DbvStep_DMREC_CHANNEL                       DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x000a) // 0x0401000a
#define DbvStep_DMREC_LOGCHAN                       DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x000b) // 0x0401000b
#define DbvStep_DMREC_ROUTER                        DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x000c) // 0x0401000c
#define DbvStep_DMREC_SUBNET_LABEL                  DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x000d) // 0x0401000d
#define DbvStep_DMREC_ALT_ADDRESS                   DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x000e) // 0x0401000e
#define DbvStep_DMREC_DB_RCVRY_CONTEXT              DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x000f) // 0x0401000f
#define DbvStep_DMREC_DB_RCVRY_MT_ADDR              DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0010) // 0x04010010
#define DbvStep_DMREC_DB_RCVRY_SBGRP                DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0011) // 0x04010011
#define DbvStep_DMREC_DB_RCVRY_SBGRP_MBR            DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0012) // 0x04010012
#define DbvStep_DMREC_BITMAP                        DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0013) // 0x04010013
#define DbvStep_DMREC_XINFO                         DBV_RECORD_ID(DbvPhase_MDM_SCHEMA, 0x0014) // 0x04010014

#define DbvStep_DMIDX_PROPS_REC                     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0000) // 0x04020000
#define DbvStep_DMIDX_SUBNET_REC                    DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0001) // 0x04020001
#define DbvStep_DMKEY_SUBNET_ID                     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0002) // 0x04020002
#define DbvStep_DMLNK_SUBNET_LOGCHAN                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0003) // 0x04020003
#define DbvStep_DMIDX_NODE_REC                      DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0004) // 0x04020004
#define DbvStep_DMKEY_NODE_HANDLE                   DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0005) // 0x04020005
#define DbvStep_DMKEY_NODE_SNODE                    DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0006) // 0x04020006
#define DbvStep_DMLNK_NODE_SYSTEM                   DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0007) // 0x04020007
#define DbvStep_DMLNK_NODE_CHANNEL                  DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0008) // 0x04020008
#define DbvStep_DMIDX_TGTD_NODE_REC                 DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0009) // 0x04020009
#define DbvStep_DMKEY_TGTD_NODE_HANDLE              DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x000a) // 0x0402000a
#define DbvStep_DMIDX_SYSTEM_REC                    DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x000b) // 0x0402000b
#define DbvStep_DMKEY_SYSTEM_ID                     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x000c) // 0x0402000c
#define DbvStep_DMIDX_GROUP_REC                     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x000d) // 0x0402000d
#define DbvStep_DMKEY_GROUP_NUM                     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x000e) // 0x0402000e
#define DbvStep_DMIDX_GROUP_XREF_REC                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x000f) // 0x0402000f
#define DbvStep_DMKEY_GROUP_XREF_ID                 DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0010) // 0x04020010
#define DbvStep_DMKEY_GROUP_XREF_GROUP              DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0011) // 0x04020011
#define DbvStep_DMIDX_SUBGROUP_REC                  DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0012) // 0x04020012
#define DbvStep_DMKEY_SUBGROUP_ID                   DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0013) // 0x04020013
#define DbvStep_DMKEY_SUBGROUP_SOURCE_NODE          DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0014) // 0x04020014
#define DbvStep_DMKEY_SUBGROUP_SELECTOR             DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0015) // 0x04020015
#define DbvStep_DMKEY_SUBGROUP_DEST_COUNT           DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0016) // 0x04020016
#define DbvStep_DMLNK_SUBGROUP_GROUP                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0017) // 0x04020017
#define DbvStep_DMIDX_SUBGROUP_MEMBER_REC           DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0018) // 0x04020018
#define DbvStep_DMKEY_SUBGROUP_MEMBER_NODE          DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0019) // 0x04020019
#define DbvStep_DMKEY_SUBGROUP_MEMBER_SUBG          DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x001a) // 0x0402001a
#define DbvStep_DMKEY_SUBGROUP_MEMBER_SUBG_NODE     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x001b) // 0x0402001b
#define DbvStep_DMKEY_SUBGROUP_MEMBER_ID            DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x001c) // 0x0402001c
#define DbvStep_DMLNK_SUBGROUP_MEMBER_SUBGROUP      DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x001d) // 0x0402001d
#define DbvStep_DMIDX_CHGD_SUBGROUP_REC             DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x001e) // 0x0402001e
#define DbvStep_DMKEY_CHGD_SUBGROUP_ID              DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x001f) // 0x0402001f
#define DbvStep_DMIDX_CHANNEL_REC                   DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0020) // 0x04020020
#define DbvStep_DMKEY_CHANNEL_HANDLE                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0021) // 0x04020021
#define DbvStep_DMLNK_CHANNEL_CHANNEL               DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0022) // 0x04020022
#define DbvStep_DMLNK_CHANNEL_LOGCHAN               DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0023) // 0x04020023
#define DbvStep_DMIDX_LOGCHAN_REC                   DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0024) // 0x04020024
#define DbvStep_DMLNK_LOGCHAN_LOGCHAN               DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0025) // 0x04020025
#define DbvStep_DMIDX_ROUTER_REC                    DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0026) // 0x04020026
#define DbvStep_DMKEY_ROUTER_HANDLE                 DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0027) // 0x04020027
#define DbvStep_DMLNK_ROUTER_CHANNEL                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0028) // 0x04020028
#define DbvStep_DMLNK_ROUTER_LOGCHAN                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0029) // 0x04020029
#define DbvStep_DMIDX_DB_RCVRY_REC                  DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x002a) // 0x0402002a
#define DbvStep_DMIDX_DB_RCVRY_MT_ADDR              DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x002b) // 0x0402002b
#define DbvStep_DMKEY_DB_RCVRY_MT_ADDR_NVMT         DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x002c) // 0x0402002c
#define DbvStep_DMKEY_DB_RCVRY_MT_ADDR_SCAN         DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x002d) // 0x0402002d
#define DbvStep_DMIDX_DB_RCVRY_SBGRP                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x002e) // 0x0402002e
#define DbvStep_DMKEY_DB_RCVRY_SBGRP_NVSELECTOR     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x002f) // 0x0402002f
#define DbvStep_DMIDX_DB_RCVRY_SBGRP_MBR            DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0030) // 0x04020030
#define DbvStep_DMKEY_DB_RCVRY_SBGRP_MBR_SCAN       DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0031) // 0x04020031
#define DbvStep_DMIDX_SUBNET_LABEL_REC              DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0032) // 0x04020032
#define DbvStep_DMKEY_SUBNET_LABEL                  DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0033) // 0x04020033
#define DbvStep_DMKEY_SUBNET_LABEL_SORTED           DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0034) // 0x04020034
#define DbvStep_DMLNK_SUBNET_LABEL                  DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0035) // 0x04020035
#define DbvStep_DMIDX_ALT_ADDRESS_REC               DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0036) // 0x04020036
#define DbvStep_DMLNK_ALT_ADDRESS                   DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0037) // 0x04020037
#define DbvStep_DMIDX_XINFO_REC                     DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0038) // 0x04020038
#define DbvStep_DMKEY_XINFO_BY_OWNER                DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x0039) // 0x04020039
#define DbvStep_DMKEY_XINFO_OWNER_TYPE              DBV_FIELD_ID(DbvPhase_MDM_SCHEMA, 0x003A) // 0x0402003A

///////////////////////////////
// DbvPhase_MCH_SCHEMA
///////////////////////////////
#define DbvStep_CHREC_PROPS                         DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0000) // 0x05010000
#define DbvStep_CHREC_CHAN                          DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0001) // 0x05010001
#define DbvStep_CHREC_NODE                          DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0002) // 0x05010002
#define DbvStep_CHREC_ADDR                          DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0003) // 0x05010003
#define DbvStep_CHREC_NV                            DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0004) // 0x05010004
#define DbvStep_CHREC_ALIAS                         DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0005) // 0x05010005
#define DbvStep_CHREC_PROGRAM                       DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0006) // 0x05010006
#define DbvStep_CHREC_DOMAIN                        DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0007) // 0x05010007
#define DbvStep_CHREC_SYSTEM                        DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0008) // 0x05010008
#define DbvStep_CHREC_ROUTER                        DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0009) // 0x05010009
#define DbvStep_CHREC_ROUTER_ADDRESS                DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x000a) // 0x0501000a
#define DbvStep_CHREC_ROUTING_TABLE                 DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x000b) // 0x0501000b
#define DbvStep_CHREC_NSI                           DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x000c) // 0x0501000c
#define DbvStep_CHREC_CP                            DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x000d) // 0x0501000d
#define DbvStep_CHREC_CPVALUE                       DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x000e) // 0x0501000e
#define DbvStep_CHREC_CPSET                         DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x000f) // 0x0501000f
#define DbvStep_CHREC_CPOWNER                       DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0010) // 0x05010010
#define DbvStep_CHREC_NV_EXT                        DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0011) // 0x05010011
#define DbvStep_CHREC_XINFO                         DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0012) // 0x05010012
#define DbvStep_CHREC_MCS                           DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0013) // 0x05010013
#define DbvStep_CHREC_MCP                           DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0014) // 0x05010014
#define DbvStep_CHREC_FREE_RES                      DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0015) // 0x05010015
#define DbvStep_CHREC_BITMAP                        DBV_RECORD_ID(DbvPhase_MCH_SCHEMA, 0x0016) // 0x05010016

#define DbvStep_CHIDX_PROPS_REC                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0000)  // 0x05020000
#define DbvStep_CHIDX_CHAN_REC                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0001)  // 0x05020001
#define DbvStep_CHIDX_NODE_REC                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0002)  // 0x05020002
#define DbvStep_CHKEY_NODE_HANDLE                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0003)  // 0x05020003
#define DbvStep_CHKEY_NODE_NEURONID                 DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0004)  // 0x05020004
#define DbvStep_CHFLD_NODE_CLASS                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0005)  // 0x05020005
#define DbvStep_CHLNK_NODE_DOMAIN                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0006)  // 0x05020006
#define DbvStep_CHLNK_NODE_PROGRAM                  DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0007)  // 0x05020007
#define DbvStep_CHLNK_NODE_OWNER                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0008)  // 0x05020008
#define DbvStep_CHIDX_NV_REC                        DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0009)  // 0x05020009
#define DbvStep_CHKEY_NV_ID                         DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x000a)  // 0x0502000a
#define DbvStep_CHKEY_NV_SELEC                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x000b)  // 0x0502000b
#define DbvStep_CHLNK_NV_NODE                       DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x000c)  // 0x0502000c
#define DbvStep_CHIDX_ALIAS_REC                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x000d)  // 0x0502000d
#define DbvStep_CHKEY_ALIAS_ID                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x000e)  // 0x0502000e
#define DbvStep_CHKEY_ALIAS_SELEC                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x000f)  // 0x0502000f
#define DbvStep_CHLNK_ALIAS_NODE                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0010)  // 0x05020010
#define DbvStep_CHIDX_ADDR_REC                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0011)  // 0x05020011
#define DbvStep_CHKEY_ADDR_ID                       DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0012)  // 0x05020012
#define DbvStep_CHKEY_ADDR_DATA                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0013)  // 0x05020013
#define DbvStep_CHLNK_ADDR_NODE                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0014)  // 0x05020014
#define DbvStep_CHIDX_PROGRAM_REC                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0015)  // 0x05020015
#define DbvStep_CHKEY_PROGRAM_ID                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0016)  // 0x05020016
#define DbvStep_CHIDX_DOMAIN_REC                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0017)  // 0x05020017
#define DbvStep_CHKEY_DOMAIN_SIG                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0018)  // 0x05020018
#define DbvStep_CHLNK_DOMAIN_SYSTEM                 DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0019)  // 0x05020019
#define DbvStep_CHIDX_SYSTEM_REC                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x001a)  // 0x0502001a
#define DbvStep_CHKEY_SYSTEM_HANDLE                 DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x001b)  // 0x0502001b
#define DbvStep_CHIDX_ROUTER_REC                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x001c)  // 0x0502001c
#define DbvStep_CHKEY_ROUTER_HANDLE                 DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x001d)  // 0x0502001d
#define DbvStep_CHLNK_ROUTER_NODE                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x001e)  // 0x0502001e
#define DbvStep_CHIDX_ROUTER_ADDRESS_REC            DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x001f)  // 0x0502001f
#define DbvStep_CHKEY_ROUTER_ADDRESS                DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0020)  // 0x05020020
#define DbvStep_CHLNK_ROUTER_ADDRESS                DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0021)  // 0x05020021
#define DbvStep_CHLNK_ROUTING_TABLE                 DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0022)  // 0x05020022
#define DbvStep_CHIDX_ROUTING_TABLE_REC             DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0023)  // 0x05020023
#define DbvStep_CHIDX_NSI_REC                       DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0024)  // 0x05020024
#define DbvStep_CHKEY_NSI_HANDLE                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0025)  // 0x05020025
#define DbvStep_CHLNK_NSI_NODE                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0026)  // 0x05020026
#define DbvStep_CHIDX_CP_REC                        DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0027)  // 0x05020027
#define DbvStep_CHKEY_CP_ID                         DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0028)  // 0x05020028
#define DbvStep_CHLNK_CP_PROG                       DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0029)  // 0x05020029
#define DbvStep_CHIDX_CPVALUE_REC                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x002a)  // 0x0502002a
#define DbvStep_CHKEY_CPVALUE_ID                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x002b)  // 0x0502002b
#define DbvStep_CHLNK_CPVALUE_NODE                  DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x002c)  // 0x0502002c
#define DbvStep_CHIDX_CPSET_REC                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x002d)  // 0x0502002d
#define DbvStep_CHKEY_CPSET_ID                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x002e)  // 0x0502002e
#define DbvStep_CHLNK_CPSET_NODE                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x002f)  // 0x0502002f
#define DbvStep_CHIDX_CPOWNER_REC                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0030)  // 0x05020030
#define DbvStep_CHKEY_CPOWNER_ID                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0031)  // 0x05020031
#define DbvStep_CHLNK_CPOWNER_CP                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0032)  // 0x05020032
#define DbvStep_CHIDX_NVEXT_REC                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0033)  // 0x05020033
#define DbvStep_CHKEY_NVEXT_ID                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0034)  // 0x05020034
#define DbvStep_CHLNK_NVEXT_NODE                    DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0035)  // 0x05020035
#define DbvStep_CHIDX_XINFO_REC                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0036)  // 0x05020036
#define DbvStep_CHKEY_XINFO_BY_TYPE                 DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0037)  // 0x05020037
#define DbvStep_CHKEY_XINFO_BY_OWNER                DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0038)  // 0x05020038
#define DbvStep_CHLNK_XINFO_MCS                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0039)  // 0x05020039
#define DbvStep_CHLNK_XINFO_MCP                     DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x003a)  // 0x0502003a
#define DbvStep_CHIDX_MCS_REC                       DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x003b)  // 0x0502003b
#define DbvStep_CHKEY_MCS_ID                        DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x003c)  // 0x0502003c
#define DbvStep_CHLNK_MCS_NODE                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x003d)  // 0x0502003d
#define DbvStep_CHIDX_MCP_REC                       DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x003e)  // 0x0502003e
#define DbvStep_CHKEY_MCP_ID                        DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x003f)  // 0x0502003f
#define DbvStep_CHLNK_MCP_NODE                      DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0040)  // 0x05020040
#define DbvStep_CHIDX_FREE_RES_REC                  DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0041)  // 0x05020041
#define DbvStep_CHKEY_FREE_RES_ID                   DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0042)  // 0x05020042
#define DbvStep_CHLNK_FREE_RES_NODE                 DBV_FIELD_ID(DbvPhase_MCH_SCHEMA, 0x0043)  // 0x05020043

///////////////////////////////
// DbvPhase_CONNECTIONS
///////////////////////////////
#define DbvStep_SM_CONNDIAG_SUBGROUPS               DBV_STEP_ID_STD(DbvPhase_CONNECTIONS, 0x0001) // 0x06000001
#define DbvStep_SM_CONNDIAG_EXLUSIVE_SELECTORS      DBV_STEP_ID_STD(DbvPhase_CONNECTIONS, 0x0002) // 0x06000002
#define DbvStep_SM_CONNDIAG_SELECTOR_POOLS          DBV_STEP_ID_STD(DbvPhase_CONNECTIONS, 0x0003) // 0x06000003
#define DbvStep_SM_CONNDIAG_ALIAS_RECORDS           DBV_STEP_ID_STD(DbvPhase_CONNECTIONS, 0x0004) // 0x06000004

#endif
