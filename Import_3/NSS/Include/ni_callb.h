//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_callb.h#4 $
//

#ifndef _NI_CALLB_H
#define _NI_CALLB_H

/****************************************************************************
 *  Filename:     ni_callb.h
 *
 *  Description:  This include file contains definitions for the NI
 *                API callback functions.
 *
 *
 *  Copyright (c) 1994-2000 Echelon Corporation.  All Rights Reserved.
 *
 ***************************************************************************/

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */



/***************************************************************************
   Function:  NiRegisterCallback()

   Desc:      This function is used to register message handlers for network
              management and network diagnostic messages that must be
              processed by the host.  These messages are: set node mode,
              wink, update network variable config, query net variable config,
              query SNVT, network variable fetch, poll network variable,
              update network variable, and reset.  Additionally, a callback
              exists for all incoming application and foreign messages, and
              a generic callback for any other messages that need to be
              processed.

              A set of default callbacks exist which process incoming
              messages in a standard fashion.  For example, the default
              set node mode message handler NiHandleSetNodeMode() sends a
              local online or offline command to the network interface node.

              You can use this function to register your own callback functions.

   ************************************************************************/
typedef Bool (*NiCallbackProc)(ExpAppBuffer* pMsgIn);

typedef struct
{
   NiCallbackProc proc;
   unsigned int   nmCommand;

} NiCallbackData;

typedef enum
{
   GENERAL_CALLBACK = 0,
   APP_MSG_CALLBACK,
   RESET_CALLBACK,
   WINK_CALLBACK,
   QUERY_SNVT_CALLBACK,
   NV_FETCH_CALLBACK,
   NV_POLL_CALLBACK,
   NV_UPDATE_CALLBACK,
   QUERY_NV_CNFG_CALLBACK,
   UPDATE_NV_CNFG_CALLBACK,
   SET_NODE_MODE_CALLBACK,
   NSS_QUERY_RESULT_CALLBACK,

   /* Add new callback procedures above here: */
   MAX_CALLBACK_NUM,
   ALL_CALLBACK = MAX_CALLBACK_NUM
} CallbackType;

extern Bool NiRegisterCallback(CallbackType type, NiCallbackProc procedure);


/***************************************************************************
   Function:  NiGetCallback()

   Desc:      This function is used to query the current message handlers
              for network management and network diagnostic messages that
              must be processed by the host.  

              In Win32, a NULL proc is always returned for the following
              types:

                   GENERAL_CALLBACK
                   APP_MSG_CALLBACK
                   QUERY_SNVT_CALLBACK
                   NV_FETCH_CALLBACK
                   NV_POLL_CALLBACK
                   NV_UPDATE_CALLBACK
                   QUERY_NV_CNFG_CALLBACK
                   UPDATE_NV_CNFG_CALLBACK

   ************************************************************************/

extern NiCallbackProc NiGetCallback(CallbackType type);


/***************************************************************************
    The following are callback functions for various messages.
    
    WIN32 Note:  Starting with LNS 3.0, only NiHandleWink() and 
                NiHandleSetNodeMode() are supported.  The other
                handlers will not be registered, and will do nothing
                if called.  Their functionality is now done by the VNI.  

                They will continue to be used in non-Win32 environment.

   ************************************************************************/

extern Bool NiHandleWink(ExpAppBuffer* pMsgIn);
extern Bool NiHandleSetNodeMode(ExpAppBuffer* pMsgIn);
extern Bool NiHandleAppMsgs(ExpAppBuffer* pMsgIn);
extern Bool NiHandleQuerySnvt(ExpAppBuffer* pMsgIn);
extern Bool NiHandleNvFetch(ExpAppBuffer* pMsgIn);
extern Bool NiHandleNvPoll(ExpAppBuffer* pMsgIn);
extern Bool NiHandleNvUpdate(ExpAppBuffer* pMsgIn);
extern Bool NiHandleQueryNvCnfg(ExpAppBuffer* pMsgIn);
extern Bool NiHandleUpdateNvCnfg(ExpAppBuffer* pMsgIn);


/* default response structure */
typedef struct
{
   snvt_struct_v0 querySnvtResponseHeader;
   Byte           selfDocTerminator;
} QuerySnvtResponseMsg;


Bool EvokeCallback(CallbackType type, ExpAppBuffer* pMsgIn);
Bool EvokeNetManCommandCallback(unsigned int nmCommand, ExpAppBuffer* pMsgIn);
void NiCheckCallbacksEnabled(void);
Bool NiRouteMessage(ExpAppBuffer* pMsgIn, NiReferenceId niRefId);
Bool NiCommandCallback(ExpAppBuffer* pMsgIn);


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
