//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_mgmt.h#11 $
//

#ifndef _NI_MGMT_H
#define _NI_MGMT_H

/****************************************************************************
 *  Filename:     ni_mgmt.h
 *
 *  Description:  This include file contains LonTalk network management
 *                codes and messages.  These definitions are a subset
 *                of the LonTalk network management structures.  The
 *                types have been changed where necessary because of the
 *                difference in representation between the Neuron and
 *                MS-DOS.  If you aren't using DOS you may need to modify
 *                these structures.
 *
 *  Copyright (c) 1994-2000 Echelon Corporation.  All Rights Reserved.
 *
 ***************************************************************************/

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

#define NULL_IDX  (-1)                /* unused address table index */

/* message codes for network management and diagnostic classes of messages. */
typedef enum _NM_message_code
{
    ND_query_status                 = 0x51,
    ND_proxy                        = 0x52,
    ND_clear_status                 = 0x53,
    ND_query_xcvr                   = 0x54,
    ND_query_evn                    = 0x55,
    ND_query_status_flex_domain     = 0x56,
    ND_bidir_xcvr_status            = 0x57,
    ND_compute_phase                = 0x58,
    ND_query_full_version           = 0x59,

    NM_expanded                     = 0x60,
    NM_query_id                     = 0x61,
    NM_respond_to_query             = 0x62,
    NM_update_domain                = 0x63,
    NM_leave_domain                 = 0x64,
    NM_update_key                   = 0x65,
    NM_update_addr                  = 0x66,
    NM_query_addr                   = 0x67,
    NM_query_nv_cnfg                = 0x68,
    NM_update_group_addr            = 0x69,
    NM_query_domain                 = 0x6A,
    NM_update_nv_cnfg               = 0x6B,
    NM_set_node_mode                = 0x6C,
    NM_read_memory                  = 0x6D,
    NM_write_memory                 = 0x6E,
    NM_checksum_recalc              = 0x6F,
    NM_wink                         = 0x70,
    NM_install                      = NM_wink,
    NM_app_cmd                      = NM_wink,
    NM_memory_refresh               = 0x71,
    NM_query_snvt                   = 0x72,
    NM_nv_fetch                     = 0x73,
    NM_device_esc                   = 0x7D,
    NM_router_esc                   = 0x7E,
    NM_service_pin                  = 0x7F,
} _NM_message_code;
typedef NetEnum(_NM_message_code) NM_message_code;

/* success and failure response codes for network management and diagnostic classes
   of messages.  The values overlap between the two classes; but their uses are
   context-dependent */
typedef enum _NM_response_code
{
    /* success codes for network diagnostic commands */
    ND_query_status_success         = 0x31,
    ND_proxy_success                = 0x32,
    ND_clear_status_success         = 0x33,
    ND_query_xcvr_success           = 0x34,
    ND_query_evn_success            = 0x35,
    ND_query_status_flex_domain_success = 0x36,
    ND_bidir_xcvr_status_success    = 0x37,
    ND_compute_phase_success        = 0x38,
    ND_query_full_version_success   = 0x39,

    /* success codes for network management commands */
    NM_expanded_success             = 0x20,
    NM_query_id_success             = 0x21,
    NM_respond_to_query_success     = 0x22,
    NM_update_domain_success        = 0x23,
    NM_leave_domain_success         = 0x24,
    NM_update_key_success           = 0x25,
    NM_update_addr_success          = 0x26,
    NM_query_addr_success           = 0x27,
    NM_query_nv_cnfg_success        = 0x28,
    NM_update_group_addr_success    = 0x29,
    NM_query_domain_success         = 0x2A,
    NM_update_nv_cnfg_success       = 0x2B,
    NM_set_node_mode_success        = 0x2C,
    NM_read_memory_success          = 0x2D,
    NM_write_memory_success         = 0x2E,
    NM_checksum_recalc_success      = 0x2F,
    NM_wink_success                 = 0x30,
    NM_install_success              = NM_wink_success,
    NM_app_cmd_success              = NM_wink_success,
    NM_memory_refresh_success       = 0x31,
    NM_query_snvt_success           = 0x32,
    NM_nv_fetch_success             = 0x33,
    NM_device_esc_success           = 0x3D,
    NM_router_esc_success           = 0x3E,
    NM_service_pin_success          = 0x3F, /* doesn't really exist */

    /* failure codes for network diagnostic commands */
    ND_query_status_fail            = 0x11,
    ND_proxy_fail                   = 0x12,
    ND_clear_status_fail            = 0x13,
    ND_query_xcvr_fail              = 0x14,
    ND_query_evn_fail               = 0x15,
    ND_query_status_flex_domain_fail = 0x16,
    ND_bidir_xcvr_status_fail       = 0x17,
    ND_compute_phase_fail           = 0x18,
    ND_query_full_version_fail      = 0x19,

    /* failure codes for network management commands */
    NM_expanded_fail                = 0x00,
    NM_query_id_fail                = 0x01,
    NM_respond_to_query_fail        = 0x02,
    NM_update_domain_fail           = 0x03,
    NM_leave_domain_fail            = 0x04,
    NM_update_key_fail              = 0x05,
    NM_update_addr_fail             = 0x06,
    NM_query_add_failr              = 0x07,
    NM_query_nv_cnfg_fail           = 0x08,
    NM_update_group_addr_fail       = 0x09,
    NM_query_domain_fail            = 0x0A,
    NM_update_nv_cnfg_fail          = 0x0B,
    NM_set_node_mode_fail           = 0x0C,
    NM_read_memory_fail             = 0x0D,
    NM_write_memory_fail            = 0x0E,
    NM_checksum_recalc_fail         = 0x0F,
    NM_wink_fail                    = 0x10,
    NM_install_fail                 = NM_wink_fail,
    NM_app_fail                     = NM_wink_fail,
    NM_memory_refresh_fail          = 0x11,
    NM_query_snvt_fail              = 0x12,
    NM_nv_fetch_fail                = 0x13,
    NM_device_esc_fail              = 0x1D,
    NM_router_esc_fail              = 0x1E,
    NM_service_pin_fail             = 0x1F,  /* doesn't really exist */
} _NM_response_code;
typedef NetEnum(_NM_response_code) NM_response_code;

/* aliases for backward compatibility */
#define NM_ESCAPE_CODE NM_device_esc
#define ND_query_status_succ    ND_query_status_success
#define ND_clear_status_succ    ND_clear_status_success
#define NM_update_nv_cnfg_succ  NM_update_nv_cnfg_success
#define NM_query_nv_cnfg_succ   NM_query_nv_cnfg_success
#define NM_set_node_mode_succ   NM_set_node_mode_success
#define NM_query_snvt_succ      NM_query_snvt_success
#define NM_NV_fetch             NM_nv_fetch
#define NM_NV_fetch_succ        NM_nv_fetch_success
#define NM_NV_fetch_fail        NM_nv_fetch_fail


/* NM_set_node_mode */
typedef enum
{
   APPL_OFFLINE = 0,         /* Soft offline state */
   APPL_ONLINE,
   APPL_RESET,
   CHANGE_STATE
} nm_node_mode;

/*
   The neuron "state" reported by the query status command contains a 3 bit
   state value stored in the node's EEPROM, plus an online bit and an
   offline/bypass mode bit.  In general the offline and offline/bypass mode
   bits are useful only if the 3 bit neuron state state is "configured".
   However, for debugging purposes it might be useful to know if the node was
   taken offline prior to having its state changed - the offline bit indicates
   this (although if the node is reset after the state change the offline bit
   will not be set).

   The following literals and macros can be used to interpret the node state.

*/

#define OFFLINE_BIT 0x8
#define NODE_STATE_MASK 0x7

/* return the 3 bit neuron state stored in EEPROM. */
#define NEURON_STATE(state) ((state)&NODE_STATE_MASK)

/* return nm_node_state - literals defined below */
#define NODE_STATE(state) ((NEURON_STATE(state) == CNFG_ONLINE) \
    ? (state) : NEURON_STATE(state))

/* Return 0 if the state indicates offline, non-zero otherwise. */
#define NODE_STATE_OFFLINE(state) ((state) & OFFLINE_BIT)


/* This enumeration is used with
        NM_set_node_mode_request  and
        ND_query_status_response.


   This ENUM contains the documented values of the neuron state. To convert
   the state value returned by the ND_query_status command to one of these
   values, use the macro NODE_STATE() defined above.  To see if the offline
   bit is set, use the macro NODE_STATE_OFFLINE().
    */

typedef enum {
   STATE_INVALID        = 0,    /* invalid or echelon use only          */
   STATE_INVALID_1      = 1,    /* equivalent to STATE_INVALID          */
   APPL_UNCNFG          = 2,    /* has application, unconfigured        */
   NO_APPL_UNCNFG       = 3,    /* applicationless, unconfigured        */
   CNFG_ONLINE          = 4,    /* configured, online                   */
   STATE_INVALID_5      = 5,    /* equivalent to STATE_INVALID          */
   CNFG_OFFLINE         = 6,    /* hard offline                         */
   STATE_INVALID_7      = 7,    /* equivalent to STATE_INVALID          */
   SOFT_OFFLINE         =
    (CNFG_ONLINE|OFFLINE_BIT),  /* (12) configured, soft-offline        */
   CNFG_BYPASS          =       /* 0x8c - configured, in bypass mode    */
#ifdef COMPILE_8                /* special casting for neuron C compiler*/
                          (signed char)
#endif
                          (0x80|SOFT_OFFLINE), /* 0x8C, 140 (decimal)   */
} nm_node_state;

typedef struct
{
   Byte code;
   Byte mode;          /* Interpret with 'nm_node_mode'        */
   Byte node_state;    /* Optional field if mode==CHANGE_STATE */
                  /* Interpret with 'nm_node_state'       */
} NM_set_node_mode_request;

/* NM_write_memory */
typedef enum
{
    ABSOLUTE_MEM_MODE  = 0,         /* Address is absolute neuron mem address */
    READ_ONLY_RELATIVE = 1,         /* Address is offset from beginning of
                                       read-only memory structures. */
    CONFIG_RELATIVE    = 2,         /* Address is offset from beginning of
                                       config data structures. */
    STATS_RELATIVE     = 3,         /* Address is offset from beginning of
                                       statistics data structures. */
    MEM_MODE_RESERVED_A= 4,         /* Reserved for Echelon internal use only */
    MEM_ABSOLUTE_VOLATILE_MODE=5,   /* absolute volatile mode for NM_write_memory requests, 
                                       used in 5000 (and subsequent) family chips for writing 
                                       memory without shadowing to underlying non-volatile memory. */
    MEM_EXTENDED_SYS = 6,           /* Used to write system image above 0x10000. */
    MEM_EXTENDED_APP = 7,           /* Used to write app image above 0x10000 */
    MEM_EXTENDED_ABSOLUTE = 8,      /* Used to read or write flashu using absolute addresses above 0x10000 */

    MEM_BANK_1_OFFSET = 9,
    MEM_TRANSIENT_BP   = 10,        /* Transient breakpoint table */
} nm_mem_mode;

#define NEURON_EXTENDED_ADDRESS_BASE 0x10000    // Used to represent areas of flash that do not map directly
                                                // to neuron memory.  Use MEM_EXTENDED_* addressing modes.  In these
                                                // modes the location and count represent '16 byte words'.
#define NEURON_EXTENDED_WORD_SIZE 16

#define ENCODE_EXTENDED_ADDRESS(addr) (((addr)-NEURON_EXTENDED_ADDRESS_BASE)/NEURON_EXTENDED_WORD_SIZE)
#define DECODE_EXTENDED_ADDRESS(addr) ((((unsigned long)(addr))*NEURON_EXTENDED_WORD_SIZE) + NEURON_EXTENDED_ADDRESS_BASE)

typedef enum
{
   NO_ACTION      = 0,
   BOTH_CS_RECALC = 1,
   DELTA_CS_RECALC= 3,
   CNFG_CS_RECALC = 4,
   ONLY_RESET     = 8,
   BOTH_CS_RECALC_RESET = 9,
   CNFG_CS_RECALC_RESET = 12
} nm_mem_form;

typedef struct
{
   Byte    code;
   Byte    mode;
   Byte    offset_hi;
   Byte    offset_lo;
   Byte    count;
   Byte    form;          /* followed by the data */
} NM_write_memory_request;

/* this is an alternative declaration for NM_write_memory_request that
   resembles the declaration in the neuron-c include file */
typedef struct
{
   Byte    code;
   Byte    mode;
   NetWord offset;
   Byte    count;
   Byte    form;          /* followed by the data */
} NM_write_memory_request_nc;


/* NM_query_snvt */
/* Partial list of SNVT type index values */
typedef enum
{
   SNVT_str_asc  = 36,
   SNVT_lev_cont = 21,
   SNVT_lev_disc = 22,
   SNVT_count_f  = 51
} SNVT_t;

typedef struct
{
    Byte      code;
    NetWord   offset;     /* big-endian 16-bits */
    Byte      count;
} NM_query_snvt_request;


/* Structure used by host application to store network variables */

typedef enum
{
   NV_IN = 0,
   NV_OUT = 1
} nv_direction;

typedef struct
{                                      /* structure to define NVs */
   int          size;                  /* number of Bytes */
   nv_direction direction;             /* input or output */
   const char*  name;                  /* name of variable */
   void ( * print_func )(Byte *);      /* routine to print value */
   void ( * read_func )(Byte *);       /* routine to read value */
   Byte         data[MAX_NETVAR_DATA]; /* actual storage for value */
} network_variable;


typedef struct
{
   Byte         code;
   Byte         index;
} short_index_struct;

typedef struct
{
   short_index_struct short_index;
   nv_struct          nv_cnfg_data;
} short_config_data_struct;


#define HOST_NV_INDEX_ESCAPE 255
typedef struct
{
    Byte            code;
    Byte            index_escape;   /* Must be 255 (HOST_NV_INDEX_ESCAPE) */
    NetWord         index;
} long_index_struct;

// The maximum size of an NM_update_nv_config request is for an alias with both the alias and
// primary index escaped
#define MAX_NM_UPDATE_NV_CNFG_REQUEST \
    (1 +                         /* code */ \
    (1+2) +                      /* NV or ALIAS index (1 byte escape + 2 byte index) */ \
    sizeof(nv_struct) +          /* NV structure */ \
    (1+2))                       /* Primary index (1 byte escape, 2 byte index) */ 

/* NM_query_nv_cnfg */
typedef union NM_query_nv_cnfg_request
{
   short_index_struct   short_index;
   long_index_struct long_index;
} NM_query_nv_cnfg_request;

// The maximum size of an NM_query_nv_config response is for an alias whose 
// primary index is escaped, plus an extended address table entry.
#define MAX_NM_UPDATE_NV_CNFG_RESPONSE \
    (sizeof(nv_struct) +          /* NV structure */ \
    (1+2))                       /* Primary index (1 byte escape, 2 byte index) */ 

/*  NM_nv_fetch */
typedef NM_query_nv_cnfg_request NM_nv_fetch_request;

typedef union NM_nv_fetch_response
{
   short_index_struct   short_index;
   long_index_struct    long_index;   /* followed by data */
} NM_nv_fetch_response;


/* NM_update_domain */
typedef struct
{
    Byte        code;
    Byte        domain_index;
    Byte        id[ DOMAIN_ID_LEN ];
    Byte        subnet;
#ifdef BITF_LITTLE_ENDIAN
    bits        node        : 7;
    bits        must_be_one : 1;    /* this bit must be set to 1 */
#else
    bits        must_be_one : 1;    /* this bit must be set to 1 */
    bits        node        : 7;
#endif
    Byte        len;
    Byte        key[ AUTH_KEY_LEN ];
} NM_update_domain_request;

/* NM_query_domain */
typedef struct
{
    Byte        code;
    Byte        domain_index;
} NM_query_domain_request;

typedef struct
{
    Byte        code;
    Byte        id[ DOMAIN_ID_LEN ];
    Byte        subnet;
#ifdef BITF_LITTLE_ENDIAN
    bits        node        : 7;
    bits        must_be_one : 1;    /* this bit must be set to 1 */
#else
    bits        must_be_one : 1;    /* this bit must be set to 1 */
    bits        node        : 7;
#endif
    Byte        len;
    Byte        key[ AUTH_KEY_LEN ];
} NM_query_domain_response;

/* NM_leave_domain */
typedef struct
{
    Byte        code;
    Byte        domain_index;
} NM_leave_domain_request;

/* NM_read_memory */
typedef struct
{
    Byte        code;
    Byte        mode;               /* enum nm_mem_mode */
    NetWord     offset;
    Byte        count;
} NM_read_memory_request;

/* NM_device_esc
 * The LONTalk network management commands for device escape all begin
 * with the following 3-byte sequence:
 *      <Device Escape Code> <Device Code> <Optional Command code>
 */
#define DEVICE_ESC_SEQ_LEN          3

/*
 * Header structure for all messages using the device escape code.
 */
typedef struct
{
    Byte     device_code;
    Byte     device_command;
    /* command-specific data follows */
} NM_device_request_header;

typedef struct
{
    Byte        code;           /* NM_device_esc */
    NM_device_request_header devReqHdr;
} NM_device_request;

/*
 * Device Escape Product Query
 */
#define PRODUCT_QUERY_PRODUCT_CODE 1
#define PRODUCT_QUERY_COMMAND 1

/* values for response field "product" */
#define MIP_PRODUCT_CODE    2
#define NS_PRODUCT_CODE     3
#define NS_OVERRIDE_CODE      5

/* values for NS_PRODUCT_CODE response field "model" */
#define NSS_10_MODEL_CODE 1     /* returned by NSS-10 and NSI-10 firmware */
#define LNS_VNI_MODEL_CODE 2    /* returned by NSS/NSI using VNI */
#define ILON_MODEL_CODE 4		/* returned by iLON MIP APP */
#define ILON10_MODEL_CODE 5     /* returned by iLON-10 */
#define PLR_MODEL_CODE 6	    /* returned by PLR */


typedef struct
{
    Byte        code;           /* NM response code */
    Byte        product;
    Byte        model;          /* product specific values */
    Byte        version;        /* product/model specific values */
    Byte        product_specific; /* product specific interpretation */
#ifdef BITF_LITTLE_ENDIAN
    bits        transceiver_ID  : 5;
    bits        XID_reserved    : 3;
#else
    bits        XID_reserved    : 3;
    bits        transceiver_ID  : 5;
#endif
} NM_product_query_response;


typedef struct
{
#ifdef BITF_LITTLE_ENDIAN
    bits        max_wait    : 3;
    bits        nm_auth     : 1;
    bits        rcv_timer   : 4;
#else
    bits        rcv_timer   : 4;
    bits        nm_auth     : 1;
    bits        max_wait    : 3;
#endif
} nmMsgParms;

/* NEURON CHIP model numbers returned by the Query Status command response. */
/* Note: all 256 values are potentially valid, and new models may not be in this list. */
/* Values 128 and above  are defined as LonTalk protocol implementations */
/* not hosted on a Neuron Chip. */
typedef enum nm_model_code
{
        /* 0 */         NEURON_3150_CODE   =  0, /* 3150 and FT-3150 */
        /* 1 */         NEURON_PL3150_CODE =  1, 
        /* 8 */         NEURON_3120_CODE   =  8,
        /* 9 */         NEURON_3120E1_CODE =  9,
        /* 10 */        NEURON_3120E2_CODE = 10,
        /* 11 */        NEURON_3120E3_CODE = 11,
        /* 12 */        NEURON_3120A20_CODE = 12,
        /* 13 */        NEURON_3120E5_CODE = 13,
        /* 14 */        NEURON_3120E4_CODE = 14,
        /* 32 */        NEURON_FT5000_CODE = 32,
        /* 33 */        NEURON_5000_CODE   = 33,
        /* 128 */       NON_NEURON_GENERIC_CODE = 128,
        /* 129 */       NON_NEURON_PENTAGON_CODE = 129,
        /* 130 */       NON_NEURON_MIPS_CODE = 130,
} nm_model_code;


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
