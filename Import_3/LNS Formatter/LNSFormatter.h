// LNSFormatter.h: interface for the LNSFormatter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LNSFORMATTER_H__DE6A54A9_2346_43F1_9875_6D3A41EC644D__INCLUDED_)
#define AFX_LNSFORMATTER_H__DE6A54A9_2346_43F1_9875_6D3A41EC644D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class LNSFormatter  
{
public:
	LNSFormatter();
	virtual ~LNSFormatter();

	static LNSFormatter* m_pInst;

	static LNSFormatter* GetInst() 
	{
		if (!m_pInst)
			m_pInst = new LNSFormatter();

		return m_pInst;
	}

	static void Cleanup() 
	{
		if (m_pInst) {
			delete m_pInst;
			m_pInst = NULL;
		}
	}

	void ChooseFormat(int &nScope, ProgramId& curProgID, CString& strCurFormat, CWnd* pParentWnd, LPCTSTR szTitle);
};

#endif // !defined(AFX_LNSFORMATTER_H__DE6A54A9_2346_43F1_9875_6D3A41EC644D__INCLUDED_)
