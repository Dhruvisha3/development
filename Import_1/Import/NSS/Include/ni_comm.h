//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_comm.h#5 $
//

#ifndef _NI_COMM_H
#define _NI_COMM_H

/****************************************************************************
 *  Filename:     ni_comm.h
 *
 *  Description:  This file contains messaging related definitions for
 *                the Network Interface API (NI API).
 *
 *  Copyright (c) 1997-2000 Echelon Corporation.  All Rights Reserved.
 *
 ***************************************************************************/

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

#define niDEFAULT_WAIT_TIME 10   /* Seconds */

#define FOREIGN_MSG_CODE_END_RANGE     0x4E

/*
 ****************************************************************************
 * Application buffer structures for sending and receiving messages to and
 * from a network interface.  The 'ExpAppBuffer' and 'ImpAppBuffer'
 * structures define the application buffer structures with and without
 * explicit addressing.  These structures have up to four parts:
 *
 *   Network Interface Command (NI_Hdr)                        (2 Bytes)
 *   Message Header (MsgHdr)                                   (3 Bytes)
 *   Network Address (ExplicitAddr)                            (11 Bytes)
 *   Data (MsgData)                                            (varies)
 *
 * Network Interface Command (NI_Hdr):
 *
 *   The network interface command is always present.  It contains the
 *   network interface command and queue specifier.  This is the only
 *   field required for local network interface commands such as niRESET.
 *
 * Message Header (MsgHdr: union of NetVarHdr and ExpMsgHdr):
 *
 *   This field is present if the buffer is a data transfer or a completion
 *   event.  The message header describes the type of LONTALK message
 *   contained in the data field.
 *
 *   NetVarHdr is used if the message is a network variable message and
 *   network interface selection is enabled.
 *
 *   ExpMsgHdr is used if the message is an explicit message, or a network
 *   variable message and host selection is enabled (this is the default
 *   for the SLTA).
 *
 * Network Address (ExplicitAddr:  SendAddrDtl, RcvAddrDtl, or RespAddrDtl)
 *
 *   This field is present if the message is a data transfer or completion
 *   event, and explicit addressing is enabled.  The network address
 *   specifies the destination address for downlink application buffers,
 *   or the source address for uplink application buffers.  Explicit
 *   addressing is the default for the SLTA.
 *
 *   SendAddrDtl is used for outgoing messages or NV updates.
 *
 *   RcvAddrDtl is used  for incoming messages or unsolicited NV updates.
 *
 *   RespAddrDtl is used for incoming responses or NV updates solicited
 *   by a poll.
 *
 * Data (MsgData: union of UnprocessedNV, ProcessedNV, and ExplicitMsg)
 *
 *   This field is present if the message is a data transfer or completion
 *   event.
 *
 *   If the message is a completion event, then the first two Bytes of the
 *   data are included.  This provides the NV index, the NV selector, or the
 *   message code as appropriate.
 *
 *   UnprocessedNV is used if the message is a network variable update, and
 *   host selection is enabled. It consists of a two-Byte header followed by
 *   the NV data.
 *
 *   ProcessedNV is used if the message is a network variable update, and
 *   network interface selection is enabled. It consists of a two-Byte header
 *   followed by the NV data.
 *
 *   ExplicitMsg is used if the message is an explicit message.  It consists
 *   of a one-Byte code field followed by the message data.
 *
 * Note - the fields defined here are for a little-endian (Intel-style)
 * host processor, such as the 80x86 processors used in PC compatibles.
 * Bit fields are allocated right-to-left within a Byte.
 * For a big-endian (Motorola-style) host, bit fields are typically
 * allocated left-to-right.  For this type of processor, reverse
 * the bit fields within each Byte.  Compare the NEURON C include files
 * ADDRDEFS.H and MSG_ADDR.H, which are defined for the big-endian NEURON
 * CHIP.
 ****************************************************************************
 */



/*
 ****************************************************************************
 * Network Interface Command data structure.  This is the application-layer
 * header used for all messages to and from a LONWORKS network interface.
 ****************************************************************************
 */

/* Literals for the 'cmd.q.queue' nibble of NI_Hdr. */

typedef enum
{
   niTQ          = 2,     /* Transaction queue                     */
   niTQ_P        = 3,     /* Priority transaction queue            */
   niNTQ         = 4,     /* Non-transaction queue                 */
   niNTQ_P       = 5,     /* Priority non-transaction queue        */
   niRESPONSE    = 6,     /* Response msg & completion event queue */
   niINCOMING    = 8      /* Received message queue                */
} NI_Queue;

/* Literals for the 'cmd.q.q_cmd' nibble of NI_Hdr. */

typedef enum
{
   niCOMM    = 1,    /* Data transfer to/from network        */
   niNETMGMT = 2     /* Local network management/diagnostics */
} NI_QueueCmd;

/* Literals for the 'cmd.noq' Byte of NI_Hdr. */

typedef enum
{
    niNULL           = 0x00,
    niTIMEOUT        = 0x30,        /* Not used */
    niCRC            = 0x40,        /* Not used */
    niRESET          = 0x50,
    niFLUSH_COMPLETE = 0x60,        /* Uplink   */
    niFLUSH_CANCEL   = 0x60,        /* Downlink */
    niONLINE         = 0x70,
    niOFFLINE        = 0x80,
    niFLUSH          = 0x90,
    niFLUSH_IGN      = 0xA0,
    niSLEEP          = 0xB0,        /* SLTA only */
    niACK            = 0xC0,
    niNACK           = 0xC1,        /* SLTA only */
    niDOWNLOAD	     = 0xD1,
    niUPLOAD		 = 0xD3,
    niSSTATUS        = 0xE0,        /* SLTA only */
    niPUPXOFF        = 0xE1,
    niPUPXON         = 0xE2,
    niPTRHROTL       = 0xE4,        /* Not used  */
    niSERVICE		 = 0xE6,
    niSBUFC			 = 0xE7,
    niTXID           = 0xE8,
    niDRV_CMD        = 0xF0,        /* Not used  */

    niSIM_WINK       = 0x5F,        /* use with VNI only */
} NI_NoQueueCmd;

/* Reset command ind = icates reset in high order nibble (value 0x5), and reset source in low order nibble.
   Following literals and macros are used to extract these components from the reset message and determine
   the source of the reset message.
*/
#define RESET_MASK          0xF0
#define RESET_SOURCE_MASK   0x0F

typedef enum
{
    niMIP_RESET     = 0x00,
    niNSI_RESET     = 0x01
} NI_ResetSource;

#define NI_RESET_MSG(cmd)  (((cmd)&RESET_MASK) == niRESET)
#define NI_MIP_RESET(cmd)  (NI_RESET_MSG(cmd) && (((cmd)&RESET_SOURCE_MASK)==niMIP_RESET))
#define NI_NSI_RESET(cmd)  (NI_RESET_MSG(cmd) && (((cmd)&RESET_SOURCE_MASK)==niNSI_RESET))


/*
 * MAX_NETMSG_DATA specifies the maximum size of the data portion of an
 * application buffer.  MAX_NETVAR_DATA specifies the maximum size of the
 * data portion of a network variable update.  The values specified here
 * are the absolute maximums,based on the LONTALK protocol. Actual limits
 * are based on the buffer sizes defined on the attached NEURON CHIP.
 */

#define MAX_NETMSG_DATA 228
#define MAX_NETVAR_DATA (MAX_NETMSG_DATA-1)

/*
 * Header for network interface messages.  The header is a union of
 * two command formats: the 'q' format is used for the niCOMM and
 * niNETMGMT commands that require a queue specification; the 'noq'
 * format is used for all other network interface commands.
 * Both formats have a length specification where:
 *
 *      length = header (3) + address field (11 if present) + data field
 *
 * WARNING:  The fields shown in this structure do NOT reflect the actual
 * structure required by the network interface.  Depending on the network
 * interface, the network driver may change the order of the data and add
 * additional fields to change the application-layer header to a link-layer
 * header.  See the description of the link-layer header in Chapter 2 of the
 * Host Application Programmer's Guide.
 */

typedef union
{
   struct
   {
#ifdef BITF_LITTLE_ENDIAN
      bits queue  :4;          /* Network interface message queue      */
                               /* Use value of type 'NI_Queue'         */
      bits q_cmd  :4;          /* Network interface command with queue */
                               /* Use value of type 'NI_QueueCmd'      */
#else
      bits q_cmd  :4;          /* Network interface command with queue */
                               /* Use value of type 'NI_QueueCmd'      */
      bits queue  :4;          /* Network interface message queue      */
                               /* Use value of type 'NI_Queue'         */
#endif
      bits length :8;          /* Length of the buffer to follow       */
    } q;                       /* Queue option                         */
    struct
    {
       Byte     cmd;           /* Network interface command w/o queue  */
                               /* Use value of type 'NI_NoQueueCmd'    */
       Byte     length;        /* Length of the buffer to follow       */
    } noq;                     /* No queue option                      */
} NI_Hdr;


/*
 ****************************************************************************
 * Message Header structure for sending and receiving explicit
 * messages and network variables which are not processed by the
 * network interface (host selection enabled).
 ****************************************************************************
 */

/* Literals for 'st' fields of ExpMsgHdr and NetVarHdr. */

typedef enum
{
   ACKD            = 0,
   UNACKD_RPT      = 1,
   UNACKD          = 2,
   REQUEST         = 3
} ServiceType;
typedef NetEnum(ServiceType) service_type;

/* Literals for 'cmpl_code' fields of ExpMsgHdr and NetVarHdr. */

typedef enum
{
   MSG_NOT_COMPL  = 0,             /* Not a completion event            */
   MSG_SUCCEEDS   = 1,             /* Successful completion event       */
   MSG_FAILS      = 2              /* Failed completion event           */
} ComplType;

/* Explicit message and Unprocessed NV Application Buffer. */

#ifdef BITF_LITTLE_ENDIAN
typedef struct
{
   bits   tag       :4;        /* Message tag for implicit addressing  */
                               /* Magic cookie for explicit addressing */
   bits   auth      :1;        /* 1 => Authenticated                   */
   bits   st        :2;        /* Service Type - see 'ServiceType'     */
   bits   msg_type  :1;        /* 0 => explicit message                */
                               /*      or unprocessed NV               */
/*--------------------------------------------------------------------------*/
   bits   response  :1;        /* 1 => Response, 0 => Other            */
   bits   pool      :1;        /* 0 => Outgoing                        */
   bits   alt_path  :1;        /* 1 => Use path specified in 'path'    */
                               /* 0 => Use default path                */
   bits   addr_mode :1;        /* 1 => Explicit addressing,            */
                               /* 0 => Implicit                        */
                               /* Outgoing buffers only                */
   bits   cmpl_code :2;        /* Completion Code - see 'ComplType'    */
   bits   path      :1;        /* 1 => Use alternate path,             */
                               /* 0 => Use primary path                */
                               /*      (if 'alt_path' is set)          */
   bits   priority  :1;        /* 1 => Priority message                */
/*--------------------------------------------------------------------------*/
   Byte   length;              /* Length of msg or NV to follow        */
                               /* not including any explicit address   */
                               /* field, includes code Byte or         */
                               /* selector Bytes                       */
} ExpMsgHdr;

#else
typedef struct
{
   bits   msg_type  :1;        /* 0 => explicit message                */
                               /*      or unprocessed NV               */
   bits   st        :2;        /* Service Type - see 'ServiceType'     */
   bits   auth      :1;        /* 1 => Authenticated                   */
   bits   tag       :4;        /* Message tag for implicit addressing  */
                               /* Magic cookie for explicit addressing */
/*--------------------------------------------------------------------------*/
   bits   priority  :1;        /* 1 => Priority message                */
   bits   path      :1;        /* 1 => Use alternate path,             */
                               /* 0 => Use primary path                */
                               /*      (if 'alt_path' is set)          */
   bits   cmpl_code :2;        /* Completion Code - see 'ComplType'    */
   bits   addr_mode :1;        /* 1 => Explicit addressing,            */
                               /* 0 => Implicit                        */
                               /* Outgoing buffers only                */
   bits   alt_path  :1;        /* 1 => Use path specified in 'path'    */
                               /* 0 => Use default path                */
   bits   pool      :1;        /* 0 => Outgoing                        */
   bits   response  :1;        /* 1 => Response, 0 => Other            */

/*--------------------------------------------------------------------------*/
   Byte   length;              /* Length of msg or NV to follow        */
                               /* not including any explicit address   */
                               /* field, includes code Byte or         */
                               /* selector Bytes                       */
} ExpMsgHdr;
#endif

/*
 ****************************************************************************
 * Message Header structure for sending and receiving network variables
 * that are processed by the network interface (network interface
 * selection enabled).
 ****************************************************************************
 */

#ifdef BITF_LITTLE_ENDIAN
typedef struct
{
    bits   tag       :4;        /* Magic cookie for correlating         */
                                /* responses and completion events      */
    bits   rsvd0     :2;
    bits   poll      :1;        /* 1 => Poll, 0 => Other                */
    bits   msg_type  :1;        /* 1 => Processed network variable      */
/*--------------------------------------------------------------------------*/
    bits   response  :1;        /* 1 => Poll response, 0 => Other       */
    bits   pool      :1;        /* 0 => Outgoing                        */
    bits   trnarnd   :1;        /* 1 => Turnaround Poll, 0 => Other     */
    bits   addr_mode :1;        /* 1 => Explicit addressing,            */
                                /* 0 => Implicit addressing             */
    bits   cmpl_code :2;        /* Completion Code - see above          */
    bits   path      :1;        /* 1 => Used alternate path             */
                                /* 0 => Used primary path               */
                                /*      (incoming only)                 */
    bits   priority  :1;        /* 1 => Priority msg (incoming only)    */
/*--------------------------------------------------------------------------*/
    Byte   length;              /* Length of network variable to follow */
                                /* not including any explicit address   */
                                /* not including index and rsvd0 Byte   */
} NetVarHdr;

#else
typedef struct
{
    bits   msg_type  :1;        /* 1 => Processed network variable      */
    bits   poll      :1;        /* 1 => Poll, 0 => Other                */
    bits   rsvd0     :2;
    bits   tag       :4;        /* Magic cookie for correlating         */
                                /* responses and completion events      */
/*--------------------------------------------------------------------------*/
    bits   priority  :1;        /* 1 => Priority msg (incoming only)    */
    bits   path      :1;        /* 1 => Used alternate path             */
                                /* 0 => Used primary path               */
                                /*      (incoming only)                 */
    bits   cmpl_code :2;        /* Completion Code - see above          */
    bits   addr_mode :1;        /* 1 => Explicit addressing,            */
                                /* 0 => Implicit addressing             */
    bits   trnarnd   :1;        /* 1 => Turnaround Poll, 0 => Other     */
    bits   pool      :1;        /* 0 => Outgoing                        */
    bits   response  :1;        /* 1 => Poll response, 0 => Other       */
/*--------------------------------------------------------------------------*/
    Byte   length;              /* Length of network variable to follow */
                                /* not including any explicit address   */
                                /* not including index and rsvd0 Byte   */
} NetVarHdr;

#endif

/* Union of all message headers. */

typedef union
{
   ExpMsgHdr  exp;
   NetVarHdr  pnv;
   Byte       noqData[3]; /* Message data for NI_NoQueueCmd */
} MsgHdr;

/*
 ****************************************************************************
 * Network Address structures for sending messages with explicit addressing
 * enabled.
 ****************************************************************************
 */

/* Literals for 'type' field of destination addresses for outgoing messages. */

typedef enum
{
   NI_UNASSIGNED     = 0,
   NI_SUBNET_NODE    = 1,
   NI_NEURON_ID      = 2,
   NI_BROADCAST      = 3,
   NI_IMPLICIT       = 126,    /* not a real destination type */
   NI_LOCAL          = 127     /* not a real destination type */
} AddrType;

#ifndef NS_API_WIN32
  #define UNASSIGNED    NI_UNASSIGNED
  #define SUBNET_NODE   NI_SUBNET_NODE
  #define NEURON_ID     NI_NEURON_ID
  #define BROADCAST     NI_BROADCAST
  #define IMPLICIT      NI_IMPLICIT
  #define LOCAL         NI_LOCAL
#endif

/* Group address structure.  Use for multicast destination addresses. */

#ifdef BITF_LITTLE_ENDIAN
typedef struct
{
    bits   size      :7;        /* Group size (0 => huge group)         */
    bits   type      :1;        /* 1 => Group                           */

    bits   member    :7;        /* Member ID, not used on sends.        */
    bits   domain    :1;        /* Domain index                         */

    bits   retry     :4;        /* Retry count                          */
    bits   rpt_timer :4;        /* Retry repeat timer                   */

    bits   tx_timer  :4;        /* Transmit timer index                 */
    bits   rsvd0     :4;

    Byte       group;           /* Group ID                             */
} SendGroup;
#else

typedef struct
{
    bits   type      :1;        /* 1 => Group                           */
    bits   size      :7;        /* Group size (0 => huge group)         */

    bits   domain    :1;        /* Domain index                         */
    bits   member    :7;        /* Member ID, not used on sends.        */

    bits   rpt_timer :4;        /* Retry repeat timer                   */
    bits   retry     :4;        /* Retry count                          */

    bits   rsvd0     :4;
    bits   tx_timer  :4;        /* Transmit timer index                 */

    Byte       group;           /* Group ID                             */
} SendGroup;
#endif

/* Subnet/node ID address.  Use for a unicast destination address. */

#ifdef BITF_LITTLE_ENDIAN
typedef struct
{
    Byte   type;                /* SUBNET_NODE                          */

    bits   node      :7;        /* Node number                          */
    bits   domain    :1;        /* Domain index                         */

    bits   retry     :4;        /* Retry count                          */
    bits   rpt_timer :4;        /* Retry repeat timer                   */

    bits   tx_timer  :4;        /* Transmit timer index                 */
    bits   rsvd0     :4;

    bits   subnet    :8;        /* Subnet ID                            */
} SendSnode;

#else
typedef struct
{
    Byte   type;                /* SUBNET_NODE                          */

    bits   domain    :1;        /* Domain index                         */
    bits   node      :7;        /* Node number                          */

    bits   rpt_timer :4;        /* Retry repeat timer                   */
    bits   retry     :4;        /* Retry count                          */

    bits   rsvd0     :4;
    bits   tx_timer  :4;        /* Transmit timer index                 */

    bits   subnet    :8;        /* Subnet ID                            */
} SendSnode;
#endif

/* 48-bit NEURON ID destination address. */

#ifdef BITF_LITTLE_ENDIAN
typedef struct
{
    Byte   type;                /* NEURON_ID                            */

    bits   rsvd0      :7;
    bits   domain     :1;       /* Domain index                         */

    bits   retry      :4;       /* Retry count                          */
    bits   rpt_timer  :4;       /* Retry repeat timer                   */

    bits   tx_timer   :4;       /* Transmit timer index                 */
    bits   rsvd1      :4;

    bits   subnet     :8;       /* Subnet ID, 0 => pass all routers     */
    Byte   nid[ NEURON_ID_LEN ];  /* NEURON ID                          */
} SendNrnid;

#else
typedef struct
{
    Byte   type;                /* NEURON_ID                            */

    bits   domain     :1;       /* Domain index                         */
    bits   rsvd0      :7;

    bits   rpt_timer  :4;       /* Retry repeat timer                   */
    bits   retry      :4;       /* Retry count                          */

    bits   rsvd1      :4;
    bits   tx_timer   :4;       /* Transmit timer index                 */

    bits   subnet     :8;       /* Subnet ID, 0 => pass all routers     */
    Byte   nid[ NEURON_ID_LEN ];  /* NEURON ID                          */
} SendNrnid;
#endif

/* Broadcast destination address. */

#ifdef BITF_LITTLE_ENDIAN
typedef struct
{
    Byte   type;                /* BROADCAST                            */

    bits   backlog     :6;      /* Backlog                              */
    bits   rsvd0       :1;
    bits   domain      :1;      /* Domain index                         */

    bits   retry       :4;      /* Retry count                          */
    bits   rpt_timer   :4;      /* Retry repeat timer                   */

    bits   tx_timer    :4;      /* Transmit timer index                 */
    bits   max_responses : 4;

    bits   subnet      :8;      /* Subnet ID, 0 => domain-wide          */
} SendBcast;

#else
typedef struct
{
    Byte   type;                /* BROADCAST                            */

    bits   domain      :1;      /* Domain index                         */
    bits   rsvd0       :1;
    bits   backlog     :6;      /* Backlog                              */

    bits   rpt_timer   :4;      /* Retry repeat timer                   */
    bits   retry       :4;      /* Retry count                          */

    bits   max_responses : 4;
    bits   tx_timer    :4;      /* Transmit timer index                 */

    bits   subnet      :8;      /* Subnet ID, 0 => domain-wide          */
} SendBcast;

#endif


/* turnaround_struct is defined in neuron.h                             */

/* Address formats for special host addresses.                          */

typedef struct
{
   Byte   type;                /*  LOCAL         */
} SendLocal;

typedef struct
{
   Byte    type;                /* IMPLICIT */
   Byte    msg_tag;             /* address table entry number */
} SendImplicit;

/* Union of all destination addresses. */
typedef union
{
   SendGroup      gp;
   SendSnode      sn;
   SendBcast      bc;
   SendNrnid      id;
   SendLocal      lc;
   SendImplicit   im;
} SendAddrDtl;


/*
 ****************************************************************************
 * Network Address structures for receiving messages with explicit
 * addressing enabled.
 ****************************************************************************
 */

/* Received subnet/node ID destination address.  Used for unicast messages. */

typedef struct
{
   bits       subnet :8;
#ifdef BITF_LITTLE_ENDIAN
   bits       node   :7;
   bits              :1;
#else
   bits              :1;
   bits       node   :7;
#endif
} RcvSnode;

/* Received 48-bit NEURON ID destination address. */

typedef struct
{
   Byte   subnet;
   Byte   nid[NEURON_ID_LEN];
} RcvNrnid;

/* Union of all received destination addresses. */
typedef union
{
   Byte       gp;                  /* Group ID for multicast destination   */
   RcvSnode   sn;                  /* Subnet/node ID for unicast           */
   RcvNrnid   id;                  /* 48-bit NEURON ID destination address */
   Byte       subnet;              /* Subnet ID for broadcast destination  */                                  /* 0 => domain-wide */
} RcvDestAddr;

/* Source address of received message.  Identifies */
/* network address of node sending the message.    */
typedef struct
{
   bits   subnet  :8;
#ifdef BITF_LITTLE_ENDIAN
   bits   node    :7;
   bits           :1;
#else
   bits           :1;
   bits   node    :7;
#endif
} RcvSrcAddr;

/* Literals for the 'format' field of RcvAddrDtl. */
typedef enum
{
   ADDR_RCV_BCAST  = 0,
   ADDR_RCV_GROUP  = 1,
   ADDR_RCV_SNODE  = 2,
   ADDR_RCV_NRNID  = 3,
   ADDR_RCV_TURNAROUND = 4      /* available only with VNI */
} RcvDstAddrFormat;

/* Address field of incoming message. */
typedef struct
{
#ifdef BITF_LITTLE_ENDIAN
   bits    format      :6;     /* Destination address type             */
                               /* See 'RcvDstAddrFormat'               */
   bits    flex_domain :1;     /* 1 => broadcast to unconfigured node  */
   bits    domain      :1;     /* Domain table index                   */
#else
   bits    domain      :1;     /* Domain table index                   */
   bits    flex_domain :1;     /* 1 => broadcast to unconfigured node  */
   bits    format      :6;     /* Destination address type             */
                               /* See 'RcvDstAddrFormat'               */
#endif
   RcvSrcAddr  source;         /* Source address of incoming message   */
   RcvDestAddr dest;           /* Destination address of incoming msg  */
} RcvAddrDtl;

/*
 ****************************************************************************
 * Network Address structures for receiving responses with explicit
 * addressing enabled.
 ****************************************************************************
 */

/* Source address of response message. */

typedef struct
{
   bits   subnet   :8;
#ifdef BITF_LITTLE_ENDIAN
   bits   node     :7;
   bits   is_snode :1;        /* 0 => Group response,   */
                              /* 1 => snode response    */
#else
   bits   is_snode :1;        /* 0 => Group response,   */
                              /* 1 => snode response    */
   bits   node     :7;
#endif
} RespSrcAddr;

/* Destination of response to unicast request. */
typedef struct
{
   bits   subnet   :8;
#ifdef BITF_LITTLE_ENDIAN
   bits   node     :7;
   bits            :1;
#else
   bits            :1;
   bits   node     :7;
#endif
} RespSnode;

/* Destination of response to multicast request. */

#ifdef BITF_LITTLE_ENDIAN
typedef struct
{
   bits   subnet   :8;

   bits   node     :7;
   bits            :1;

   bits   group    :8;

   bits   member   :6;
   bits            :2;
} RespGroup;
#else
typedef struct
{
   bits   subnet   :8;

   bits            :1;
   bits   node     :7;

   bits   group    :8;

   bits            :2;
   bits   member   :6;

} RespGroup;
#endif

/* Union of all response destination addresses. */

typedef union
{
   RespSnode  sn;
   RespGroup  gp;
} RespDestAddr;

/* Address field of incoming response. */

typedef struct
{
#ifdef BITF_LITTLE_ENDIAN
   bits                 :6;
   bits     flex_domain :1;    /* 1=> Broadcast to unconfigured node   */
   bits     domain      :1;    /* Domain table index                   */
#else
   bits     domain      :1;    /* Domain table index                   */
   bits     flex_domain :1;    /* 1=> Broadcast to unconfigured node   */
   bits                 :6;
#endif
   RespSrcAddr  source;        /* Source address of incoming response  */
   RespDestAddr dest;          /* Destination address of incoming resp */
} RespAddrDtl;

/* Explicit address field if explicit addressing is enabled. */

typedef union
{
   RcvAddrDtl  rcv;
   SendAddrDtl snd;
   RespAddrDtl rsp;
} ExplicitAddr;

/*
 ****************************************************************************
 * Data field structures for explicit messages and network variables.
 ****************************************************************************
 */

/* Data field for network variables (host selection enabled). */

typedef struct
{
#ifdef BITF_LITTLE_ENDIAN
   bits   NV_selector_hi :6;
   bits   direction      :1;     /* 1 => output NV, 0 => input NV      */
   bits   must_be_one    :1;     /* Must be set to 1 for NV            */
#else
   bits   must_be_one    :1;     /* Must be set to 1 for NV            */
   bits   direction      :1;     /* 1 => output NV, 0 => input NV      */
   bits   NV_selector_hi :6;
#endif

   bits   NV_selector_lo :8;
   Byte   data[MAX_NETVAR_DATA]; /* Network variable data              */
} UnprocessedNV;

/* Data field for network variables (network interface selection enabled). */

typedef struct
{
   Byte       index;                 /* Index into NV configuration table  */
   Byte       rsvd0;
   Byte       data[MAX_NETVAR_DATA]; /* Network variable data */
} ProcessedNV;

/* Data field for explicit messages. */

typedef struct
{
   Byte       code;                  /* Message code                     */
   Byte       data[MAX_NETMSG_DATA]; /* Message data                     */
} ExplicitMsg;

/* Union of all data fields. */
typedef union
{
   UnprocessedNV unv;
   ProcessedNV   pnv;
   ExplicitMsg   exp;
} MsgData;

/*
 ****************************************************************************
 * Message buffer types.
 ****************************************************************************
 */

/* Application buffer when using explicit addressing. */

typedef struct
{
   NI_Hdr       ni_hdr;            /* Network interface header */
   MsgHdr       msg_hdr;           /* Message header */
   ExplicitAddr addr;              /* Network address */
   MsgData      data;              /* Message data */
} ExpAppBuffer;

/* Application buffer when not using explicit addressing. */
typedef struct
{
   NI_Hdr      ni_hdr;            /* Network interface header             */
   MsgHdr      msg_hdr;           /* Message header                       */
   MsgData     data;              /* Message data                         */
} ImpAppBuffer;


/* Linked list of pending messages: */
typedef struct TagMsgList
{
   struct TagMsgList* next_msg;  /* linked list pointer */
   Byte               length;
   ExplicitAddr       addr;
   ServiceType        service;   /* for pending list messages */
   Bool               priority;  /*           "               */
   Bool               auth;      /*           "               */
   Byte               tag;       /*           "               */
   Byte               data[1];   /* followed by the real data */
} MsgList;


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
