//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_app.h#5 $
//

#ifndef _NI_APP_H
#define _NI_APP_H

/****************************************************************************
 *  Filename:     ni_app.h
 *
 *  Description:  This include file contains LonTalk network management
 *                codes and messages for host applications.
 *                If you aren't using DOS you may need to modify
 *                these structures.
 *
 *  Copyright (c) 1996-2000 Echelon Corporation.  All Rights Reserved.
 *
 ***************************************************************************/

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

/* structure of the SI data's header, version 0 and 1.  They are
   alternative definition of snvt_struct which contains the different
   versions as union members. */
typedef struct
{
   Byte    length_hi;           /* length of the entire SI data structure */
   Byte    length_lo;
   Byte    num_netvars;
   Byte    version;             /* version 0 format */
   Byte    mtag_count;
} snvt_struct_v0;

typedef struct
{
   Byte    length_hi;           /* length of the entire SI data structure */
   Byte    length_lo;
   Byte    num_netvars_lo;
   Byte    version;             /* version 1 format */
   Byte    num_netvars_hi;
   Byte    mtag_count;
} snvt_struct_v1;

typedef struct
{
    Byte    length_hi;          /* length of header only.  Others are read via the WINK's subcommands */
    Byte    length_lo;          /* APP_QUERY_NV_INFO and APP_QUERY_NODE_INFO */
    Byte    num_netvars_lo;     /* Max # of NVs which can be defined (static + dynamic) */
    Byte    version;            /* version 2 format */
    Byte    num_netvars_hi;     /* Max # of NVs which can be defined (static + dynamic) */
    Byte    mtag_count;
    NetWord static_nv_count;
    NetWord current_nv_count;   /* Number of currently defined NVs */
    NetWord max_nv_in_use;      /* Maximum NV index.  0xffff indicates none defined */
    NetWord alias_count;
    NetWord node_sd_text_length;
#ifdef BITF_LITTLE_ENDIAN
    bits    unused      :6;
    bits    query_stats :1;
    bits    binding_II  :1;
#else
    bits    binding_II  :1;
    bits    query_stats :1;
    bits    unused      :6;
#endif
} snvt_struct_v2;

/* this is an extension of the v2 data.  We don't want to
   bump the version because that would make the node uninstallable by
   legacy network managers.
 */ 
typedef struct
{
    /* this part is identical to snvt_struct_v2 */
    Byte    length_hi;          /* length of header only.  Others are read via the WINK's subcommands */
    Byte    length_lo;          /* APP_QUERY_NV_INFO and APP_QUERY_NODE_INFO */
    Byte    num_netvars_lo;     /* Max # of NVs which can be defined (static + dynamic) */
    Byte    version;            /* version 2 format */
    Byte    num_netvars_hi;     /* Max # of NVs which can be defined (static + dynamic) */
    Byte    mtag_count;
    NetWord static_nv_count;
    NetWord current_nv_count;   /* Number of currently defined NVs */
    NetWord max_nv_in_use;      /* Maximum NV index */
    NetWord alias_count;
    NetWord node_sd_text_length;
#ifdef BITF_LITTLE_ENDIAN
    bits    unused      :6;
    bits    query_stats :1;
    bits    binding_II  :1;
#else
    bits    binding_II  :1;
    bits    query_stats :1;
    bits    unused      :6;
#endif

    /* extended capabilities */
    snvt_capability_info cap;      
} snvt_struct_v2a;

/* Default NV configuration attributes
*/
typedef struct {
#ifdef BITF_LITTLE_ENDIAN
    bits    nv_service          :2;
    bits    nv_priority         :1;
    bits    nv_auth             :1;
    bits    nv_direction        :1;
    bits    reserved            :3;     /* Must be set to 0 */
#else
    bits    reserved            :3;     /* Must be set to 0 */
    bits    nv_direction        :1;
    bits    nv_auth             :1;
    bits    nv_priority         :1;
    bits    nv_service          :2;
#endif
} nv_dflts;

typedef struct {
#ifdef BITF_LITTLE_ENDIAN
    bits    nv_config_class     :1;
    bits    nv_auth_config      :1;
    bits    nv_priority_config  :1;
    bits    nv_service_type_config :1;
    bits    nv_offline          :1;
    bits    nv_polled           :1;
    bits    nv_sync             :1;
    bits    reserved            :1;       /* Must be set to 0 */
#else
    bits    reserved            :1;       /* Must be set to 0 */
    bits    nv_sync             :1;
    bits    nv_polled           :1;
    bits    nv_offline          :1;
    bits    nv_service_type_config :1;
    bits    nv_priority_config  :1;
    bits    nv_auth_config      :1;
    bits    nv_config_class     :1;
#endif
    Byte    snvt_type_index;               /* use enum SNVT_t */
} nv_attr;

typedef struct {
#ifdef BITF_LITTLE_ENDIAN
    bits    reserved1           :3;     /* Must be set to 0 */
    bits    nm_supplied         :1;     /* For QUERY_NV_INFO/NV_INFO_DESC, name is appended to message */
    bits    sd                  :1;     /* NV SD text is available */
    bits    nm                  :1;     /* NV Name is available */
    bits    re                  :1;     /* Rate Estimate data is available */
    bits    mre                 :1;     /* Max Rate Est data is available */
#else
    bits    mre                 :1;     /* Max Rate Est data is available */
    bits    re                  :1;     /* Rate Estimate data is available */
    bits    nm                  :1;     /* NV Name is available */
    bits    sd                  :1;     /* NV SD text is available */
    bits    nm_supplied         :1;     /* For QUERY_NV_INFO/NV_INFO_DESC, name is appended to message */
    bits    reserved1           :3;     /* Must be set to 0 */
#endif
} nv_exten;

typedef struct {
    NetWord count;        /* Total # of NVs in current array, 0 for non-arrays */
    NetWord element;      /* Element # of current NV in array, 0 for non-arrays */
} nv_array;

/* Values for nv_desc.origin field
*/
#define     NV_UNDEFINED    0x00    /* NV is currently undefined */
#define     NV_STATIC       0x01    /* NV is statically defined */
#define     NV_DYNAMIC      0x02    /* NV is dynamically defined  */

typedef struct  {
#ifdef BITF_LITTLE_ENDIAN
    bits        origin:3;      /* How NV was defined */
    bits        length:5;      /* # of bytes in NV value */
#else
    bits        length:5;      /* # of bytes in NV value */
    bits        origin:3;      /* How NV was defined */
#endif
    nv_dflts    dflts;         /* Default NV Configuration settings */
    nv_attr     attr;          /* Basic NV attributes */
    nv_exten    ext;           /* Available NV Extension attributes */
    nv_array    array;         /* NV array attributes */
} nv_desc;

typedef enum
{
    APP_WINK            = 0,
    APP_INSTALL         = 1,
    APP_NV_DEFINE       = 2,
    APP_NV_REMOVE       = 3,
    APP_QUERY_NV_INFO   = 4,
    APP_QUERY_NODE_INFO = 5,
    APP_UPDATE_NV_INFO  = 6
} AppCommand;

/* Request to define new NV(s) on the node. The app should respond successfully even
   if the NV is already defined.
*/
typedef struct
{
    Byte        app_command;    /* APP_NV_DEFINE (AppCommand enum) */
    NetWord     nv_index;       /* New NV index */
    NetWord     array_len;      /* # of elements in NV array,
                                   0 if new NV is not an array */
    Byte        nv_length;      /* # of bytes in NV value (1-31) */
    nv_dflts    dflts;          /* Default NV Configuration settings */
    nv_attr     attr;           /* NV self-documentation attributes */

    /* The following field is optional.  Devices that support this format 
       must set the EXTCAP_SUPPORTS_XNVT flag, and must accept commands 
       without this field. */
    xnvt_record xnvt;       /* extended network variable type information. */

} NM_app_define_nv;

#define LEGACY_DEFINE_APP_NV_SIZE (MIN_SIZEOF(NM_app_define_nv, attr))

typedef struct
{
    Byte                code;               /* NM_install */
    NM_app_define_nv    data;
} CMD_app_define_nv;

typedef struct
{
    Byte    code;               /* NM_install_fail/NM_install_success */
} CMD_app_response;

/* Request to remove a set of NVs. The host should respond successfully
   even if 1 or more of the specified NVs does not exist.
*/
typedef struct
{
    Byte    app_command;        /* APP_NV_REMOVE (AppCommand enum) */
    NetWord nv_index;           /* Index of first NV to remove*/
    NetWord nv_count;           /* # of NVs to remove */
} NM_app_remove_nv;

typedef struct
{
    Byte                code;               /* NM_install */
    NM_app_remove_nv    data;
} CMD_app_remove_nv;


/************************************************************************
    Query Network Variable Self-documentation data
 ************************************************************************/

#define NV_INFO_DESC         0  // Deprecated - includes 1 byte SNVT ID
#define NV_INFO_RATE_EST     1
#define NV_INFO_NAME         2
#define NV_INFO_SD_TEXT      3
#define NV_INFO_SNVT_INDEX   4  // Deprecated - uses 1 byte SNVT ID.
#define NV_INFO_DESC_2       5  // Includes 2 byte SNVT ID
#define NV_INFO_SNVT_INDEX_2 6

typedef struct
{
    Byte    app_command;        /* APP_QUERY_NV_INFO (AppCommand enum) */
    Byte    nv_info;            /* Requested NV Info (NV_INFO...) */
    NetWord nv_index;
    union   {
        struct {
            NetWord offset;
            Byte    length;
        } sd;                   /* For requesting NV_INFO_SD_TEXT */
    } addl;
} NM_app_query_nv_info;

typedef struct
{
    Byte                    code;               /* NM_install */
    NM_app_query_nv_info    data;
} CMD_app_query_nv_info;

typedef union
{
    /* Response for requested info NV_INFO_DESC */
    struct {
        /* Deprecated.  Only supports 1 byte SNVTs.  However, all devices supporting 
           SI version 2 must support this command. */
        nv_desc     desc;
        char        nv_name[NV_NAME_LEN]; /* Optional field - Included only if
                                             desc.ext.nm_supplied is set */
    } desc;

    /* Response for requested info NV_INFO_RATE_EST */
    struct {
        Byte    nv_rate_est;        /* Encoded rate estimate. Only valid if 're' is set in NV desc */
        Byte    nv_max_rate_est;    /* Encoded max rate estimate. Only valid if 'mre' is set in NV desc */
    } rate;

    /* Response for requested info NV_INFO_NAME */
    char    nv_name[NV_NAME_LEN];   /* NV name. Only valid if 'nm' set in NV desc */

    /* Response for requested info NV_INFO_SD_TEXT */
    struct {
        Byte    length;
        Byte    text[1];
    } sd;

    /* Response for requested info NV_INFO_SNVT_INDEX - Deprecated */
    Byte    snvt_type_index;

    /* Response for requested info NV_INFO_DESC_2 */
    struct {
        nv_desc      desc;
        xnvt_record  xnvt;  /* extended network variable type information. */
        Byte         nv_length;   /* NV length, to support NVs > 32 bytes long. */
        char         nv_name[NV_NAME_LEN]; /* Optional field - Included only if desc.ext.nm_supplied is set */
    } desc2;

    /* Response for requested info NV_INFO_SNVT_INDEX_2  */
    xnvt_record xnvt;   

} NM_app_query_nv_info_response;

typedef struct
{
    Byte                             code;  /* NM_install_fail/NM_install_success */
    NM_app_query_nv_info_response    data;
} CMD_app_query_nv_info_response;

/************************************************************************
    Update Network Variable Self-documentation data
 ************************************************************************/

typedef struct
{
    Byte    app_command;        /* APP_UPDATE_NV_INFO (AppCommand enum) */
    Byte    nv_info;            /* NV Info to be updated (NV_INFO...) */
    NetWord nv_index;           /* NV index - If the target NV represents an array,
                                    then the update applies to all elements of the
                                    array.
                                */
    union {
        /* Data for updated info NV_INFO_NAME */
        char    nv_name[NV_NAME_LEN];   /* NV name (ascii) - 0 terminated if < 16 characters */

        /* Data for updated info NV_INFO_SD_TEXT */
        struct {
            Byte    length;
            NetWord offset;
            Byte    text[1];            /* May not be 0 terminated */
        } sd;

        /* Data for updated info NV_INFO_RATE_EST */
        struct {
#ifdef BITF_LITTLE_ENDIAN
            bits    update_re   : 1;    /* Update rate est with nv_rate_est */
            bits    update_mre  : 1;    /* Update max rate est with nv_max_rate_est */
            bits    clear_re    : 1;    /* Clear 'ext' rec 're' field */
            bits    clear_mre   : 1;    /* Clear 'ext' rec 'mre' field */
            bits    reserved    : 4;    /* Must be set to 0 */
#else
            bits    reserved    : 4;    /* Must be set to 0 */
            bits    clear_mre   : 1;    /* Clear 'ext' rec 'mre' field */
            bits    clear_re    : 1;    /* Clear 'ext' rec 're' field */
            bits    update_mre  : 1;    /* Update max rate est with nv_max_rate_est */
            bits    update_re   : 1;    /* Update rate est with nv_rate_est */
#endif
            Byte    nv_rate_est;        /* Encoded rate estimate.  */
            Byte    nv_max_rate_est;    /* Encoded max rate estimate.  */
        } rate;

        /* Data for updated info NV_INFO_SNVT_INDEX  - deprecated */
        Byte    snvt_type_index;

        /* Data for updated info NV_INFO_SNVT_INDEX_2  */
        xnvt_record xnvt;   
    } info;
} NM_app_update_nv_info;

typedef struct
{
    Byte                    code;       /* NM_install */
    NM_app_update_nv_info   data;
} CMD_app_update_nv_info;

/************************************************************************
    Query Node Self-documentation data
 ************************************************************************/

#define NODE_INFO_SD_TEXT     3

typedef struct
{
    Byte    app_command;        /* APP_QUERY_NODE_INFO (AppCommand enum) */
    Byte    node_info;          /* Requested Node Info (NODE_INFO...) */
    union   {
        struct {
            NetWord offset;
            Byte    length;
        } sd;                   /* For requesting NODE_INFO_SD_TEXT */
    } addl;
} NM_app_query_node_info;

typedef struct
{
    Byte                    code;               /* NM_install */
    NM_app_query_node_info data;
} CMD_app_query_node_info;

typedef union
{
    /* Response for requested info NODE_INFO_SD_TEXT */
    struct {
        Byte    length;
        Byte    text[1];        /* May not be 0 terminated */
    } sd;
} NM_app_query_node_info_response;

typedef struct
{
    Byte                            code;  /* NM_install_fail/NM_install_success */
    NM_app_query_node_info_response data;
} CMD_app_query_node_info_response;


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
