//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ns_batch.h#4 $
//

/***************************************************************
 *  Filename:     ns_batch.h
 *
 *  Description:  This file contains definitions for the utilities 
 *                used to batch up multiple NSS service invocations.
 *
 *  Copyright (c) 1998-2000 Echelon Corporation.  All Rights Reserved.
 *
 ****************************************************************/
#ifndef _NS_BATCH_H
#define _NS_BATCH_H

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

/**************************************************************************
  ********************* BATCH SERVICE SUPPORT FUNCTIONS *******************

  Following are some support functions to use to manage the NS_BATCH_SERVICE
  service parameters and results.
  ***********************************************************************/

/**************************************************************************

   Function:  NsInitBatchServiceBlock

   Desc:      This function is used to allocate service parameter and result 
              blocks for the NS_BATCH_SERVICE service, and to initialize the 
              header portion of the service parameter block.  Use the 
              NsInitBachServiceElement() function to initialize individual 
              elements of the service parameter block.  Use NsFreeBatchData() 
              to free the memory allocated by this function.

  Parameters
    Parameter               Description


    numberOfServices        The number of services to perform.  If 
                            optionsFlag contains the BATCH_SERVICE_SPECIFIED 
                            option, then the service control block will 
                            contain numberOfServices service control blocks.

    optionFlags	            A set of flags, which can be OR�d together.  
                            Refer to definition in NS_SRVC.h.

    serviceParameterElementSize	
                            The length, in bytes of each service parameter 
                            element.  Typically this would be set to 
                            "sizeof(Ns<serviceName>)".

    resultDataSize	        For fixed sized results, this represents the length, 
                            in bytes of the result data - typically 
                            sizeof(NsResult<serviceName>).  For variable 
                            sized results, this represents the total size 
                            reserved for result data.

    index	                Used only when iteration is used, indicates the 
                            number of elements to skip (0 meaning none).  
                            Used to emulate "indexed" access - though not 
                            very efficiently.

    serviceCode             Service code for this service.

    pFirstServiceParameter	Pointer to first service parameter block.

    pBatchControlBlock      The following is returned via this pointer:  
                            Pointer to the service control block, pointer to 
                            the result block, and sizes of each.  This data 
                            will be used by other batch functions and 
                            contains the data necessary to invoke the batch 
                            service.

***********************************************************************/
typedef struct NsBatchControlBlock
{
	NsBatchServiceHdr	    *pBatchServiceParms;
	NetWord	                 serviceSize;
	NsResultBatchServiceHdr	*pResult;
	NetWord	                 resultSize;
} NsBatchControlBlock;  

extern SrSts NsInitBatchServiceBlock(
                 Byte	               numberOfServices, 
	             BatchServiceOptions   optionFlags, 
	             NetWord	           serviceParameterElementSize, 
	             NetWord	           resultDataSize,
	             NetWord	           index,
	             ServiceCode	       serviceCode,
                 void                 *pFirstServiceParameter,
	             NsBatchControlBlock  *pBatchControlBlock);

/**************************************************************************

   Function:  NsInitBatchServiceElement

   Desc:      This function is used to initialize a service block element in 
              a NS_BATCH_SERVICE service parameter block previously allocated 
              and initialized using NsInitBatchServiceBlock(). 

  Parameters
    Parameter               Description
    pBatchControlBlock	    Pointer to the batch service control block 
                            initialized by NsInitBatchServiceBlock.

    index	                Index into service parameter array, starting at 0.

    pServiceData	        Service parameter block to be stored at this element.

    serviceDataLength	    The length of the data pointed to by pServiceData.


***********************************************************************/
extern SrSts NsInitBatchServiceElement(NsBatchControlBlock	*pBatchControlBlock, 
	                                   Byte 	             index, 
	                                   const void 	        *pServiceData,
	                                   NetWord	             serviceDataLength);  

/**************************************************************************

   Function:  NsGetBatchResultElement

   Desc:      This function is used to access the "nth" result element of a 
              NS_BATCH_SERVICE.  The return status of this function 
              represents the status corresponding to the specified result 
              entry.

  Parameters
    Parameter               Description
    pBatchControlBlock	    Pointer to the batch service control block 
                            initialized by NsInitBatchServiceBlock.

    index	                The index into the array of result elements.

    ppResultEntry	        *ppResultEntry gets set to point into the result 
                            buffer at the start of the result data for this 
                            element.

    ppCorrespondingServiceParms	
                            This is used for iterative batch services to 
                            correlate the result with the invocation.  This 
                            is done by setting *ppCorrespondingServiceParms 
                            to point to the service parameters portion of the 
                            block, updated with the iterator value.  For 
                            example, if the result corresponds to a 
                            PROP_NV_CONN_INFO for node 3, nv 2, then *
                            ppCorrespondingServiceParms would point to an 
                            NvObjRef block with the following fields:

	                            category:	CAT_NVMT
	                            property:	PROP_NV_CONN_INFO
	                            nodeHandle:	3
	                            nvMtIndex:	Nv2

                            Note that to implement this, this function points 
                            to the original service portion of the batch 
                            service control block, and modifies the iterator 
                            portion of the service from the result data.  
                            Thus the original iterator data is lost.

                            May be NULL.

    pResultLength	        Receives the length of the result (typically used 
                            for variable length results).  May be NULL.

***********************************************************************/

extern SrSts NsGetBatchResultElement(NsBatchControlBlock *pBatchControlBlock, 
	                                 Byte                 index, 
	                                 void 	            **ppResultEntry,
	                                 void               **ppCorrespondingServiceParms,
	                                 NetWord             *pResultLength);  

/**************************************************************************

   Function:  NsSetupNextBatchService

   Desc:      This function is used to set the service parameters up for 
              getting another batch of services.  Used for iterative services only.

  Parameters
    Parameter               Description
    pBatchControlBlock	    Pointer to the batch service control block 
                            initialized by NsInitBatchServiceBlock.

***********************************************************************/

extern SrSts NsSetupNextBatchService(NsBatchControlBlock *pBatchControlBlock);

/**************************************************************************

   Function:  NsFreeBatchData

   Desc:      This function is used to free the service parameter block and 
              result block allocated by NsInitBatchServiceBlock().

  Parameters
    Parameter               Description
    pBatchControlBlock	    Pointer to the batch service control block 
                            initialized by NsInitBatchServiceBlock.

***********************************************************************/

extern SrSts NsFreeBatchData(NsBatchControlBlock *pBatchControlBlock);


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
