//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_ns.h#5 $
//

#ifndef _NI_NS
#define _NI_NS

/*************************************************************
   These prototypes are defined in NI32.DEF but is meant only
   for NS32.DLL, not application
 ************************************************************/

#ifndef NS_API_WIN32
NICode NiSetRcvTimer(Byte newRcvTimer);
Byte NiGetRcvTimer(void);
Byte NiGetRefid(void);                 /* Return an available reference ID */
Byte    NiGetLastRequestTag(void);
#endif

CLOCK_TICKS NiGetLastMsgRcvTime(void);

/* Return the reference ID (Tag) from the most recently received request.
*/


void NiMsgDisplay(const ExpAppBuffer* msg_ptr);

NICode NiRegisterEventHandle(HANDLE NiRegisterEventHandle);  /* Set event to be toggled when messages are available */
NICode NiSetAuthKeyOverride(int dom_idx, Bool override, const struct AuthKeyEx *pKey);

typedef void (*MsgDisplayCallback)(const Byte *msgDataPtr, size_t dataLength, Bool swapRequired);

void NiSetMsgDisplayCallback(MsgDisplayCallback msgDisplayCallback);

/* Set flag indicating whether layer above is a Network Services host.
*/
NICode NiSetNsHost(Bool isNsHost);

void NiEnablePredefinedMsgPoint(NiPredefinedMpIndex mpIndex, Bool toEnable);


#ifdef NI_MEM_DEBUG
  #define NiMalloc(size)          _NiMalloc(size, __FILE__, __LINE__)
  #define NiFree(ptr)             _NiFree(ptr, __FILE__, __LINE__)
  #define NiRealloc(ptr, size)    _NiRealloc(ptr, size, __FILE__, __LINE__)

  void *_NiMalloc(unsigned long size, char *file, int line);
  void *_NiRealloc(void *ptr, unsigned long size, char *file, int line);
  void _NiFree(void *ptr, char *file, int line);

#else
  #define NiMalloc(size)          _NiMalloc(size)
  #define NiFree(ptr)             _NiFree(ptr)
  #define NiRealloc(ptr, size)    _NiRealloc(ptr, size)

  void *_NiMalloc(unsigned long size);
  void *_NiRealloc(void *ptr, unsigned long size);
  void _NiFree(void *ptr);
#endif


#endif
