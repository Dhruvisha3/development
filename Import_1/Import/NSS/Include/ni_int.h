//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_int.h#4 $
//

#ifndef _NI_INT_H
#define _NI_INT_H

/****************************************************************************
 *  Filename:     ni_int.h
 *
 *  Description:  This file is the main internal include file for the Network
 *                Interface API (NI API).
 *
 *
 *  Copyright (c) 1995-2000 Echelon Corporation.  All Rights Reserved.
 *
 ***************************************************************************/

#ifndef NS_API_WIN32
#include "ldv_intf.h"
    /* stub definitions for lon layer */
typedef Byte RefID;
#define LON_REFID_ANY 15
#endif

#include "ni_ns.h"

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef NS_API_WIN32
#include "NiVni_e.h"
#endif

/* Forward references for internal functions: */
#ifndef NS_API_WIN32
NICode NiGetBaseFwVersion(Byte *pVersion); /* Return network i/f base firmware version */
void   InitResponseList(void);
NICode NiOpen(const char* deviceName, unsigned long openOpts);
#endif


#ifdef NS_API_WIN32
#define LOCK_MSG_READ(pNiData)       {EnterCriticalSection(&pNiData->csReadMsg); pNiData->exclusiveReadMsg = TRUE;}
#define UNLOCK_MSG_READ(pNiData)     {pNiData->exclusiveReadMsg = FALSE; if (pNiData->csInit) {LeaveCriticalSection(&pNiData->csReadMsg);}}
#else

#define LOCK_MSG_READ(pNiData)
#define UNLOCK_MSG_READ(pNiData)
#endif

#ifdef NI_DEBUG
  #define NIPRINTF(x)     NiPrintf x
#else
  #define NIPRINTF(x)
#endif

#ifdef _MSC_VER
#pragma pack()
#endif
typedef struct NiData
{
    NiInterfaceHandle   niHandle;
    Bool                niTracing;
    Bool                verboseTracing;
    FILE*               traceFileHandle;
    Bool                niOpened;      /* True if the network interface (or VNI) is opened. */
    Bool                nsHost;        /* This gets set to TRUE by the NS API only */
    time_t              niWaitTime[NI_NUM_WAIT_TIMES];    /* Wait time for network interface */
    NiCallbackData      callbackTable[MAX_CALLBACK_NUM];
    Bool                niCallbacksEnabled;
        // This could not be a thread local storage because 
        // the response may be done from a different service thread 
    Byte                lastRequestTag;

#ifdef NS_API_WIN32
    CLOCK_TICKS          lastMsgRecvTime;
    CRITICAL_SECTION     csReadMsg;
    Bool                 csInit;
    Bool                 exclusiveReadMsg;
    Bool                 isVniInterface;
    config_data_struct              neuron_config_data;
    read_only_data_struct_complete  neuron_read_only_data_complete;
    domain_struct                   neuron_domain_entry;
    address_struct_ext              neuron_address_entry;
    HANDLE               stackHandle;
#endif


} NiData;

#ifdef NS_API_WIN32
#define MAX_NI_ENTRIES 1000
extern NiData *niDataDirectory[MAX_NI_ENTRIES];
extern NiThreadLocalData *NiGetApiTLS(void);
#define GetCurrentNiData() (*(NiGetApiTLS()->currentNiDataPointer))

extern NICode VniServerOpen(void);

extern void VniServerClose(void);

extern NICode VniStackOpen(NiData *pNiData, const NiVniInterfaceDesc *pNiVniInterfaceDesc,
                           NiVniOpenOptions options);
extern void VniStackClose(NiData *pNiData);
#define NI_MAX_OPEN_NETWORK_INTERFACES 256
#else
#define MAX_NI_ENTRIES 1
extern NiData currentNiData;
#define GetCurrentNiData() (&currentNiData;)
#endif

#endif

