@Echo Off

Rem check commadline arguments
if "%1" == "" goto argu
if "%2" == "" goto argu
if "%3" == "" goto argu

Rem set commadline arguments
Set filePath=%1
Set /a newBuildNumber =%2
Set GITBRANCH =%~3

Rem Get latest revision from remote repository
git checkout "%filePath%"
if ErrorLevel 1 (
echo There is an error in checkout.
echo Please check the file is exist and filepath should be in lower case.
Goto done_error		
)

Rem     fetch current (previous) version number header file
Echo Checking version number header...
Call GetLnsVer.bat -e -f "%filepath%"
Echo   Version.h is currently at V%VerMajor%.%VerMinor%.%VerBuild%

Rem     prepend '0's if less than 10, and/or less than 100
If %newBuildNumber% LSS 10 (Set newBuildNumber=00%newBuildNumber%) Else If %newBuildNumber% LSS 100 Set newBuildNumber=0%newBuildNumber%

Set VersionNumber=%VerMajor%.%VerMinor%.%newBuildNumber%
echo %VersionNumber%

If %newBuildNumber% EQU %VerBuild% (
	echo .
	echo No version update required...
	Goto have_uptoDate_version
)

if "%BuildScriptDebug%" == "TRUE" (
	echo Debugging script: skipping version file checkin
	goto have_UptoDate_version
)

:must_edit_version

Echo ...
Echo ... Version.h is old (%VerBuild%), must edit
Echo .

Echo Call setlnsver -f "%filepath%" %newBuildNumber%
Call SetLnsVer.bat -f "%filepath%" %newBuildNumber%
If ErrorLevel 1 Goto done_error


Set VerBuild=%newBuildNumber%

Rem Stag the changes of version.h file to commit.
Rem Submit the open changelist
Echo ... 
Echo ... Checking in version file changes
Echo ...

git add %filePath%
git commit -m "Version updates for LonScanner Build %newBuildNumber%" -- %filePath%

rem echo Push the data using git push origin %GITBRANCH%
git push origin %GITBRANCH%

if ErrorLevel 1 (
echo There is an error in push data to GIT on branch %GITBRANCH%
git reset --hard origin/%GITBRANCH%
Goto done_error		
)

Set LabeledChangesDone=1
rem git log -1

:have_UptoDate_version
goto done

:argu
Echo Insufficent arguments. Please provide filePath, version and branch

:done_error
rem Echo on
Echo Build Failed
goto end

:done
echo completed sucessfully

:end