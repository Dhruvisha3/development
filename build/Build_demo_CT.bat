@Echo Off
Set Import_view_File=view\Import_LonMaker_Dev.txt


set component_Repo=E:\Bitbucket\IzotCT\component\
set development_repo=E:\Bitbucket\IzotCT\development\
set export_repo=E:\Bitbucket\IzotCT\export\

set component_Repo_branch=Dev
set development_repo_branch=master
set export_repo_branch=Dev

call build\GIT_Import_Export.bat %Import_view_File% "Data imported using File %Import_view_File% forLonMaker"
if ErrorLevel 1 Goto done_error


:done_success
Echo ...................................................
Echo ...................................................
Echo ......... Build Completed Sucessfully..............
Echo ...................................................
Echo ...................................................
goto done


:done_error
Echo ...................................................
Echo ...................................................
Echo ...........Build Complete with Error...............
Echo ...................................................
Echo ...................................................
goto done


:done
echo. 
echo Build_done