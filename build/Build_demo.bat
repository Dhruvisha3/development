@Echo Off
Set Import_view_File=view/Import_Demo.txt
set Export_view_File=view/Export_Demo.txt
Set Import_view_File_1=view/Import_Demo1.txt
set Export_view_File_1=view/Export_Demo1.txt
set Version_File=include/version.h

set alpha_repo=E:/SLS_ImEx/alpha/
set beta_repo=E:/SLS_ImEx/beta/
set development_repo=E:/SLS_ImEx/development/
set export_repo=E:/SLS_ImEx/export/

set alpha_repo_branch=v1.0
set beta_repo_branch=v2.0
set development_repo_branch=master
set export_repo_branch=v3.0

call build/GIT_Import_Export.bat %Import_view_File% "Data imported using File %Import_view_File% for version number %1"
if ErrorLevel 1 Goto done_error
call build/GIT_Import_Export.bat %Import_view_File_1% "Data imported using File %Import_view_File_1% for version number %1"
if ErrorLevel 1 Goto done_error

call build/Change_version.bat %Version_File% %1 master
if ErrorLevel 1 Goto done_error

call build/GIT_Import_Export.bat %Export_view_File% "Data exported using File %Export_view_File% for vesrion number %1"
if ErrorLevel 1 Goto done_error
call build/GIT_Import_Export.bat %Export_view_File_1% "Data imported using File %Export_view_File_1% for version number %1"
if ErrorLevel 1 Goto done_error

:done_success
Echo ...................................................
Echo ...................................................
Echo ......... Build Completed Sucessfully..............
Echo ...................................................
Echo ...................................................
goto done


:done_error
Echo ...................................................
Echo ...................................................
Echo ...........Build Complete with Error...............
Echo ...................................................
Echo ...................................................
goto done


:done
echo. 
echo Build_done