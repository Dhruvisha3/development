///////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// (c) Copyright 1999-2014, Echelon Corporation.  All rights reserved.
//
// Version Macros
//
// This file is included by (possibly customized) VERSION.H to set LBVER
// version information and can also be included in the RC2 file to set
// resource version information.
//
//////////////////////////////////////////////////////////////////////////////

#ifndef __VERMACRO_H__
#define __VERMACRO_H__


//
// Default copyright date range.
// The values can be overridden by individual source files or
// by passing options to the compiler.
//

#ifndef COPYRIGHT_FROM
#  define COPYRIGHT_FROM                1992
#endif

#ifndef COPYRIGHT_TO
#  define COPYRIGHT_TO                  2020
#endif


//////////////////////////////////////////////////////////////////////////////

// these (mystical) macros take the numbers above and create string equivalents
#define STRING(s)                       #s
#define XSTRING(s)                      STRING(s)
#define JOIN(x, y)                      x ## y
#define JOIN3(x, y, z)                  x ## y ## z
#define XJOIN(x, y)                     JOIN(x, y)
#define XJOIN3(x, y, z)                 JOIN3(x, y, z)
#define STRING_JOIN(x, y)               XSTRING(XJOIN(x, y))

// WARNING - these are not decimal numbers (e.g. beware 08 & 09)
//     - use the _D versions below for decimal equivalents
#define VER_MAJOR                       RELEASE_NUMBER_MAJOR
#define VER_MINOR                       XJOIN(RELEASE_NUMBER_MINOR1, RELEASE_NUMBER_MINOR2)
#ifdef RELEASE_NUMBER_BUILD0
    // three-digit build number (beware of comparisons with older two-digit build numbers)
#   define VER_BUILD                    XJOIN3(RELEASE_NUMBER_BUILD0, RELEASE_NUMBER_BUILD1, RELEASE_NUMBER_BUILD2)
#else
    // two-digit build number (for compatibility)
#   define VER_BUILD                    XJOIN(RELEASE_NUMBER_BUILD1, RELEASE_NUMBER_BUILD2)
#endif
#define VER_MM                          XJOIN(VER_MAJOR, VER_MINOR)
#define VER                             XJOIN(VER_MM, VER_BUILD)

#define VER_MAJOR_D                     RELEASE_NUMBER_MAJOR
#define VER_MINOR_D                     (10 * RELEASE_NUMBER_MINOR1 + RELEASE_NUMBER_MINOR2)
#ifdef RELEASE_NUMBER_BUILD0
    // three-digit build number (beware of comparisons with older two-digit build numbers)
#   define VER_BUILD_D                  (100 * RELEASE_NUMBER_BUILD0 + 10 * RELEASE_NUMBER_BUILD1 + RELEASE_NUMBER_BUILD2)
#else
    // two-digit build number (for compatibility)
#   define VER_BUILD_D                  (10 * RELEASE_NUMBER_BUILD1 + RELEASE_NUMBER_BUILD2)
#endif
#define VER_MM_D                        (100 * VER_MAJOR_D + VER_MINOR_D)
#ifdef RELEASE_NUMBER_BUILD0
    // three-digit build number (beware of comparisons with older two-digit build numbers)
#   define VER_D                        (1000 * VER_MM_D + VER_BUILD_D)
#else
    // two-digit build number (for compatibility)
#   define VER_D                        (100 * VER_MM_D + VER_BUILD_D)
#endif

#define VER_STRING2(v1, v2)             STRING(v1) "." STRING(v2) "\0"
#define VER_STRING3(v1, v2, v3)         STRING(v1) "." STRING(v2) "." STRING(v3) "\0"

#if COPYRIGHT_FROM == COPYRIGHT_TO
#   define COPYRIGHT_STRING(from, to)   "Copyright (c) Echelon Corporation " STRING(from) "\0"
#else
#   define COPYRIGHT_STRING(from, to)   "Copyright (c) Echelon Corporation " STRING(from) "-" STRING(to) "\0"
#endif


//////////////////////////////////////////////////////////////////////////////


// argument to FILEVERSION and PRODUCTVERSION resources (these are used by InstallShield)
#define VER_RES1                        VER_MAJOR,VER_MINOR,VER_BUILD,0

// argument to FileVersion and/or ProductVersion string table resources
#define VER_RES2                        VER_STRING3(VER_MAJOR, VER_MINOR, VER_BUILD)
#define VER_RES3                        VER_STRING2(VER_MAJOR, VER_MINOR)

// used as type library version
#define VER_RES4                        VER_MAJOR.VER_MINOR

// warn about a previously introduced typo
#ifdef VER_EXTA_COPYRIGHT
#   pragma message("TYPO: Please rename macro VER_EXTA_COPYIGHT to VER_EXTRA_COPYRIGHT!")
#   define VER_EXTRA_COPYRIGHT          VER_EXTA_COPYRIGHT
#endif

// argument to LegalCopyright string table resource
#ifndef VER_COPYRIGHT
#   ifndef VER_EXTRA_COPYRIGHT
#       define VER_EXTRA_COPYRIGHT      ""
#   endif
#   define VER_COPYRIGHT                COPYRIGHT_STRING(COPYRIGHT_FROM, COPYRIGHT_TO) VER_EXTRA_COPYRIGHT
#endif

// argument to CompanyName string table resource
#ifndef VER_COMPANY
#   ifndef VER_EXTRA_COMPANY
#       define VER_EXTRA_COMPANY        ""
#   endif
#   define VER_COMPANY                  "Echelon Corporation" VER_EXTRA_COMPANY
#endif

// argument to LegalTrademarks string table resource
#ifndef VER_TRADEMARKS
#   ifndef VER_EXTRA_TRADEMARKS
#       define VER_EXTRA_TRADEMARKS     ""
#   endif
#   define VER_TRADEMARKS               "Echelon, LON, the Echelon logo, LonWorks, NodeBuilder, LonTalk, LonPoint, Neuron, 3120, 3150, LNS, i.LON, ShortStack, Pyxos, LonMaker, IzoT, LonLink, LonResponse, LonSupport, OpenLDV, LonScanner, and LonBridge are trademarks of Echelon Corporation that may be registered in the United States and other countries. For a complete list of registered trademarks see the Echelon Web site at www.echelon.com. All rights reserved." VER_EXTRA_TRADEMARKS
#endif

// argument to ProductName string table resource
#ifndef VER_PRODUCT
#   define VER_PRODUCT                  "Echelon IzoT Network Services Server"
#endif

// backwards-compatibility macros - use the VER_ versions for new code
#define COPYRIGHT                       VER_COPYRIGHT  "\0"
#define COMPANY                         VER_COMPANY    "\0"
#define TRADEMARKS                      VER_TRADEMARKS "\0"
#define PRODUCT                         VER_PRODUCT    "\0"


// 'resync' version numbers to allow eventual resync with above numbers
// for released DLLs with existing major version higher than those used here
// e.g. last released LDV32 version number:          4.16.0.0
//      resyncing version number using VER_RES*_RS(4)
//      when normal version number would be 3.06.02: 4.306.2.0
// i.e. combines major and minor number into minor number field
#define VER_RES1_RS(M)                  M, VER_MM, VER_BUILD, 0
#define VER_RES2_RS(M)                  VER_STRING3(M, VER_MM, VER_BUILD)
#define VER_RES3_RS(M)                  VER_STRING2(M, VER_MM)
#define VER_RES4_RS(M)                  M.VER_MM

#endif // __VERMACRO_H__
