//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_nmexp.h#13 $
//
// This file contains definitions for expanded network management commands.
// This is not to be confused with the EXTENDED command set (ECS).
// The first network management version using the EXPANDED command set is
// version 2.  
#ifndef _NI_NMEXP_H
#define _NI_NMEXP_H

#include "SecNmMsgs.h"

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

/********************************************************************
    
    NMEXP Message Structure:
        
        0                1           2          
    +--------------+-----------+------------------------------------>
    | NM Code 0x60 | Command   | data
    |(NM_expanded) |           |           
    +--------------+-----------+------------------------------------>

 ********************************************************************/

#define INITIAL_NM_VER     1
#define INITIAL_NM_EXP_VER 2           /* the first valid expanded version */
    /* return TRUE if it supports the expanded command set. */
#define IS_NMEXP_NODE(ver_nm_max) (ver_nm_max >= INITIAL_NM_EXP_VER) 
#define CURRENT_NM_VER 2           /* current NM version number */
typedef Byte StandardNMVersion; // Standard (as opposed to ECS) NM version number.

#define IS_EXP_OMA_NODE(capabilities) ((capabilities) & NMEXP_CAP_OMA ? TRUE : FALSE)
// The following enum defines the expanded network management version sub-command
// values.
typedef enum _NmExp_command {
	NMEXP_QUERY_COMMAND_SET_VERSION			= 0x01,		// V2
    NMEXP_QUERY_MEMORY_MAP                  = 0x05,     // V2 - introduced in version 18
	NMEXP_JOIN_DOMAIN_NO_KEY				= 0x07,		// V2 - NMEXP_CAP_OMA
	NMEXP_QUERY_DOMAIN_NO_KEY				= 0x08,		// V2 - NMEXP_CAP_OMA
	NMEXP_QUERY_OMA_KEY						= 0x09,		// V2 - NMEXP_CAP_OMA
	NMEXP_UPDATE_OMA_KEY					= 0x0a,		// V2 - NMEXP_CAP_OMA
	NMEXP_UPDATE_PROXY_ROUTE_TABLE			= 0x0b,		// V2 - NMEXP_CAP_PROXY - Not Used By LNS
	NMEXP_QUERY_PROXY_ROUTE_TABLE			= 0x0c,		// V2 - NMEXP_CAP_PROXY - Not Used By LNS
	NMEXP_UPDATE_PROXY_ROUTE_INDEX_TABLE	= 0x0d,		// V2 - NMEXP_CAP_PROXY - Not Used By LNS
	NMEXP_QUERY_PROXY_ROUTE_INDEX_TABLE		= 0x0e,		// V2 - NMEXP_CAP_PROXY - Not Used By LNS 
	NMEXP_INIT_CONFIG	                    = 0x0f,     // V2 - NMEXP_CAP_INIT_CONFIG
	NMEXP_UPDATE_NV_CONFIG	                = 0x11,     // Any non-ECS with > 15 address table entries or NMEXP_CAP_SEC_II
	NMEXP_QUERY_NV_CONFIG	                = 0x12,     // Any non-ECS with > 15 address table entries or NMEXP_CAP_SEC_II
	NMEXP_UPDATE_ALIAS_CONFIG	            = 0x13,     // Any non-ECS with > 15 address table entries or NMEXP_CAP_SEC_II
	NMEXP_QUERY_ALIAS_CONFIG	            = 0x14,     // Any non-ECS with > 15 address table entries or NMEXP_CAP_SEC_II

	NMEXP_DEVICE_LS_ADDR_MAPPING_ANNOUNCEMENT  = 0x15,  // Announce LS address. Content doesn't matter, source addr format does.
	NMEXP_SUBNETS_LS_ADDR_MAPPING_ANNOUNCEMENT = 0x16,  // Announce subnets using LS derived IP addresses
	NMEXP_SET_LS_ADDR_MAPPING_ANNOUNCEMENTS    = 0x17,  // V2 - LT_EXP_CAP_LSIP_ADDR_MAPPING_ANNOUNCEMENTS
	NMEXP_QUERY_LS_ADDR_MAPPING_ANNOUNCEMENTS  = 0x18,  // V2 - LT_EXP_CAP_LSIP_ADDR_MAPPING_ANNOUNCEMENTS
	NMEXP_QUERY_IP_ADDR                        = 0x19,  // Query IP address used by device

    NMEXP_TRANSLATE_RAM_ADDRESS                = 0x28,
    /* Codes 0x30-0x41 are defined in SecNmMsgs.h */
} _NmExp_command;
typedef NetEnum(NmExp_command) NmExp_command;

// The following definitions are used to forma a mask indicating the NMEXP capabilities
// of a given device.
#define NMEXP_CAP_OMA						    	0x0001
#define NMEXP_CAP_PROXY						    	0x0002
#define NMEXP_CAP_PHASE_DETECTION				    0x0004
#define NMEXP_CAP_BI_DIRECTIONAL_SIGNAL_STRENGTH	0x0008
#define NMEXP_CAP_UPDATE_NV_INDEX_V19	            0x0010
#define NMEXP_CAP_INIT_CONFIG                       0x0010 	/* Note, in firmware version 19 this is NMEXP_CAP_UPDATE_NV_INDEX*/
#define NMEXP_CAP_QUERY_MEMMAP		                0x0020
#define NMEXP_CAP_UPDATE_NV_INDEX	                0x0040
#define NMEXP_CAP_SECURITY_II	                    0x0080 	/* Device supports Security II and the associated network management messages.*/
#define NMEXP_CAP_LS_MODE_MASK                      0x0300      // The LS mode capability is given by two bits
#define NMEXP_CAP_LS_MODE_COMPATIBILITY_ONLY           0x0000  
#define NMEXP_CAP_LS_MODE_ENHANCED_ONLY                0x0100
#define NMEXP_CAP_LS_MODE_COMPATIBILITY_OR_ENHANCED    0x0200
#define NMEXP_CAP_LS_MODE_RSVD                         0x0300
#define NMEXP_CAP_LSIP_ADDR_MAPPING_ANNOUNCEMENTS  0x0400

// 
#define GET_NMEXP_LS_MODE_CAPABILITIES(cap) (((cap) & NMEXP_CAP_LS_MODE_MASK) >> 8)

// The following capabilities that are ignored by NSS because we don't use them
// NOTE:  NSS does actually use the NMEXP_QUERY_MEMORY_MAP command.  However, we
// ignore this flag so that NSS can commission nodeSim devices that use Neuron
// xifs based on firmware version >= 18.  NodeSim doesn't support the query memory map command
// because it really can't return anything very sensible.
#define NMEXP_CAP_IGNORED (NMEXP_CAP_PROXY                           | \
                           NMEXP_CAP_PHASE_DETECTION                 | \
                           NMEXP_CAP_BI_DIRECTIONAL_SIGNAL_STRENGTH  | \
                           NMEXP_CAP_QUERY_MEMMAP                    | \
                           NMEXP_CAP_INIT_CONFIG                     | \
                           NMEXP_CAP_UPDATE_NV_INDEX                 | \
                           NMEXP_CAP_LSIP_ADDR_MAPPING_ANNOUNCEMENTS)

typedef NetWord NmExpCapabilities;

/*
 *  Query the network management version and capabilities of the device
 */
typedef struct NmExp_QueryCommandSetVersionReq {
    NmExp_command		command;			// NMEXP_QUERY_COMMAND_SET_VERSION
} NmExp_QueryCommandSetVersionReq;

typedef struct NmExp_QueryCommandSetVersionResp
{
    NmExp_command		command;			// NMEXP_QUERY_COMMAND_SET_VERSION
	StandardNMVersion	command_set_version;	
	NmExpCapabilities	capabilities;				
} NmExp_QueryCommandSetVersionResp;

/*
 *  Query the memory map of the device
 */
typedef struct NmExp_QueryMemoryMapReq {
    NmExp_command		command;			// NMEXP_QUERY_MEMORY_MAP
} NmExp_QueryMemoryMapReq;

typedef struct FlashLibraryHeader
{
    NetWord spiRead;
    NetWord spiWrite;
    NetWord spiCompare;
    NetWord spiWaitForReady;
    NetWord spiEraseStateManagement;
    Byte    flashDriverId;
    Byte    firmwareVersion;
} FlashLibraryHeader;

typedef struct MemoryMap
{
        // [0] The first 256 byte block of NEAR RAM
    Byte    nearRamStartBlock;   
        // [1-2]Points to the kas byte in EEPROM that is the application space 
        // and is checksumed
    NetWord eeEndOfChecksummedDataHigh; 
        // [3] The last 256 byte block of extended EEPROM.
    Byte   lastExtendedEEBlock;
        // [4] The first 256 byte block of extended RAM.
    Byte   firstExtendedRamBlock;
        // [5] The flash erase state.  Zero indicates that the flash has not been
        // erased.
    Byte   flashEraseState;
        // [6] The Last block of flash
    Byte   lasFlashBlock;
        // [7-8] Pointer to flash library.  Absolute address of the
        // FlashLibraryHeader.
    NetWord flashLibraryPointer;

        // The pad values are defined here to allow for future growth and are not
        // sent by any existing devices.
    Byte   padding[10];  
} MemoryMap;

#define EESIZE_64K 0xff
typedef struct NmExp_QueryMemoryMapResp
{
    NmExp_command		command;			// NMEXP_QUERY_MEMORY_MAP
	Byte            	memoryMapVersion;	// Version of the memory map
	Byte	            numEEPages;			// # of pages of non-volatile memory.
                                            // Note that 64K is represented as
                                            // EESIZE_64K (since the number of pages
                                            // won't fit in a byte.
    MemoryMap           memoryMap;
} NmExp_QueryMemoryMapResp;

/* Minimum response size includes everything up to and including the flash library pointer */
#define MIN_QUERY_MEMORY_MAP_RESP_SIZE MIN_SIZEOF(NmExp_QueryMemoryMapResp, memoryMap.flashLibraryPointer)
/*
 * Join domain without changing the authentication key
 */
typedef struct NmExp_JoinDomainNoKeyReq {
    NmExp_command        command;		        // NMEXP_JOIN_DOMAIN_NO_KEY
    Byte                 domain_index;
    domain_struct_no_key domain;    
} NmExp_JoinDomainNoKeyReq;

typedef struct NmExp_JoinDomainNoKeyResp
{
    NmExp_command   command;		    // NMEXP_JOIN_DOMAIN_NO_KEY
} NmExp_JoinDomainNoKeyResp;

/*
 * Query a domain without exposing the authentication key
 */
typedef struct NmExp_QueryDomainNoKeyReq {
    NmExp_command   command;		    // NMEXP_QUERY_DOMAIN_NO_KEY
    Byte            domain_index;
} NmExp_QueryDomainNoKeyReq;

typedef struct NmExp_QueryDomainNoKeyResp
{
    NmExp_command   command;		    // NMEXP_QUERY_DOMAIN_NO_KEY
    domain_struct_no_key domain;
} NmExp_QueryDomainNoKeyResp;

/*
 * Query a 96 bit authentication key
 */
typedef struct NmExp_QueryOmaKeyReq {
    NmExp_command            command;		// NMEXP_QUERY_OMA_KEY
} NmExp_QueryOmaKeyReq;

typedef struct NmExp_QueryOmaKeyResp
{
    NmExp_command   command;		        // NMEXP_QUERY_OMA_KEY
    Byte            key[OMA_KEY_LEN];
} NmExp_QueryOmaKeyResp;

/*
 * Update a 96 bit authentication key
 */
typedef struct NmExp_UpdateOmaKeyReq {
    NmExp_command   command;		    // NMEXP_UPDATE_OMA_KEY
    Bool            increment;          // If true, treat key value as an increment to current key
    Byte            key[OMA_KEY_LEN];
} NmExp_UpdateOmaKeyReq;

typedef struct NmExp_UpdateOmaKeyResp
{
    NmExp_command            command;		// NMEXP_UPDATE_OMA_KEY
} NmExp_UpdateOmaKeyResp;

// Update NV config, using the expanded NV config structure that supports 255 address
// table entries and AES encryption.
typedef struct NmExp_UpdateNvConfigReq
{
	NmExp_command	command;		// NMEXP_UPDATE_NV_CONFIG
	NetWord         nv_index;	    // The network varialble index
	nv_struct_exp	nv_config;
} NmExp_UpdateNvConfigReq;

typedef struct NmExp_UpdateNvConfigResp
{
    NmExp_command   command;		    // NMEXP_UPDATE_NV_CONFIG
} NmExp_UpdateNvConfigResp;

// Query NV config, returning the nv_struct_exp structure.	
typedef struct NmExp_QueryNvConfigReq
{
	NmExp_command	command;	// NMEXP_QUERY_NV_CONFIG
	NetWord         nv_index;	// The network varialble index
} NmExp_QueryNvConfigReq;

typedef struct NmExp_QueryNvConfigResp
{
	NmExp_command	command;	// NMEXP_QUERY_NV_CONFIG 
	nv_struct_exp	nv_config;
} NmExp_QueryNvConfigResp;

// Update alias config, using the expanded alias config structure that supports 255 address
// table entries and AES encryption.
typedef struct NmExp_UpdateAliasConfigReq
{
	NmExp_command	    command;	    // NMEXP_UPDATE_NV_ALIAS_CONFIG 
	NetWord             alias_index;	// The 0 based alias index
	alias_struct_exp	alias_config;
} NmExp_UpdateAliasConfigReq;

typedef struct NmExp_UpdateAliasConfigResp
{
    NmExp_command   command;		    // NMEXP_UPDATE_NV_ALIAS_CONFIG
} NmExp_UpdateAliasConfigResp;

// Query alias config, returning the alias_struct_exp structure.	
typedef struct NmExp_QueryAliasConfigReq
{
	NmExp_command	command;	    // NMEXP_QUERY_NV_ALIAS_CONFIG
	NetWord         alias_index;	// The 0 based alias index
} NmExp_QueryAliasConfigReq;

typedef struct NmExp_QueryAliasConfigResp
{
	NmExp_command	    command;	// NMEXP_QUERY_NV_ALIAS_CONFIG 
	alias_struct_exp	alias_config;
} NmExp_QueryAliasConfigResp;

typedef struct NmExp_QueryIpAddrReq 
{
	NmExp_command	command;        // NMEXP_QUERY_IP_ADDR
} NmExp_QueryIpAddrReq;

typedef struct NmExp_QueryIpAddrResp
{
	NmExp_command	command;        // NMEXP_QUERY_IP_ADDR
	Byte	        ipAddr[16];	    // 4 bytes for IPV4, 16 for IPV6
} NmExp_QueryIpAddrResp;

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
