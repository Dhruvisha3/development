//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ns_echo.h#4 $
//

// debugging -- this is an "echo" command for testing messages to the NSI
// to include it, just add the following statement in ns_nmc.h, as in:
//  #include "echo.h", and rebuild NSI and nsstst32.
//
#define NM_NS_ECHO 126
typedef struct NsEcho
{
    Byte        seq;
    char        string[5];
} NsEcho;

typedef struct NsEchoCommand
{
    Byte        productCode; /* NS_PRODUCT_CODE */
    Byte        nmCode;      /* NM_NS_ECHO */
    NsEcho      echo;
} NsEchoCommand;

