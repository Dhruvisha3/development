//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ni_nmext.h#4 $
//

#ifndef _NI_NMEXT_H
#define _NI_NMEXT_H

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

/********************************************************************
    
    ECS Message Structure:
        
        0           1           2           3
    +-----------+-----------+-----------+------------------------->
    | NM Code   | Command   | Resource  |   data
    | 0x70      |           |           | (e.g. index)
    +-----------+-----------+-----------+------------------------->

 ********************************************************************/


#define INITIAL_ECS_VER 1           /* the first valid ECS version */
#define IS_ECS_NODE(ver_nm_max) (ver_nm_max >= INITIAL_ECS_VER) /* return TRUE if ECS */
#define CURRENT_ECS_VER 1           /* current ECS version number */

/********************************************************************
 * resource ids -- each id represents a type of resource in a node
 ********************************************************************/
typedef enum _nme_resource
{
    /* system resources */
                                    /* 0x00 reserved for future use */
    NM_NODE             = 0x01,
    NM_DOMAIN           = 0x02,     /* domain entry */
    NM_ADDRESS          = 0x03,     /* address table entry */
    NM_NV_DEF           = 0x04,     /* nv definition */
    NM_NV_CONFIG        = 0x05,     /* nv configuration entry */
    NM_ALIAS_CONFIG     = 0x06,     /* alias configuration entry */

    NM_MCS              = 0x7D,     /* monitor/control set */
    NM_MCP              = 0x7E,     /* monitor/control point */
                                    /* values up to 0x7E not used currently */
                                    /* 0x7F reserved for future use */
    /* device-specific resources */
                                    /* 0x80 reserved for future use */
                                    /* values up to 0xFE not used currently */
                                    /* 0xFF reserved for future use */
} _nme_resource;
typedef NetEnum(_nme_resource) nme_resource;


/********************************************************************
 * extended commands -- appears as the first byte of the wink message
 ********************************************************************/
typedef enum _nme_command
{
    /* Legacy Commands */
    /* defined in ni_app.h:
    APP_WINK                = 0x00,
    SEND_ID_INFO            = 0x01,
    APP_NV_DEFINE           = 0x02,
    APP_NV_REMOVE           = 0x03,
    QUERY_NV_INFO           = 0x04,
    QUERY_NODE_INFO         = 0x05,
    UPDATE_NV_INFO          = 0x06
    */

    /* System commands -- do not require nme_resource to be specified */
    NM_GET_CAPABILITY_INFO  = 0x07,
    NM_SET_NV               = 0x08,     /* write nv by index */
    NM_BATCH                = 0x09,
    NM_BATCH_ENUM           = 0x0A,
    NM_BATCH_LEGACY         = 0x0B,
    NM_BATCH_NMND           = 0x0C,
                                        /* values up to 0x1F not used currently */

    /* Resource Common Commands -- applied to the specified nme_resource */
    NM_EXT_COMMON           = 0x20,     /* a marker */
    NM_INITIALIZE           = NM_EXT_COMMON,
    NM_CREATE               = NM_EXT_COMMON + 1,     /* 0x21 */
    NM_REMOVE               = NM_EXT_COMMON + 2,     /* 0x22 */
    NM_SET                  = NM_EXT_COMMON + 3,     /* 0x23 */
    NM_GET                  = NM_EXT_COMMON + 4,     /* 0x24 */
    NM_UPDATE               = NM_EXT_COMMON + 5,     /* 0x25 */
    NM_ENUMERATE            = NM_EXT_COMMON + 6,     /* 0x26 */
                                        /* values up to 0x7D not used currently */
                                        /* 0x7F reserved for future use */
    /* Resource Specific Commands */
    NM_EXT_SPECIFIC         = 0x80,     /* a marker */
                                        /* 0x80 reserved for future use */
    /* Node-Specific Commands */
    NM_START_UPDATE         = NM_EXT_SPECIFIC + 1,  /* 0x81 - Start a flush transaction */
    NM_COMMIT_UPDATE        = NM_EXT_SPECIFIC + 2,  /* 0x82 - Commit a flush transaction (starts UPDATE_COMMIT_PENDING) */
    NM_GET_UPDATE_STATUS    = NM_EXT_SPECIFIC + 3,  /* 0x83 - Query flush status */
    NM_GET_INFO             = NM_EXT_SPECIFIC + 4,  /* 0x84 - Return the node's index in package */
                                        /* values up to 0xFE not used currently */

    /* Domain-Specific Commands */
    NM_SET_AUTH             = NM_EXT_SPECIFIC + 1,  /* 0x81 */
        /* All Domain-Specific commands  below this requires ECS version 2 or greater */ 
    NM_UPDATE_NO_KEY        = NM_EXT_SPECIFIC + 2,  /* 0x82 - Update domain without changing key */
    NM_ENUMERATE_NO_KEY     = NM_EXT_SPECIFIC + 3,  /* 0x83 - Return domain without exposing key */
    NM_ENUMERATE_AUTH_OMA   = NM_EXT_SPECIFIC + 4,  /* 0x84 - retrieve a 48 or 96 bit key.       */ 
    NM_SET_AUTH_OMA         = NM_EXT_SPECIFIC + 5,  /* 0x85 - Set a 48 or 96 bit key.   */

                                        /* values up to 0xFE not used currently */

    /* Address-Specific Commands */
                                        /* values up to 0xFE not used currently */

    /* NV-Specific  Commands */
                                        /* values up to 0xFE not used currently */

    /* Monitor/Control Set-Specific Commands */
    NM_SET_DESC             = NM_EXT_SPECIFIC + 1,  /* 0x81 */
    NM_GET_DESC             = NM_EXT_SPECIFIC + 2,  /* 0x82 */
                                        /* values up to 0xFE not used currently */

    /* Monitor/Control Point-Specific Commands */
    /* NM_SET_DESC          = NM_EXT_SPECIFIC + 1,  */
    /* NM_GET_DESC          = NM_EXT_SPECIFIC + 2,  */
                                        /* values up to 0xFE not used currently */

                                        /* 0xFF reserved for future use */
} _nme_command;
typedef NetEnum(_nme_command) nme_command;


/********************************************************************
 * Negative-response cause code
 ********************************************************************/
typedef enum _nme_cause
{
    NMERR_OK                = 0x00,     /* for convenience only.  Since an OK cause is not sent with negative response */

    /* System-Defined Causes */
    NMERR_CAUSE_UNKNOWN     = 0x01,
    NMERR_UNSUPPORTED       = 0x02,
    NMERR_NOT_FOUND         = 0x03,
    NMERR_ENUMERATION_END   = 0x04,
    NMERR_ILLEGAL_PARMS     = 0x05,
    NMERR_DENIED            = 0x06,
    NMERR_AUTHENTICATION    = 0x07,
    NMERR_WRITE_FAILURE     = 0x08,
    NMERR_INTERNAL_FAILURE  = 0x09,
                                        /* values up to 0x7E not used currently */
                                        /* 0x7F reserved for future use */
    /* Device-Specific Causes */
                                        /* 0x80 reserved for future use */
                                        /* values up to 0xFE not used currently */

                                        /* 0xFF reserved for future use */
} _nme_cause;
typedef NetEnum(_nme_cause) nme_cause;


/********************************************************************
 * Negative-response message structure
 ********************************************************************/
typedef struct NM_wink_negative_response
{
    nme_cause       cause;
} NM_wink_negative_response;


/********************************************************************
   NM_GET_CAPABILITY_INFO -- used with SI version 0 and 1, or no SI.
   Used like QUERY_SNVT, but offset is based on location of alias_field.

   Neuron indicates its support in read_only_data_struct_2.
   Host has no indication -- network manager relies on negative response, no data, or timeout 
 ********************************************************************/
typedef struct NmGetCapabilityInfo
{
    nme_command     command;            /* NM_GET_CAPABILITY_INFO */
    NetWord         offset;             /* offsetof(alias_field) in v0/1 SI data. */
    Byte            count;
} NmGetCapabilityInfo;
/* response is alias_field (note variable size), followed by snvt_capability_info */

/********************************************************************
   NM_SET_NV -- set an NV value
 ********************************************************************/
typedef struct {
    nme_command     command;            /* NM_SET_NV */
    NetWord         index;		        /* NV index */
    unsigned        value[1];		    /* NV value */
} NmSetNv;

/********************************************************************
 * Batch Command Structures
 ********************************************************************/

/* NM_BATCH, NM_BATCH_LEGACY, or NM_BATCH_NMND -- non-enumerated batch request header */
typedef struct NmBatchReqHdr
{
    nme_command     command;            /* NM_BATCH, NM_BATCH_LEGACY, or NM_BATCH_NMND */
    Byte            req_count;
#ifdef LITTLE_ENDIAN
    BitField         req_size    : 4;    /* 0 - 16 */
    BitField         resp_size   : 4;    /* 0 - 16 */
#else
    BitField         resp_size   : 4;    /* 0 - 16 */
    BitField         req_size    : 4;    /* 0 - 16 */
#endif
    nme_command     common_command;     /* command applied to each element */
    nme_resource    common_resource;    /* resource applied to each element */
    Byte            request_element;    /* array [req_count][req_size] */
} NmBatchReqHdr;


/* NM_BATCH_ENUM -- enumeated batch request header */
typedef struct NmBatchEnumReqHdr
{
    nme_command     command;            /* NM_BATCH_ENUM */
    Byte            req_count;          /* number of enumeration (>1) */
#ifdef LITTLE_ENDIAN
    BitField                   : 4;      /* unused; set to zero */
    BitField         resp_size : 4;      /* 1 - 16 */
#else
    BitField         resp_size : 4;      /* 1 - 16 */
    BitField                   : 4;      /* unused; set to zero */
#endif
    /* followed by an NM_ENUMERATE structure such as NmNodeEnumerateReq */
} NmBatchEnumReqHdr;


/* batch response, for both enumerated and non-enumerated requests */
typedef struct NmBatchRespHdr
{
    Byte            success_counts;
    /* followed by success_counts of response elements, each resp_size size. */
} NmBatchRespHdr;


/********************************************************************
 * Node Command Structures
 ********************************************************************/


/* NM_NODE::NM_INITIALIZE */
typedef struct 
{
    nme_command     command;            /* NM_INITIALIZE */
    nme_resource    resource;           /* NM_NODE */
    nm_node_state   state;              /* if non-zero, the state to go to afterwards */

    /* THE FOLLOWING FIELDS ARE NEW IN VERSION 2 */
#ifdef LITTLE_ENDIAN
    BitField                        : 7;   /* unused; set to zero */
    BitField        preserveAuth    : 1;   /* Preserve NM authentication and the auth key, 
                                              and OMA bit of the domain that authenticated 
                                              this request. */
#else
    BitField        preserveAuth    : 1;   /* Preserve NM authentication and the auth key, 
                                              and OMA bit of the domain that authenticated 
                                              this request. */
    BitField                        : 7;   /* unused; set to zero */
#endif
} NmNodeInitializeReq;


/* NM_NODE::NM_ENUMERATE */
typedef struct 
{
    nme_command     command;            /* NM_ENUMERATE */
    nme_resource    resource;           /* NM_NODE */
    NetWord         index;              /* package-wide node index, allocated by user or app */
} NmNodeEnumerateReq;

typedef struct
{
    NetWord index;                      /* specified or next allocated node index */
    Byte            neuron_id[6];
    Byte            id_string[8];
    Byte            location[6];
    NetWord         xcvr_id;            /* transceiver id */
    NetWord         xcvr_id_instance;   /* an instance of the transceiver id (zero based) */
} NmNodeEnumerateResp;


/* NM_NODE::NM_GET_INFO */
typedef struct 
{
    nme_command     command;            /* NM_GET_INFO */
    nme_resource    resource;           /* NM_NODE */
} NmNodeGetInfoReq;

typedef NmNodeEnumerateResp NmNodeGetInfoResp;

/* update state */
typedef enum _nme_update_state
{
    UPDATE_FAILED           = 0x00,
    UPDATE_COMMITTED        = 0x01,
    UPDATE_START_PENDING    = 0x02,
    UPDATE_STARTED          = 0x03,
    UPDATE_AUTO             = 0x04,
    UPDATE_COMMIT_PENDING   = 0x05,
} _nme_update_state;
typedef NetEnum(_nme_update_state) nme_update_state;


/* NM_NODE::NM_START_UPDATE */
typedef struct 
{
    nme_command     command;            /* NM_START_UPDATE */
    nme_resource    resource;           /* NM_NODE */
} NmNodeStartUpdateReq;

typedef struct 
{
    nme_update_state update_state;      /* NM_UPDATE_START_PENDING or NM_UPDATE_STARTED */
} NmNodeStartUpdateResp;


/* NM_NODE::NM_COMMIT_UPDATE */
typedef struct
{
    nme_command     command;            /* NM_COMMIT_UPDATE */
    nme_resource    resource;           /* NM_NODE */
} NmNodeCommitUpdateReq;

typedef struct 
{
    nme_update_state update_state;      /* NM_UPDATE_COMMIT_PENDING or NM_UPDATE_COMMITTED */
} NmNodeCommitUpdateResp;


/* NM_NODE::NM_GET_UPDATE_STATUS */
typedef struct 
{
    nme_command     command;            /* NM_GET_UPDATE_STATUS */
    nme_resource    resource;           /* NM_NODE */
} NmNodeGetUpdateStatusReq;

typedef struct 
{
    nme_update_state update_state;      /* NM_UPDATE_START_PENDING or NM_UPDATE_STARTED */
    NetLong         last_flushed;       /* number of seconds since last flush completed */
} NmNodeGetUpdateStatusResp;

/********************************************************************
 * Domain Command Structures
 ********************************************************************/

/* NM_DOMAIN::NM_INITIALIZE */
typedef struct 
{
    nme_command     command;            /* NM_INITIALIZE */
    nme_resource    resource;           /* NM_DOMAIN */
    NetWord         index;              /* start index. */
    NetWord         index_end;          /* end index.  All 1's means all remaining entries */
} NmDomainInitializeReq;

typedef struct 
{
    nme_command     command;            /* NM_UPDATE */
    nme_resource    resource;           /* NM_DOMAIN */
    NetWord         index;              /* domain index */
    domain_struct_1 value;              /* entry data */
} NmDomainUpdateReq;


/* NM_DOMAIN::NM_ENUMERATE */
typedef struct 
{
    nme_command     command;            /* NM_ENUMERATE */
    nme_resource    resource;           /* NM_DOMAIN */
    NetWord         index;              /* address table index */
} NmDomainEnumerateReq;

typedef struct 
{
    NetWord         index;              /* specified or next non-empty entry */
    domain_struct   value;              /* entry data */
} NmDomainEnumerateResp;


/* NM_DOMAIN::NM_SET_AUTH */
typedef struct 
{
    nme_command     command;            /* NM_SET_AUTH */
    nme_resource    resource;           /* NM_DOMAIN */
    NetWord         index;              /* The domain index to update, or 0xFF for all domains */
#ifdef LITTLE_ENDIAN
    BitField                   : 7;      /* unused */
    BitField         increment : 1;      /* True to treat the authentication key below as an
                                                increment based on the current key, false to use it
                                                as an absolute value */
#else
    BitField         increment : 1;      /* True to treat the authentication key below as an
                                                increment based on the current key, false to use it
                                                as an absolute value */
    BitField                   : 7;      /* unused */
#endif
    Byte            authKey[AUTH_KEY_LEN];
} NmDomainSetAuthenticationReq;

/* 
 * NM_DOMAIN:: NM_UPDATE_NO_KEY 
 *
 * Same as the NM_DOMAIN::NM_UPDATE command except that the key is not included 
 * nor does this command affect the key in the device.
*/
typedef struct 
{
    nme_command         command;        /* NM_UPDATE_NO_KEY */
    nme_resource        resource;       /* NM_DOMAIN */
    NetWord             index;          /* domain index */
    NetWord             indexOfKey;     /* domain index to copy key from. index==indexOfKey to preserve */
    domain_struct_no_key value;         /* entry data */
} NmDomainUpdateNoKeyReq;

/*
 * NM_DOMAIN::NM_ENUMERATE_NO_KEY 
 *
 * Same as the NM_DOMAIN::NM_ENUMERATE command except that the key is not 
 * returned in the response. 
*/
typedef struct 
{
    nme_command     command;            /* NM_ENUMERATE_NO_KEY */
    nme_resource    resource;           /* NM_DOMAIN */
    NetWord         index;              /* domain table index */
} NmDomainEnumerateNoKeyReq;

typedef struct 
{
    NetWord         index;              /* specified or next non-empty entry */
    domain_struct_no_key value;         /* entry data */
} NmDomainEnumerateNoKeyResp;

/*
 * NM_DOMAIN::NM_ENUMERATE_AUTH_OMA 
 * 
 * Retrieve a 48 or 96 authentication key. 
 * All nodes supporting ECS version 2 and greater support this command, 
 * though the keyIsOma flag will always be FALSE if the node does not support 
 * OMA.
*/
typedef struct 
{
    nme_command     command;          /* NM_ENUMERATE_AUTH_OMA */
    nme_resource    resource;        /* NM_DOMAIN */
    NetWord         index;           /* domain table index */
} NmDomainEnumerateAuthOmaReq;

typedef struct 
{
    NetWord         index;  /* specified or next non-empty entry */
#ifdef LITTLE_ENDIAN
    BitField            : 7;   /* unused */
    BitField  keyIsOma  : 1;   /* True key is 96 bits long, false its 48 bits long */
#else
    BitField  keyIsOma  : 1;   /* True key is 96 bits long, false its 48 bits long */
    BitField            : 7;   /* unused */
#endif
    Byte            authKey[OMA_KEY_LEN];
} NmDomainEnumerateAuthOmaResp;


/* 
 * NM_DOMAIN::NM_SET_AUTH_OMA 
 *
 * Same as NM_DOMAIN::NM_SET_AUTH command except that the key may be 48 or 96 
 * bits long. Note that this command is accepted whether the device support 
 * OMA or not.  Whether the key is a 48 or96 bit key is controlled by the
 * oma bit in the domain configuration. 
*/
typedef struct 
{
    nme_command     command;   /* NM_SET_AUTH_OMA */
    nme_resource    resource;  /* NM_DOMAIN */
    NetWord         index;     /* The domain index to update, or 0xFF for all domains */
#ifdef LITTLE_ENDIAN
    BitField            : 7;   /* unused */
    BitField  increment : 1;   /* True to treat the authentication key below as an increment based on the current key, false to use it as an absolute value */
#else
    BitField  increment : 1;   /* True to treat the authentication key below as an increment based on the current key, false to use it as an absolute value */
    BitField            : 7;   /* unused */
#endif
    Byte            authKey[OMA_KEY_LEN];
} NmDomainSetAuthenticationOmaReq;

/********************************************************************
 * Address Table Command Structures
 ********************************************************************/

/* NM_ADDRESS::NM_INITIALIZE */
typedef struct 
{
    nme_command     command;            /* NM_INITIALIZE */
    nme_resource    resource;           /* NM_ADDRESS */
    NetWord         index;              /* start index. */
    NetWord         index_end;          /* end index.  All 1's means all remaining entries */
} NmAddressInitializeReq;


/* NM_ADDRESS::NM_UPDATE */
typedef struct 
{
    nme_command     command;            /* NM_UPDATE */
    nme_resource    resource;           /* NM_ADDRESS */
    NetWord         index;              /* address table index */
    address_struct_ext value;           /* entry data */
} NmAddressUpdateReq;


/* NM_ADDRESS::NM_ENUMERATE */
typedef struct 
{
    nme_command     command;            /* NM_ENUMERATE */
    nme_resource    resource;           /* NM_ADDRESS */
    NetWord         index;              /* address table index */
} NmAddressEnumerateReq;

typedef struct 
{
    NetWord index;                      /* specified or next non-empty entry */
    address_struct_ext value;           /* entry data */
} NmAddressEnumerateResp;


/********************************************************************
 * NV DEF Command Structures
 * The NV config index is a single space covering static, dynamic, and 
 * monitor/control NVs, in that order.
 ********************************************************************/

/* there are currently no commands defined for this resource, since it
   could be managed via the legacy QUERY_NV_INFO and UPDATE_NV_INFO
   commands */

/********************************************************************
 * NV CONFIG Command Structures
 * The NV config index is a single space covering static, dynamic, and 
 * monitor/control NVs, alias, and private NVs, in that order.
 ********************************************************************/

/* NM_NV_CONFIG::NM_INITIALIZE */
typedef struct 
{
    nme_command     command;            /* NM_INITIALIZE */
    nme_resource    resource;           /* NM_NV_CONFIG */
    NetWord         index;              /* start index. */
    NetWord         index_end;          /* end index.  All 1's means all remaining entries */
} NmNvConfigInitializeReq;


/* NM_NV_CONFIG::NM_UPDATE */
typedef struct 
{
    nme_command     command;            /* NM_UPDATE */
    nme_resource    resource;           /* NM_NV_CONFIG (for NVs and aliases) */
    NetWord         index;              /* nv table index */
    nv_struct_ext value;                /* entry data */
} NmNvConfigUpdateReq;


/* NM_NV_CONFIG::NM_ENUMERATE */
typedef struct 
{
    nme_command     command;            /* NM_ENUMERATE */
    nme_resource    resource;           /* NM_NV_CONFIG */
    NetWord         index;              /* nv index */
} NmNvConfigEnumerateReq;

typedef struct 
{
    NetWord         index;              /* specified or next allocated nv */
    nv_struct_ext   value;              /* entry data */
} NmNvConfigEnumerateResp;

/********************************************************************
 * NV CONFIG Command Structures
 * The NV config index is a single space covering static, dynamic, and 
 * monitor/control NVs, alias, and private NVs, in that order.
 ********************************************************************/

/* NM_ALIAS_CONFIG::NM_INITIALIZE */
typedef struct 
{
    nme_command     command;            /* NM_INITIALIZE */
    nme_resource    resource;           /* NM_ALIAS_CONFIG */
    NetWord         index;              /* start index. */
    NetWord         index_end;          /* end index.  All 1's means all remaining entries */
} NmAliasConfigInitializeReq;


/* NM_ALIAS_CONFIG::NM_UPDATE */
typedef struct 
{
    nme_command     command;            /* NM_UPDATE */
    nme_resource    resource;           /* NM_ALIAS_CONFIG (for NVs and aliases) */
    NetWord         index;              /* nv table index */
    alias_struct_ext value;             /* entry data */
} NmAliasConfigUpdateReq;


/* NM_ALIAS_CONFIG::NM_ENUMERATE */
typedef struct 
{
    nme_command     command;            /* NM_ENUMERATE */
    nme_resource    resource;           /* NM_ALIAS_CONFIG */
    NetWord         index;              /* nv index */
} NmAliasConfigEnumerateReq;

typedef struct 
{
    NetWord         index;              /* specified or next allocated nv */
    alias_struct_ext value;             /* entry data */
} NmAliasConfigEnumerateResp;

/********************************************************************
 * Monitor/Control Set Command Structures
 ********************************************************************/
/* NM_MCS::NM_INITIALIZE */
typedef struct
{
    nme_command     command;            /* NM_INITIALIZE */
    nme_resource    resource;           /* NM_MCS */
    NetWord         index;              /* start index */
    NetWord         index_end;          /* end index */
} NmMcsInitializeReq;

/* bit values for options: */
/* none defined */

typedef Byte McsOptions;

/* NM_MCS::NM_UPDATE */
typedef struct
{
    McsOptions      mcs_options;   
    NetWord         desc_length;        /* size of description data */
} McsEntry;

typedef struct
{
    nme_command     command;            /* NM_CREATE */
    nme_resource    resource;           /* NM_MCS */
    NetWord         index;              /* monitor set index */
    McsEntry        entry;              /* contents of a monitor set, excluding description data */
} NmMcsCreateReq;

typedef NmMcsCreateReq NmMcsUpdateReq;  /* NM_UPDATE */

/* NM_MCS::NM_ENUMERATE */
typedef struct
{
    nme_command     command;            /* NM_ENUMERATE */
    nme_resource    resource;           /* NM_MCS */
    NetWord         index;              /* monitor set index */
} NmMcsEnumerateReq;

typedef struct
{
    NetWord         index;              /* specified or next monitor set index */
    McsEntry        entry;              /* contents of a monitor set, excluding description data */
} NmMcsEnumerateResp;


/* NM_MCS::NM_SET_DESC */
#define PARTIAL_DESC_DATA 0x00
#define COMMIT_DESC_DATA  0x01
typedef Byte DescriptionOptions;

typedef struct
{
    nme_command     command;            /* NM_SET_DESC */
    nme_resource    resource;           /* NM_MCS */
    NetWord         index;              /* monitor set index */
    DescriptionOptions options;         /* COMMIT_DESC_DATA */
    NetWord         offset;             /* offset in description data */
    Byte            length;             /* length to get */
	Byte    	    data[1];	        /* variable data with indicated length */
} NmMcsSetDescReq;


/* NM_MCS::NM_GET_DESC */
typedef struct
{
    nme_command     command;            /* NM_GET_DESC */
    nme_resource    resource;           /* NM_MCS */
    NetWord         index;              /* monitor set index */
    NetWord         offset;             /* offset in description data */
    Byte            length;             /* length to get */
} NmMcsGetDescReq;

typedef struct
{
    DescriptionOptions options;         /* COMMIT_DESC_DATA */
    Byte            length;             /* length returned, excluding optoin */
    Byte            data[1];            /* data returned */
} NmMcsGetDescResp;

/********************************************************************
 * Monitor/Control Point Command Structures
 ********************************************************************/

/* NM_MCP::NM_INITIALIZE */
typedef struct
{
    nme_command     command;            /* NM_INITIALIZE */
    nme_resource    resource;           /* NM_MCP */
    NetWord         index;              /* monitor point start index */
    NetWord         index_end;          /* monitor point end index */
} NmMcpInitializeReq;

/* NM_MCP::NM_UPDATE */

/* bit values for options: */
#define MCP_IS_MESSAGE_POINT 0x01
#define MCP_IS_EXPLICIT_MSG MCP_IS_MESSAGE_POINT
typedef Byte McpOptions;

/* bit values for nv point options: */
typedef enum _McnvOptions
{
    MCNV_USE_BOUND_UPDATES			= 0x01,
    MCNV_POLL_RESET				    = 0x02,
    MCNV_SUPPRESS_POLLING_IF_BOUND	= 0x04,
    MCNV_NOTIFY_CHANGE_ONLY			= 0x08,
    MCNV_USE_LOCAL_VALUE            = 0x10,
    MCNV_GENERATE_INITIAL_FETCH     = 0x20,

    /* Add definitions to MCNV_VALID_OPTIONS below */
} _McnvOptions;
typedef NetEnum(_McnvOptions) McnvOptions;

#define MCNV_VALID_OPTIONS (MCNV_USE_BOUND_UPDATES          |   \
                            MCNV_POLL_RESET				    |   \
                            MCNV_SUPPRESS_POLLING_IF_BOUND	|   \
                            MCNV_NOTIFY_CHANGE_ONLY			|   \
                            MCNV_USE_LOCAL_VALUE            |   \
                            MCNV_GENERATE_INITIAL_FETCH)

/* bit values for message point options: */
typedef enum _McmsgOptions
{
    MCMSG_FILTER_BY_CODE	        = 0x01,
    MCMSG_FILTER_BY_ADDRESS	        = 0x02,
    MCMSG_FILTER_OUTPUT_ONLY        = 0x08,
} _McmsgOptions;
typedef NetEnum(_McmsgOptions) McmsgOptions;

typedef struct NmMcpParms
{
    McpOptions  mcp_options;            /* common options */
    union
    {
        struct
        {
            McnvOptions mcnv_options;   /* see MCNV_xxx options above */
            NetLong     poll_interval;  /* in millesecond increments */
            NetLong     notify_interval; /* in millesecond increments */
        } nv;
        struct
        {
            McmsgOptions mcmsg_options; /* see MCMSG_xxx options above */
            Byte        filter_code;    /* used if MCMSG_FILTER_BY_CODE is set */
        } msg;
    } p;
} NmMcpParms;

typedef struct
{
    NetWord         mcs_index;          /* monitor set index */
    NetWord         desc_length;        /* size of description data */
    NetWord         nv_or_addr_index;   /* 16-bit nv index or address table index */
    NmMcpParms      parms;              /* monitor point parameters */
} McpEntry;

typedef struct
{
    nme_command     command;            /* NM_CREATE */
    nme_resource    resource;           /* NM_MCP */
    NetWord         index;              /* monitor point index */
    McpEntry        entry;              /* contents of a monitor point, excluding description data */
} NmMcpCreateReq;

typedef NmMcpCreateReq NmMcpUpdateReq;  /* command = NM_UPDATE */

/* NM_MCP::NM_ENUMERATE */
typedef NmMcsEnumerateReq NmMcpEnumerateReq;    /* resource = NM_MCP */

typedef struct
{
    NetWord         index;              /* specified or next monitor point index in requested set */
    McpEntry        entry;              /* contents of a monitor point, excluding description data */
} NmMcpEnumerateResp;


/* NM_MCP::NM_SET_DESC */
typedef NmMcsSetDescReq NmMcpSetDescReq;    /* resource = NM_MCP */


/* NM_MCP::NM_GET_DESC */
typedef NmMcsGetDescReq NmMcpGetDescReq;    /* resource = NM_MCP */
typedef NmMcsGetDescResp NmMcpGetDescResp;

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
