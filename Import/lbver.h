/*
 *  $Header$
 *
 *  � Copyright 1991-2009, Echelon Corporation.  All Rights Reserved.
 *
 *  File: lbver.h       Author: Fremont Bainbridge
 *
 *	Definition of LonBuilder version structure
 *	and Echelon signature, which also serves as
 *	a tag to locate the version info in a binary.
 *
 *	6/17/92		Robert Einkauf
 *	Modified declaration for init_devel_mode() to permit use on Sun.
 *
 *	5/13/92		Fremont Bainbridge
 *	Added declarations for init_devel_mode() and development_sys.
 *
 *	1/19/92		William Capolongo
 *	Added TIE_PROFILER flag, which brings in different release nums.
 *
 *	4/17/90		Fremont Bainbridge
 *	Added release number definitions.
 */

#ifndef LBVER_H
#define LBVER_H

#include "version.h"		/* pull in version numbers for single file include */


#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned char lb_signature_type[8];

typedef struct
{
    unsigned char rev[4];
} lb_version_struct;

typedef struct
{
    lb_signature_type lb_signature;
    lb_version_struct lb_version;
} lb_tagged_version_struct;

#ifndef TAGGED_VERSION
#define TAGGED_VERSION lb_tagged_version
#endif

extern lb_tagged_version_struct TAGGED_VERSION;
extern char copyrightString[];

/* Development system definitions */
extern int development_sys;


void init_devel_mode(
#ifdef DOS
void
#endif
);

#ifdef __cplusplus
}
#endif

#endif
