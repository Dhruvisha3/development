/*
 *  $Header$
 *
 *  � Copyright 1991-2009, Echelon Corporation.  All Rights Reserved.
 *
 *  File: lbverdef.h        Author: Fremont Bainbridge
 *
 *  Declaration of LonBuilder version structure
 *  and Echelon signature, which also serves as
 *  a tag to locate the version info in a binary.
 *  Definitions are in lbver.h.
 *
 *      5/13/92         Fremont Bainbridge
 *      Use DEVEL_BIND_NUM.
 *
 *      4/17/91         Fremont Bainbridge
 *      Use release number definitions from lbver.h.
 */

#ifndef LBVERDEF_H
#define LBVERDEF_H

#include "lbver.h"

#ifdef __cplusplus
extern "C" {
#endif

lb_tagged_version_struct TAGGED_VERSION =
{
    /* This value is the string "Echelon" with the high bits set */
#ifdef DOS
    "\xC5\xE3\xE8\xE5\xEC\xEF\xEE",
#else
    "\305\343\350\345\354\357\356",
#endif

    /* This is the actual version number: W.XY.Z
       W is the major release number, X and Y make up the
       minor release number (and thus must each be restricted
       to the range 0 to 9), and Z is the build number.

       Prior to LNS 3 timeframe, the build number was set to
       DEVEL_BIND_NUM (255), relying on the release build tools to
       fix it up using LBVERSET.  In addition to being cumbersome for
       build tools, for Windows binaries it is inconsistent with
       the Windows version info which do reflect the actual build.
       So since LNS 3 the build number reflects the actual number (unless
       VER_BUILD is not defined).

       Some legacy tools such as LonBuilder relied on DEVEL_BIND_NUM
       to enable certain development features.  If they are ever rebuilt,
       they should use a different method, such as other predefined symbols
       or _DEBUG (for windows), to distinguish development builds.
     */

    { { RELEASE_NUMBER_MAJOR,
        RELEASE_NUMBER_MINOR1,
        RELEASE_NUMBER_MINOR2,
#ifdef VER_BUILD_D
        VER_BUILD_D
#else
        DEVEL_BIND_NUM
#endif
         } }
};

char copyrightString[] = "Copyright (c) " XSTRING(COPYRIGHT_FROM) "-" XSTRING(COPYRIGHT_TO) " Echelon Corporation.";


#ifdef __cplusplus
}
#endif

#endif
