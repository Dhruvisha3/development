

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0500 */
/* at Tue Oct 14 17:04:29 2014
 */
/* Compiler settings for .\LCADRF32.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __LCADRF32_h__
#define __LCADRF32_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ILdrfGeneral_FWD_DEFINED__
#define __ILdrfGeneral_FWD_DEFINED__
typedef interface ILdrfGeneral ILdrfGeneral;
#endif 	/* __ILdrfGeneral_FWD_DEFINED__ */


#ifndef __ILdrfGeneral2_FWD_DEFINED__
#define __ILdrfGeneral2_FWD_DEFINED__
typedef interface ILdrfGeneral2 ILdrfGeneral2;
#endif 	/* __ILdrfGeneral2_FWD_DEFINED__ */


#ifndef __ILdrfMiscFns1_FWD_DEFINED__
#define __ILdrfMiscFns1_FWD_DEFINED__
typedef interface ILdrfMiscFns1 ILdrfMiscFns1;
#endif 	/* __ILdrfMiscFns1_FWD_DEFINED__ */


#ifndef __ILdrfCatalog_FWD_DEFINED__
#define __ILdrfCatalog_FWD_DEFINED__
typedef interface ILdrfCatalog ILdrfCatalog;
#endif 	/* __ILdrfCatalog_FWD_DEFINED__ */


#ifndef __ILdrfLangResource_FWD_DEFINED__
#define __ILdrfLangResource_FWD_DEFINED__
typedef interface ILdrfLangResource ILdrfLangResource;
#endif 	/* __ILdrfLangResource_FWD_DEFINED__ */


#ifndef __ILdrfTypes_FWD_DEFINED__
#define __ILdrfTypes_FWD_DEFINED__
typedef interface ILdrfTypes ILdrfTypes;
#endif 	/* __ILdrfTypes_FWD_DEFINED__ */


#ifndef __ILdrfTypeTree_FWD_DEFINED__
#define __ILdrfTypeTree_FWD_DEFINED__
typedef interface ILdrfTypeTree ILdrfTypeTree;
#endif 	/* __ILdrfTypeTree_FWD_DEFINED__ */


#ifndef __ILdrfFuncProf_FWD_DEFINED__
#define __ILdrfFuncProf_FWD_DEFINED__
typedef interface ILdrfFuncProf ILdrfFuncProf;
#endif 	/* __ILdrfFuncProf_FWD_DEFINED__ */


#ifndef __ILdrfScalar64Types_FWD_DEFINED__
#define __ILdrfScalar64Types_FWD_DEFINED__
typedef interface ILdrfScalar64Types ILdrfScalar64Types;
#endif 	/* __ILdrfScalar64Types_FWD_DEFINED__ */


#ifndef __ILdrfMiscFns2_FWD_DEFINED__
#define __ILdrfMiscFns2_FWD_DEFINED__
typedef interface ILdrfMiscFns2 ILdrfMiscFns2;
#endif 	/* __ILdrfMiscFns2_FWD_DEFINED__ */


#ifndef __LdrfGeneral_FWD_DEFINED__
#define __LdrfGeneral_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfGeneral LdrfGeneral;
#else
typedef struct LdrfGeneral LdrfGeneral;
#endif /* __cplusplus */

#endif 	/* __LdrfGeneral_FWD_DEFINED__ */


#ifndef __LdrfGeneral2_FWD_DEFINED__
#define __LdrfGeneral2_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfGeneral2 LdrfGeneral2;
#else
typedef struct LdrfGeneral2 LdrfGeneral2;
#endif /* __cplusplus */

#endif 	/* __LdrfGeneral2_FWD_DEFINED__ */


#ifndef __LdrfMiscFns1_FWD_DEFINED__
#define __LdrfMiscFns1_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfMiscFns1 LdrfMiscFns1;
#else
typedef struct LdrfMiscFns1 LdrfMiscFns1;
#endif /* __cplusplus */

#endif 	/* __LdrfMiscFns1_FWD_DEFINED__ */


#ifndef __LdrfCatalog_FWD_DEFINED__
#define __LdrfCatalog_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfCatalog LdrfCatalog;
#else
typedef struct LdrfCatalog LdrfCatalog;
#endif /* __cplusplus */

#endif 	/* __LdrfCatalog_FWD_DEFINED__ */


#ifndef __LdrfLangResource_FWD_DEFINED__
#define __LdrfLangResource_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfLangResource LdrfLangResource;
#else
typedef struct LdrfLangResource LdrfLangResource;
#endif /* __cplusplus */

#endif 	/* __LdrfLangResource_FWD_DEFINED__ */


#ifndef __LdrfTypes_FWD_DEFINED__
#define __LdrfTypes_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfTypes LdrfTypes;
#else
typedef struct LdrfTypes LdrfTypes;
#endif /* __cplusplus */

#endif 	/* __LdrfTypes_FWD_DEFINED__ */


#ifndef __LdrfTypeTree_FWD_DEFINED__
#define __LdrfTypeTree_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfTypeTree LdrfTypeTree;
#else
typedef struct LdrfTypeTree LdrfTypeTree;
#endif /* __cplusplus */

#endif 	/* __LdrfTypeTree_FWD_DEFINED__ */


#ifndef __LdrfFuncProf_FWD_DEFINED__
#define __LdrfFuncProf_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfFuncProf LdrfFuncProf;
#else
typedef struct LdrfFuncProf LdrfFuncProf;
#endif /* __cplusplus */

#endif 	/* __LdrfFuncProf_FWD_DEFINED__ */


#ifndef __LdrfScalar64Types_FWD_DEFINED__
#define __LdrfScalar64Types_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfScalar64Types LdrfScalar64Types;
#else
typedef struct LdrfScalar64Types LdrfScalar64Types;
#endif /* __cplusplus */

#endif 	/* __LdrfScalar64Types_FWD_DEFINED__ */


#ifndef __LdrfMiscFns2_FWD_DEFINED__
#define __LdrfMiscFns2_FWD_DEFINED__

#ifdef __cplusplus
typedef class LdrfMiscFns2 LdrfMiscFns2;
#else
typedef struct LdrfMiscFns2 LdrfMiscFns2;
#endif /* __cplusplus */

#endif 	/* __LdrfMiscFns2_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ILdrfGeneral_INTERFACE_DEFINED__
#define __ILdrfGeneral_INTERFACE_DEFINED__

/* interface ILdrfGeneral */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfGeneral;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1CB4B4C1-EC74-11CF-8CC7-444553540000")
    ILdrfGeneral : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckHeaderCRC( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckDataCRC( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckCRC( 
            /* [in] */ BSTR pBlock,
            /* [in] */ long oldCRC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OpenFile( 
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [in] */ long majorVersion,
            /* [in] */ long checkCRC,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EditFile( 
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileHdrInfo( 
            /* [in] */ long pInfo,
            /* [out] */ long *pUser,
            /* [out] */ BSTR *pDesc,
            /* [out] */ BSTR *pCreator,
            /* [out] */ BSTR *pURL,
            /* [out] */ long *pResDescScope,
            /* [out] */ long *pResDescIndex,
            /* [out] */ long *pResCreScope,
            /* [out] */ long *pResCreIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFileHdrInfo( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR creator,
            /* [in] */ BSTR phone,
            /* [in] */ BSTR webid,
            /* [in] */ BSTR URL,
            /* [in] */ long resDescScope,
            /* [in] */ long resDescIndex,
            /* [in] */ long resCreScope,
            /* [in] */ long resCreIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileVersion( 
            /* [in] */ long pInfo,
            /* [out] */ long *pMajorFmtVer,
            /* [out] */ long *pMinorFmtVer,
            /* [out] */ long *pMajorDataVer,
            /* [out] */ long *pMinorDataVer,
            /* [out] */ long *pYear,
            /* [out] */ long *pMonth,
            /* [out] */ long *pDay,
            /* [out] */ long *pHour,
            /* [out] */ long *pMinute,
            /* [out] */ long *pSecond,
            /* [out] */ long *pScope,
            /* [out] */ BSTR *pRefID,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFileVersion( 
            /* [in] */ long pInfo,
            /* [in] */ long majorDataVer,
            /* [in] */ long minorDataVer,
            /* [in] */ long scope,
            /* [in] */ BSTR refID,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseFile( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE TestFixture( 
            /* [in] */ long testSelect,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfGeneralVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfGeneral * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfGeneral * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfGeneral * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfGeneral * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfGeneral * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfGeneral * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfGeneral * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckHeaderCRC )( 
            ILdrfGeneral * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckDataCRC )( 
            ILdrfGeneral * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckCRC )( 
            ILdrfGeneral * This,
            /* [in] */ BSTR pBlock,
            /* [in] */ long oldCRC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OpenFile )( 
            ILdrfGeneral * This,
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [in] */ long majorVersion,
            /* [in] */ long checkCRC,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EditFile )( 
            ILdrfGeneral * This,
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileHdrInfo )( 
            ILdrfGeneral * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pUser,
            /* [out] */ BSTR *pDesc,
            /* [out] */ BSTR *pCreator,
            /* [out] */ BSTR *pURL,
            /* [out] */ long *pResDescScope,
            /* [out] */ long *pResDescIndex,
            /* [out] */ long *pResCreScope,
            /* [out] */ long *pResCreIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFileHdrInfo )( 
            ILdrfGeneral * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR creator,
            /* [in] */ BSTR phone,
            /* [in] */ BSTR webid,
            /* [in] */ BSTR URL,
            /* [in] */ long resDescScope,
            /* [in] */ long resDescIndex,
            /* [in] */ long resCreScope,
            /* [in] */ long resCreIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileVersion )( 
            ILdrfGeneral * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pMajorFmtVer,
            /* [out] */ long *pMinorFmtVer,
            /* [out] */ long *pMajorDataVer,
            /* [out] */ long *pMinorDataVer,
            /* [out] */ long *pYear,
            /* [out] */ long *pMonth,
            /* [out] */ long *pDay,
            /* [out] */ long *pHour,
            /* [out] */ long *pMinute,
            /* [out] */ long *pSecond,
            /* [out] */ long *pScope,
            /* [out] */ BSTR *pRefID,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFileVersion )( 
            ILdrfGeneral * This,
            /* [in] */ long pInfo,
            /* [in] */ long majorDataVer,
            /* [in] */ long minorDataVer,
            /* [in] */ long scope,
            /* [in] */ BSTR refID,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseFile )( 
            ILdrfGeneral * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TestFixture )( 
            ILdrfGeneral * This,
            /* [in] */ long testSelect,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfGeneralVtbl;

    interface ILdrfGeneral
    {
        CONST_VTBL struct ILdrfGeneralVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfGeneral_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfGeneral_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfGeneral_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfGeneral_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfGeneral_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfGeneral_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfGeneral_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfGeneral_CheckHeaderCRC(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CheckHeaderCRC(This,pInfo,returnCode) ) 

#define ILdrfGeneral_CheckDataCRC(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CheckDataCRC(This,pInfo,returnCode) ) 

#define ILdrfGeneral_CheckCRC(This,pBlock,oldCRC,returnCode)	\
    ( (This)->lpVtbl -> CheckCRC(This,pBlock,oldCRC,returnCode) ) 

#define ILdrfGeneral_OpenFile(This,path,fileType,majorVersion,checkCRC,ppInfo,returnCode)	\
    ( (This)->lpVtbl -> OpenFile(This,path,fileType,majorVersion,checkCRC,ppInfo,returnCode) ) 

#define ILdrfGeneral_EditFile(This,path,fileType,ppInfo,returnCode)	\
    ( (This)->lpVtbl -> EditFile(This,path,fileType,ppInfo,returnCode) ) 

#define ILdrfGeneral_GetFileHdrInfo(This,pInfo,pUser,pDesc,pCreator,pURL,pResDescScope,pResDescIndex,pResCreScope,pResCreIndex,returnCode)	\
    ( (This)->lpVtbl -> GetFileHdrInfo(This,pInfo,pUser,pDesc,pCreator,pURL,pResDescScope,pResDescIndex,pResCreScope,pResCreIndex,returnCode) ) 

#define ILdrfGeneral_SetFileHdrInfo(This,pInfo,creator,phone,webid,URL,resDescScope,resDescIndex,resCreScope,resCreIndex,returnCode)	\
    ( (This)->lpVtbl -> SetFileHdrInfo(This,pInfo,creator,phone,webid,URL,resDescScope,resDescIndex,resCreScope,resCreIndex,returnCode) ) 

#define ILdrfGeneral_GetFileVersion(This,pInfo,pMajorFmtVer,pMinorFmtVer,pMajorDataVer,pMinorDataVer,pYear,pMonth,pDay,pHour,pMinute,pSecond,pScope,pRefID,returnCode)	\
    ( (This)->lpVtbl -> GetFileVersion(This,pInfo,pMajorFmtVer,pMinorFmtVer,pMajorDataVer,pMinorDataVer,pYear,pMonth,pDay,pHour,pMinute,pSecond,pScope,pRefID,returnCode) ) 

#define ILdrfGeneral_SetFileVersion(This,pInfo,majorDataVer,minorDataVer,scope,refID,returnCode)	\
    ( (This)->lpVtbl -> SetFileVersion(This,pInfo,majorDataVer,minorDataVer,scope,refID,returnCode) ) 

#define ILdrfGeneral_CloseFile(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CloseFile(This,pInfo,returnCode) ) 

#define ILdrfGeneral_TestFixture(This,testSelect,returnCode)	\
    ( (This)->lpVtbl -> TestFixture(This,testSelect,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfGeneral_INTERFACE_DEFINED__ */


#ifndef __ILdrfGeneral2_INTERFACE_DEFINED__
#define __ILdrfGeneral2_INTERFACE_DEFINED__

/* interface ILdrfGeneral2 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfGeneral2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B2364CC0-C857-11D4-AAC1-002078B03647")
    ILdrfGeneral2 : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckHeaderCRC( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckDataCRC( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckCRC( 
            /* [in] */ BSTR pBlock,
            /* [in] */ long oldCRC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OpenFile( 
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [in] */ long majorVersion,
            /* [in] */ long checkCRC,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EditFile( 
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileHdrInfo( 
            /* [in] */ long pInfo,
            /* [out] */ long *pUser,
            /* [out] */ BSTR *pDesc,
            /* [out] */ BSTR *pCreator,
            /* [out] */ BSTR *pURL,
            /* [out] */ long *pResDescScope,
            /* [out] */ long *pResDescIndex,
            /* [out] */ long *pResCreScope,
            /* [out] */ long *pResCreIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFileHdrInfo( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR creator,
            /* [in] */ BSTR phone,
            /* [in] */ BSTR webid,
            /* [in] */ BSTR URL,
            /* [in] */ long resDescScope,
            /* [in] */ long resDescIndex,
            /* [in] */ long resCreScope,
            /* [in] */ long resCreIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileVersion( 
            /* [in] */ long pInfo,
            /* [out] */ long *pMajorFmtVer,
            /* [out] */ long *pMinorFmtVer,
            /* [out] */ long *pMajorDataVer,
            /* [out] */ long *pMinorDataVer,
            /* [out] */ long *pYear,
            /* [out] */ long *pMonth,
            /* [out] */ long *pDay,
            /* [out] */ long *pHour,
            /* [out] */ long *pMinute,
            /* [out] */ long *pSecond,
            /* [out] */ long *pScope,
            /* [out] */ BSTR *pRefID,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFileVersion( 
            /* [in] */ long pInfo,
            /* [in] */ long majorDataVer,
            /* [in] */ long minorDataVer,
            /* [in] */ long scope,
            /* [in] */ BSTR refID,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseFile( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE TestFixture( 
            /* [in] */ long testSelect,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EditFileVer( 
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [in] */ long majFmtForCreate,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetNVTObsolete( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNVTObsolete( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pObsolete,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCPTObsolete( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCPTObsolete( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pObsolete,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTObsolete( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTObsolete( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pObsolete,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetScalarInvalidValue( 
            /* [in] */ long pTypeTree,
            /* [in] */ long invalidValue,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetScalarInvalidValue( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pInvalidValuePresent,
            /* [out] */ long *pInvalidValue,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConvertFile( 
            /* [in] */ BSTR pathIn,
            /* [in] */ BSTR pathOut,
            /* [in] */ long toVersion,
            /* [in] */ long checkCRC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetEnumMemberCount( 
            /* [in] */ long pInfo,
            /* [out] */ long *pNumMembers,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetEnumMemberByIndex( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pValue,
            /* [out] */ BSTR *pString,
            /* [out] */ long *pResScope,
            /* [out] */ long *pResIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogDependencyCode( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR progID,
            /* [out] */ long *pDepCode,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SupportedFormats( 
            /* [in] */ long fileType,
            /* [out] */ long *pMajFmtLow,
            /* [out] */ long *pMajFmtHigh,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ChangeSelectedEnumSetTag( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR tag,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ChangeSelectedEnumSetFile( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR file,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteEnumMemberByIndex( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearFPTInherit( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTInherit( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTInherit( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pInherit,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ChangeFPTNVMemberNumber( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long oldMemberNumber,
            /* [in] */ long newMemberNumber,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ChangeFPTCPMemberNumber( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long oldMemberNumber,
            /* [in] */ long newMemberNumber,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResolveAllTypeTreeRefs( 
            /* [in] */ long pCatalogInfo,
            /* [in] */ BSTR progID,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pTypeSize,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTNVMemberNumber( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVIndex,
            /* [out] */ long *pNVMemberNumber,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTCPMemberNumber( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pCPMemberNumber,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTNVIndex( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVMemberNumber,
            /* [out] */ long *pNVIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTCPIndex( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPMemberNumber,
            /* [out] */ long *CPIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfGeneral2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfGeneral2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfGeneral2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfGeneral2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfGeneral2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfGeneral2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfGeneral2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfGeneral2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckHeaderCRC )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckDataCRC )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckCRC )( 
            ILdrfGeneral2 * This,
            /* [in] */ BSTR pBlock,
            /* [in] */ long oldCRC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OpenFile )( 
            ILdrfGeneral2 * This,
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [in] */ long majorVersion,
            /* [in] */ long checkCRC,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EditFile )( 
            ILdrfGeneral2 * This,
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileHdrInfo )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pUser,
            /* [out] */ BSTR *pDesc,
            /* [out] */ BSTR *pCreator,
            /* [out] */ BSTR *pURL,
            /* [out] */ long *pResDescScope,
            /* [out] */ long *pResDescIndex,
            /* [out] */ long *pResCreScope,
            /* [out] */ long *pResCreIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFileHdrInfo )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR creator,
            /* [in] */ BSTR phone,
            /* [in] */ BSTR webid,
            /* [in] */ BSTR URL,
            /* [in] */ long resDescScope,
            /* [in] */ long resDescIndex,
            /* [in] */ long resCreScope,
            /* [in] */ long resCreIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileVersion )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pMajorFmtVer,
            /* [out] */ long *pMinorFmtVer,
            /* [out] */ long *pMajorDataVer,
            /* [out] */ long *pMinorDataVer,
            /* [out] */ long *pYear,
            /* [out] */ long *pMonth,
            /* [out] */ long *pDay,
            /* [out] */ long *pHour,
            /* [out] */ long *pMinute,
            /* [out] */ long *pSecond,
            /* [out] */ long *pScope,
            /* [out] */ BSTR *pRefID,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFileVersion )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long majorDataVer,
            /* [in] */ long minorDataVer,
            /* [in] */ long scope,
            /* [in] */ BSTR refID,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseFile )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TestFixture )( 
            ILdrfGeneral2 * This,
            /* [in] */ long testSelect,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EditFileVer )( 
            ILdrfGeneral2 * This,
            /* [in] */ BSTR path,
            /* [in] */ long fileType,
            /* [in] */ long majFmtForCreate,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetNVTObsolete )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNVTObsolete )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pObsolete,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCPTObsolete )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCPTObsolete )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pObsolete,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTObsolete )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTObsolete )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pObsolete,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetScalarInvalidValue )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long invalidValue,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetScalarInvalidValue )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pInvalidValuePresent,
            /* [out] */ long *pInvalidValue,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConvertFile )( 
            ILdrfGeneral2 * This,
            /* [in] */ BSTR pathIn,
            /* [in] */ BSTR pathOut,
            /* [in] */ long toVersion,
            /* [in] */ long checkCRC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetEnumMemberCount )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pNumMembers,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetEnumMemberByIndex )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pValue,
            /* [out] */ BSTR *pString,
            /* [out] */ long *pResScope,
            /* [out] */ long *pResIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogDependencyCode )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR progID,
            /* [out] */ long *pDepCode,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SupportedFormats )( 
            ILdrfGeneral2 * This,
            /* [in] */ long fileType,
            /* [out] */ long *pMajFmtLow,
            /* [out] */ long *pMajFmtHigh,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ChangeSelectedEnumSetTag )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR tag,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ChangeSelectedEnumSetFile )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR file,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteEnumMemberByIndex )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearFPTInherit )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTInherit )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTInherit )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pInherit,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ChangeFPTNVMemberNumber )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long oldMemberNumber,
            /* [in] */ long newMemberNumber,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ChangeFPTCPMemberNumber )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long oldMemberNumber,
            /* [in] */ long newMemberNumber,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResolveAllTypeTreeRefs )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pCatalogInfo,
            /* [in] */ BSTR progID,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pTypeSize,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTNVMemberNumber )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVIndex,
            /* [out] */ long *pNVMemberNumber,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTCPMemberNumber )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pCPMemberNumber,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTNVIndex )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVMemberNumber,
            /* [out] */ long *pNVIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTCPIndex )( 
            ILdrfGeneral2 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPMemberNumber,
            /* [out] */ long *CPIndex,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfGeneral2Vtbl;

    interface ILdrfGeneral2
    {
        CONST_VTBL struct ILdrfGeneral2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfGeneral2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfGeneral2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfGeneral2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfGeneral2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfGeneral2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfGeneral2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfGeneral2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfGeneral2_CheckHeaderCRC(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CheckHeaderCRC(This,pInfo,returnCode) ) 

#define ILdrfGeneral2_CheckDataCRC(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CheckDataCRC(This,pInfo,returnCode) ) 

#define ILdrfGeneral2_CheckCRC(This,pBlock,oldCRC,returnCode)	\
    ( (This)->lpVtbl -> CheckCRC(This,pBlock,oldCRC,returnCode) ) 

#define ILdrfGeneral2_OpenFile(This,path,fileType,majorVersion,checkCRC,ppInfo,returnCode)	\
    ( (This)->lpVtbl -> OpenFile(This,path,fileType,majorVersion,checkCRC,ppInfo,returnCode) ) 

#define ILdrfGeneral2_EditFile(This,path,fileType,ppInfo,returnCode)	\
    ( (This)->lpVtbl -> EditFile(This,path,fileType,ppInfo,returnCode) ) 

#define ILdrfGeneral2_GetFileHdrInfo(This,pInfo,pUser,pDesc,pCreator,pURL,pResDescScope,pResDescIndex,pResCreScope,pResCreIndex,returnCode)	\
    ( (This)->lpVtbl -> GetFileHdrInfo(This,pInfo,pUser,pDesc,pCreator,pURL,pResDescScope,pResDescIndex,pResCreScope,pResCreIndex,returnCode) ) 

#define ILdrfGeneral2_SetFileHdrInfo(This,pInfo,creator,phone,webid,URL,resDescScope,resDescIndex,resCreScope,resCreIndex,returnCode)	\
    ( (This)->lpVtbl -> SetFileHdrInfo(This,pInfo,creator,phone,webid,URL,resDescScope,resDescIndex,resCreScope,resCreIndex,returnCode) ) 

#define ILdrfGeneral2_GetFileVersion(This,pInfo,pMajorFmtVer,pMinorFmtVer,pMajorDataVer,pMinorDataVer,pYear,pMonth,pDay,pHour,pMinute,pSecond,pScope,pRefID,returnCode)	\
    ( (This)->lpVtbl -> GetFileVersion(This,pInfo,pMajorFmtVer,pMinorFmtVer,pMajorDataVer,pMinorDataVer,pYear,pMonth,pDay,pHour,pMinute,pSecond,pScope,pRefID,returnCode) ) 

#define ILdrfGeneral2_SetFileVersion(This,pInfo,majorDataVer,minorDataVer,scope,refID,returnCode)	\
    ( (This)->lpVtbl -> SetFileVersion(This,pInfo,majorDataVer,minorDataVer,scope,refID,returnCode) ) 

#define ILdrfGeneral2_CloseFile(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CloseFile(This,pInfo,returnCode) ) 

#define ILdrfGeneral2_TestFixture(This,testSelect,returnCode)	\
    ( (This)->lpVtbl -> TestFixture(This,testSelect,returnCode) ) 

#define ILdrfGeneral2_EditFileVer(This,path,fileType,majFmtForCreate,ppInfo,returnCode)	\
    ( (This)->lpVtbl -> EditFileVer(This,path,fileType,majFmtForCreate,ppInfo,returnCode) ) 

#define ILdrfGeneral2_SetNVTObsolete(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> SetNVTObsolete(This,pInfo,index,returnCode) ) 

#define ILdrfGeneral2_GetNVTObsolete(This,pInfo,index,pObsolete,returnCode)	\
    ( (This)->lpVtbl -> GetNVTObsolete(This,pInfo,index,pObsolete,returnCode) ) 

#define ILdrfGeneral2_SetCPTObsolete(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> SetCPTObsolete(This,pInfo,index,returnCode) ) 

#define ILdrfGeneral2_GetCPTObsolete(This,pInfo,index,pObsolete,returnCode)	\
    ( (This)->lpVtbl -> GetCPTObsolete(This,pInfo,index,pObsolete,returnCode) ) 

#define ILdrfGeneral2_SetFPTObsolete(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> SetFPTObsolete(This,pInfo,index,returnCode) ) 

#define ILdrfGeneral2_GetFPTObsolete(This,pInfo,index,pObsolete,returnCode)	\
    ( (This)->lpVtbl -> GetFPTObsolete(This,pInfo,index,pObsolete,returnCode) ) 

#define ILdrfGeneral2_SetScalarInvalidValue(This,pTypeTree,invalidValue,returnCode)	\
    ( (This)->lpVtbl -> SetScalarInvalidValue(This,pTypeTree,invalidValue,returnCode) ) 

#define ILdrfGeneral2_GetScalarInvalidValue(This,pTypeTree,pInvalidValuePresent,pInvalidValue,returnCode)	\
    ( (This)->lpVtbl -> GetScalarInvalidValue(This,pTypeTree,pInvalidValuePresent,pInvalidValue,returnCode) ) 

#define ILdrfGeneral2_ConvertFile(This,pathIn,pathOut,toVersion,checkCRC,returnCode)	\
    ( (This)->lpVtbl -> ConvertFile(This,pathIn,pathOut,toVersion,checkCRC,returnCode) ) 

#define ILdrfGeneral2_GetEnumMemberCount(This,pInfo,pNumMembers,returnCode)	\
    ( (This)->lpVtbl -> GetEnumMemberCount(This,pInfo,pNumMembers,returnCode) ) 

#define ILdrfGeneral2_GetEnumMemberByIndex(This,pInfo,index,pValue,pString,pResScope,pResIndex,returnCode)	\
    ( (This)->lpVtbl -> GetEnumMemberByIndex(This,pInfo,index,pValue,pString,pResScope,pResIndex,returnCode) ) 

#define ILdrfGeneral2_CatalogDependencyCode(This,pInfo,progID,pDepCode,returnCode)	\
    ( (This)->lpVtbl -> CatalogDependencyCode(This,pInfo,progID,pDepCode,returnCode) ) 

#define ILdrfGeneral2_SupportedFormats(This,fileType,pMajFmtLow,pMajFmtHigh,returnCode)	\
    ( (This)->lpVtbl -> SupportedFormats(This,fileType,pMajFmtLow,pMajFmtHigh,returnCode) ) 

#define ILdrfGeneral2_ChangeSelectedEnumSetTag(This,pInfo,tag,returnCode)	\
    ( (This)->lpVtbl -> ChangeSelectedEnumSetTag(This,pInfo,tag,returnCode) ) 

#define ILdrfGeneral2_ChangeSelectedEnumSetFile(This,pInfo,file,returnCode)	\
    ( (This)->lpVtbl -> ChangeSelectedEnumSetFile(This,pInfo,file,returnCode) ) 

#define ILdrfGeneral2_DeleteEnumMemberByIndex(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> DeleteEnumMemberByIndex(This,pInfo,index,returnCode) ) 

#define ILdrfGeneral2_ClearFPTInherit(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> ClearFPTInherit(This,pInfo,index,returnCode) ) 

#define ILdrfGeneral2_SetFPTInherit(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> SetFPTInherit(This,pInfo,index,returnCode) ) 

#define ILdrfGeneral2_GetFPTInherit(This,pInfo,index,pInherit,returnCode)	\
    ( (This)->lpVtbl -> GetFPTInherit(This,pInfo,index,pInherit,returnCode) ) 

#define ILdrfGeneral2_ChangeFPTNVMemberNumber(This,pInfo,FPTIndex,oldMemberNumber,newMemberNumber,returnCode)	\
    ( (This)->lpVtbl -> ChangeFPTNVMemberNumber(This,pInfo,FPTIndex,oldMemberNumber,newMemberNumber,returnCode) ) 

#define ILdrfGeneral2_ChangeFPTCPMemberNumber(This,pInfo,FPTIndex,oldMemberNumber,newMemberNumber,returnCode)	\
    ( (This)->lpVtbl -> ChangeFPTCPMemberNumber(This,pInfo,FPTIndex,oldMemberNumber,newMemberNumber,returnCode) ) 

#define ILdrfGeneral2_ResolveAllTypeTreeRefs(This,pCatalogInfo,progID,pTypeTree,pTypeSize,returnCode)	\
    ( (This)->lpVtbl -> ResolveAllTypeTreeRefs(This,pCatalogInfo,progID,pTypeTree,pTypeSize,returnCode) ) 

#define ILdrfGeneral2_GetFPTNVMemberNumber(This,pInfo,FPTIndex,NVIndex,pNVMemberNumber,returnCode)	\
    ( (This)->lpVtbl -> GetFPTNVMemberNumber(This,pInfo,FPTIndex,NVIndex,pNVMemberNumber,returnCode) ) 

#define ILdrfGeneral2_GetFPTCPMemberNumber(This,pInfo,FPTIndex,CPIndex,pCPMemberNumber,returnCode)	\
    ( (This)->lpVtbl -> GetFPTCPMemberNumber(This,pInfo,FPTIndex,CPIndex,pCPMemberNumber,returnCode) ) 

#define ILdrfGeneral2_GetFPTNVIndex(This,pInfo,FPTIndex,NVMemberNumber,pNVIndex,returnCode)	\
    ( (This)->lpVtbl -> GetFPTNVIndex(This,pInfo,FPTIndex,NVMemberNumber,pNVIndex,returnCode) ) 

#define ILdrfGeneral2_GetFPTCPIndex(This,pInfo,FPTIndex,CPMemberNumber,CPIndex,returnCode)	\
    ( (This)->lpVtbl -> GetFPTCPIndex(This,pInfo,FPTIndex,CPMemberNumber,CPIndex,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfGeneral2_INTERFACE_DEFINED__ */


#ifndef __ILdrfMiscFns1_INTERFACE_DEFINED__
#define __ILdrfMiscFns1_INTERFACE_DEFINED__

/* interface ILdrfMiscFns1 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfMiscFns1;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A6273645-034C-477F-8B9E-026E377CA096")
    ILdrfMiscFns1 : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteResourceString( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteEnumSet( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteNVT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteCPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteFPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EnableEmptyEntries( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PurgeFile( 
            /* [in] */ BSTR pathIn,
            /* [in] */ BSTR pathOut,
            /* [in] */ long checkCRC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTCPArrayDetails( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [in] */ long minArraySize,
            /* [in] */ long maxArraySize,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTCPArrayDetails( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pMinArraySize,
            /* [out] */ long *pMaxArraySize,
            /* [out] */ long *pDetailsAreDefaults,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ValidateEnumSet( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ValidateNVT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ValidateCPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ValidateFPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ValidateResourceString( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindEmptyNVT( 
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindEmptyCPT( 
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindEmptyFPT( 
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindEmptyResourceString( 
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDRFAPIErrorString( 
            /* [in] */ long errorCode,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDRFAPIVersion( 
            /* [out] */ long *pMajor,
            /* [out] */ long *pMinor,
            /* [out] */ long *pFix,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExtendedDataTypeAware( 
            /* [in] */ long pInfo,
            /* [in] */ long dataType,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDoubleFloatDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ double minValid,
            /* [in] */ double maxValid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDoubleFloatDetails( 
            /* [in] */ long pTypeTree,
            /* [out] */ double *pMinValid,
            /* [out] */ double *pMaxValid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextSupportedNVTType( 
            /* [in] */ long pInfo,
            /* [out] */ long *pNVTType,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTypeNameString( 
            /* [in] */ long nvtType,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE LookupTypeNameString( 
            /* [in] */ BSTR string,
            /* [out] */ long *pNVTType,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfMiscFns1Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfMiscFns1 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfMiscFns1 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfMiscFns1 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfMiscFns1 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfMiscFns1 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfMiscFns1 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfMiscFns1 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteResourceString )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteEnumSet )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteNVT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteCPT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteFPT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnableEmptyEntries )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PurgeFile )( 
            ILdrfMiscFns1 * This,
            /* [in] */ BSTR pathIn,
            /* [in] */ BSTR pathOut,
            /* [in] */ long checkCRC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTCPArrayDetails )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [in] */ long minArraySize,
            /* [in] */ long maxArraySize,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTCPArrayDetails )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pMinArraySize,
            /* [out] */ long *pMaxArraySize,
            /* [out] */ long *pDetailsAreDefaults,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ValidateEnumSet )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ValidateNVT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ValidateCPT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ValidateFPT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ValidateResourceString )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindEmptyNVT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindEmptyCPT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindEmptyFPT )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindEmptyResourceString )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDRFAPIErrorString )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long errorCode,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDRFAPIVersion )( 
            ILdrfMiscFns1 * This,
            /* [out] */ long *pMajor,
            /* [out] */ long *pMinor,
            /* [out] */ long *pFix,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ExtendedDataTypeAware )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [in] */ long dataType,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDoubleFloatDetails )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pTypeTree,
            /* [in] */ double minValid,
            /* [in] */ double maxValid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDoubleFloatDetails )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pTypeTree,
            /* [out] */ double *pMinValid,
            /* [out] */ double *pMaxValid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNextSupportedNVTType )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pNVTType,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTypeNameString )( 
            ILdrfMiscFns1 * This,
            /* [in] */ long nvtType,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *LookupTypeNameString )( 
            ILdrfMiscFns1 * This,
            /* [in] */ BSTR string,
            /* [out] */ long *pNVTType,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfMiscFns1Vtbl;

    interface ILdrfMiscFns1
    {
        CONST_VTBL struct ILdrfMiscFns1Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfMiscFns1_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfMiscFns1_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfMiscFns1_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfMiscFns1_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfMiscFns1_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfMiscFns1_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfMiscFns1_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfMiscFns1_DeleteResourceString(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> DeleteResourceString(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_DeleteEnumSet(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> DeleteEnumSet(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_DeleteNVT(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> DeleteNVT(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_DeleteCPT(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> DeleteCPT(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_DeleteFPT(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> DeleteFPT(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_EnableEmptyEntries(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> EnableEmptyEntries(This,pInfo,returnCode) ) 

#define ILdrfMiscFns1_PurgeFile(This,pathIn,pathOut,checkCRC,returnCode)	\
    ( (This)->lpVtbl -> PurgeFile(This,pathIn,pathOut,checkCRC,returnCode) ) 

#define ILdrfMiscFns1_SetFPTCPArrayDetails(This,pInfo,FPTIndex,CPIndex,minArraySize,maxArraySize,returnCode)	\
    ( (This)->lpVtbl -> SetFPTCPArrayDetails(This,pInfo,FPTIndex,CPIndex,minArraySize,maxArraySize,returnCode) ) 

#define ILdrfMiscFns1_GetFPTCPArrayDetails(This,pInfo,FPTIndex,CPIndex,pMinArraySize,pMaxArraySize,pDetailsAreDefaults,returnCode)	\
    ( (This)->lpVtbl -> GetFPTCPArrayDetails(This,pInfo,FPTIndex,CPIndex,pMinArraySize,pMaxArraySize,pDetailsAreDefaults,returnCode) ) 

#define ILdrfMiscFns1_ValidateEnumSet(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> ValidateEnumSet(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_ValidateNVT(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> ValidateNVT(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_ValidateCPT(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> ValidateCPT(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_ValidateFPT(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> ValidateFPT(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_ValidateResourceString(This,pInfo,index,returnCode)	\
    ( (This)->lpVtbl -> ValidateResourceString(This,pInfo,index,returnCode) ) 

#define ILdrfMiscFns1_FindEmptyNVT(This,pInfo,pIndex,returnCode)	\
    ( (This)->lpVtbl -> FindEmptyNVT(This,pInfo,pIndex,returnCode) ) 

#define ILdrfMiscFns1_FindEmptyCPT(This,pInfo,pIndex,returnCode)	\
    ( (This)->lpVtbl -> FindEmptyCPT(This,pInfo,pIndex,returnCode) ) 

#define ILdrfMiscFns1_FindEmptyFPT(This,pInfo,pIndex,returnCode)	\
    ( (This)->lpVtbl -> FindEmptyFPT(This,pInfo,pIndex,returnCode) ) 

#define ILdrfMiscFns1_FindEmptyResourceString(This,pInfo,pIndex,returnCode)	\
    ( (This)->lpVtbl -> FindEmptyResourceString(This,pInfo,pIndex,returnCode) ) 

#define ILdrfMiscFns1_GetDRFAPIErrorString(This,errorCode,pString,returnCode)	\
    ( (This)->lpVtbl -> GetDRFAPIErrorString(This,errorCode,pString,returnCode) ) 

#define ILdrfMiscFns1_GetDRFAPIVersion(This,pMajor,pMinor,pFix,returnCode)	\
    ( (This)->lpVtbl -> GetDRFAPIVersion(This,pMajor,pMinor,pFix,returnCode) ) 

#define ILdrfMiscFns1_ExtendedDataTypeAware(This,pInfo,dataType,returnCode)	\
    ( (This)->lpVtbl -> ExtendedDataTypeAware(This,pInfo,dataType,returnCode) ) 

#define ILdrfMiscFns1_SetDoubleFloatDetails(This,pTypeTree,minValid,maxValid,returnCode)	\
    ( (This)->lpVtbl -> SetDoubleFloatDetails(This,pTypeTree,minValid,maxValid,returnCode) ) 

#define ILdrfMiscFns1_GetDoubleFloatDetails(This,pTypeTree,pMinValid,pMaxValid,returnCode)	\
    ( (This)->lpVtbl -> GetDoubleFloatDetails(This,pTypeTree,pMinValid,pMaxValid,returnCode) ) 

#define ILdrfMiscFns1_GetNextSupportedNVTType(This,pInfo,pNVTType,returnCode)	\
    ( (This)->lpVtbl -> GetNextSupportedNVTType(This,pInfo,pNVTType,returnCode) ) 

#define ILdrfMiscFns1_GetTypeNameString(This,nvtType,pString,returnCode)	\
    ( (This)->lpVtbl -> GetTypeNameString(This,nvtType,pString,returnCode) ) 

#define ILdrfMiscFns1_LookupTypeNameString(This,string,pNVTType,returnCode)	\
    ( (This)->lpVtbl -> LookupTypeNameString(This,string,pNVTType,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfMiscFns1_INTERFACE_DEFINED__ */


#ifndef __ILdrfCatalog_INTERFACE_DEFINED__
#define __ILdrfCatalog_INTERFACE_DEFINED__

/* interface ILdrfCatalog */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfCatalog;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1CB4B4C7-EC74-11CF-8CC7-444553540000")
    ILdrfCatalog : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OpenCatalog( 
            /* [in] */ BSTR directory,
            /* [in] */ long readOnly,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCatalogInfo( 
            /* [in] */ long pInfo,
            /* [out] */ long *pStale,
            /* [out] */ long *pNumDirec,
            /* [out] */ long *pNumLangFiles,
            /* [out] */ long *pNumTypeFiles,
            /* [out] */ long *pNumFPTFiles,
            /* [out] */ long *pNumFormatFiles,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseCatalog( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogAddDir( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR newDir,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogGetDir( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ BSTR *pDirName,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogRmvDir( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR oldDir,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogRefresh( 
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogAddFile( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR newFile,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogGetFile( 
            /* [in] */ long pInfo,
            /* [in] */ long fileType,
            /* [in] */ long index,
            /* [out] */ long *pMatchScope,
            /* [out] */ BSTR *pProgID,
            /* [out] */ long *pDirIndex,
            /* [out] */ long *pMajorVersion,
            /* [out] */ long *pMinorVersion,
            /* [out] */ long *pLocale,
            /* [out] */ BSTR *pFileName,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CatalogRmvFile( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR oldFile,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SearchCatalog( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR progID,
            /* [in] */ long matchScope,
            /* [in] */ long fileType,
            /* [in] */ long locale,
            /* [out] */ BSTR *pFile,
            /* [out] */ long *pMajorVersion,
            /* [out] */ long *pMinorVersion,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE MatchProgID( 
            /* [in] */ long matchScope,
            /* [in] */ BSTR fileRefID,
            /* [in] */ BSTR progID,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfCatalogVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfCatalog * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfCatalog * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfCatalog * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfCatalog * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfCatalog * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfCatalog * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfCatalog * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OpenCatalog )( 
            ILdrfCatalog * This,
            /* [in] */ BSTR directory,
            /* [in] */ long readOnly,
            /* [out] */ long *ppInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCatalogInfo )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pStale,
            /* [out] */ long *pNumDirec,
            /* [out] */ long *pNumLangFiles,
            /* [out] */ long *pNumTypeFiles,
            /* [out] */ long *pNumFPTFiles,
            /* [out] */ long *pNumFormatFiles,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseCatalog )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogAddDir )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR newDir,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogGetDir )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ BSTR *pDirName,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogRmvDir )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR oldDir,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogRefresh )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogAddFile )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR newFile,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogGetFile )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [in] */ long fileType,
            /* [in] */ long index,
            /* [out] */ long *pMatchScope,
            /* [out] */ BSTR *pProgID,
            /* [out] */ long *pDirIndex,
            /* [out] */ long *pMajorVersion,
            /* [out] */ long *pMinorVersion,
            /* [out] */ long *pLocale,
            /* [out] */ BSTR *pFileName,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CatalogRmvFile )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR oldFile,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SearchCatalog )( 
            ILdrfCatalog * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR progID,
            /* [in] */ long matchScope,
            /* [in] */ long fileType,
            /* [in] */ long locale,
            /* [out] */ BSTR *pFile,
            /* [out] */ long *pMajorVersion,
            /* [out] */ long *pMinorVersion,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *MatchProgID )( 
            ILdrfCatalog * This,
            /* [in] */ long matchScope,
            /* [in] */ BSTR fileRefID,
            /* [in] */ BSTR progID,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfCatalogVtbl;

    interface ILdrfCatalog
    {
        CONST_VTBL struct ILdrfCatalogVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfCatalog_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfCatalog_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfCatalog_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfCatalog_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfCatalog_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfCatalog_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfCatalog_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfCatalog_OpenCatalog(This,directory,readOnly,ppInfo,returnCode)	\
    ( (This)->lpVtbl -> OpenCatalog(This,directory,readOnly,ppInfo,returnCode) ) 

#define ILdrfCatalog_GetCatalogInfo(This,pInfo,pStale,pNumDirec,pNumLangFiles,pNumTypeFiles,pNumFPTFiles,pNumFormatFiles,returnCode)	\
    ( (This)->lpVtbl -> GetCatalogInfo(This,pInfo,pStale,pNumDirec,pNumLangFiles,pNumTypeFiles,pNumFPTFiles,pNumFormatFiles,returnCode) ) 

#define ILdrfCatalog_CloseCatalog(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CloseCatalog(This,pInfo,returnCode) ) 

#define ILdrfCatalog_CatalogAddDir(This,pInfo,newDir,returnCode)	\
    ( (This)->lpVtbl -> CatalogAddDir(This,pInfo,newDir,returnCode) ) 

#define ILdrfCatalog_CatalogGetDir(This,pInfo,index,pDirName,returnCode)	\
    ( (This)->lpVtbl -> CatalogGetDir(This,pInfo,index,pDirName,returnCode) ) 

#define ILdrfCatalog_CatalogRmvDir(This,pInfo,oldDir,returnCode)	\
    ( (This)->lpVtbl -> CatalogRmvDir(This,pInfo,oldDir,returnCode) ) 

#define ILdrfCatalog_CatalogRefresh(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> CatalogRefresh(This,pInfo,returnCode) ) 

#define ILdrfCatalog_CatalogAddFile(This,pInfo,newFile,returnCode)	\
    ( (This)->lpVtbl -> CatalogAddFile(This,pInfo,newFile,returnCode) ) 

#define ILdrfCatalog_CatalogGetFile(This,pInfo,fileType,index,pMatchScope,pProgID,pDirIndex,pMajorVersion,pMinorVersion,pLocale,pFileName,returnCode)	\
    ( (This)->lpVtbl -> CatalogGetFile(This,pInfo,fileType,index,pMatchScope,pProgID,pDirIndex,pMajorVersion,pMinorVersion,pLocale,pFileName,returnCode) ) 

#define ILdrfCatalog_CatalogRmvFile(This,pInfo,oldFile,returnCode)	\
    ( (This)->lpVtbl -> CatalogRmvFile(This,pInfo,oldFile,returnCode) ) 

#define ILdrfCatalog_SearchCatalog(This,pInfo,progID,matchScope,fileType,locale,pFile,pMajorVersion,pMinorVersion,returnCode)	\
    ( (This)->lpVtbl -> SearchCatalog(This,pInfo,progID,matchScope,fileType,locale,pFile,pMajorVersion,pMinorVersion,returnCode) ) 

#define ILdrfCatalog_MatchProgID(This,matchScope,fileRefID,progID,returnCode)	\
    ( (This)->lpVtbl -> MatchProgID(This,matchScope,fileRefID,progID,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfCatalog_INTERFACE_DEFINED__ */


#ifndef __ILdrfLangResource_INTERFACE_DEFINED__
#define __ILdrfLangResource_INTERFACE_DEFINED__

/* interface ILdrfLangResource */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfLangResource;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1CB4B4CD-EC74-11CF-8CC7-444553540000")
    ILdrfLangResource : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLangFileInfo( 
            /* [in] */ long pInfo,
            /* [out] */ long *pLocale,
            /* [out] */ long *pNumResources,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLangFileInfo( 
            /* [in] */ long pInfo,
            /* [in] */ long locale,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetResourceString( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetASCIIResource( 
            /* [in] */ long pInfo,
            /* [in] */ long shareDup,
            /* [in] */ long index,
            /* [in] */ BSTR string,
            /* [out] */ long *dupIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNumLanguages( 
            /* [in] */ BSTR infDirectory,
            /* [out] */ long *pNumLanguages,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLanguageInfo( 
            /* [in] */ BSTR infDirectory,
            /* [in] */ long myLocale,
            /* [in] */ long key,
            /* [in] */ BSTR catDirectory,
            /* [out] */ long *pMSLocaleID,
            /* [out] */ long *pLocale,
            /* [out] */ BSTR *pFileExtension,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLanguageKeyFromLocale( 
            /* [in] */ BSTR infDirectory,
            /* [in] */ long locale,
            /* [out] */ long *pKey,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLanguageKeyFromExtension( 
            /* [in] */ BSTR infDirectory,
            /* [in] */ BSTR fileExtension,
            /* [out] */ long *pKey,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StartStringService( 
            /* [in] */ long locale,
            /* [in] */ BSTR catDirectory,
            /* [out] */ long *ppService,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddStringServiceLocale( 
            /* [in] */ long pService,
            /* [in] */ long locale,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StopStringService( 
            /* [in] */ long pService,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StringServiceRequest( 
            /* [in] */ long pService,
            /* [in] */ BSTR progID,
            /* [in] */ long scope,
            /* [in] */ long index,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLanguageKeyFromMSLocaleID( 
            /* [in] */ BSTR infDirectory,
            /* [in] */ long MSLocaleID,
            /* [out] */ long *pKey,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfLangResourceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfLangResource * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfLangResource * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfLangResource * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfLangResource * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfLangResource * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfLangResource * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfLangResource * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLangFileInfo )( 
            ILdrfLangResource * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pLocale,
            /* [out] */ long *pNumResources,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLangFileInfo )( 
            ILdrfLangResource * This,
            /* [in] */ long pInfo,
            /* [in] */ long locale,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetResourceString )( 
            ILdrfLangResource * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetASCIIResource )( 
            ILdrfLangResource * This,
            /* [in] */ long pInfo,
            /* [in] */ long shareDup,
            /* [in] */ long index,
            /* [in] */ BSTR string,
            /* [out] */ long *dupIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNumLanguages )( 
            ILdrfLangResource * This,
            /* [in] */ BSTR infDirectory,
            /* [out] */ long *pNumLanguages,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLanguageInfo )( 
            ILdrfLangResource * This,
            /* [in] */ BSTR infDirectory,
            /* [in] */ long myLocale,
            /* [in] */ long key,
            /* [in] */ BSTR catDirectory,
            /* [out] */ long *pMSLocaleID,
            /* [out] */ long *pLocale,
            /* [out] */ BSTR *pFileExtension,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLanguageKeyFromLocale )( 
            ILdrfLangResource * This,
            /* [in] */ BSTR infDirectory,
            /* [in] */ long locale,
            /* [out] */ long *pKey,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLanguageKeyFromExtension )( 
            ILdrfLangResource * This,
            /* [in] */ BSTR infDirectory,
            /* [in] */ BSTR fileExtension,
            /* [out] */ long *pKey,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartStringService )( 
            ILdrfLangResource * This,
            /* [in] */ long locale,
            /* [in] */ BSTR catDirectory,
            /* [out] */ long *ppService,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddStringServiceLocale )( 
            ILdrfLangResource * This,
            /* [in] */ long pService,
            /* [in] */ long locale,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopStringService )( 
            ILdrfLangResource * This,
            /* [in] */ long pService,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StringServiceRequest )( 
            ILdrfLangResource * This,
            /* [in] */ long pService,
            /* [in] */ BSTR progID,
            /* [in] */ long scope,
            /* [in] */ long index,
            /* [out] */ BSTR *pString,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLanguageKeyFromMSLocaleID )( 
            ILdrfLangResource * This,
            /* [in] */ BSTR infDirectory,
            /* [in] */ long MSLocaleID,
            /* [out] */ long *pKey,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfLangResourceVtbl;

    interface ILdrfLangResource
    {
        CONST_VTBL struct ILdrfLangResourceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfLangResource_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfLangResource_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfLangResource_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfLangResource_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfLangResource_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfLangResource_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfLangResource_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfLangResource_GetLangFileInfo(This,pInfo,pLocale,pNumResources,returnCode)	\
    ( (This)->lpVtbl -> GetLangFileInfo(This,pInfo,pLocale,pNumResources,returnCode) ) 

#define ILdrfLangResource_SetLangFileInfo(This,pInfo,locale,returnCode)	\
    ( (This)->lpVtbl -> SetLangFileInfo(This,pInfo,locale,returnCode) ) 

#define ILdrfLangResource_GetResourceString(This,pInfo,index,pString,returnCode)	\
    ( (This)->lpVtbl -> GetResourceString(This,pInfo,index,pString,returnCode) ) 

#define ILdrfLangResource_SetASCIIResource(This,pInfo,shareDup,index,string,dupIndex,returnCode)	\
    ( (This)->lpVtbl -> SetASCIIResource(This,pInfo,shareDup,index,string,dupIndex,returnCode) ) 

#define ILdrfLangResource_GetNumLanguages(This,infDirectory,pNumLanguages,returnCode)	\
    ( (This)->lpVtbl -> GetNumLanguages(This,infDirectory,pNumLanguages,returnCode) ) 

#define ILdrfLangResource_GetLanguageInfo(This,infDirectory,myLocale,key,catDirectory,pMSLocaleID,pLocale,pFileExtension,pString,returnCode)	\
    ( (This)->lpVtbl -> GetLanguageInfo(This,infDirectory,myLocale,key,catDirectory,pMSLocaleID,pLocale,pFileExtension,pString,returnCode) ) 

#define ILdrfLangResource_GetLanguageKeyFromLocale(This,infDirectory,locale,pKey,returnCode)	\
    ( (This)->lpVtbl -> GetLanguageKeyFromLocale(This,infDirectory,locale,pKey,returnCode) ) 

#define ILdrfLangResource_GetLanguageKeyFromExtension(This,infDirectory,fileExtension,pKey,returnCode)	\
    ( (This)->lpVtbl -> GetLanguageKeyFromExtension(This,infDirectory,fileExtension,pKey,returnCode) ) 

#define ILdrfLangResource_StartStringService(This,locale,catDirectory,ppService,returnCode)	\
    ( (This)->lpVtbl -> StartStringService(This,locale,catDirectory,ppService,returnCode) ) 

#define ILdrfLangResource_AddStringServiceLocale(This,pService,locale,returnCode)	\
    ( (This)->lpVtbl -> AddStringServiceLocale(This,pService,locale,returnCode) ) 

#define ILdrfLangResource_StopStringService(This,pService,returnCode)	\
    ( (This)->lpVtbl -> StopStringService(This,pService,returnCode) ) 

#define ILdrfLangResource_StringServiceRequest(This,pService,progID,scope,index,pString,returnCode)	\
    ( (This)->lpVtbl -> StringServiceRequest(This,pService,progID,scope,index,pString,returnCode) ) 

#define ILdrfLangResource_GetLanguageKeyFromMSLocaleID(This,infDirectory,MSLocaleID,pKey,returnCode)	\
    ( (This)->lpVtbl -> GetLanguageKeyFromMSLocaleID(This,infDirectory,MSLocaleID,pKey,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfLangResource_INTERFACE_DEFINED__ */


#ifndef __ILdrfTypes_INTERFACE_DEFINED__
#define __ILdrfTypes_INTERFACE_DEFINED__

/* interface ILdrfTypes */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfTypes;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1CB4B4D3-EC74-11CF-8CC7-444553540000")
    ILdrfTypes : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTypeFileInfo( 
            /* [in] */ long pInfo,
            /* [out] */ long *pResDep0,
            /* [out] */ long *pResDep1,
            /* [out] */ long *pResDep2,
            /* [out] */ long *pResDep3,
            /* [out] */ long *pResDep4,
            /* [out] */ long *pResDep5,
            /* [out] */ long *pResDep6,
            /* [out] */ long *pTypDep0,
            /* [out] */ long *pTypDep1,
            /* [out] */ long *pTypDep2,
            /* [out] */ long *pTypDep3,
            /* [out] */ long *pTypDep4,
            /* [out] */ long *pTypDep5,
            /* [out] */ long *pTypDep6,
            /* [out] */ long *pNumNVTs,
            /* [out] */ long *pNumCPTs,
            /* [out] */ long *pNumEnumSets,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTypeFileInfo( 
            /* [in] */ long pInfo,
            /* [in] */ long resDep0,
            /* [in] */ long resDep1,
            /* [in] */ long resDep2,
            /* [in] */ long resDep3,
            /* [in] */ long resDep4,
            /* [in] */ long resDep5,
            /* [in] */ long resDep6,
            /* [in] */ long typDep0,
            /* [in] */ long typDep1,
            /* [in] */ long typDep2,
            /* [in] */ long typDep3,
            /* [in] */ long typDep4,
            /* [in] */ long typDep5,
            /* [in] */ long typDep6,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectEnumSet( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ BSTR *pTag,
            /* [out] */ BSTR *pFile,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectEnumSetByTag( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR tag,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectEnumSetByFile( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR file,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectNewEnumSet( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR tag,
            /* [in] */ BSTR file,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetEnumMember( 
            /* [in] */ long pInfo,
            /* [in] */ long value,
            /* [out] */ BSTR *pString,
            /* [out] */ long *pResScope,
            /* [out] */ long *pResIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetEnumValue( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR string,
            /* [out] */ long *pValue,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetEnumMember( 
            /* [in] */ long pInfo,
            /* [in] */ long value,
            /* [in] */ BSTR string,
            /* [in] */ long resScope,
            /* [in] */ long resIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNVT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *ppTypeTree,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNVTByName( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *ppTypeTree,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetNVT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long pTypeTree,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCPTByName( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long pTypeTree,
            /* [in] */ long inheritable,
            /* [in] */ BSTR pMin,
            /* [in] */ BSTR pMax,
            /* [in] */ BSTR pInit,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCPTByNameEx( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [out] */ BSTR *ppInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCPTEx( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [out] */ BSTR *ppInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCPTEx( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long pTypeTree,
            /* [in] */ long inheritable,
            /* [in] */ BSTR pMin,
            /* [in] */ BSTR pMax,
            /* [in] */ BSTR pInit,
            /* [in] */ BSTR pInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfTypesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfTypes * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfTypes * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfTypes * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfTypes * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfTypes * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfTypes * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfTypes * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTypeFileInfo )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pResDep0,
            /* [out] */ long *pResDep1,
            /* [out] */ long *pResDep2,
            /* [out] */ long *pResDep3,
            /* [out] */ long *pResDep4,
            /* [out] */ long *pResDep5,
            /* [out] */ long *pResDep6,
            /* [out] */ long *pTypDep0,
            /* [out] */ long *pTypDep1,
            /* [out] */ long *pTypDep2,
            /* [out] */ long *pTypDep3,
            /* [out] */ long *pTypDep4,
            /* [out] */ long *pTypDep5,
            /* [out] */ long *pTypDep6,
            /* [out] */ long *pNumNVTs,
            /* [out] */ long *pNumCPTs,
            /* [out] */ long *pNumEnumSets,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTypeFileInfo )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long resDep0,
            /* [in] */ long resDep1,
            /* [in] */ long resDep2,
            /* [in] */ long resDep3,
            /* [in] */ long resDep4,
            /* [in] */ long resDep5,
            /* [in] */ long resDep6,
            /* [in] */ long typDep0,
            /* [in] */ long typDep1,
            /* [in] */ long typDep2,
            /* [in] */ long typDep3,
            /* [in] */ long typDep4,
            /* [in] */ long typDep5,
            /* [in] */ long typDep6,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SelectEnumSet )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ BSTR *pTag,
            /* [out] */ BSTR *pFile,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SelectEnumSetByTag )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR tag,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SelectEnumSetByFile )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR file,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SelectNewEnumSet )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR tag,
            /* [in] */ BSTR file,
            /* [out] */ long *pIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetEnumMember )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long value,
            /* [out] */ BSTR *pString,
            /* [out] */ long *pResScope,
            /* [out] */ long *pResIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetEnumValue )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR string,
            /* [out] */ long *pValue,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetEnumMember )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long value,
            /* [in] */ BSTR string,
            /* [in] */ long resScope,
            /* [in] */ long resIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNVT )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *ppTypeTree,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNVTByName )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *ppTypeTree,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetNVT )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long pTypeTree,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCPT )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCPTByName )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCPT )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long pTypeTree,
            /* [in] */ long inheritable,
            /* [in] */ BSTR pMin,
            /* [in] */ BSTR pMax,
            /* [in] */ BSTR pInit,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCPTByNameEx )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [out] */ BSTR *ppInvalid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCPTEx )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *ppTypeTree,
            /* [out] */ long *pInheritable,
            /* [out] */ BSTR *ppMin,
            /* [out] */ BSTR *ppMax,
            /* [out] */ BSTR *ppInit,
            /* [out] */ BSTR *ppInvalid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCPTEx )( 
            ILdrfTypes * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long pTypeTree,
            /* [in] */ long inheritable,
            /* [in] */ BSTR pMin,
            /* [in] */ BSTR pMax,
            /* [in] */ BSTR pInit,
            /* [in] */ BSTR pInvalid,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfTypesVtbl;

    interface ILdrfTypes
    {
        CONST_VTBL struct ILdrfTypesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfTypes_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfTypes_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfTypes_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfTypes_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfTypes_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfTypes_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfTypes_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfTypes_GetTypeFileInfo(This,pInfo,pResDep0,pResDep1,pResDep2,pResDep3,pResDep4,pResDep5,pResDep6,pTypDep0,pTypDep1,pTypDep2,pTypDep3,pTypDep4,pTypDep5,pTypDep6,pNumNVTs,pNumCPTs,pNumEnumSets,returnCode)	\
    ( (This)->lpVtbl -> GetTypeFileInfo(This,pInfo,pResDep0,pResDep1,pResDep2,pResDep3,pResDep4,pResDep5,pResDep6,pTypDep0,pTypDep1,pTypDep2,pTypDep3,pTypDep4,pTypDep5,pTypDep6,pNumNVTs,pNumCPTs,pNumEnumSets,returnCode) ) 

#define ILdrfTypes_SetTypeFileInfo(This,pInfo,resDep0,resDep1,resDep2,resDep3,resDep4,resDep5,resDep6,typDep0,typDep1,typDep2,typDep3,typDep4,typDep5,typDep6,returnCode)	\
    ( (This)->lpVtbl -> SetTypeFileInfo(This,pInfo,resDep0,resDep1,resDep2,resDep3,resDep4,resDep5,resDep6,typDep0,typDep1,typDep2,typDep3,typDep4,typDep5,typDep6,returnCode) ) 

#define ILdrfTypes_SelectEnumSet(This,pInfo,index,pTag,pFile,returnCode)	\
    ( (This)->lpVtbl -> SelectEnumSet(This,pInfo,index,pTag,pFile,returnCode) ) 

#define ILdrfTypes_SelectEnumSetByTag(This,pInfo,tag,pIndex,returnCode)	\
    ( (This)->lpVtbl -> SelectEnumSetByTag(This,pInfo,tag,pIndex,returnCode) ) 

#define ILdrfTypes_SelectEnumSetByFile(This,pInfo,file,pIndex,returnCode)	\
    ( (This)->lpVtbl -> SelectEnumSetByFile(This,pInfo,file,pIndex,returnCode) ) 

#define ILdrfTypes_SelectNewEnumSet(This,pInfo,tag,file,pIndex,returnCode)	\
    ( (This)->lpVtbl -> SelectNewEnumSet(This,pInfo,tag,file,pIndex,returnCode) ) 

#define ILdrfTypes_GetEnumMember(This,pInfo,value,pString,pResScope,pResIndex,returnCode)	\
    ( (This)->lpVtbl -> GetEnumMember(This,pInfo,value,pString,pResScope,pResIndex,returnCode) ) 

#define ILdrfTypes_GetEnumValue(This,pInfo,string,pValue,returnCode)	\
    ( (This)->lpVtbl -> GetEnumValue(This,pInfo,string,pValue,returnCode) ) 

#define ILdrfTypes_SetEnumMember(This,pInfo,value,string,resScope,resIndex,returnCode)	\
    ( (This)->lpVtbl -> SetEnumMember(This,pInfo,value,string,resScope,resIndex,returnCode) ) 

#define ILdrfTypes_GetNVT(This,pInfo,index,ppTypeTree,returnCode)	\
    ( (This)->lpVtbl -> GetNVT(This,pInfo,index,ppTypeTree,returnCode) ) 

#define ILdrfTypes_GetNVTByName(This,pInfo,name,pIndex,ppTypeTree,returnCode)	\
    ( (This)->lpVtbl -> GetNVTByName(This,pInfo,name,pIndex,ppTypeTree,returnCode) ) 

#define ILdrfTypes_SetNVT(This,pInfo,index,pTypeTree,returnCode)	\
    ( (This)->lpVtbl -> SetNVT(This,pInfo,index,pTypeTree,returnCode) ) 

#define ILdrfTypes_GetCPT(This,pInfo,index,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,returnCode)	\
    ( (This)->lpVtbl -> GetCPT(This,pInfo,index,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,returnCode) ) 

#define ILdrfTypes_GetCPTByName(This,pInfo,name,pIndex,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,returnCode)	\
    ( (This)->lpVtbl -> GetCPTByName(This,pInfo,name,pIndex,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,returnCode) ) 

#define ILdrfTypes_SetCPT(This,pInfo,index,pTypeTree,inheritable,pMin,pMax,pInit,returnCode)	\
    ( (This)->lpVtbl -> SetCPT(This,pInfo,index,pTypeTree,inheritable,pMin,pMax,pInit,returnCode) ) 

#define ILdrfTypes_GetCPTByNameEx(This,pInfo,name,pIndex,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,ppInvalid,returnCode)	\
    ( (This)->lpVtbl -> GetCPTByNameEx(This,pInfo,name,pIndex,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,ppInvalid,returnCode) ) 

#define ILdrfTypes_GetCPTEx(This,pInfo,index,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,ppInvalid,returnCode)	\
    ( (This)->lpVtbl -> GetCPTEx(This,pInfo,index,ppTypeTree,pInheritable,ppMin,ppMax,ppInit,ppInvalid,returnCode) ) 

#define ILdrfTypes_SetCPTEx(This,pInfo,index,pTypeTree,inheritable,pMin,pMax,pInit,pInvalid,returnCode)	\
    ( (This)->lpVtbl -> SetCPTEx(This,pInfo,index,pTypeTree,inheritable,pMin,pMax,pInit,pInvalid,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfTypes_INTERFACE_DEFINED__ */


#ifndef __ILdrfTypeTree_INTERFACE_DEFINED__
#define __ILdrfTypeTree_INTERFACE_DEFINED__

/* interface ILdrfTypeTree */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfTypeTree;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1CB4B4D9-EC74-11CF-8CC7-444553540000")
    ILdrfTypeTree : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FreeTypeTree( 
            /* [in] */ long pTypeTree,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE NewTypeTreeNode( 
            /* [in] */ long newNodeType,
            /* [in] */ BSTR name,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long resUntScope,
            /* [in] */ long resUntIndex,
            /* [out] */ long *ppTypeTree,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetScalarDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ long minValid,
            /* [in] */ long maxValid,
            /* [in] */ long scaleA,
            /* [in] */ long scaleB,
            /* [in] */ long scaleC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFloatDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ float minValid,
            /* [in] */ float maxValid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBitfieldDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ long bitfSize,
            /* [in] */ long bitfOffset,
            /* [in] */ long bitfSigned,
            /* [in] */ long minValid,
            /* [in] */ long maxValid,
            /* [in] */ long scaleA,
            /* [in] */ long scaleB,
            /* [in] */ long scaleC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetEnumDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ long enumScope,
            /* [in] */ long enumIndex,
            /* [in] */ long minValid,
            /* [in] */ long maxValid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetArrayDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ long numElements,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetStructUnionDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ long numFields,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetReferenceDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ long typeScope,
            /* [in] */ long typeIndex,
            /* [in] */ long typeSize,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ScanTypeTree( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pTypeSize,
            /* [out] */ long *pHasRefs,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindTypeTreeNode( 
            /* [in] */ long pTypeTree,
            /* [in] */ long relative,
            /* [in] */ BSTR fieldName,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReadTypeTreeNode( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pNodeType,
            /* [out] */ long *pTypeOffset,
            /* [out] */ long *pTypeSize,
            /* [out] */ BSTR *pName,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pResUntScope,
            /* [out] */ long *pResUntIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetScalarDetails( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pMinValid,
            /* [out] */ long *pMaxValid,
            /* [out] */ long *pScaleA,
            /* [out] */ long *pScaleB,
            /* [out] */ long *pScaleC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFloatDetails( 
            /* [in] */ long pTypeTree,
            /* [out] */ float *pMinValid,
            /* [out] */ float *pMaxValid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBitfieldDetails( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pBitfSize,
            /* [out] */ long *pBitfOffset,
            /* [out] */ long *pBitfSigned,
            /* [out] */ long *pMinValid,
            /* [out] */ long *pMaxValid,
            /* [out] */ long *pScaleA,
            /* [out] */ long *pScaleB,
            /* [out] */ long *pScaleC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetEnumDetails( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pEnumScope,
            /* [out] */ long *pEnumIndex,
            /* [out] */ long *pMinValid,
            /* [out] */ long *pMaxValid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetArrayDetails( 
            /* [in] */ long pTypeTree,
            /* [in] */ long multiple,
            /* [out] */ long *pNumElements,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetStructUnionDetails( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pNumFields,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetReferenceDetails( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pTypeScope,
            /* [out] */ long *pTypeIndex,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GraftReference( 
            /* [in] */ long pTypeTree,
            /* [in] */ long pReferentTree,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ApplyValOverride( 
            /* [in] */ long pTypeTree,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ApplyValOverrideEx( 
            /* [in] */ long pTypeTree,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [in] */ BSTR valInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfTypeTreeVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfTypeTree * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfTypeTree * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfTypeTree * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfTypeTree * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfTypeTree * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfTypeTree * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfTypeTree * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FreeTypeTree )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *NewTypeTreeNode )( 
            ILdrfTypeTree * This,
            /* [in] */ long newNodeType,
            /* [in] */ BSTR name,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long resUntScope,
            /* [in] */ long resUntIndex,
            /* [out] */ long *ppTypeTree,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetScalarDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long minValid,
            /* [in] */ long maxValid,
            /* [in] */ long scaleA,
            /* [in] */ long scaleB,
            /* [in] */ long scaleC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFloatDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ float minValid,
            /* [in] */ float maxValid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBitfieldDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long bitfSize,
            /* [in] */ long bitfOffset,
            /* [in] */ long bitfSigned,
            /* [in] */ long minValid,
            /* [in] */ long maxValid,
            /* [in] */ long scaleA,
            /* [in] */ long scaleB,
            /* [in] */ long scaleC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetEnumDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long enumScope,
            /* [in] */ long enumIndex,
            /* [in] */ long minValid,
            /* [in] */ long maxValid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetArrayDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long numElements,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetStructUnionDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long numFields,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetReferenceDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long typeScope,
            /* [in] */ long typeIndex,
            /* [in] */ long typeSize,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScanTypeTree )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pTypeSize,
            /* [out] */ long *pHasRefs,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindTypeTreeNode )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long relative,
            /* [in] */ BSTR fieldName,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReadTypeTreeNode )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pNodeType,
            /* [out] */ long *pTypeOffset,
            /* [out] */ long *pTypeSize,
            /* [out] */ BSTR *pName,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pResUntScope,
            /* [out] */ long *pResUntIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetScalarDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pMinValid,
            /* [out] */ long *pMaxValid,
            /* [out] */ long *pScaleA,
            /* [out] */ long *pScaleB,
            /* [out] */ long *pScaleC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFloatDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ float *pMinValid,
            /* [out] */ float *pMaxValid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBitfieldDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pBitfSize,
            /* [out] */ long *pBitfOffset,
            /* [out] */ long *pBitfSigned,
            /* [out] */ long *pMinValid,
            /* [out] */ long *pMaxValid,
            /* [out] */ long *pScaleA,
            /* [out] */ long *pScaleB,
            /* [out] */ long *pScaleC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetEnumDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pEnumScope,
            /* [out] */ long *pEnumIndex,
            /* [out] */ long *pMinValid,
            /* [out] */ long *pMaxValid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetArrayDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long multiple,
            /* [out] */ long *pNumElements,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetStructUnionDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pNumFields,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetReferenceDetails )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pTypeScope,
            /* [out] */ long *pTypeIndex,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GraftReference )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ long pReferentTree,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ApplyValOverride )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ApplyValOverrideEx )( 
            ILdrfTypeTree * This,
            /* [in] */ long pTypeTree,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [in] */ BSTR valInvalid,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfTypeTreeVtbl;

    interface ILdrfTypeTree
    {
        CONST_VTBL struct ILdrfTypeTreeVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfTypeTree_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfTypeTree_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfTypeTree_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfTypeTree_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfTypeTree_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfTypeTree_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfTypeTree_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfTypeTree_FreeTypeTree(This,pTypeTree,returnCode)	\
    ( (This)->lpVtbl -> FreeTypeTree(This,pTypeTree,returnCode) ) 

#define ILdrfTypeTree_NewTypeTreeNode(This,newNodeType,name,resNmScope,resNmIndex,resCmtScope,resCmtIndex,resUntScope,resUntIndex,ppTypeTree,returnCode)	\
    ( (This)->lpVtbl -> NewTypeTreeNode(This,newNodeType,name,resNmScope,resNmIndex,resCmtScope,resCmtIndex,resUntScope,resUntIndex,ppTypeTree,returnCode) ) 

#define ILdrfTypeTree_SetScalarDetails(This,pTypeTree,minValid,maxValid,scaleA,scaleB,scaleC,returnCode)	\
    ( (This)->lpVtbl -> SetScalarDetails(This,pTypeTree,minValid,maxValid,scaleA,scaleB,scaleC,returnCode) ) 

#define ILdrfTypeTree_SetFloatDetails(This,pTypeTree,minValid,maxValid,returnCode)	\
    ( (This)->lpVtbl -> SetFloatDetails(This,pTypeTree,minValid,maxValid,returnCode) ) 

#define ILdrfTypeTree_SetBitfieldDetails(This,pTypeTree,bitfSize,bitfOffset,bitfSigned,minValid,maxValid,scaleA,scaleB,scaleC,returnCode)	\
    ( (This)->lpVtbl -> SetBitfieldDetails(This,pTypeTree,bitfSize,bitfOffset,bitfSigned,minValid,maxValid,scaleA,scaleB,scaleC,returnCode) ) 

#define ILdrfTypeTree_SetEnumDetails(This,pTypeTree,enumScope,enumIndex,minValid,maxValid,returnCode)	\
    ( (This)->lpVtbl -> SetEnumDetails(This,pTypeTree,enumScope,enumIndex,minValid,maxValid,returnCode) ) 

#define ILdrfTypeTree_SetArrayDetails(This,pTypeTree,numElements,returnCode)	\
    ( (This)->lpVtbl -> SetArrayDetails(This,pTypeTree,numElements,returnCode) ) 

#define ILdrfTypeTree_SetStructUnionDetails(This,pTypeTree,numFields,returnCode)	\
    ( (This)->lpVtbl -> SetStructUnionDetails(This,pTypeTree,numFields,returnCode) ) 

#define ILdrfTypeTree_SetReferenceDetails(This,pTypeTree,typeScope,typeIndex,typeSize,returnCode)	\
    ( (This)->lpVtbl -> SetReferenceDetails(This,pTypeTree,typeScope,typeIndex,typeSize,returnCode) ) 

#define ILdrfTypeTree_ScanTypeTree(This,pTypeTree,pTypeSize,pHasRefs,returnCode)	\
    ( (This)->lpVtbl -> ScanTypeTree(This,pTypeTree,pTypeSize,pHasRefs,returnCode) ) 

#define ILdrfTypeTree_FindTypeTreeNode(This,pTypeTree,relative,fieldName,returnCode)	\
    ( (This)->lpVtbl -> FindTypeTreeNode(This,pTypeTree,relative,fieldName,returnCode) ) 

#define ILdrfTypeTree_ReadTypeTreeNode(This,pTypeTree,pNodeType,pTypeOffset,pTypeSize,pName,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pResUntScope,pResUntIndex,returnCode)	\
    ( (This)->lpVtbl -> ReadTypeTreeNode(This,pTypeTree,pNodeType,pTypeOffset,pTypeSize,pName,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pResUntScope,pResUntIndex,returnCode) ) 

#define ILdrfTypeTree_GetScalarDetails(This,pTypeTree,pMinValid,pMaxValid,pScaleA,pScaleB,pScaleC,returnCode)	\
    ( (This)->lpVtbl -> GetScalarDetails(This,pTypeTree,pMinValid,pMaxValid,pScaleA,pScaleB,pScaleC,returnCode) ) 

#define ILdrfTypeTree_GetFloatDetails(This,pTypeTree,pMinValid,pMaxValid,returnCode)	\
    ( (This)->lpVtbl -> GetFloatDetails(This,pTypeTree,pMinValid,pMaxValid,returnCode) ) 

#define ILdrfTypeTree_GetBitfieldDetails(This,pTypeTree,pBitfSize,pBitfOffset,pBitfSigned,pMinValid,pMaxValid,pScaleA,pScaleB,pScaleC,returnCode)	\
    ( (This)->lpVtbl -> GetBitfieldDetails(This,pTypeTree,pBitfSize,pBitfOffset,pBitfSigned,pMinValid,pMaxValid,pScaleA,pScaleB,pScaleC,returnCode) ) 

#define ILdrfTypeTree_GetEnumDetails(This,pTypeTree,pEnumScope,pEnumIndex,pMinValid,pMaxValid,returnCode)	\
    ( (This)->lpVtbl -> GetEnumDetails(This,pTypeTree,pEnumScope,pEnumIndex,pMinValid,pMaxValid,returnCode) ) 

#define ILdrfTypeTree_GetArrayDetails(This,pTypeTree,multiple,pNumElements,returnCode)	\
    ( (This)->lpVtbl -> GetArrayDetails(This,pTypeTree,multiple,pNumElements,returnCode) ) 

#define ILdrfTypeTree_GetStructUnionDetails(This,pTypeTree,pNumFields,returnCode)	\
    ( (This)->lpVtbl -> GetStructUnionDetails(This,pTypeTree,pNumFields,returnCode) ) 

#define ILdrfTypeTree_GetReferenceDetails(This,pTypeTree,pTypeScope,pTypeIndex,returnCode)	\
    ( (This)->lpVtbl -> GetReferenceDetails(This,pTypeTree,pTypeScope,pTypeIndex,returnCode) ) 

#define ILdrfTypeTree_GraftReference(This,pTypeTree,pReferentTree,returnCode)	\
    ( (This)->lpVtbl -> GraftReference(This,pTypeTree,pReferentTree,returnCode) ) 

#define ILdrfTypeTree_ApplyValOverride(This,pTypeTree,valMin,valMax,returnCode)	\
    ( (This)->lpVtbl -> ApplyValOverride(This,pTypeTree,valMin,valMax,returnCode) ) 

#define ILdrfTypeTree_ApplyValOverrideEx(This,pTypeTree,valMin,valMax,valInvalid,returnCode)	\
    ( (This)->lpVtbl -> ApplyValOverrideEx(This,pTypeTree,valMin,valMax,valInvalid,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfTypeTree_INTERFACE_DEFINED__ */


#ifndef __ILdrfFuncProf_INTERFACE_DEFINED__
#define __ILdrfFuncProf_INTERFACE_DEFINED__

/* interface ILdrfFuncProf */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfFuncProf;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1CB4B4DF-EC74-11CF-8CC7-444553540000")
    ILdrfFuncProf : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTFileInfo( 
            /* [in] */ long pInfo,
            /* [out] */ long *pResDep0,
            /* [out] */ long *pResDep1,
            /* [out] */ long *pResDep2,
            /* [out] */ long *pResDep3,
            /* [out] */ long *pResDep4,
            /* [out] */ long *pResDep5,
            /* [out] */ long *pResDep6,
            /* [out] */ long *pTypDep0,
            /* [out] */ long *pTypDep1,
            /* [out] */ long *pTypDep2,
            /* [out] */ long *pTypDep3,
            /* [out] */ long *pTypDep4,
            /* [out] */ long *pTypDep5,
            /* [out] */ long *pTypDep6,
            /* [out] */ long *pNumFPTs,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTFileInfo( 
            /* [in] */ long pInfo,
            /* [in] */ long resDep0,
            /* [in] */ long resDep1,
            /* [in] */ long resDep2,
            /* [in] */ long resDep3,
            /* [in] */ long resDep4,
            /* [in] */ long resDep5,
            /* [in] */ long resDep6,
            /* [in] */ long typDep0,
            /* [in] */ long typDep1,
            /* [in] */ long typDep2,
            /* [in] */ long typDep3,
            /* [in] */ long typDep4,
            /* [in] */ long typDep5,
            /* [in] */ long typDep6,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pKey,
            /* [out] */ BSTR *pName,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pManNVs,
            /* [out] */ long *pOptNVs,
            /* [out] */ long *pManCPs,
            /* [out] */ long *pOptCPs,
            /* [out] */ long *pPrincipalNV,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTByName( 
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *pKey,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pManNVs,
            /* [out] */ long *pOptNVs,
            /* [out] */ long *pManCPs,
            /* [out] */ long *pOptCPs,
            /* [out] */ long *pPrincipalNV,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTByKey( 
            /* [in] */ long pInfo,
            /* [in] */ long key,
            /* [out] */ long *pIndex,
            /* [out] */ BSTR *pName,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pManNVs,
            /* [out] */ long *pOptNVs,
            /* [out] */ long *pManCPs,
            /* [out] */ long *pOptCPs,
            /* [out] */ long *pPrincipalNV,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPT( 
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long key,
            /* [in] */ BSTR name,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long manNVs,
            /* [in] */ long optNVs,
            /* [in] */ long manCPs,
            /* [in] */ long optCPs,
            /* [in] */ long principalNV,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTNV( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVIndex,
            /* [out] */ BSTR *pNVName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pNVTScope,
            /* [out] */ long *pNVTIndex,
            /* [out] */ long *pDirPollServ,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTCP( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pAppliesTo,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pCPTScope,
            /* [out] */ long *pCPTIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTCPByAttributes( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long appliesTo,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [out] */ long *pCPIndex,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTNV( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVMemberNumber,
            /* [in] */ BSTR NVName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long NVTScope,
            /* [in] */ long NVTIndex,
            /* [in] */ long dirPollServ,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTCP( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPMemberNumber,
            /* [in] */ long appliesTo,
            /* [in] */ BSTR CPName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [in] */ long modifyArray,
            /* [in] */ BSTR dflt,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTNVEx( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVIndex,
            /* [out] */ BSTR *pNVName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pNVTScope,
            /* [out] */ long *pNVTIndex,
            /* [out] */ long *pDirPollServ,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [out] */ BSTR *pValInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTCPEx( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pAppliesTo,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pCPTScope,
            /* [out] */ long *pCPTIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [out] */ BSTR *pValInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFPTCPByAttributesEx( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long appliesTo,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [out] */ long *pCPIndex,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [out] */ BSTR *pValInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTNVEx( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVMemberNumber,
            /* [in] */ BSTR NVName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long NVTScope,
            /* [in] */ long NVTIndex,
            /* [in] */ long dirPollServ,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [in] */ BSTR valInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFPTCPEx( 
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPMemberNumber,
            /* [in] */ long appliesTo,
            /* [in] */ BSTR CPName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [in] */ long modifyArray,
            /* [in] */ BSTR dflt,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [in] */ BSTR valInvalid,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfFuncProfVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfFuncProf * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfFuncProf * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfFuncProf * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfFuncProf * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfFuncProf * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfFuncProf * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfFuncProf * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTFileInfo )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [out] */ long *pResDep0,
            /* [out] */ long *pResDep1,
            /* [out] */ long *pResDep2,
            /* [out] */ long *pResDep3,
            /* [out] */ long *pResDep4,
            /* [out] */ long *pResDep5,
            /* [out] */ long *pResDep6,
            /* [out] */ long *pTypDep0,
            /* [out] */ long *pTypDep1,
            /* [out] */ long *pTypDep2,
            /* [out] */ long *pTypDep3,
            /* [out] */ long *pTypDep4,
            /* [out] */ long *pTypDep5,
            /* [out] */ long *pTypDep6,
            /* [out] */ long *pNumFPTs,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTFileInfo )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long resDep0,
            /* [in] */ long resDep1,
            /* [in] */ long resDep2,
            /* [in] */ long resDep3,
            /* [in] */ long resDep4,
            /* [in] */ long resDep5,
            /* [in] */ long resDep6,
            /* [in] */ long typDep0,
            /* [in] */ long typDep1,
            /* [in] */ long typDep2,
            /* [in] */ long typDep3,
            /* [in] */ long typDep4,
            /* [in] */ long typDep5,
            /* [in] */ long typDep6,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPT )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [out] */ long *pKey,
            /* [out] */ BSTR *pName,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pManNVs,
            /* [out] */ long *pOptNVs,
            /* [out] */ long *pManCPs,
            /* [out] */ long *pOptCPs,
            /* [out] */ long *pPrincipalNV,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTByName )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ BSTR name,
            /* [out] */ long *pIndex,
            /* [out] */ long *pKey,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pManNVs,
            /* [out] */ long *pOptNVs,
            /* [out] */ long *pManCPs,
            /* [out] */ long *pOptCPs,
            /* [out] */ long *pPrincipalNV,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTByKey )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long key,
            /* [out] */ long *pIndex,
            /* [out] */ BSTR *pName,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pManNVs,
            /* [out] */ long *pOptNVs,
            /* [out] */ long *pManCPs,
            /* [out] */ long *pOptCPs,
            /* [out] */ long *pPrincipalNV,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPT )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long index,
            /* [in] */ long key,
            /* [in] */ BSTR name,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long manNVs,
            /* [in] */ long optNVs,
            /* [in] */ long manCPs,
            /* [in] */ long optCPs,
            /* [in] */ long principalNV,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTNV )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVIndex,
            /* [out] */ BSTR *pNVName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pNVTScope,
            /* [out] */ long *pNVTIndex,
            /* [out] */ long *pDirPollServ,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTCP )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pAppliesTo,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pCPTScope,
            /* [out] */ long *pCPTIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTCPByAttributes )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long appliesTo,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [out] */ long *pCPIndex,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTNV )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVMemberNumber,
            /* [in] */ BSTR NVName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long NVTScope,
            /* [in] */ long NVTIndex,
            /* [in] */ long dirPollServ,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTCP )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPMemberNumber,
            /* [in] */ long appliesTo,
            /* [in] */ BSTR CPName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [in] */ long modifyArray,
            /* [in] */ BSTR dflt,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTNVEx )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVIndex,
            /* [out] */ BSTR *pNVName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pNVTScope,
            /* [out] */ long *pNVTIndex,
            /* [out] */ long *pDirPollServ,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [out] */ BSTR *pValInvalid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTCPEx )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPIndex,
            /* [out] */ long *pAppliesTo,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pCPTScope,
            /* [out] */ long *pCPTIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [out] */ BSTR *pValInvalid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFPTCPByAttributesEx )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long appliesTo,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [out] */ long *pCPIndex,
            /* [out] */ BSTR *pCPName,
            /* [out] */ long *pMandatory,
            /* [out] */ long *pResNmScope,
            /* [out] */ long *pResNmIndex,
            /* [out] */ long *pResCmtScope,
            /* [out] */ long *pResCmtIndex,
            /* [out] */ long *pModifyArray,
            /* [out] */ BSTR *pDflt,
            /* [out] */ BSTR *pValMin,
            /* [out] */ BSTR *pValMax,
            /* [out] */ BSTR *pValInvalid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTNVEx )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long NVMemberNumber,
            /* [in] */ BSTR NVName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long NVTScope,
            /* [in] */ long NVTIndex,
            /* [in] */ long dirPollServ,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [in] */ BSTR valInvalid,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFPTCPEx )( 
            ILdrfFuncProf * This,
            /* [in] */ long pInfo,
            /* [in] */ long FPTIndex,
            /* [in] */ long CPMemberNumber,
            /* [in] */ long appliesTo,
            /* [in] */ BSTR CPName,
            /* [in] */ long mandatory,
            /* [in] */ long resNmScope,
            /* [in] */ long resNmIndex,
            /* [in] */ long resCmtScope,
            /* [in] */ long resCmtIndex,
            /* [in] */ long CPTScope,
            /* [in] */ long CPTIndex,
            /* [in] */ long modifyArray,
            /* [in] */ BSTR dflt,
            /* [in] */ BSTR valMin,
            /* [in] */ BSTR valMax,
            /* [in] */ BSTR valInvalid,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfFuncProfVtbl;

    interface ILdrfFuncProf
    {
        CONST_VTBL struct ILdrfFuncProfVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfFuncProf_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfFuncProf_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfFuncProf_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfFuncProf_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfFuncProf_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfFuncProf_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfFuncProf_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfFuncProf_GetFPTFileInfo(This,pInfo,pResDep0,pResDep1,pResDep2,pResDep3,pResDep4,pResDep5,pResDep6,pTypDep0,pTypDep1,pTypDep2,pTypDep3,pTypDep4,pTypDep5,pTypDep6,pNumFPTs,returnCode)	\
    ( (This)->lpVtbl -> GetFPTFileInfo(This,pInfo,pResDep0,pResDep1,pResDep2,pResDep3,pResDep4,pResDep5,pResDep6,pTypDep0,pTypDep1,pTypDep2,pTypDep3,pTypDep4,pTypDep5,pTypDep6,pNumFPTs,returnCode) ) 

#define ILdrfFuncProf_SetFPTFileInfo(This,pInfo,resDep0,resDep1,resDep2,resDep3,resDep4,resDep5,resDep6,typDep0,typDep1,typDep2,typDep3,typDep4,typDep5,typDep6,returnCode)	\
    ( (This)->lpVtbl -> SetFPTFileInfo(This,pInfo,resDep0,resDep1,resDep2,resDep3,resDep4,resDep5,resDep6,typDep0,typDep1,typDep2,typDep3,typDep4,typDep5,typDep6,returnCode) ) 

#define ILdrfFuncProf_GetFPT(This,pInfo,index,pKey,pName,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pManNVs,pOptNVs,pManCPs,pOptCPs,pPrincipalNV,returnCode)	\
    ( (This)->lpVtbl -> GetFPT(This,pInfo,index,pKey,pName,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pManNVs,pOptNVs,pManCPs,pOptCPs,pPrincipalNV,returnCode) ) 

#define ILdrfFuncProf_GetFPTByName(This,pInfo,name,pIndex,pKey,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pManNVs,pOptNVs,pManCPs,pOptCPs,pPrincipalNV,returnCode)	\
    ( (This)->lpVtbl -> GetFPTByName(This,pInfo,name,pIndex,pKey,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pManNVs,pOptNVs,pManCPs,pOptCPs,pPrincipalNV,returnCode) ) 

#define ILdrfFuncProf_GetFPTByKey(This,pInfo,key,pIndex,pName,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pManNVs,pOptNVs,pManCPs,pOptCPs,pPrincipalNV,returnCode)	\
    ( (This)->lpVtbl -> GetFPTByKey(This,pInfo,key,pIndex,pName,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pManNVs,pOptNVs,pManCPs,pOptCPs,pPrincipalNV,returnCode) ) 

#define ILdrfFuncProf_SetFPT(This,pInfo,index,key,name,resNmScope,resNmIndex,resCmtScope,resCmtIndex,manNVs,optNVs,manCPs,optCPs,principalNV,returnCode)	\
    ( (This)->lpVtbl -> SetFPT(This,pInfo,index,key,name,resNmScope,resNmIndex,resCmtScope,resCmtIndex,manNVs,optNVs,manCPs,optCPs,principalNV,returnCode) ) 

#define ILdrfFuncProf_GetFPTNV(This,pInfo,FPTIndex,NVIndex,pNVName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pNVTScope,pNVTIndex,pDirPollServ,pValMin,pValMax,returnCode)	\
    ( (This)->lpVtbl -> GetFPTNV(This,pInfo,FPTIndex,NVIndex,pNVName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pNVTScope,pNVTIndex,pDirPollServ,pValMin,pValMax,returnCode) ) 

#define ILdrfFuncProf_GetFPTCP(This,pInfo,FPTIndex,CPIndex,pAppliesTo,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pCPTScope,pCPTIndex,pModifyArray,pDflt,pValMin,pValMax,returnCode)	\
    ( (This)->lpVtbl -> GetFPTCP(This,pInfo,FPTIndex,CPIndex,pAppliesTo,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pCPTScope,pCPTIndex,pModifyArray,pDflt,pValMin,pValMax,returnCode) ) 

#define ILdrfFuncProf_GetFPTCPByAttributes(This,pInfo,FPTIndex,appliesTo,CPTScope,CPTIndex,pCPIndex,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pModifyArray,pDflt,pValMin,pValMax,returnCode)	\
    ( (This)->lpVtbl -> GetFPTCPByAttributes(This,pInfo,FPTIndex,appliesTo,CPTScope,CPTIndex,pCPIndex,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pModifyArray,pDflt,pValMin,pValMax,returnCode) ) 

#define ILdrfFuncProf_SetFPTNV(This,pInfo,FPTIndex,NVMemberNumber,NVName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,NVTScope,NVTIndex,dirPollServ,valMin,valMax,returnCode)	\
    ( (This)->lpVtbl -> SetFPTNV(This,pInfo,FPTIndex,NVMemberNumber,NVName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,NVTScope,NVTIndex,dirPollServ,valMin,valMax,returnCode) ) 

#define ILdrfFuncProf_SetFPTCP(This,pInfo,FPTIndex,CPMemberNumber,appliesTo,CPName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,CPTScope,CPTIndex,modifyArray,dflt,valMin,valMax,returnCode)	\
    ( (This)->lpVtbl -> SetFPTCP(This,pInfo,FPTIndex,CPMemberNumber,appliesTo,CPName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,CPTScope,CPTIndex,modifyArray,dflt,valMin,valMax,returnCode) ) 

#define ILdrfFuncProf_GetFPTNVEx(This,pInfo,FPTIndex,NVIndex,pNVName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pNVTScope,pNVTIndex,pDirPollServ,pValMin,pValMax,pValInvalid,returnCode)	\
    ( (This)->lpVtbl -> GetFPTNVEx(This,pInfo,FPTIndex,NVIndex,pNVName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pNVTScope,pNVTIndex,pDirPollServ,pValMin,pValMax,pValInvalid,returnCode) ) 

#define ILdrfFuncProf_GetFPTCPEx(This,pInfo,FPTIndex,CPIndex,pAppliesTo,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pCPTScope,pCPTIndex,pModifyArray,pDflt,pValMin,pValMax,pValInvalid,returnCode)	\
    ( (This)->lpVtbl -> GetFPTCPEx(This,pInfo,FPTIndex,CPIndex,pAppliesTo,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pCPTScope,pCPTIndex,pModifyArray,pDflt,pValMin,pValMax,pValInvalid,returnCode) ) 

#define ILdrfFuncProf_GetFPTCPByAttributesEx(This,pInfo,FPTIndex,appliesTo,CPTScope,CPTIndex,pCPIndex,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pModifyArray,pDflt,pValMin,pValMax,pValInvalid,returnCode)	\
    ( (This)->lpVtbl -> GetFPTCPByAttributesEx(This,pInfo,FPTIndex,appliesTo,CPTScope,CPTIndex,pCPIndex,pCPName,pMandatory,pResNmScope,pResNmIndex,pResCmtScope,pResCmtIndex,pModifyArray,pDflt,pValMin,pValMax,pValInvalid,returnCode) ) 

#define ILdrfFuncProf_SetFPTNVEx(This,pInfo,FPTIndex,NVMemberNumber,NVName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,NVTScope,NVTIndex,dirPollServ,valMin,valMax,valInvalid,returnCode)	\
    ( (This)->lpVtbl -> SetFPTNVEx(This,pInfo,FPTIndex,NVMemberNumber,NVName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,NVTScope,NVTIndex,dirPollServ,valMin,valMax,valInvalid,returnCode) ) 

#define ILdrfFuncProf_SetFPTCPEx(This,pInfo,FPTIndex,CPMemberNumber,appliesTo,CPName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,CPTScope,CPTIndex,modifyArray,dflt,valMin,valMax,valInvalid,returnCode)	\
    ( (This)->lpVtbl -> SetFPTCPEx(This,pInfo,FPTIndex,CPMemberNumber,appliesTo,CPName,mandatory,resNmScope,resNmIndex,resCmtScope,resCmtIndex,CPTScope,CPTIndex,modifyArray,dflt,valMin,valMax,valInvalid,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfFuncProf_INTERFACE_DEFINED__ */


#ifndef __ILdrfScalar64Types_INTERFACE_DEFINED__
#define __ILdrfScalar64Types_INTERFACE_DEFINED__

/* interface ILdrfScalar64Types */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfScalar64Types;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C5BEA9DC-21BA-4C63-9942-F7E6EED7DD88")
    ILdrfScalar64Types : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetScalar64Details( 
            /* [in] */ long pTypeTree,
            /* [out] */ hyper *pMinValid,
            /* [out] */ hyper *pMaxValid,
            /* [out] */ long *pScaleA,
            /* [out] */ long *pScaleB,
            /* [out] */ long *pScaleC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetScalar64Details( 
            /* [in] */ long pTypeTree,
            /* [in] */ hyper minValid,
            /* [in] */ hyper maxValid,
            /* [in] */ long scaleA,
            /* [in] */ long scaleB,
            /* [in] */ long scaleC,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetScalar64InvalidValue( 
            /* [in] */ long pTypeTree,
            /* [out] */ long *pInvalidValuePresent,
            /* [out] */ hyper *pInvalidValue,
            /* [retval][out] */ long *returnCode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetScalar64InvalidValue( 
            /* [in] */ long pTypeTree,
            /* [in] */ hyper invalidValue,
            /* [retval][out] */ long *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfScalar64TypesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfScalar64Types * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfScalar64Types * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfScalar64Types * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfScalar64Types * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfScalar64Types * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfScalar64Types * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfScalar64Types * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetScalar64Details )( 
            ILdrfScalar64Types * This,
            /* [in] */ long pTypeTree,
            /* [out] */ hyper *pMinValid,
            /* [out] */ hyper *pMaxValid,
            /* [out] */ long *pScaleA,
            /* [out] */ long *pScaleB,
            /* [out] */ long *pScaleC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetScalar64Details )( 
            ILdrfScalar64Types * This,
            /* [in] */ long pTypeTree,
            /* [in] */ hyper minValid,
            /* [in] */ hyper maxValid,
            /* [in] */ long scaleA,
            /* [in] */ long scaleB,
            /* [in] */ long scaleC,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetScalar64InvalidValue )( 
            ILdrfScalar64Types * This,
            /* [in] */ long pTypeTree,
            /* [out] */ long *pInvalidValuePresent,
            /* [out] */ hyper *pInvalidValue,
            /* [retval][out] */ long *returnCode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetScalar64InvalidValue )( 
            ILdrfScalar64Types * This,
            /* [in] */ long pTypeTree,
            /* [in] */ hyper invalidValue,
            /* [retval][out] */ long *returnCode);
        
        END_INTERFACE
    } ILdrfScalar64TypesVtbl;

    interface ILdrfScalar64Types
    {
        CONST_VTBL struct ILdrfScalar64TypesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfScalar64Types_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfScalar64Types_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfScalar64Types_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfScalar64Types_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfScalar64Types_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfScalar64Types_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfScalar64Types_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfScalar64Types_GetScalar64Details(This,pTypeTree,pMinValid,pMaxValid,pScaleA,pScaleB,pScaleC,returnCode)	\
    ( (This)->lpVtbl -> GetScalar64Details(This,pTypeTree,pMinValid,pMaxValid,pScaleA,pScaleB,pScaleC,returnCode) ) 

#define ILdrfScalar64Types_SetScalar64Details(This,pTypeTree,minValid,maxValid,scaleA,scaleB,scaleC,returnCode)	\
    ( (This)->lpVtbl -> SetScalar64Details(This,pTypeTree,minValid,maxValid,scaleA,scaleB,scaleC,returnCode) ) 

#define ILdrfScalar64Types_GetScalar64InvalidValue(This,pTypeTree,pInvalidValuePresent,pInvalidValue,returnCode)	\
    ( (This)->lpVtbl -> GetScalar64InvalidValue(This,pTypeTree,pInvalidValuePresent,pInvalidValue,returnCode) ) 

#define ILdrfScalar64Types_SetScalar64InvalidValue(This,pTypeTree,invalidValue,returnCode)	\
    ( (This)->lpVtbl -> SetScalar64InvalidValue(This,pTypeTree,invalidValue,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfScalar64Types_INTERFACE_DEFINED__ */


#ifndef __ILdrfMiscFns2_INTERFACE_DEFINED__
#define __ILdrfMiscFns2_INTERFACE_DEFINED__

/* interface ILdrfMiscFns2 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILdrfMiscFns2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6CAA00B9-8453-4702-BDF9-FCD16272FC74")
    ILdrfMiscFns2 : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EnableExtendedSNVTID( 
            LONG pInfo,
            /* [retval][out] */ LONG *returnCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILdrfMiscFns2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILdrfMiscFns2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILdrfMiscFns2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILdrfMiscFns2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILdrfMiscFns2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILdrfMiscFns2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILdrfMiscFns2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILdrfMiscFns2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnableExtendedSNVTID )( 
            ILdrfMiscFns2 * This,
            LONG pInfo,
            /* [retval][out] */ LONG *returnCode);
        
        END_INTERFACE
    } ILdrfMiscFns2Vtbl;

    interface ILdrfMiscFns2
    {
        CONST_VTBL struct ILdrfMiscFns2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILdrfMiscFns2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILdrfMiscFns2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILdrfMiscFns2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILdrfMiscFns2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILdrfMiscFns2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILdrfMiscFns2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILdrfMiscFns2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILdrfMiscFns2_EnableExtendedSNVTID(This,pInfo,returnCode)	\
    ( (This)->lpVtbl -> EnableExtendedSNVTID(This,pInfo,returnCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILdrfMiscFns2_INTERFACE_DEFINED__ */



#ifndef __LCADRF32Lib_LIBRARY_DEFINED__
#define __LCADRF32Lib_LIBRARY_DEFINED__

/* library LCADRF32Lib */
/* [version][helpstring][uuid] */ 


EXTERN_C const IID LIBID_LCADRF32Lib;

EXTERN_C const CLSID CLSID_LdrfGeneral;

#ifdef __cplusplus

class DECLSPEC_UUID("1CB4B4C5-EC74-11CF-8CC7-444553540000")
LdrfGeneral;
#endif

EXTERN_C const CLSID CLSID_LdrfGeneral2;

#ifdef __cplusplus

class DECLSPEC_UUID("B4464100-C875-11D4-AAC1-002078B03647")
LdrfGeneral2;
#endif

EXTERN_C const CLSID CLSID_LdrfMiscFns1;

#ifdef __cplusplus

class DECLSPEC_UUID("253F1F78-749D-454B-AFC3-7027455ADC4F")
LdrfMiscFns1;
#endif

EXTERN_C const CLSID CLSID_LdrfCatalog;

#ifdef __cplusplus

class DECLSPEC_UUID("1CB4B4CB-EC74-11CF-8CC7-444553540000")
LdrfCatalog;
#endif

EXTERN_C const CLSID CLSID_LdrfLangResource;

#ifdef __cplusplus

class DECLSPEC_UUID("1CB4B4D1-EC74-11CF-8CC7-444553540000")
LdrfLangResource;
#endif

EXTERN_C const CLSID CLSID_LdrfTypes;

#ifdef __cplusplus

class DECLSPEC_UUID("1CB4B4D7-EC74-11CF-8CC7-444553540000")
LdrfTypes;
#endif

EXTERN_C const CLSID CLSID_LdrfTypeTree;

#ifdef __cplusplus

class DECLSPEC_UUID("1CB4B4DD-EC74-11CF-8CC7-444553540000")
LdrfTypeTree;
#endif

EXTERN_C const CLSID CLSID_LdrfFuncProf;

#ifdef __cplusplus

class DECLSPEC_UUID("1CB4B4E3-EC74-11CF-8CC7-444553540000")
LdrfFuncProf;
#endif

EXTERN_C const CLSID CLSID_LdrfScalar64Types;

#ifdef __cplusplus

class DECLSPEC_UUID("717BD1C7-AE17-4EEB-AA11-A7F3CC75211D")
LdrfScalar64Types;
#endif

EXTERN_C const CLSID CLSID_LdrfMiscFns2;

#ifdef __cplusplus

class DECLSPEC_UUID("5E63CDC2-FB11-44B3-ADA3-C7E6D33D1BA2")
LdrfMiscFns2;
#endif
#endif /* __LCADRF32Lib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


