//////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 2002-2011, Gary Bartlett, Echelon Corporation.  All rights reserved.
//
// LnsMts.h: Microsoft Terminal Services (MTS) support module.
//           This module provides a mechanism to support the Global
//           and Session (Local) name spaces introduced when MTS is
//           installed and enabled (e.g. when using Windows XP Fast
//           User Switching).
//
//           It is recommended to include this header file in your
//           project's pre-compiled header file.  This way the MTS
//           module is initialized only once, and only one variable
//           is added to your application.
//
//           To verify that this module is hooked into your project code
//           correctly, use Dependency Viewer (or DUMPBIN) to confirm
//           that your modules do not import {Create|Open}{Event|Mutex|...}
//           directly from KERNEL32.DLL, but instead import
//           LnsMts_{Create|Open}{Event|Mutex|...} from LnsMts.dll.
//
//           See also the LnsMtsShim module for an alternative way of
//           hooking the kernel named object routines.
//
//////////////////////////////////////////////////////////////////////////////
//
// Configurable Settings:
//
// The following is a list of preprocessor symbols that can be set
// prior to including this header that affect the configuration of
// this module.
//
//  LNSMTS_GLOBAL_DEFAULT           (default: TRUE)
//
//      Set this symbol to FALSE if the creation of named objects
//      that don't explicitly specify a namespace prefix
//      (i.e. "Local\" or "Global\") should be created in the Local
//      namespace.  The default value of this symbol is TRUE
//      (i.e. unspecified namespace objects will be created in the
//      Global namespace).
//
//  LNSMTS_MODULE_NAME              (default: "LNS_" if defined)
//
//      (Preferably in one module only, or in a precompiled-header file),
//      set this symbol to a short name describing the including
//      module.  It will be used as the second component in the
//      'standardized' name of all created named objects.
//      All named objects created through this module will be
//      renamed to conform to the following format:
//
//          [Global|Local]\ECHELON_{MTS_MODULE_NAME}{Object Name}
//
//      If not defined or defined as "", no module name will be added.
//
//  LNSMTS_REDEFINE_KNO             (default: undefined)
//
//      Define this symbol to redefine the standard Win32 API kernel
//      named-object (KNO) routines (e.g. CreateEvent, OpenEvent,...)
//      to the MTS-aware versions defined in this module
//      (e.g. LnsMts_CreateEvent).  The MTS-aware versions can still
//      be used by calling them explicitly.
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

// The following ifdef block is the standard way of creating macros which
// make exporting from a DLL simpler.  All files within this DLL are compiled
// with the LNSMTS_EXPORTS symbol defined on the command line.  This
// symbol should not be defined on any project that uses this DLL.  This way
// any other project whose source files include this file see LNSMTS_API
// functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef LNSMTS_EXPORTS
    #define LNSMTS_API __declspec(dllexport)
#else
    #define LNSMTS_API __declspec(dllimport)
    #pragma comment(lib, "LnsMts.lib")          // link with LNS MTS library
#endif


#ifndef LNSMTS_GLOBAL_DEFAULT
    #define LNSMTS_GLOBAL_DEFAULT      TRUE
#endif



//////////////////////////////////////////////////////////////////////
// "C" API

#ifdef __cplusplus
    #define DEFAULT(d)      = (d)
    extern "C" {
#else
    #define DEFAULT(d)      /* no default value for C */
#endif


LNSMTS_API BOOL WINAPI
LnsMts_Initialize(
    LPCSTR                  szModuleName DEFAULT(NULL)      // name of module
);



//////////////////////////////////////////////////////////////////////
// Events

LNSMTS_API HANDLE WINAPI
LnsMts_CreateEventA(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    BOOL                    bManualReset,                   // reset type
    BOOL                    bInitialState,                  // initial state
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_CreateEventW(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    BOOL                    bManualReset,                   // reset type
    BOOL                    bInitialState,                  // initial state
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_CreateEvent      LnsMts_CreateEventW
#else
    #define LnsMts_CreateEvent      LnsMts_CreateEventA
#endif


LNSMTS_API HANDLE WINAPI
LnsMts_OpenEventA(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_OpenEventW(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_OpenEvent        LnsMts_OpenEventW
#else
    #define LnsMts_OpenEvent        LnsMts_OpenEventA
#endif



//////////////////////////////////////////////////////////////////////
// Mutexes

LNSMTS_API HANDLE WINAPI
LnsMts_CreateMutexA(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    BOOL                    bInitialOwner,                  // initial owner
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_CreateMutexW(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    BOOL                    bInitialOwner,                  // initial owner
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_CreateMutex      LnsMts_CreateMutexW
#else
    #define LnsMts_CreateMutex      LnsMts_CreateMutexA
#endif


LNSMTS_API HANDLE WINAPI
LnsMts_OpenMutexA(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_OpenMutexW(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_OpenMutex        LnsMts_OpenMutexW
#else
    #define LnsMts_OpenMutex        LnsMts_OpenMutexA
#endif



//////////////////////////////////////////////////////////////////////
// Semaphores

LNSMTS_API HANDLE WINAPI
LnsMts_CreateSemaphoreA(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    LONG                    lInitialCount,                  // initial count
    LONG                    lMaximumCount,                  // maximum count
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_CreateSemaphoreW(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    LONG                    lInitialCount,                  // initial count
    LONG                    lMaximumCount,                  // maximum count
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_CreateSemaphore      LnsMts_CreateSemaphoreW
#else
    #define LnsMts_CreateSemaphore      LnsMts_CreateSemaphoreA
#endif


LNSMTS_API HANDLE WINAPI
LnsMts_OpenSemaphoreA(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_OpenSemaphoreW(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_OpenSemaphore        LnsMts_OpenSemaphoreW
#else
    #define LnsMts_OpenSemaphore        LnsMts_OpenSemaphoreA
#endif



//////////////////////////////////////////////////////////////////////
// Waitable Timers (Windows NT 4.0 and later; Windows 98 and later)

LNSMTS_API HANDLE WINAPI
LnsMts_CreateWaitableTimerA(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    BOOL                    bManualReset,                   // reset type
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_CreateWaitableTimerW(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    BOOL                    bManualReset,                   // reset type
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_CreateWaitableTimer      LnsMts_CreateWaitableTimerW
#else
    #define LnsMts_CreateWaitableTimer      LnsMts_CreateWaitableTimerA
#endif


LNSMTS_API HANDLE WINAPI
LnsMts_OpenWaitableTimerA(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_OpenWaitableTimerW(
    DWORD                   dwDesiredAccess,                // access
    BOOL                    bInheritHandle,                 // inheritance option
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_OpenWaitableTimer        LnsMts_OpenWaitableTimerW
#else
    #define LnsMts_OpenWaitableTimer        LnsMts_OpenWaitableTimerA
#endif



//////////////////////////////////////////////////////////////////////
// File Mappings

LNSMTS_API HANDLE WINAPI
LnsMts_CreateFileMappingA(
    HANDLE                  hFile,                          // handle to file
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    DWORD                   flProtect,                      // protection
    DWORD                   dwMaximumSizeHigh,              // high-order DWORD of size
    DWORD                   dwMaximumSizeLow,               // low-order DWORD of size
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_CreateFileMappingW(
    HANDLE                  hFile,                          // handle to file
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    DWORD                   flProtect,                      // protection
    DWORD                   dwMaximumSizeHigh,              // high-order DWORD of size
    DWORD                   dwMaximumSizeLow,               // low-order DWORD of size
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_CreateFileMapping    LnsMts_CreateFileMappingW
#else
    #define LnsMts_CreateFileMapping    LnsMts_CreateFileMappingA
#endif


LNSMTS_API HANDLE WINAPI
LnsMts_OpenFileMappingA(
    DWORD                   dwDesiredAccess,                // access mode
    BOOL                    bInheritHandle,                 // inherit flag
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_OpenFileMappingW(
    DWORD                   dwDesiredAccess,                // access mode
    BOOL                    bInheritHandle,                 // inherit flag
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_OpenFileMapping      LnsMts_OpenFileMappingW
#else
    #define LnsMts_OpenFileMapping      LnsMts_OpenFileMappingA
#endif



//////////////////////////////////////////////////////////////////////
// Job Objects (Windows 2000 and later)

LNSMTS_API HANDLE WINAPI
LnsMts_CreateJobObjectA(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    LPCSTR                  lpName,                         // job name 
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_CreateJobObjectW(
    LPSECURITY_ATTRIBUTES   lpAttributes,                   // security attributes
    LPCWSTR                 lpName,                         // job name 
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_CreateJobObject      LnsMts_CreateJobObjectW
#else
    #define LnsMts_CreateJobObject      LnsMts_CreateJobObjectA
#endif


LNSMTS_API HANDLE WINAPI
LnsMts_OpenJobObjectA(
    DWORD                   dwDesiredAccess,                // access mode
    BOOL                    bInheritHandle,                 // inherit flag
    LPCSTR                  lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

LNSMTS_API HANDLE WINAPI
LnsMts_OpenJobObjectW(
    DWORD                   dwDesiredAccess,                // access mode
    BOOL                    bInheritHandle,                 // inherit flag
    LPCWSTR                 lpName,                         // object name
    BOOL                    bGlobal DEFAULT(LNSMTS_GLOBAL_DEFAULT));  // global (or session) name space

#ifdef UNICODE
    #define LnsMts_OpenJobObject        LnsMts_OpenJobObjectW
#else
    #define LnsMts_OpenJobObject        LnsMts_OpenJobObjectA
#endif



//////////////////////////////////////////////////////////////////////
// Unlock Objects

LNSMTS_API DWORD WINAPI
LnsMts_UnlockObjectA(
     LPCSTR                 lpName,                         // object name/path
     SE_OBJECT_TYPE         objectType);                    // object type

LNSMTS_API DWORD WINAPI
LnsMts_UnlockObjectW(
     LPCWSTR                lpName,                         // object name/path
     SE_OBJECT_TYPE         objectType);                    // object type

#ifdef UNICODE
    #define LnsMts_UnlockObject         LnsMts_UnlockObjectW
#else
    #define LnsMts_UnlockObject         LnsMts_UnlockObjectA
#endif


#ifdef __cplusplus
}


// NULL DACL helper class.  An instance of this class can be used
// in place of any of the following types:
//
//      [P]SECURITY_ATTRIBUTES
//      [P]SECURITY_DESCRIPTOR
//
// Typically a single (e.g. global) instance of this class can be shared
// by all code needing a NULL DACL security descriptor/attribute.
class LnsMtsNullDacl {
public:
    explicit LnsMtsNullDacl(BOOL bInheritHandle = FALSE)
    {
        // initialize security descriptor (with a NULL DACL)
        ZeroMemory(&m_sdNull, sizeof(m_sdNull));
        InitializeSecurityDescriptor(&m_sdNull, SECURITY_DESCRIPTOR_REVISION);
        SetSecurityDescriptorDacl(&m_sdNull, TRUE, NULL, FALSE);

        // initialize security attributes
        ZeroMemory(&m_saNull, sizeof(m_saNull));
        m_saNull.nLength = sizeof(m_saNull);
        m_saNull.bInheritHandle = bInheritHandle;
        m_saNull.lpSecurityDescriptor = &m_sdNull;
    };

    // object converters
    operator SECURITY_DESCRIPTOR&()         { return  m_sdNull; };
    operator SECURITY_DESCRIPTOR*()         { return &m_sdNull; };
    operator SECURITY_ATTRIBUTES&()         { return  m_saNull; };
    operator SECURITY_ATTRIBUTES*()         { return &m_saNull; };

private:
    LnsMtsNullDacl(const LnsMtsNullDacl&);  /* no copying */
    
    SECURITY_DESCRIPTOR                     m_sdNull;
    SECURITY_ATTRIBUTES                     m_saNull;
};


// HANDLE helper class.  Automatically calls CloseHandle() on destruction.
class LnsMtsHandle {
public:
    /**/            LnsMtsHandle(HANDLE hHandle = NULL)
                        : m_hHandle(hHandle)        {}
    LnsMtsHandle&   operator=(HANDLE hHandle)       { if (m_hHandle) CloseHandle(m_hHandle); m_hHandle = hHandle; return *this; }

    /**/            LnsMtsHandle(LnsMtsHandle& rhs)
                        : m_hHandle(rhs.m_hHandle)  { rhs.m_hHandle = NULL; }
    LnsMtsHandle&   operator=(LnsMtsHandle& rhs)          { if (m_hHandle) CloseHandle(m_hHandle); m_hHandle = rhs.m_hHandle; rhs.m_hHandle = NULL; return *this; }

    /**/           ~LnsMtsHandle()                  { if (m_hHandle) CloseHandle(m_hHandle); }


    /**/            operator HANDLE()  const        { return  m_hHandle; }
    HANDLE*         operator&()                     { return &m_hHandle; }

private:
    HANDLE          m_hHandle;
};


#endif



//////////////////////////////////////////////////////////////////////
// Configurable Settings (see header block for details)

// Create a static variable at global scope that will initialize
// the MTS module with the application's module name at application
// startup time.  Because the variable is static, this same code
// can be included in multiple compilation units without causing
// linker errors.  However, each time it is included a new static
// variable will be created and the MTS module will be reinitialized,
// so it makes sense to only define MTS_MODULE_NAME once per executable.
#if defined(__cplusplus) && !defined(_WINDLL) && !defined(_USRDLL) && defined(LNSMTS_MODULE_NAME)
    // local static initializer to set module name in MTS module
    static const BOOL c_bMtsInitialized = LnsMts_Initialize(LNSMTS_MODULE_NAME);
    //#pragma message("Notice: Code added to initialize MTS for module \"" LNSMTS_MODULE_NAME "\"")
#endif  // MTS_MODULE_NAME


// Redefine existing calls to the kernel named-object routines
// to use the default version of these MTS-aware versions.
// (The default version uses the Global name space by default).
// Predefine LNSMTS_REDEFINE_KNO before including this header
// to enable this redefinition.
#ifdef LNSMTS_REDEFINE_KNO
    #ifdef __cplusplus
        //lint -save -e652                      // #define of symbol Symbol declared previously at Location

        // ANSI:
        #define CreateEventA                    LnsMts_CreateEventA
        #define CreateMutexA                    LnsMts_CreateMutexA
        #define CreateSemaphoreA                LnsMts_CreateSemaphoreA
        #define CreateWaitableTimerA            LnsMts_CreateWaitableTimerA
        #define CreateFileMappingA              LnsMts_CreateFileMappingA
        #define CreateJobObjectA                LnsMts_CreateJobObjectA

        #define OpenEventA                      LnsMts_OpenEventA
        #define OpenMutexA                      LnsMts_OpenMutexA
        #define OpenSemaphoreA                  LnsMts_OpenSemaphoreA
        #define OpenWaitableTimerA              LnsMts_OpenWaitableTimerA
        #define OpenFileMappingA                LnsMts_OpenFileMappingA
        #define OpenJobObjectA                  LnsMts_OpenJobObjectA

        // Unicode:
        #define CreateEventW                    LnsMts_CreateEventW
        #define CreateMutexW                    LnsMts_CreateMutexW
        #define CreateSemaphoreW                LnsMts_CreateSemaphoreW
        #define CreateWaitableTimerW            LnsMts_CreateWaitableTimerW
        #define CreateFileMappingW              LnsMts_CreateFileMappingW
        #define CreateJobObjectW                LnsMts_CreateJobObjectW

        #define OpenEventW                      LnsMts_OpenEventW
        #define OpenMutexW                      LnsMts_OpenMutexW
        #define OpenSemaphoreW                  LnsMts_OpenSemaphoreW
        #define OpenWaitableTimerW              LnsMts_OpenWaitableTimerW
        #define OpenFileMappingW                LnsMts_OpenFileMappingW
        #define OpenJobObjectW                  LnsMts_OpenJobObjectW
        //lint -restore
    #else
        // for C, handle lack of default parameters by providing
        // explicit macros parameters

        // ANSI:
        #define CreateEventA(a,m,i,n)           LnsMts_CreateEventA(a,m,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateMutexA(a,i,n)             LnsMts_CreateMutexA(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateSemaphoreA(a,i,m,n)       LnsMts_CreateSemaphoreA(a,i,m,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateWaitableTimerA(a,m,n)     LnsMts_CreateWaitableTimerA(a,m,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateFileMappingA(f,a,p,h,l,n) LnsMts_CreateFileMappingA(f,a,p,h,l,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateJobObjectA(a,n)           LnsMts_CreateJobObjectA(a,n,LNSMTS_GLOBAL_DEFAULT)

        #define OpenEventA(a,i,n)               LnsMts_OpenEventA(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenMutexA(a,i,n)               LnsMts_OpenMutexA(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenSemaphoreA(a,i,n)           LnsMts_OpenSemaphoreA(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenWaitableTimerA(a,i,n)       LnsMts_OpenWaitableTimerA(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenFileMappingA(a,i,n)         LnsMts_OpenFileMappingA(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenJobObjectA(a,i,n)           LnsMts_OpenJobObjectA(a,i,n,LNSMTS_GLOBAL_DEFAULT)

        // Unicode:
        #define CreateEventW(a,m,i,n)           LnsMts_CreateEventW(a,m,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateMutexW(a,i,n)             LnsMts_CreateMutexW(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateSemaphoreW(a,i,m,n)       LnsMts_CreateSemaphoreW(a,i,m,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateWaitableTimerW(a,m,n)     LnsMts_CreateWaitableTimerW(a,m,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateFileMappingW(f,a,p,h,l,n) LnsMts_CreateFileMappingW(f,a,p,h,l,n,LNSMTS_GLOBAL_DEFAULT)
        #define CreateJobObjectW(a,n)           LnsMts_CreateJobObjectW(a,n,LNSMTS_GLOBAL_DEFAULT)

        #define OpenEventW(a,i,n)               LnsMts_OpenEventW(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenMutexW(a,i,n)               LnsMts_OpenMutexW(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenSemaphoreW(a,i,n)           LnsMts_OpenSemaphoreW(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenWaitableTimerW(a,i,n)       LnsMts_OpenWaitableTimerW(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenFileMappingW(a,i,n)         LnsMts_OpenFileMappingW(a,i,n,LNSMTS_GLOBAL_DEFAULT)
        #define OpenJobObjectW(a,i,n)           LnsMts_OpenJobObjectW(a,i,n,LNSMTS_GLOBAL_DEFAULT)
    #endif  // __cplusplus

    // display compile-time message indicating that object creation will be MTS-mapped
    //#pragma message("Notice: Win32 Kernel Named Objects remapped to MTS-aware versions.")
#endif  // LNSMTS_REDEFINE_KNO
