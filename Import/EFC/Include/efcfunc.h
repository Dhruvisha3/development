/////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999-2005, Echelon Corp.  All rights reserved.
//
// efcfunc.h - Global utility routines.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef _EFCFUNC_H
#define _EFCFUNC_H


// Get a flag that tells you whether the system is
// currently running with large fonts.  This call
// goes to the Registry the first time, but caches
// the Registry value for subsequent calls.
extern bool EFC_API     GetLargeFontsFlag(void);

// Get the ProductVersion field from the standard
// MFC version info for the file.
extern CString EFC_API  GetFileVersion(CString const &FileName);
extern CString EFC_API  GetFileVersion(CString const &FileName, DWORD &langCodePageID);

#endif // _EFCFUNC_H
