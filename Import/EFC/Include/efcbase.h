/////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999-2011, Echelon Corp.  All rights reserved.
//
// EFCbase.h - utility classes
//
/////////////////////////////////////////////////////////////////////////////

#ifndef _EFCBASE_H
#define _EFCBASE_H

// define appropriate linkage for EFC
#ifdef EFCDLL
    #ifdef EFC_EXPORTS                          // defined only when building EFC
        #define EFC_API __declspec(dllexport)
    #else
        #define EFC_API __declspec(dllimport)
    #endif  /* EFC_EXPORTS */
#else
    #define EFC_API
#endif  /* EFCDLL */

//
//	27-Feb-2009  Bernd
//	The EchelonAboutDialogEx() function requires the MsiEnumRelatedProducts function from the 
//	Windows Installer API, which is only available in certain versions of the Windows Installer
//	API (_WIN32_MSI >= 110, according to msi.h).
//  The EchelonAboutDialogEx() API is always available to all callers, but the extra functionality
//	provided with this function might be unavailable, subject to the chosen EFC DLL.
//  

extern "C" {
EFC_API
// void __stdcall EchelonAboutDialog(unsigned int MainBitmapID, 
void __stdcall EchelonAboutDialog(LPCTSTR AppFullPath,
					LPCTSTR BitmapFile, 
					bool isLNSapp, LPCTSTR lpAdditionalInfo,
					LPCTSTR lpProductGroupKey,
					unsigned int ProductNameFontSize /*= 16 */,
					HWND hParent);
}

// 26-Feb-2009  Bernd  EchelonAboutDialogEx()
// The EchelonAboutDialogEx() function is available to C++ (MFC) callers. 
// Using this function, the caller does not need to include efc.rc (thus
// avoiding resource Id conflicts).
//
// Note that the availability of this function's fundamental feature is 
// controlled by the EXCLUDE_ABOUTDIALOGEX symbol; see efcafx.h.
//
// EchelonAboutDialogEx() supports the following arguments:
//
// appPath:			supply full path of calling application (see example below)
// mainBitmap:		supply loaded CBitmap object with caller's bitmap; height=300,
//					width=100, 24 colours. Use NULL for no application-specific image.
//					Loading this by the caller prevents a resource id management issue;
//                  see example code below.
// additionalInfo:  caller-supplied addt'l info, will be appended to all other version
//					numbers. Use "\n" for linebreaks.
// productGroupKey	product group registry key for serial number retrival (NULL for none)
// additionalProducts  OR'ed combination of EfcProduct flags. Each product indicated thus
//					will be reported with proper name and current version number, if installed.
// ProductNameFontSize defaults to 16, might have to be reduced for long product names
// pParent			Parent window
//
// void show_about(void) {
//    CString appPath(m_pszHelpFilePath);
//	  appPath.Replace(_T(".chm"), _T(".exe"));
//
//	  CBitmap bitmap;
//    bitmap.LoadBitmap(IDB_NB_ABOUT);
//    EchelonAboutDialogEx((LPCTSTR)appPath, &bitmap, _T(""), _T(""), (EfcProduct)(EFC_PROD_LNS | EFC_PROD_LONMAKER), 11, m_pMainWnd->m_hWnd);
// }
//
// The EfcProduct enumeration is used to indicate Echelon software products which can be reported with 
// EchelonAboutDialogEx().  
// See the GetAdditionalProductInfo() function for the update code table; adding the this enumeration
// requires adding the corresponding product upgrade code in the update code table.
//

typedef enum {
	EFC_PROC_NONE					= 0x00000000u,
	EFC_PROD_LNS					= 0x00000001u,
	EFC_PROD_MINI					= 0x00000002u,
	EFC_PROD_NODEBUILDER			= 0x00000004u,
	EFC_PROD_LONMARKRESOURCEFILES	= 0x00000008u,
	EFC_PROD_SHORTSTACK             = 0x00000010u,
	EFC_PROD_FTXL                   = 0x00000020u,
	EFC_PROD_RESOURCEEDITOR         = 0x00000040u,
	EFC_PROD_OPENLDV                = 0x00000080u,
	EFC_PROD_ISI                    = 0x00000100u,
	EFC_PROD_LONMAKER               = 0x00000200u
} EfcProduct;

EFC_API void __stdcall EchelonAboutDialogEx(LPCTSTR appPath,
				 CBitmap* mainBitmap,
                 LPCTSTR additionalInfo,
                 LPCTSTR productGroupKey,
				 EfcProduct additionalProducts,
				 UINT	ProductNameFontSize,
				 HWND	hParent);


extern "C" {
    // 11-Mar-2009  Bernd  EchelonAboutDialogEx2()
    // EchelonAboutDialogEx2() is just like EchelonAboutDialogEx(), except it
    // takes a path to the application's bitmap rather than a pointer to a
    // CBitmap object.  Suitable for non-MFC applications using an external
    // bitmap file.
    EFC_API void __stdcall EchelonAboutDialogEx2(
        LPCTSTR     appPath,
        LPCTSTR     mainBitmap,
        LPCTSTR     additionalInfo,
        LPCTSTR     productGroupKey,
        EfcProduct  additionalProducts,
        UINT        ProductNameFontSize,
        HWND        hParent);

    // EchelonAboutDialogEx3() is just like EchelonAboutDialogEx(), except it
    // takes a GDI bitmap handle to the application's bitmap rather than a
    // pointer to a CBitmap object.  Suitable for non-MFC applications using
    // an embedded bitmap object.
    EFC_API void __stdcall EchelonAboutDialogEx3(
        LPCTSTR     appPath,
        HBITMAP     hMainBitmap,
        LPCTSTR     additionalInfo,
        LPCTSTR     productGroupKey,
        EfcProduct  additionalProducts,
        UINT        ProductNameFontSize,
        HWND        hParent);
}

// Echelon Registry Constants
const LPCTSTR c_EchelonRegistryKey        = _T("SOFTWARE\\LonWorks");
const LPCTSTR c_RegisteredOwnerItem       = _T("RegisteredOwner");
const LPCTSTR c_RegisteredOrgItem         = _T("RegisteredOrganization");
const LPCTSTR c_RegisteredSerialNumItem   = _T("RegisteredSerialNumber");



/////////////////////////////////////////////////////////////////////////////
// CBitmapMasked

class EFC_API CBitmapMasked : public CBitmap
{
public:

    BOOL    LoadBitmap(UINT nIDResource, COLORREF backColor=0);
};


/////////////////////////////////////////////////////////////////////////////
// CSysColorDialog dialog

class EFC_API CSysColorDialog : public CDialog
{
// Construction
public:
    CSysColorDialog();
    CSysColorDialog(UINT nIDTemplate, CWnd* pParent = NULL);   // standard constructor
    CSysColorDialog(LPCTSTR lpszTemplateName, CWnd* pParentWnd=NULL);
    DECLARE_DYNCREATE(CSysColorDialog)

// Dialog Data
    //{{AFX_DATA(CSysColorDialog)
        // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA


// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSysColorDialog)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:

    // Generated message map functions
    //{{AFX_MSG(CSysColorDialog)
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};



/////////////////////////////////////////////////////////////////////////////
// CSysColorPropertyPage property page

class EFC_API CSysColorPropertyPage : public CPropertyPage
{
// Construction
public:
    CSysColorPropertyPage();
    CSysColorPropertyPage(UINT nIDTemplate, UINT nIDCaption=0);
    CSysColorPropertyPage(LPCTSTR lpszTemplateName, UINT nIDCaption=0); 
    ~CSysColorPropertyPage();
    DECLARE_DYNCREATE(CSysColorPropertyPage)

// Dialog Data
    //{{AFX_DATA(CSysColorPropertyPage)
        // NOTE - ClassWizard will add data members here.
        //    DO NOT EDIT what you see in these blocks of generated code !
    //}}AFX_DATA


// Overrides
    // ClassWizard generate virtual function overrides
    //{{AFX_VIRTUAL(CSysColorPropertyPage)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CSysColorPropertyPage)
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

};



/////////////////////////////////////////////////////////////////////////////
// COverlayCtrl dialog

class EFC_API COverlayCtrl : public CSysColorDialog
{
// Construction
public:
    COverlayCtrl(UINT dialogTemplateID, CWnd* pParent = NULL);   // standard constructor

    BOOL Create(CWnd* pParentWnd = NULL) { return CSysColorDialog::Create(m_DialogTemplateID, pParentWnd); }

// Dialog Data
    //{{AFX_DATA(COverlayCtrl)
        // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA


// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(COverlayCtrl)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:

    // Generated message map functions
    //{{AFX_MSG(COverlayCtrl)
        // NOTE: the ClassWizard will add member functions here
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

    UINT    m_DialogTemplateID;

};

#endif // define _EFCBASE_H
