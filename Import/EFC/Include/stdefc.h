/////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999-2009, Echelon Corp.  All rights reserved.
//
// stdefc.h - Standard precompiled headers for EFC.
//
// NOTE: Pre-define EFCDLL to use EFC as a dynamic library (untested).
//       Pre-define EFC_VERBOSE_AUTO_LINK to see a compile-time trace
//       of the library being automatically linked to.
//
/////////////////////////////////////////////////////////////////////////////

// define appropriate linkage for EFC
#ifdef EFCDLL
    #ifdef EFC_EXPORTS                          // defined only when building EFC
        #define EFC_API __declspec(dllexport)
    #else
        #define EFC_API __declspec(dllimport)
    #endif  /* EFC_EXPORTS */
#else
    #define EFC_API
#endif  /* EFCDLL */


// this is the standard stuff, included in all projects
#include "efcfunc.h"
#include "efcbase.h"
#include "efcsplsh.h"
#include "efcabout.h"
#include "efcfoldr.h"

// FOR DEBUGGING:
//      Macros to output a compile-time message
//      showing the value of a preprocessor symbol.
//#define STRING(s)                           #s
//#define XSTRING(s)                          STRING(s)
//#define SHOWSYMBOL(s)                       #s " = " STRING(s)
//
//#pragma message(SHOWSYMBOL(_MSC_VER))       // compiler-defined
//#pragma message(SHOWSYMBOL(_MFC_VER))       // MFC header-defined
//#pragma message(SHOWSYMBOL(_AFXDLL))        // project-defined


// define appropriate version of MFC
#ifndef EFC_MFC_VER
    #if _MFC_VER == 0x0C00
        #define EFC_MFC_VER     "120"            // MFC 9.0 (Microsoft Visual Studio 2013)
    #elif _MFC_VER == 0x0900
		#define EFC_MFC_VER     "90"            // MFC 8.0 (Microsoft Visual Studio 2008)
    #elif _MFC_VER == 0x0800
        #define EFC_MFC_VER     "80"            // MFC 8.0 (Microsoft Visual Studio 2005)
    #elif _MFC_VER == 0x0710
        #define EFC_MFC_VER     "71"            // MFC 7.1 (Microsoft Visual Studio .NET 2003)
    #elif _MFC_VER == 0x0700
        #define EFC_MFC_VER     "70"            // MFC 7.0 (Microsoft Visual Studio .NET)
    #elif _MFC_VER < 0x0700
        #define EFC_MFC_VER     ""              // MFC 4.2 (Microsoft Visual Studio)
    #else
        #error This version of MFC is not yet supported by the EFC classes.  Please update EFC to recognize this version.
    #endif  /* _MFC_VER */
#endif  /* EFC_MFC_VER */

// define appropriate linkage to MFC
#ifndef EFC_MFC_TYPE
    #if defined EFCDLL
        #define EFC_MFC_TYPE    "dyn"           // dynamic EFC links to dynamic MFC
        #ifndef _AFXDLL
            #error Must link to dynamic MFC to use dynamic EFC.
        #endif  /* _AFXDLL */

    #elif defined _AFXDLL
        #define EFC_MFC_TYPE    ""              // static  EFC links to dynamic MFC

    #else
        #define EFC_MFC_TYPE    "stat"          // static  EFC links to static  MFC
    #endif  /* _AFXDLL */
#endif  /* EFC_MFC_TYPE */

// define appropriate build configuration
#ifndef EFC_CONFIG
    #ifdef _DEBUG
        #define EFC_CONFIG      "d"             // Debug
    #else
        #define EFC_CONFIG      ""              // Release
    #endif
#endif  /* EFC_CONFIG */


// build EFC library name
#ifndef EFC_LIB_NAME
    #define EFC_LIB_NAME        "efc" EFC_MFC_VER EFC_MFC_TYPE EFC_CONFIG ".lib"
#endif  /* EFC_LIB_NAME */


#ifndef NO_LIB_PRAGMA
    // autoload appropriate version of EFC library
    #pragma comment(lib, EFC_LIB_NAME)

    #ifdef EFC_VERBOSE_AUTO_LINK
        // display the version of EFC that will be loaded
        #pragma message("EFC: Auto linking to " EFC_LIB_NAME)
    #endif  /* EFC_VERBOSE_AUTO_LINK */
#endif  /* NO_LIB_PRAGMA */
