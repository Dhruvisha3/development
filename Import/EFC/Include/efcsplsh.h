/////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999-2005, Echelon Corp.  All rights reserved.
//
// EFCSplsh.h : header file
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_EFCSPLSH_H__5BDE0F82_5E0A_11D2_A01E_00A024D8449E__INCLUDED_)
#define AFX_EFCSPLSH_H__5BDE0F82_5E0A_11D2_A01E_00A024D8449E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "efcrc.h"



/////////////////////////////////////////////////////////////////////////////
// CEchelonSplashDlg dialog

class EFC_API CEchelonSplashDlg : public CSysColorDialog
{
// Construction
public:
    CEchelonSplashDlg(UINT MainBitmapID,   // standard constructor
                     CString const &ProductGroupKey,
                     UINT   ProductNameFontSize = 16,   // default is 16-point font
                     CWnd* pParent = NULL);
    // use this constructor if you don't have a bitmap to display
    CEchelonSplashDlg(CString const &ProductGroupKey,   
                     UINT   ProductNameFontSize = 16,   // default is 16-point font
                     CWnd* pParent = NULL);
    ~CEchelonSplashDlg();

    BOOL Create(CWnd* pParentWnd = NULL) { return CSysColorDialog::Create(m_currIDD, pParentWnd); }


// Dialog Data
    //{{AFX_DATA(CEchelonSplashDlg)
    enum { IDD_WITH_BMP = IDD_EFC_SPLASH_DLG,
           IDD_WITHOUT_BMP = IDD_EFC_SPLASH_NOBMP_DLG
         };
    CStatic m_MainBitmapCtrl;
    CStatic m_LogoBitmapCtrl;
    //}}AFX_DATA


// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CEchelonSplashDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
public:

    UINT  AddToProgressPosition(int Position);  // add to progress position, return
                                                // old value of progress position

    void  SetProgressFrameText(CString& ProgressText);  // sets the text in the
                                                        // progress control frame,
                                                        // gives progress description


protected:

    // Generated message map functions
    //{{AFX_MSG(CEchelonSplashDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

    UINT            m_currIDD;
    CBitmapMasked   m_LogoBitmap;
    CBitmap        *m_MainBitmapP;
    CDC            *m_dcDisplayMemoryP;
    UINT            m_LoadProgressPosition; // the current position of progress ctrl

    // passed as parameters to the constructor
    UINT            m_MainBitmapID;
    CString         m_ProductGroupKey;      // product group in Registry
    UINT            m_ProductNameFontSize;  // Font size for app title (16-pt default)
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EFCSPLSH_H__5BDE0F82_5E0A_11D2_A01E_00A024D8449E__INCLUDED_)
