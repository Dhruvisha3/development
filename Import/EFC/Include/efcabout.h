/////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 1999-2005, Echelon Corp.  All rights reserved.
//
// EFCabout.h : header file
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_EFCABOUT_H__909E6132_7E49_11D2_A073_00A024D8449E__INCLUDED_)
#define AFX_EFCABOUT_H__909E6132_7E49_11D2_A073_00A024D8449E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "efcrc.h"
#include "efcsetui.h"


#ifdef EFCDLL
    /////////////////////////////////////////////////////////////////////////////
    // CEfcDll
    // See EfcAbout.cpp for the implementation of this class
    //

    class CEfcDll : public CWinApp
    {
    public:
        CEfcDll();
        BOOL InitInstance();
        int ExitInstance();
	    
	    // Overrides
	    // ClassWizard generated virtual function overrides
	    //{{AFX_VIRTUAL(CEfcDll)
	    //}}AFX_VIRTUAL

	    //{{AFX_MSG(CEfcDll)
		    // NOTE - the ClassWizard will add and remove member functions here.
		    //    DO NOT EDIT what you see in these blocks of generated code !
	    //}}AFX_MSG

        DECLARE_MESSAGE_MAP()
    };
#endif /*EFCDLL*/



/////////////////////////////////////////////////////////////////////////////
// CEchelonAboutDlg dialog

// 12-Mar-2009  Bernd  
// Ritesh and I agreed to remove the CEchelonAboutDlg from the EFC API; use
// the EchelonAboutDialogEx or EchelonAboutDialogEx2() API instead. These API
// function address the haphazard resource management of the previous API. 
// 

class CEchelonAboutDlg : public CSysColorDialog
{
// Construction
public:
	CEchelonAboutDlg(CString const &AppFullPathFile,
					 CString const &MainBitmapFile,
					 bool isLNSapp,
					 CString const &AdditionalInfo,
					 CString const &ProductGroupKey,
					 UINT	ProductNameFontSize = 16,	// default is 16-point font
					 CWnd* pParent = NULL);
	CEchelonAboutDlg(UINT MainBitmapID,
					 bool isLNSapp,
					 CString const &AdditionalInfo,
					 CString const &ProductGroupKey,
					 UINT	ProductNameFontSize = 16,	// default is 16-point font
					 CWnd* pParent = NULL);

	// 24-Feb-2009  Bernd
	// Use a combination of the EfcProduct enumeration in the constructor that supports a product enumeration.
	// The About Box will automatically list these produces with their full and proper name  and version number,
	// if installed. Products requested but not installed will not show. Products not included
	// in that bitvector are not shown, but additional information can still be supplied through the alphanumeric
	// AdditionalInfo parameter
	// Use the plain c'tor, and the Create function, in combination. This avoids issues with the ambiguous constructors.
	CEchelonAboutDlg(CWnd* parent, CBitmap* mainBitmap);

	// use this constructor if you have no bitmap
	CEchelonAboutDlg(bool isLNSapp,
					 CString const &AdditionalInfo,
					 CString const &ProductGroupKey,
					 UINT	ProductNameFontSize = 16,	// default is 16-point font
					 CWnd* pParent = NULL);
	~CEchelonAboutDlg();

	void Create (LPCTSTR fullApplicationPath,
				 bool isLNSapp,
                 LPCTSTR additionalInfo,
                 LPCTSTR productGroupKey,
				 EfcProduct additionalProducts,
				 UINT	ProductNameFontSize = 16);	// default is 16-point font

// Dialog Data
	//{{AFX_DATA(CEchelonAboutDlg)
enum { IDD_WITH_BMP = IDD_EFC_ABOUT_DLG,
		   IDD_WITHOUT_BMP = IDD_EFC_ABOUT_NOBMP_DLG
		 };
	CStatic	m_LogoBitmapCtrl;
	CStatic m_MainBitmapCtrl;
	CStatic	m_copyrightPlaceHolder;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEchelonAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEchelonAboutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg BOOL OnHelpInfo(HELPINFO* lpHelpInfo); 

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

    CBitmapMasked	m_LogoBitmap;
    COverlayCtrl   *m_CopyrightP;
	CBitmap		   *m_MainBitmapP;
	CDC			   *m_dcDisplayMemoryP;

	// passed as parameters to the constructor
	CString			m_MainBitmapFile;
	CString			m_FullAppPath;
	UINT			m_MainBitmapID;
	UINT	        m_ProductNameFontSize;	// Font size for app title (16-pt default)
	bool			m_IsLNSapp;				// is this an LNS app?
	CString			m_AdditionalInfo;		// added after LNS version
	CString			m_ProductGroupKey;		// product group in Registry
	CWnd*			m_Parent;				// parent of this dialog
	EfcProduct		m_AdditionalProducts;	// additional products to report (zero if none)

	// for constructors that work only in team with Create():
	bool			m_created;
	// for users of EchelonAboutDialogEx(), who pre-load the caller's bitmap at construction time:
	bool			m_foreignBitmap;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EFCABOUT_H__909E6132_7E49_11D2_A073_00A024D8449E__INCLUDED_)
