/////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 2006-2009, Echelon Corporation.  All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////

// CSetUILang
// C++ class to set a module's Resource instance handle to a language DLL
// Written by Bill Hall

// CSetUILang attempts to load a language DLL for the module which contains it.
// The name is assumed to be of the form (Appname + XXX).DLL, where XXX is the
// Microsoft three letter designation assigned to a given locale.
// A static HINSTANCE variable holds the language DLL's handle, which becomes
// the resource handle for the application.

// The class must be declared at application startup, and only one instance
// can be used.
// After declaring the class, call the member function SetResourceHandle to
// initialize the static variable hInstLang.  Even though the object goes
// out of scope after the initialization function has exited, the handle
// is preserved.  When the application exits, call the static function
// DeleteResourceHandle to free the library.
// It is NOT necessary to declare an instance of the class at this time as this
// function is static.

// Suitable places for initialization are InitInstance (first line of code)
// for an MFC application, WinMain or DllMain (DLL_PROCESS_ATTACH) for a non-MFC C++
// Win32 application.
// Suitable locations for cleanup are the last line (just before the call
// to the base class) of ExitInstance for an MFC application and
// DLLMain (DLL_PROCESS_DETACH) for a non-MFC C++ Win32 DLL.

// Note: this code is DBCS enabled.

#ifndef __INC_SETUILANG__
#define __INC_SETUILANG__


//# include "lmwdll.h"
// class LCA_DECL CSetUILang
class CSetUILang
{
    public:
	CSetUILang();
	~CSetUILang();
	CString m_langAbbr;
	CString m_appName;

    public:
	HINSTANCE hInstLang;
	void DeleteResourceHandle();
	void SetResourceHandle(const CString &loader, LCID lcid=0, bool displayError=false);
	bool SetResource();
};

class CEfcRegInterface
{
public :
   CString GetAppLanguage ();
};

#endif //__INC_SETUILANG__
