//////////////////////////////////////////////////////////////////////////////
//
// $Header$
//
// � Copyright 2009, Echelon Corporation.  All rights reserved.
//
//////////////////////////////////////////////////////////////////////////////
//
// EFC FOLDER SUPPORT
// 19-Jan-2009  Bernd
//
// EfcBrowseFolder() opens a Browse Folder dialog.  If the user selects a 
// folder and clicks OK, the function returns TRUE and writes the full path 
// of the selected folder to the 'output' parameter.  'output' must take 
// MAX_PATH characters.  If the user selects Cancel, the function returns FALSE.
//
// --- Prerequisites: ---
// 
// * The caller must call CoInitialize() or OleInitialize() prior to 
//   calling EfcBrowseFolder().
// * The caller must allocate at least MAX_PATH characters for the output data.
//
// --- Parameters: ---
//
// owner    Window handle of owner, can be NULL. Shouldn't.
//
// path     In/Out. Pointer to a buffer zero-terminated buffer that contains the default location 
//          (the folder to start browsing in). If this string doesn't relate to an existing folder,
//          the tool will start browsing at the root - see 'root' parameter. 
//          When the function returns with success, this buffer holds the selected folder path. 
//          This argument cannot be NULL, and must be 0-terminated.
//
// title    Subtitle for the directory selection dialog. A suggested default is EFC_BROWSEFOLDER_TITLE.
//
// flags    See MSDN for "BROWSEINFO Structure" and its possible flags. Some typical flag combinations
//          are provided as EFC_BROWSEFOLDER_FLAGS_* macros, below.
//
// root     Specification of the root folder.  The user cannot go 'higher up' than this root folder.  
//          A suggested default is EFC_BROWSEFOLDER_ROOT (= "My Computer"). Note this functionality 
//          requires building the EFC library for XP as the minimum runtime OS.
//
// --- Caveats: ---
//
// * The 'root' argument is ineffective unless the EFC DLL is being built 
//   with NTDDI_VERSION >= NTDDI_WINXP (_WIN32_WINNT>=0x0501)
//
//////////////////////////////////////////////////////////////////////////////

#ifndef EFC_BROWSEFOLDER_H
#define EFC_BROWSEFOLDER_H

//
// suggested default for the title parameter
//
#define EFC_BROWSEFOLDER_TITLE  _T("Select folder")

//
// suggested default for 'root'
//
#define EFC_BROWSEFOLDER_ROOT   NULL

//
// suggested default for 'flags'. See MSDN('BROWSEINFO' structure) for the whole works.
//
#define EFC_BROWSEFOLDER_FLAGS                  BIF_BROWSEFORCOMPUTER|BIF_NEWDIALOGSTYLE|BIF_RETURNONLYFSDIRS
#define EFC_BROWSEFOLDER_FLAGS_ONLY_EXISTING    (EFC_BROWSEFOLDER_FLAGS|BIF_NONEWFOLDERBUTTON)
#define EFC_BROWSEFOLDER_FLAGS_ALLOW_NEW        (EFC_BROWSEFOLDER_FLAGS)

extern "C" {
    EFC_API BOOL __stdcall EfcBrowseFolder(HWND owner, LPTSTR path, LPCTSTR title, UINT flags, LPCTSTR root);
}

#endif  /* EFC_BROWSEFOLDER_H */
