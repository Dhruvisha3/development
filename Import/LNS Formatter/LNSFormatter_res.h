//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by LNSFormatter.rc
//
#define IDD_CHOOSE_FORMAT               101
#define IDC_SNVT_ONLY                   1000
#define IDC_TYPE_FILES                  1001
#define IDC_FORMATS                     1002
#define IDC_UNIT                        1003
#define IDC_NO_FORMAT                   1006
#define IDC_IS_SNVT                        1007
#define IDC_IS_UNVT                        1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
