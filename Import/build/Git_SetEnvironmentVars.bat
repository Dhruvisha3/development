@Echo Off
Rem This batch file sets environment variables
Rem
Rem Notes:
Rem 1. 
Rem    
REm    


Rem --------------------------------------------------------
if "%ECHO%" == "1" Echo On

Call .\build\SetBaseline


Rem		Set variables containing common source control operations/options.
Rem     All default options are overridable by adding to call site,
Set Get=git pull
Set CheckOut=git add
Set CheckIn=git commit
Set Revert=git checkout -- <file>

Rem     Root of source files under source control.
Set VERSIONPATH=\Include
Set VERSIONFILE=version.h

Rem	Build configuration
Set ReleaseBldCfg="Release"
Set DebugBldCfg="Debug"
Set BuildLog=Build.log

Rem 	Set P4 variables used by the p4 client
rem Set P4USER=p4bbuild
rem Set P4PORT=PERFORCE:1666


Rem	Various P4 workspaces used by the build
Set P4BUILD=Pooja_LSPA_Dev
Set P4EXPORT=Export_LonScanner_Dev_W
Set P4EXPORTBRANCH=Export_LonScanner_Dev
Set P4IMPORTBRANCH=Import_LonScanner_Dev

Rem 	Set P4 variables used by build script
Set P4DEPOTROOT=//depot
Set P4NETTOOLSROOT=%P4DEPOTROOT%/Software/NetTools
Set P4LONSCANNERROOT=%P4NETTOOLSROOT%/LMPA

Rem This line will be changed to a number by the p4changelist.bat batch file
Set P4CHANGELIST=Default  


:isLONSCANNERBUILD?
if not %COMPUTERNAME%==LONSCANNERBUILD goto isLONSCANNERBUILDDONE
Set BldDrive=C:
Set BldDir=%BldDrive%\LonScanner\Dev
Set UtilitiesDir=\Utilities
Set TargetDir=\\Virgil\Shared SW\LonScanner\Internal\System Test
Set InternalTargetDir=\\Virgil\Shared SW\LonScanner\Internal\Development
Set BinDir=C:\Bin
Set PerlDir=C:\Perl

rem Build Directories
Set MsDevDir=C:\Program Files\Microsoft Visual Studio 9.0
set WindowsSDKDir=C:\Program Files\Microsoft SDKs\Windows\v6.0A

Set STLportDir32=C:\Lonscanner\STLport\V3.20
Set STLportDir46=C:\Lonscanner\STLport\V4.6\STLPORT
Set STLportDir52=C:\Lonscanner\STLport\V5.2
Set PlatformSDKDir=C:\Program Files\Microsoft SDK
rem Set ISDir12=C:\Program Files\Macrovision\IS 12 StandaloneBuild
Set ISDir12=C:\Program Files (x86)\Installshield\2013 SP1 SAB\System
Set ISSABLD=%ISDir12%\IsCmdBld.exe

goto end
:isLONSCANNERBUILDDONE

:isSLSBUILDSRV?
if not %COMPUTERNAME%==SLSBUILDSRV goto isSLSBUILDSRVdone

Rem 	Set P4 variables used by the p4 client
Set P4USER=psarkar
Set P4PORT=pf1.echelon.echcorp.com:1666

Rem	Various P4 workspaces used by the build
Set BldDrive=C:
Set BldDir=%BldDrive%\LonScanner\Dev
Set UtilitiesDir=\Utilities
rem Set TargetDir=\\Virgil\Shared SW\LonScanner\Internal\System Test
rem Set InternalTargetDir=\\Virgil\Shared SW\LonScanner\Internal\Development
Set TargetDir=D:\Shared SW\LonScanner\Internal\System Test
Set InternalTargetDir=D:\Shared SW\LonScanner\Internal\Development
Set BinDir=C:\Bin
Set PerlDir=C:\Perl

rem Build Directories
Set MsDevDir=C:\Program Files\Microsoft Visual Studio 9.0
set WindowsSDKDir=C:\Program Files\Microsoft SDKs\Windows\v6.0A

Set STLportDir32=C:\Lonscanner\STLport\V3.20
Set STLportDir46=C:\Lonscanner\STLport\V4.6\STLPORT
Set STLportDir52=C:\Lonscanner\STLport\V5.2
Set PlatformSDKDir=C:\Program Files\Microsoft SDK
Set ISDir12=C:\Program Files (x86)\InstallShield\2013 SP1 SAB\System
ISSABLD=%ISDir12%\IsCmdBld.exe
Set ISCSCRIPT=C:\Windows\SysWOW64\cscript

goto end
:isSLSBUILDSRVdone



:unknownBuildMachine
Echo Unknown build machine
Echo You need to edit SetEnvironmentVars.Bat to include this machine
SetErrorlevel 1
goto end



:end
