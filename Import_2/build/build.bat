@Echo Off
Rem This batch file builds a LonScanner version.  It must be run from the 
Rem LonScanner working folder.
Rem
Rem Notes:
Rem 1. 

if "%ECHO%" == "1" Echo On
Set BuildLog=Build.log
Rem     Set tracing variables
Rem Set NUL=NUL
Set NUL=CON
Set COMMITVERSIONNUMBER=0
Set GITORIGIN=""
Set GITBRANCH=""
call .\build\SetEnvironmentVars

Rem Call GetVersion %SSNBRoot% %VERSIONPATH% %VERSIONFILE%
cd %BldDir%
rem Set P4CLIENT=%P4BUILD%

Rem --------------------------------------------------------------------------------
if "%1" == "help" goto help
goto start
:help
echo.
echo build [-noclean] [-noupdate] [-noimport] [-batch] [-buildInstallerOnly] [n]
echo    where n is the build number
echo          *** DO NOT specify the build # unless it's different than the current build #
echo          -noclean means don't clear out old output files
echo          -noupdate means don't get the latest version from the Perforce, 
echo          -noimport prevents of imported files into the build tree.
echo          -batch bypass any user confirmation
echo          -buildInstallerOnly builds only the installer
echo.

Rem echo	The current version is: %VersionNumber%
setErrorlevel 1


Rem --------------------------------------------------------------------------------
:start
if "%ECHO%" == "1" Echo On 

Echo -------------------------------------------------------------
Echo -
echo Program to build the product files for LonScanner
echo .
echo Make sure that the following folders are on the path
echo		- Bin folder
echo		- Perforce
echo		- InstallShield
echo		- MSDev\Bin
echo .
echo You must have already archived the previous build.
Echo -
Echo -------------------------------------------------------------
echo .
echo .

Rem Make sure the required parameters are sent in
Set ParamCount=0
Rem Note: Backward jump
if "%1" == "" goto help


:prodok1
REM process the switches
set noclean=
set upd=
set rebuild=/build
set nolp=
set repin=
set norepin=
set buildbatch=
set exp=
set imp=
set buildInstallerOnly=

:startargs
if "%1" == "" goto endargs
if not "%1" == "-noclean" goto arg2
set noclean=-noclean
goto nextarg
:arg2
if not "%1" == "-noupdate" goto arg3
set upd=noupdate
goto nextarg
:arg3
if not "%1" == "-noimport" goto arg4
set imp=-noimport
goto nextarg
:arg4
if not "%1" == "-batch" goto arg5
set buildbatch=-batch
goto nextarg
:arg5
if not "%1" == "-norepin" goto arg6
set norepin=-norepin
goto nextarg
:arg6
if not "%1" == "-buildInstallerOnly" goto arg7
set buildInstallerOnly=-buildInstallerOnly
goto nextarg
:arg7
set newBuildNumber=%1%
:nextarg
shift

goto startargs
:endargs

Rem --------------------------------------------------------

:checkClean
if "%noclean%" == "-noclean" goto skipcln
echo Cleaning all the local objects and libraries
echo .
set rebuild=/REBUILD
:skipcln

:doBuild
rem ********************************************
echo Building the project libraries and exe
rem ********************************************
Set start_build_comment="Start LonScanner Build %newBuildNumber%"
Set end_build_comment="End LonScanner Build %newBuildNumber%"
if exist %BuildLog% del %BuildLog%

Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem
Rem
Rem Edit version headers
Rem 
Rem - run getlnsver, see if version matches requested input param
Rem
git --version
if errorlevel 1  goto git_not_found 
for /f "delims=" %%a in ('git remote') do @set GITORIGIN=%%a
if %GITORIGIN%=="" ( 
	ECHO GIT Origin not found.
	git remote add origin %GITUSERREMOTEURL%
)
git remote -v
for /f "delims=" %%a in ('git rev-parse --abbrev-ref HEAD') do @set GITBRANCH=%%a
if NOT %GITBRANCH% == %GITWORKINGBRANCH% ( 
	ECHO I am NOT on %GITWORKINGBRANCH%
	git checkout %GITWORKINGBRANCH%	
)
git checkout "include/version.h"
Call GetLnsVer -f "include\version.h" > ~q1.bat
Call ~q1.bat > %NUL%
Del ~q1.bat
Echo ... verMajor := %VerMajor%
Echo ... verMinor := %VerMinor%
Echo ... verBuild := %VerBuild%
Echo .

Set VersionNumber=%VerMajor%.%VerMinor%.%newBuildNumber%

If %newBuildNumber% EQU %VerBuild% (
	echo .
	echo No version update required...
	Goto have_uptoDate_version
)

if "%BuildScriptDebug%" == "TRUE" (
	echo Debugging script: skipping version file checkin
	goto have_UptoDate_version
)

:must_edit_version

Rem Start a Perforce changelist for any changes made by the build scripts
rem Set P4CLIENT=%P4BUILD%
rem Call p4changelist -d "Version updates for LonScanner Build %newBuildNumber%"
If ErrorLevel 1 Goto done_error

SET LabeledChanges=%P4CHANGELIST%
Set LabeledChangesDone=0
set P4CHANGELIST=

Echo ...
Echo ... Version.h is old (%VerBuild%), must edit
Echo .

rem %CheckOut% -c %LabeledChanges% %P4LonScannerRoot%/Dev/Include/Version.h
If ErrorLevel 1 Goto done_error

Echo Call setlnsver -f Include\Version.h %newBuildNumber%
Call SetLnsVer -f Include\Version.h %newBuildNumber%
If ErrorLevel 1 Goto done_error

Set VerBuild=%newBuildNumber%

Rem Submit the open changelist
Echo ... 
Echo ... Checking in version file changes
Echo ...

git add include/version.h
for /f "delims=" %%a in ('git log --grep^="Version updates for LonScanner Build %newBuildNumber%" --pretty^=format:"%%H" --no-patch') do @set COMMITVERSIONNUMBER=%%a
echo Version Number is %COMMITVERSIONNUMBER%
ECHO Temporary Break
goto done_error
if %COMMITVERSIONNUMBER%==0 ( 
echo No commit found with message Version updates for LonScanner Build %newBuildNumber% in GIT.
git commit -m "Version updates for LonScanner Build %newBuildNumber%" -- include/version.h
)
rem Call %Checkin% -c %LabeledChanges% 
Rem If there are no changed files the checkin will result in an ErrorLevel of 1
Rem (i.e. in the case of a previous build that checked in version changes,  but
Rem but failed later in the script)
rem If that happens, the changelist should be deleted or it will be left hanging around
if ErrorLevel 1 (
	Rem Call %ChangeDelete% %LabeledChanges%
	Rem if this fails, something else may have gone wrong, so abort
	Echo Error in commit data.
	Goto done_error		
)
:VersionCommit
rem echo Push the data using git push origin %GITWORKINGBRANCH%
rem git push origin %GITWORKINGBRANCH%
rem if ErrorLevel 1 (
rem 	echo Error in push data
rem 	Goto done_error		
rem )
:SkipVersionCommit

Set LabeledChangesDone=1
:have_UptoDate_version

if "%imp%" == "-noimport" goto SkipImport
:ImportFiles
Rem Import files 
Echo .
Echo Importing files...
echo Call p4Import -b %P4ImportBranch% -d "LonScanner Build %VerMajor%.%VerMinor%.%VerBuild% imports"
rem Call p4Import -b %P4ImportBranch% -d "LonScanner Build %VerMajor%.%VerMinor%.%VerBuild% imports"
If ErrorLevel 1 Goto done_error
:SkipImport

Rem The changelist number can change when checked in, or an empty changelist could fail.
Rem If that happens LabeledChanges will no longer have the right changelist number, so get the last one
echo Call p4lastchange
Echo Getting last changelist number...
Rem Call p4lastchange
Set LabeledChanges=%P4LASTCHANGE%

Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem
Rem
Rem Get all build files at start
Rem
Echo ... Syncing build files to changelist %LabeledChanges% ...
Echo .
Set P4CLIENT=%P4BUILD%
Rem %Get% //...@%LabeledChanges%
Rem p4 sync //depot/Software/NetTools/LMPA/Dev/include/version.h
If ErrorLevel 1 Goto done_error

Rem Create a label for the changelist just created
Rem Save the changelist number so we can retrieve the correct paths
Echo ...
Echo ... Labelling files LonScanner_%VerMajor%.%VerMinor%.%VerBuild%
Echo ... 
echo call p4label -fc %LabeledChanges% LonScanner_%VerMajor%.%VerMinor%.%VerBuild% %P4LonScannerRoot%/Dev/... 
rem call p4label -fc %LabeledChanges% LonScanner_%VerMajor%.%VerMinor%.%VerBuild% %P4LonScannerRoot%/Dev/... 
if ErrorLevel 1 goto done_error

if "%buildInstallerOnly%" == "-buildInstallerOnly" goto buildInstaller

Rem     Initialize Build Directories
Set INCLUDE=%STLportDir52%\STLport
Set INCLUDE=%INCLUDE%;%MsDevDir%\VC\Include
Set INCLUDE=%INCLUDE%;%MsDevDir%\VC\ATLMFC\Include
Set INCLUDE=%INCLUDE%;%WindowsSDKDir%\Include

Set LIB=%STLportDir52%\Lib\VC9
Set LIB=%LIB%;%MsDevDir%\VC\Lib
Set LIB=%LIB%;%MsDevDir%\VC\ATLMFC\Lib
Set LIB=%LIB%;%MsDevDir%\VC\ATLMFC\Lib\i386
Set LIB=%LIB%;%WindowsSDKDir%\Lib
rem Set _PATH=%PATH%
rem Set PATH=%BinDir%;%MsDevDir%\VC\Bin;%MsDevDir%\Common7\IDE;%WindowsSDKDir%\BIN;%PATH%

echo call devenv lmpa.sln %rebuild% %ReleaseBldCfg% /out lmpa.out 
call devenv lmpa.sln %rebuild% %ReleaseBldCfg% /out lmpa.out 
if errorlevel 1 goto build_error

REM don't need to build debug
REM echo call devenv lmpa.sln %rebuild% %DebugBldCfg%  /useenv /out lmpa.out
REM call devenv lmpa.sln %rebuild% %DebugBldCfg%  /useenv /out lmpa.out 
REM if errorlevel 1 goto build_error

:buildInstaller
rem ********************************************
echo Building the installer
rem ********************************************
Call .\Build\buildInstaller.bat
if errorlevel 1 goto errorInstallBuild

:archive
rem ********************************************
echo Archive product components
rem ********************************************
Set BaseTgtPath=%TargetDir%\%VersionNumber%
call .\Build\SaveBuiltProducts.bat

Rem label build completion
Echo Labelling LonScanner build completion (LonScanner_%VersionNumber%)...

if "%exp%" == "-noexport" goto SkipExport

:successExit
Echo ON
Echo Build Success
Goto done

:errorInstallBuild
Echo ...
Echo ... Error on building installer
Echo ...
Echo .
Goto done_error

:version_error
Echo ...
Echo ... Error on setting build version
Echo ...
Echo .
goto done_error

:readonly_file_error
Echo ...
Echo ... Can not overwrite a read-only file
Echo ...
Echo .
goto done_error

:build_error
Echo ...
Echo ... Error building LonScanner
Echo ...
Echo .
goto done_error

:git_not_found
Echo ...
Echo ... GIT not found.Install and configure GIT properly.
Echo ...
Echo .
goto done_error

:git_not_working
Echo ...
Echo ... GIT not working.
Echo ...
Echo .
goto done_error

:remote_url_not_found
Echo ...
Echo ... GIT remote URL not found.
Echo ...
Echo .
goto done_error

:done_error
Echo on
Echo Build Failed


:done
Rem -- Clean up environment
set newBuildNumber=
Rem     Clear Build Directories
Set INCLUDE=
Set LIB=
rem Set PATH=%_PATH%
rem Set _PATH=



