@Echo Off
Rem     SaveBuildProducts.bat - Create/copy the NodeBuilder product components
Rem

Set TargetFolder=%BaseTgtPath%\CD Image
Set WebTargetFolder=%BaseTgtPath%\Web Download

Set AdobeTgtPath=%TargetFolder%\Installs\AdobeAcrobat
Set NreTgtPath=%TargetFolder%\Installs\NodeBuilderResourceEditor
Set DocuTgtPath=%TargetFolder%\Documentation
Set DemoShieldTgtPath=%TargetFolder%\Bin
Set LspaTgtPath=%TargetFolder%\Installs\LonScanner

Set LspaSrcPath=.\Install\LSPA Installer\PROJECT_ASSISTANT\CDROM_IMAGE\DiskImages\DISK1
Set WebLspaSrcPath=.\Install\LSPA Installer\PROJECT_ASSISTANT\Web\DiskImages\DISK1

Set InternalFolderName=%InternalTargetDir%\%VersionNumber%

if "x%BaseTgtPath%" == "x" Goto :NoTargetPath

Rem ****************************************
Rem  wipe out previous folder and contents
Rem ****************************************
ECHO Delete the Build output folder if exists
if exist "%BaseTgtPath%" RMDIR /S /Q "%BaseTgtPath%
If Exist "%BaseTgtPath%" Goto :DirBusy
if exist "%InternalFolderName%" RMDIR /S /Q "%InternalFolderName%
If Exist "%InternalFolderName%" Goto :DirBusy

Rem ****************************************
Rem  Create folders
Rem ****************************************
ECHO Create the Build output folder
if Not exist "%BaseTgtPath%" mkdir "%BaseTgtPath%"
if Not exist "%InternalFolderName%" mkdir "%InternalFolderName%"
MkDir "%TargetFolder%"
MkDir "%WebTargetFolder%"
MkDir "%LspaTgtPath%"
MkDir "%DemoShieldTgtPath%"
MkDir "%AdobeTgtPath%"	
MkDir "%NreTgtPath%"	
MkDir "%DocuTgtPath%"

Echo Copying files...
@echo --------------------------------------
@echo  **** Copying LonScanner Files ****
@echo --------------------------------------
Echo "%LspaSrcPath%"
Echo "%LspaTgtPath%"
xcopy /E /-Y "%LspaSrcPath%\*.*"	"%LspaTgtPath%\"

@echo --------------------------------------
@echo  **** Copying Web Download Files ****
@echo --------------------------------------
Echo "%WebLspaSrcPath%"
Echo "%WebTargetFolder%"
copy "%WebLspaSrcPath%\setup.exe"	"%WebTargetFolder%\LonScanner.exe"

@echo --------------------------------------
@echo  **** Copy DemoShield Components Files ****
@echo --------------------------------------
Echo ".\Install\DemoShield\Image\autorun.inf"
Echo "%TargetFolder%"
copy ".\Install\DemoShield\Image\autorun.inf"			"%TargetFolder%"			
copy ".\Install\DemoShield\Image\setup.exe"				"%TargetFolder%"			
copy ".\Install\DemoShield\Image\setup.ini"		"%TargetFolder%"			
copy ".\Install\DemoShield\Image\Setup.exe.manifest"		"%TargetFolder%"			
copy ".\Install\DemoShield\Image\bin\demo32.exe"		"%TargetFolder%\bin"			
copy ".\Install\DemoShield\Image\bin\demo32.exe.manifest"		"%TargetFolder%\bin"			
copy ".\Install\DemoShield\Image\bin\Lspa demoshield.dbd"		"%TargetFolder%\bin"			

@echo --------------------------------------
@echo  **** Copy Adobe Reader ****
@echo --------------------------------------
copy ".\import\install\AdbeRdr920_en_US.exe" "%AdobeTgtPath%"			

@echo --------------------------------------
@echo  **** Copy Nodebuilder Resource Editor ****
@echo --------------------------------------
copy ".\import\install\ResourceEditor401.exe" "%NreTgtPath%\"			

@echo --------------------------------------
@echo  **** Copy Readme File ****
@echo --------------------------------------
copy ".\doc\readme.htm" "%LspaTgtPath%"
copy ".\doc\readme.htm" "%WebTargetFolder%\"

@echo -----------------------------------------
@echo  **** Copy Documentation ****
@echo -----------------------------------------
copy ".\lmpa\help\LonScanner.pdf"	"%DocuTgtPath%" 


Echo ... Copying libs and PDBs to %InternalFolderName%
Echo ...
Echo .
Echo "%InternalFolderName%"
if Not Exist "%InternalFolderName%\Lib" mkdir "%InternalFolderName%\Lib"

xcopy  /s /i /Y .\Mak\Release\*.lib "%InternalFolderName%\Lib\*.lib" 
xcopy /s /i /Y .\Mak\Release\*.pdb "%InternalFolderName%\Pdb\*.pdb"  

REM ******* END COPYING FINAL FILES **************

Goto :Exit

:DirBusy
Echo %0: failed to delete folder - perhaps it's in use
Goto :Exit

:NoTargetPath
Echo %0: Error!!! No target path specified 

:Exit
Rem     That's all Folks!
EndLocal
