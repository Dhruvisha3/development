//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ns_apb.h#11 $
//

#ifndef _NXEBIN_H
#define _NXEBIN_H


/***************************************************************
 *  Filename: nxebin.h
 *
 *  Description:  This header file contains the definitions
 *  for the binary version of the NXE file.
 *
 *  Copyright (c) 1994-2000 Echelon Corporation.  All Rights Reserved.
 *
 ****************************************************************/


#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */


#define APPIMAGE_EXTEN ".APB"
#define APPIMAGE_FILE_TAG_LEN 9
#define APPIMAGE_FILE_TAG  "APPIMAGE"
#define APPIMAGE_VERSION 1

typedef struct AppImageFileHdr
{
    char fileTag[APPIMAGE_FILE_TAG_LEN];        /* contains "APPIMAGE" */
    Byte fileVersion;                           /* file format version */
    Byte neuronModel;                           /* neuron HW model #.  If 0xff, must have a AppImageSysImageBuild 
                                                   record to override - used for apps with transient functions, or,
                                                   for system image APBs, must have a AppImageSysImageHdr
                                                 */
    Byte firmwareVersion;                       /* neuron FW version # */
} AppImageFileHdr;

typedef struct AppImageDataHdr
{
    NetWord location;   /* mem addr for data write, NV index for NV cnfg */
    Byte numBytes;      /* byte count of entire record */
    Byte options;       /* options bitmask */
    Byte checksum;      /* XOR of all other bytes in record */
} AppImageDataHdr;

/* Option definitions for above field */
#define APPIMAGE_WRITE_DATA             0x00
#define APPIMAGE_RESET_AFTER_WRITE      0x01
#define APPIMAGE_WRITE_NV_CNFG          0x02   /* uses nv_struct */
#define APPIMAGE_BIGBUF_RESET           0x04
#define APPIMAGE_NO_BIGBUF_RESET        0x08
#define APPIMAGE_WRITE_OVERLAY_DATA     0x10   /* This is used for loading DM20s.  Could be overloaded
                                                  for images that use some of the features (like devices
                                                  with build numbers. */
#define APPIMAGE_EXTENSION_RECORD       0x20   /* See header EXTENSION RECORDS below. */
#define APPIMAGE_EXTENDED_WRITE         0x40   /* Same format as APPIMAGE_WRITE_DATA, except that location 
                                               is set to address/16; Valid only for APPIMAGE_FILE_TAG2 */
#define APPIMAGE_NO_ERASE               0x80   /* Don't erase flash when writing */

#define NEURON_EXTENDED_ADDRESS_BASE 0x10000    // Used to represent areas of flash that do not map directly
                                                // to neuron memory.  Use EXTENDED_SYS_ABSOLUTE or 
                                                // EXTENDED_APP_ABSOLUTE addressing modes.  In these
                                                // modes the location and count represent '16 byte words'.

#define NEURON_EXTENDED_WORD_SIZE 16

#define ENCODE_EXTENDED_ADDRESS(addr) (((addr)-NEURON_EXTENDED_ADDRESS_BASE)/NEURON_EXTENDED_WORD_SIZE)
#define DECODE_EXTENDED_ADDRESS(addr) ((((unsigned long)(addr))*NEURON_EXTENDED_WORD_SIZE) + NEURON_EXTENDED_ADDRESS_BASE)

/* The following structure is used to interpret the "data" portion of
   the AppImageData record when the APPIMAGE_WRITE_OVERLAY_DATA option is 
   used. The count field in the AppImageDataHdr should include the bank. */
typedef struct OverlayImageData
{
    Byte    bankNumber;
    Byte    data[1];
} OverlayImageData;

/* "Standard" APB data record */
typedef struct AppImageData
{
    AppImageDataHdr header;
    Byte data[1];
} AppImageData;

/*-----------------------------------------------------------------------------
 *                          EXTENSION RECORDS 
 *
 * The following definitions are used for APB extension records.  Unfortunately
 * the APB is not very extensible. For backward compatibility, extention records
 * look like AppImageData using the option type APPIMAGE_EXTENSION_RECORD. 
 * This option value (32) used to be used for APPIMAGE_WRITE_NV_CNFG_EXT.  While
 * code exists in legacy versions of LNS (prior to 3.30) to process these records,
 * nxe32Bin never created them. Also, legacy versions of LNS will ignore these records
 * so long as they are less than 8 bytes long.
 *
 * WARNING:  You must also be careful to avoid adding extension records after 
 *           a big buf reset and prior to NV records (or the end of the app load),
 *           because a side effect of processing extension records in older versions
 *           is that it will reset the neuron iff a big buf reset has been sent 
 *           and no other reset since the big buf reset has been sent.  This is not
 *           a problem in LNS 3.30 and later.
 *
 *           ALSO, extension records should not be added for base version 6 or 7 
 *           firmware until after the second app image record (the memory map).
 *
 * APB extension records all use the AppImageExensionHeader header, which contains
 * a AppImageDataHdr and an extension type.  The options feild of the AppImageDataHdr
 * must include the APPIMAGE_EXTENSION_RECORD bit.  
 * 
 *----------------------------------------------------------------------------*/

/* Maximum data size of an extension record.  If this is exeeded, legacy versions of LNS
 * (prior to 3.30) will interpret this as an APPIMAGE_WRITE_NV_CNFG_EXT record. 
 */
#define MAX_APP_IMAGE_EXTENSION_RECORD_DATA 6

/* Following are the defined APB extension record types */
#define APPIMAGEEXT_EVN 0 /* Contains the EVN used by the device. */
#define APPIMAGEEXT_FLASH_DRIVER_ID 1 /* Contains the flash driver ID. */
#define APPIMAGEEXT_SYS_BUILD_VER   2 /* Contains the system image build number. */
#define APPIMAGEEXT_SYS_IMAGE_HDR   3 /* The APB contains a system image loadable using a boot loader */

/* All extention records use the following header */
typedef struct AppImageExensionHeader
{
    AppImageDataHdr stdHdr;   /* stdHdr.option MUST include APPIMAGE_EXTENSION_RECORD */
    Byte            extType;  /* APPIMAGEEXT_* */

    /* Data follows immediatly.  Must be <= MAX_APP_IMAGE_EXTENSION_RECORD_DATA bytes long */
} AppImageExensionHeader;


/* An EVN.  This record must must have the APPIMAGE_EXTENSION_RECORD flag set in the 
 * standardHeader.option field.
 */
typedef struct AppImageEvn
{
    AppImageExensionHeader header;  /* standardHeader.option=APPIMAGE_EXTENSION_RECORD 
                                       extensionType = APPIMAGEEXT_EVN
                                     */
    extended_version_number evn;   /* The extended system image version # */
} AppImageEvn;

/* The flash driver ID, used by Neuron 5000 devices with falsh.  
 * This record must must have the APPIMAGE_EXTENSION_RECORD flag set in the 
 * standardHeader.option field.
 */
typedef struct AppImageFlashDriverId
{
    AppImageExensionHeader header;  /* standardHeader.option=APPIMAGE_EXTENSION_RECORD 
                                       extensionType = APPIMAGEEXT_FLASH_DRIVER_ID
                                     */
    Byte                   flashDriverId;   /* The flash driver ID*/
} AppImageFlashDriverId;

/* The AppImageSysImageBuild record is used to load applications into system 
 * images with major.minor.buildNumber, such as the FT 60x0 and later Neurons.
 * This record must must have the APPIMAGE_EXTENSION_RECORD flag set in the 
 * standardHeader.option field.
 */
typedef struct AppImageSysImageBuild
{
    AppImageExensionHeader header;  /* standardHeader.option=APPIMAGE_EXTENSION_RECORD 
                                       extensionType = APPIMAGEEXT_SYS_BUILD_VER
                                     */
    Byte                        neuronModel;     /* Superceeds version in header */
    full_system_image_version   firmwareVersion;
} AppImageSysImageBuild;

/* The AppImageSysImageHdr record is used to load system images that use a boot
 * loader. This record must must have the APPIMAGE_EXTENSION_RECORD flag set in the 
 * standardHeader.option field.  Not that the system image of series 3150 and 5000 
 * devices cannot be loaded via an APB, because they do not contain boot loaders.
 */
typedef struct AppImageSysImageHdr
{
    AppImageExensionHeader header;  /* standardHeader.option=APPIMAGE_EXTENSION_RECORD 
                                       extensionType = APPIMAGEEXT_SYS_IMAGE_HDR
                                     */
    Byte                   neuronModel;     /* Superceeds model in header.  Note that a given system image might be able
                                               to upgrade multiple neuron models (FT 6050 and Neuron 6050 for example),
                                               but LNS uses the model number to find the APB. */
    // The miniumum firmware version (major.minor.buildNumber) that supports loading this image
    full_system_image_version   minFirmwareVersion;

    // The minimum boot loader image version that supports loading this image
    Byte                   minBootLoaderVersion;      
} AppImageSysImageHdr;

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif


#endif
