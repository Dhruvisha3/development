//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ns_batu.h#4 $
//

/***************************************************************
 *  Filename:     ns_batu.h
 *
 *  Description:  This file contains internal definitions used to
 *                implement the NS batch service and functions and
 *                there internal utilities.
 *
 *  Copyright (c) 1998-2000 Echelon Corporation.  All Rights Reserved.
 *
 ****************************************************************/

#ifndef _NS_BATU_H
#define _NS_BATU_H

#include "ns_batch.h"

#define MAX_BATCH_ITERATOR_FIELDS 2

#define NO_BATCH_ITERATOR_KEY 0

/****************************************************************************
                        INVOCATION DATA STRUCTURE

    The following picture describes the layout of the service invocation 
    structure used by NS_BATCH_SERVICE, and the corresponding access methods 
    to use.


    Header                              
  ===============================       
  | batchServiceHdrSize         |       
  | resultBatchServiceHdrSize   |       
  | numberOfServices            |       
  | optionFlags                 |       
  | serviceParameterElementSize |       
  | resultElementSize           |       
  | index                       |       
  | numIteratorFields           |       
  | sizeOfIteratorData          |       
  | serviceCode                 |
  | systemManager               |       
  ===============================       
                                        
    array of service data               
  ===============================       
  | service data                |     use NsGetBatchServiceParms to access.  
  |-----------------------------|       
  | service data                |       
  |-----------------------------|       
                .                       
                .                       
                .                       
  |-----------------------------|
  | servie data                 |       
  ===============================       
                                        

 ***************************************************************************/

/****************************************************************************
                            RESULT DATA STRUCTURE
    
    The following picture describes the layout of the result data structures
    used by NS_BATCH_SERVICE, and the corresponding access methods to use.

      Header
    ===============================
    | resultBatchServiceHdrSize   |
    | numberOfResults             |
    | sizeOfResultElement         |
    | numIteratorFields           |
    | sizeOfIteratorData          |
    | optionFlags                 |
    | resultFlags                 |
    | serviceCode                 |
    ===============================

      Iterator directory
    ===============================
    | encodedFldSize              |   Use NsGetBatchIteratorDirFromResult to access
    | fieldOffset                 |
    |-----------------------------|
                  .
                  .
                  .
    |-----------------------------|
    | encodedFldSize              |
    | fieldOffset                 |
    ===============================

    ===============================
    | next iterator data          |  Use NsGetNextIterator to access.    
    ===============================

      resultElement
    ===============================
    |  sts                        |  
    |  iteratorBytes              |  
    |  [resultSize]               |  
    |  result data                |  
    |-----------------------------|
    |  sts                        |  Use NsGetBatchResultSts to access.
    |  iteratorBytes              |  Use NsGetBatchResultIterator to access.
    |  [resultSize] optional      |  Use NsGetBatchResultVarSize to access.
    |  result data                |  Use NsGetBatchResultData to access.   
    |-----------------------------|
                  .
                  .
                  .
    |-----------------------------|
    |  sts                        |
    |  iteratorBytes              |
    |  [resultSize]               |
    |  result data                |
    ===============================

 ***************************************************************************/


/***************************************************************************
 *        Access functions for service invocation parameters               *
 ***************************************************************************/

#define NsGetBatchServiceSize(optionFlags, numberOfServices, \
                              serviceParameterElementSize)  \
    (sizeof(NsBatchServiceHdr) + \
        NUM_BATCH_SERVICE_PARMS(optionFlags, numberOfServices) * \
            (serviceParameterElementSize))

#define NsGetBatchServiceParms(pServiceParms, index) \
    ((Byte *)(pServiceParms) + (pServiceParms)->batchServiceHdrSize + \
              ((pServiceParms)->serviceParameterElementSize * (index)))


/***************************************************************************
 *        Access functions for Result                                      *
 ***************************************************************************/

/* Calculate the size of the Batch result header */
#define NsGetBatchResultHdrSize(sizeofFixedResultHdr,                       \
                                numIteratorFields,                          \
                                sizeOfIteratorData)                         \
    ((sizeofFixedResultHdr) + /* HDR */                                     \
                /* Iterator field descriptor directory */                   \
     ((numIteratorFields) * sizeof(NsBatchIteratorDirectoryEntry)) +        \
                /* Next iterator */                                         \
     (sizeOfIteratorData))

#define NsGetBatchIteratorDirFromResult(pNsResultBactchService) \
    ((NsBatchIteratorDirectoryEntry *)((Byte *)(pNsResultBactchService) +  \
        (pNsResultBactchService)->resultBatchServiceHdrSize))
    
#define NsGetNextIterator(pResult) \
     ((pResult)->resultBatchServiceHdrSize +                                    \
       ((pResult)->numIteratorFields * sizeof(NsBatchIteratorDirectoryEntry)) + \
       ((Byte *)pResult))

extern NetWord NsGetBatchResultSize(NsBatchServiceHdr *pNsBatchService);

/***************************************************************************
 *        Access functions for Result Element                               *
 ***************************************************************************/

#define NsGetFirstBatchResultElement(pNsResultBatchService) \
    (((Byte *)(pNsResultBatchService)) + \
        NsGetBatchResultHdrSize((pNsResultBatchService)->resultBatchServiceHdrSize,     \
                                (pNsResultBatchService)->numIteratorFields,             \
                                (pNsResultBatchService)->sizeOfIteratorData))         

/* Offsets within result element */
#define OFFSETOF_BATCH_RESULT__STS 0

#define OFFSETOF_BATCH_RESULT__ITERATOR (sizeof(SrSts))

#define OFFSETOF_BATCH_RESULT__VAR_SIZE(sizeofIteratorData) \
     (OFFSETOF_BATCH_RESULT__ITERATOR + (sizeofIteratorData))

#define OFFSETOF_BATCH_RESULT__DATA(options, sizeofIteratorData) \
    (OFFSETOF_BATCH_RESULT__VAR_SIZE(sizeofIteratorData) + \
     (((options) & BATCH_SERVICE_VAR_SIZE_RESULTS) ? sizeof(VarBatchResultSize) : 0 ))

/* Access data within result element */
#define CALC_BATCH_RESULT_PTR(pResultElement, type, offset) ((type *)((Byte *)pResultElement + (offset)))

#define NsGetBatchResultSts(pResultElement) \
     *(CALC_BATCH_RESULT_PTR(pResultElement, SrSts, OFFSETOF_BATCH_RESULT__STS))

#define NsGetBatchResultIterator(pResultElement) \
     (CALC_BATCH_RESULT_PTR(pResultElement, Byte, OFFSETOF_BATCH_RESULT__ITERATOR))

#define NsGetBatchResultVarSize(pResultElement, sizeofIteratorData) \
     *(CALC_BATCH_RESULT_PTR(pResultElement,                               \
                             VarBatchResultSize,                            \
                             OFFSETOF_BATCH_RESULT__VAR_SIZE(sizeofIteratorData)))

#define NsGetBatchResultData(pResultElement, options, sizeofIteratorData) \
     CALC_BATCH_RESULT_PTR(pResultElement, Byte,  OFFSETOF_BATCH_RESULT__DATA(options,  sizeofIteratorData)) 

extern NetWord NsGetBatchResultElementSize(BatchServiceOptions optionFlags, 
                                           Byte sizeOfIteratorData, 
                                           NetWord resultDataSize);

#endif
