//
// $Header: //depot/Software/NetTools/LNS/Dev/NSS/INCLUDE/ns_event.h#10 $
//

#ifndef _NS_EVENT_H
#define _NS_EVENT_H

/***************************************************************
 *  Filename: ns_event.h
 *
 *  Description:  This file contains service structures for event
 *                services, namely:
 *
 *                 subscription services:
 *                    NS_BEGIN_EVENTS
 *                    NS_END_EVENTS
 *
 *                 specific events:
 *                    NS_SERVICE_PIN
 *                    NS_CHANGED_PROPERTY
 *
 *  Copyright (c) 1994-2000 Echelon Corporation.  All Rights Reserved.
 *
 ****************************************************************/

#ifdef _MSC_VER
#pragma pack(_NSS_PACKING)
#endif

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

/*
   An event tag allows the client to correlate events with a
 subscription.  When subscribing to events, the client assigns a tag
 value to the subscription.  When the NSS notifies the client of an
 event, it tags the event using the subscription's tag value.
 Note that for NSS-10, property change events must be tagged with a
 tag value of zero.
*/
typedef Byte EventTag;
#define NULL_EVENT_TAG          ((EventTag) -1)
#define NSS_MAX_EVENT_TAG       100


/*
  EventType describes a given event. It may be used during
  event subscription to qualify the types of events to be received.
  In most cases it an NSS Service Code with the Event Attribute set.
*/
typedef NetWord EventType;
#define NULL_EVENT_TYPE     ((EventType) -1)


/* This enum defines the different ways an event subscription may expire.*/
typedef enum _NsExpiryType
{
    EXP_PERM            = 0         /* permanent, until cancelled by client */
    /* other types are not supported in this release */
} _NsExpiryType;
typedef NetEnum(_NsExpiryType) NsExpiryType;

/* This enum describes the modification which just occurred to an object.
*/
typedef enum _ObjectAction
{
    OBJECT_ADDED,       /* 0 */
    OBJECT_REMOVED,     /* 1 */
    OBJECT_MODIFIED,    /* 2 */
    OBJECT_unused,      /* 3 - This id is no longer used, and hasn't been for some time... */
    OBJECT_REGISTERED,  /* 4 - Used by router/node database version events */
    OBJECT_DEREGISTERED,/* 5 - Used by router/node database version events */
    OBJECT_REPLACED,    /* 6 - Used by router/node database version events */
    OBJECT_UPGRADED,    /* 7 - Used by node version events when the program interace has been 
                           upgraded. */
    OBJECT_RENAMED,     /* 8 - Used by channel, router, subnet and NSD rename */
    OBJECT_MOVED,       /* 9 - Used by node and router move. */
    OBJECT_DESCRIPTION_MODIFIED,    /* 10 - used by LCA for description modifications. */
    OBJECT_SUBSYSTEM_MEMBERSHIP_MODIFIED, /* 11 - used by LCA when subsystem membership is modified */    

    OBJECT_ACTION_MAX
} _ObjectAction;

typedef NetEnum(_ObjectAction) ObjectAction;


/*************************************************************************/
/*  NS_BEGIN_EVENT - invocation parameters                               */
/*  Each structure begins with NsBeginEventsHdr, followed by zero or     */
/*  more parameters, depending on the type of events.                    */
/*  (There is no result data for this service).                          */
/*                                                                       */
/*  Note that some of the fields are for future use.  They should be     */
/*  set to the recommended values in this release.                       */
/*************************************************************************/

/* Common parameters for all NsBeginEvents invocation structures            */
typedef struct NsBeginEventsHdr
{
    ManagerHandle   clientHandle;       /* must be zero in this release     */
    NsExpiryType    expiryType;         /* must be EXP_PERM                 */
    NetWord         expiryThreshold;    /* must be zero in this release     */
    EventTag        eventTag;           /* assigned by client               */
    ServiceCode     eventServiceCode;   /* event service code of interest   */
} NsBeginEventsHdr;


/* Invocation parameters for subscribing to NS_SERVICE_PIN events           */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventServiceCode        = NS_SERVICE_PIN                        */
/*      programIdMask and locationMask indicate with bits to compare with   */
/*          the corresponding fields in the node.  An all-zero or all-one's */
/*          indicates no matching to do.                                    */
typedef struct NsBeginEventsServicePin
{
    NsBeginEventsHdr hdr;           /* header                               */
    ProgramId       programIdMask;  /* see above comment                    */
    ProgramId       programId;      /* masked program id to match           */
    Location        locationMask;   /* see above comment                    */
    Location        location;       /* masked location to match             */
    ChannelHandle   channelHandle;  /* channel id to match; 0 means all     */
} NsBeginEventsServicePin;

/* Invocation parameters for subscribing to NS_LCA_GENERATED_EVENT events   */
/* NSS maintains the subscription database, but LCA generates them.         */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventServiceCode        = NS_LCA_GENERATED_EVENT                */
/*                                                                          */
/* The remaining fields control the subscription                            */
/*      criteriaLen	                The length, in bytes, of the criteria   */
/*                                  to be match on. Range is from 0 to      */
/*                                  MAX_LCA_CRITERIA_LEN.              */
/*
/*      criteria	                The criteria to match on.  Any LCA      */
/*                                  event whose first criteriaLen bytes     */
/*                                  match these bytes will be sent to the   */
/*                                  subscriber. Specifying fewer byte than  */
/*                                  are actually in event can be used to    */
/*                                  implement wild-card subscriptions       */
/*                                                                          */
#define MAX_LCA_CRITERIA_LEN 8 /* This cannot be more than MAX_ITEM_ID_LEN_SUPPORTED */
typedef struct NsBeginEventsLcaGeneratedEvent
{
    NsBeginEventsHdr hdr;           /* header                               */
    Byte             criteriaLen;   /* length of criteria                   */
    Byte             criteria[MAX_LCA_CRITERIA_LEN];      
} NsBeginEventsLcaGeneratedEvent;

/************************************************************/
/* Generic begin event service requests for all property    */
/* change events                                            */
/************************************************************/

typedef enum
{
    CHANGE_PROP_MASK_NONE = 0,
    INVALID_PROPERTY      = 1
} _ChangePropertyMask;

typedef NetEnum(_ChangePropertyMask) ChangePropertyMask;

/* Macro returns true if an event (type NS_CHANGED_PROPERTY) represents an
   invalid event.
*/
#define INVALID_CHANGED_PROPERTY_EVENT(pEvent) \
    ((pEvent)->options & INVALID_PROPERTY)


/* Set INVALID_PROPERTY in the options mask to subscribe to invalid
   properties in addition to valid ones.  It is not possible to subscribe
   to invalid change properties only.
*/
typedef struct NsBeginEventsChangedProperty
{
    NsBeginEventsHdr   hdr;
    ChangePropertyMask options;
    ObjectRef          objRef;
} NsBeginEventsChangedProperty;

/*************************************************************/
/* CAT_NODE - Begin event service requests for node property */
/*            change events                                  */
/*************************************************************/

/* Invocation parameters for subscribing to property change events:         */
/*     PROP_NODE_ADDRESS & PROP_NODE_ADDRESS_V20 - node's domain/subnet/node address changes */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedAddrNode
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NodeRef            objRef;        /* node reference                     */
} NsBeginEventsChangedAddrNode;


/* Invocation parameters for subscribing to node attachment change events:  */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNodeAttachment
{
    NsBeginEventsHdr   hdr;           /* header                               */
    ChangePropertyMask options;       /* options                              */
    NodeRef            objRef;        /* node reference                       */
                                      /* cat = CAT_NODE;                      */
                                      /* property=PROP_NODE_ATTACHMENT;       */
} NsBeginEventsChangedNodeAttachment;

/* Invocation parameters for subscribing to property change events:         */
/*     PROP_MT_ADDRESS          - message tag address change                */
/*     PROP_NV_ADDRESS          - nv address change                         */
/*     PROP_NV_CONN_INFO        - connection's nv selector and addr change  */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedAddrNvMt
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NvMtRef            objRef;        /* nv/mt reference                    */
} NsBeginEventsChangedAddrNvMt;

/* Invocation parameters for subscribing to node interface change events:   */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNodeIntfVersion
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NodeRef            objRef;        /* node reference                     */
                                      /* cat = CAT_NODE;                    */
                                      /* property=PROP_NODE_INTF_VERSION;   */
} NsBeginEventsChangedNodeIntfVersion;

/* Invocation parameters for subscribing to node conn version change events: */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNodeConnVersion
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NodeRef            objRef;        /* node reference                     */
                                      /* cat = CAT_NODE;                    */
                                      /* property=PROP_NODE_CONN_VERSION;   */
} NsBeginEventsChangedNodeConnVersion;

/* Invocation parameters for subscribing to node commission status change events:   */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNodeCommissionStatus
{
    NsBeginEventsHdr   hdr;           /* header                               */
    ChangePropertyMask options;       /* options                              */
    NodeRef            objRef;        /* node reference                       */
                                      /* cat = CAT_NODE;                      */
                                      /* property=PROP_NODE_COMMISSION_STATUS;*/
} NsBeginEventsChangedNodeCommissionStatus;

/*************************************************************/
/* CAT_NSS  - Begin event service requests for NSS property  */
/*            change events                                  */
/*************************************************************/

/* Invocation parameters for subscribing to subnet DB version events:       */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNssSubnetVersion
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NssRef             objRef;        /* NSS reference                      */
                                      /* cat = CAT_NSS;                     */
                                      /* property=PROP_NSS_SUBNET_VERSION;  */
} NsBeginEventsChangedNssSubnetVersion;

/* Invocation parameters for subscribing to channel DB version events:      */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNssChannelVersion
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NssRef             objRef;        /* NSS reference                      */
                                    /* cat = CAT_NSS;                       */
                                    /* property=PROP_NSS_CHANNEL_VERSION;   */
} NsBeginEventsChangedNssChannelVersion;

/* Invocation parameters for subscribing to management mode events:         */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */

typedef struct NsBeginEventsChangedNssMgmtMode
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NssRef             objRef;        /* NSS reference                      */
                                    /* cat = CAT_NSS;                       */
                                    /* property=PROP_NSS_MGMT_MODE;   */
} NsBeginEventsChangedNssMgmtMode;

/* Invocation parameters for subscribing to node DB version events:         */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNssNodeVersion
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NssRef             objRef;        /* NSS reference                      */
                                      /* cat = CAT_NSS;                     */
                                      /* property=PROP_NSS_NODE_VERSION;    */
} NsBeginEventsChangedNssNodeVersion;

/* Invocation parameters for subscribing to NSI DB version events:          */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNssNsiVersion
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NssRef             objRef;        /* NSS reference                      */
                                      /* cat = CAT_NSS;                     */
                                      /* property=PROP_NSS_NSI_VERSION;    */
} NsBeginEventsChangedNssNsiVersion;

/* Invocation parameters for subscribing to router DB version events:       */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedNssRouterVersion
{
    NsBeginEventsHdr   hdr;           /* header                             */
    ChangePropertyMask options;       /* options                            */
    NssRef             objRef;        /* NSS reference                      */
                                      /* cat = CAT_NSS;                     */
                                      /* property=PROP_NSS_ROUTER_VERSION;  */
} NsBeginEventsChangedNssRouterVersion;

/***************************************************************/
/* CAT_ROUTER - Begin event service requests for node property */
/*              change events                                  */
/***************************************************************/

/* Invocation parameters for subscribing to router attachment change events:*/
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedRouterAttachment
{
    NsBeginEventsHdr   hdr;         /* header                               */
    ChangePropertyMask options;     /* options                              */
    RouterRef          objRef;      /* router reference                     */
                                    /* cat = CAT_ROUTER;                    */
                                    /* property=PROP_ROUTER_ATTACHMENT;     */
} NsBeginEventsChangedRouterAttachment;

/* Invocation parameters for subscribing to router commission status change events:   */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_CHANGED_PROPERTY                   */
typedef struct NsBeginEventsChangedRouterCommissionStatus
{
    NsBeginEventsHdr   hdr;         /* header                               */
    ChangePropertyMask options;     /* options                              */
    RouterRef          objRef;      /* router reference                     */
                                    /* cat = CAT_ROUTER;                    */
                                    /* property=PROP_ROUTER_COMMISSION_STATUS;*/
} NsBeginEventsChangedRouterCommissionStatus;


/* Invocation parameters for subscribing to NS_VALIDATE_DATABASE_PROGRESS_EVENT events           */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventServiceCode        = NS_VALIDATE_DATABASE_PROGRESS_EVENT                        */
typedef struct NsBeginEventsDatabaseValidationProgress
{
    NsBeginEventsHdr hdr;           /* header                               */
} NsBeginEventsDatabaseValidationProgress;


/* Invocation parameters for subscribing to node configuration updates       */
/* version events:                                                          */
/* Certain fields have the following values:                                */
/*      hdr.expiryType              = EXP_PERM                              */
/*      hdr.expiryThreshold         = 0                                     */
/*      hdr.eventTag                = 0 (NSS-10 only)                       */
/*      hdr.eventServiceCode        = NS_NODE_CONFIG_UPDATED_EVENT          */
typedef struct NsBeginEventsNodeConfigUpdated
{
    NsBeginEventsHdr   hdr;          /* header                             */
} NsBeginEventsNodeConfigUpdated;

/* This is the union of all NsBeginEvents... structures.                    */
typedef union NsBeginEvents
{
    NsBeginEventsServicePin                     servicePin;
    NsBeginEventsChangedProperty                changedProperty;
    NsBeginEventsChangedAddrNode                changedAddrNode;
    NsBeginEventsChangedAddrNvMt                changedAddrNvMt;
    NsBeginEventsChangedNodeAttachment          changedNodeAttachment;
    NsBeginEventsChangedNodeIntfVersion         changedNodeIntfVersion;
    NsBeginEventsChangedNodeConnVersion         changedNodeConnVersion;
    NsBeginEventsChangedNodeCommissionStatus    changedNodeCommissionStatus;
    NsBeginEventsChangedNssSubnetVersion        changedNssSubnetVersion;
    NsBeginEventsChangedNssChannelVersion       changedNssChannelVersion;
    NsBeginEventsChangedNssMgmtMode             changedNssMgmtMode;
    NsBeginEventsChangedNssNodeVersion          changedNssNodeVersion;
    NsBeginEventsChangedNssRouterVersion        changedNssRouterVersion;
    NsBeginEventsChangedRouterAttachment        changedRouterAttachement;
    NsBeginEventsChangedRouterCommissionStatus  changedRouterCommissionStatus;
    NsBeginEventsDatabaseValidationProgress     databaseValidationProgresss;
    NsBeginEventsNodeConfigUpdated              nodeConfigUpdated;
} NsBeginEvents;

/*************************************************************************/
/*  NS_END_EVENT - invocation parameters                                 */
/*  Each structure begins with NsEndEventsHdr, followed by zero or       */
/*  more parameters, depending on the type of events.                    */
/*  (There is no result data for this service).                          */
/*************************************************************************/

/* Common parameters for all NsEndEvents invocation structures.          */
/* These fields have the same values as the corresponding fields in      */
/* NsBeginEventsHdr.                                                     */
typedef struct NsEndEventsHdr
{
    ManagerHandle   clientHandle;
    EventTag        eventTag;
    ServiceCode     eventServiceCode;
} NsEndEventsHdr;

/* Invocation parameters for ending subscription to NS_SERVICE_PIN events  */
typedef struct NsEndEventsServicePin
{
    NsEndEventsHdr  hdr;
} NsEndEventsServicePin;

/* Invocation parameters for ending subscription to NS_LCA_GENERATED_EVENT events.
   Specifying fewer criteria bytes serves as a "wild-card" descubsription.
*/
typedef struct NsEndEventsLcaGeneratedEvent
{
    NsEndEventsHdr   hdr;
    Byte             criteriaLen;   /* length of criteria                   */
    Byte             criteria[MAX_LCA_CRITERIA_LEN];      
} NsEndEventsLcaGeneratedEvent;

/************************************************************/
/* Generic end event service request for all property       */
/* change events                                            */
/************************************************************/

/* Set INVALID_PROPERTY in the options mask to desubscribe to invalid
   properties only.  It is not possible to desubscribe to valid change
   properties only.
*/
typedef struct NsEndEventsChangedProperty
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    ObjectRef          objRef;
} NsEndEventsChangedProperty;

/*************************************************************/
/* CAT_NODE - End event service requests for node property   */
/*            change events                                  */
/*************************************************************/

/* Invocation parameters for ending subscription to all property change  */
/* events (PROP_ALL) on a specified node or all nodes (NODE_HANDLE_ALL)
   or a specific node property. */
typedef struct NsEndEventsChangedAddrNode
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NodeRef            objRef;
} NsEndEventsChangedAddrNode;

/* Invocation parameters for ending subscription to NVMT property changes  */
typedef struct NsEndEventsChangedNvMt
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NvMtRef            objRef;
} NsEndEventsChangedNvMt;

/* Invocation parameters for ending subscription to node attachment
   change events:
*/
typedef struct NsEndEventsChangedNodeAttachment
{
    NsEndEventsHdr     hdr;           /* header                               */
    ChangePropertyMask options;
    NodeRef            objRef;        /* node reference                       */
                                      /* cat = CAT_NODE;                      */
                                      /* property=PROP_NODE_ATTACHMENT;       */
} NsEndEventsChangedNodeAttachment;

/* Invocation parameters for ending subscription to Node interface property changes  */
typedef struct NsEndEventsChangedNodeIntfVersion
{
    NsEndEventsHdr     hdr;           /* header                               */
    ChangePropertyMask options;
    NodeRef            objRef;        /* node reference                       */
} NsEndEventsChangedNodeIntfVersion;

/* Invocation parameters for ending subscription to Node conn version property changes  */
typedef struct NsEndEventsChangedNodeConnVersion
{
    NsEndEventsHdr     hdr;           /* header                               */
    ChangePropertyMask options;
    NodeRef            objRef;        /* node reference                       */
} NsEndEventsChangedNodeConnVersion;

/* Invocation parameters for ending subscription to node commission status
   change events:
*/
typedef struct NsEndEventsChangedNodeCommissionStatus
{
    NsEndEventsHdr     hdr;           /* header                               */
    ChangePropertyMask options;
    NodeRef            objRef;        /* node reference                       */
                                      /* cat = CAT_NODE;                      */
                                      /* property=PROP_NODE_COMMISSION_STATUS;*/
} NsEndEventsChangedNodeCommissionStatus;

/*************************************************************/
/* CAT_NSS  - End event service requests for NSS property    */
/*            change events                                  */
/*************************************************************/

/* Invocation parameters for ending subscription to subnet DB version property changes  */
typedef struct NsEndEventsChangedNssSubnetVersion
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                        */
                                       /* cat = CAT_NSS;                       */
                                       /* property=PROP_NSS_SUBNET_VERSION;    */
} NsEndEventsChangedNssSubnetVersion;

/* Invocation parameters for ending subscription to channel DB version property changes  */
typedef struct NsEndEventsChangedNssChannelVersion
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                        */
                                       /* cat = CAT_NSS;                       */
                                       /* property=PROP_NSS_CHANNEL_VERSION;   */
} NsEndEventsChangedNssChannelVersion;

/* Invocation parameters for ending subscription to channel DB version property changes  */
typedef struct NsEndEventsChangedNssMgmtMode
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                        */
                                       /* cat = CAT_NSS;                       */
                                       /* property=PROP_NSS_MGMT_MODE;         */
} NsEndEventsChangedNssMgmtMode;


/* Invocation parameters for ending subscription to node DB version property changes  */
typedef struct NsEndEventsChangedNssNodeVersion
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                        */
                                       /* cat = CAT_NSS;                       */
                                       /* property=PROP_NSS_NODE_VERSION;      */
} NsEndEventsChangedNssNodeVersion;

/* Invocation parameters for ending subscription to NSI DB version property changes  */
typedef struct NsEndEventsChangedNssNsiVersion
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                        */
                                       /* cat = CAT_NSS;                       */
                                       /* property=PROP_NSS_NSI_VERSION;      */
} NsEndEventsChangedNssNsiVersion;

/* Invocation parameters for ending subscription to router DB version property changes  */
typedef struct NsEndEventsChangedNssRouterVersion
{
    NsEndEventsHdr     hdr;
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                        */
                                       /* cat = CAT_NSS;                       */
                                       /* property=PROP_NSS_ROUTER_VERSION;    */
} NsEndEventsChangedNssRouterVersion;

/***************************************************************/
/* CAT_ROUTER - End event service requests for router property */
/*              change events                                  */
/***************************************************************/

/* Invocation parameters for ending subscription to router attachment
   change events:
*/
typedef struct NsEndEventsChangedRouterAttachment
{
    NsEndEventsHdr     hdr;           /* header                               */
    ChangePropertyMask options;
    NodeRef            objRef;        /* node reference                       */
                                      /* cat = CAT_NODE;                      */
                                      /* property=PROP_ROUTER_ATTACHMENT;     */
} NsEndEventsChangedRouterAttachment;

/* Invocation parameters for ending subscription to router commission status
   change events:
*/
typedef struct NsEndEventsChangedRouterCommissionStatus
{
    NsEndEventsHdr     hdr;           /* header                               */
    ChangePropertyMask options;
    RouterRef          objRef;        /* node reference                       */
                                      /* cat = CAT_ROUTER;                    */
                                      /* property=PROP_ROUTER_COMMISSION_STATUS;*/
} NsEndEventsChangedRouterCommissionStatus;

/* Invocation parameters for ending subscription to NS_VALIDATE_DATABASE_PROGRESS_EVENT
   change events:
*/
typedef struct NsEndEventsDatabaseValidationProgress
{
    NsEndEventsHdr     hdr;           /* header                               */
} NsEndEventsDatabaseValidationProgress;

/* Invocation parameters for ending subscription to node configuration updates  */
typedef struct NsEndEventsNodeConfigUpdated
{
    NsEndEventsHdr     hdr;
} NsEndEventsNodeConfigUpdated;

/* This is the union of all NsEndEvents... structures.                    */
typedef union NsEndEvents
{
    NsEndEventsServicePin                       servicePin;
    NsEndEventsChangedProperty                  changedProperty;
    NsEndEventsChangedAddrNode                  changedAddrNode;
    NsEndEventsChangedNvMt                      changedNvMt;
    NsEndEventsChangedNodeAttachment            changedNodeAttachment;
    NsEndEventsChangedNodeIntfVersion           changedNodeIntfVersion;
    NsEndEventsChangedNodeConnVersion           changedNodeConnVersion;
    NsEndEventsChangedNodeCommissionStatus      changedNodeCommissionStatus;
    NsEndEventsChangedNssSubnetVersion          changedNssSubnetVersion;
    NsEndEventsChangedNssChannelVersion         changedNssChannelVersion;
    NsEndEventsChangedNssMgmtMode               changedNssMgmtMode;
    NsEndEventsChangedNssNodeVersion            changedNssNodeVersion;
    NsEndEventsChangedNssRouterVersion          changedNssRouterVersion;
    NsEndEventsChangedRouterAttachment          changedRouterAttachment;
    NsEndEventsChangedRouterCommissionStatus    changedRouterCommissionStatus;
    NsEndEventsDatabaseValidationProgress       databaseValidationProgress;
    NsEndEventsNodeConfigUpdated                nodeConfigUpdated;
} NsEndEvents;

/*************************************************************************
 *  NS_END_ALL_EVENTS
 *
 *  This "service" is sent as an event to all NSS client applications. Event
 *  generators on those clients will desubscribe the originator from all
 *  events they provide. No results or acknowlegements are sent.
 *
 *  It is intended for use by applications which use a volatile receive
 *  side database to ensure that event subscriptions are cleared up after
 *  an abnormal termination.
 *
 *  This service should be sent using the API function NsInvokeService().
 *************************************************************************/

typedef struct
{
    ManagerHandle   managerHandle;
} NsEndAllEvents;


/*************************************************************************/
/*  The following are event-specific service structures                  */
/*  When an event occurs, the NSS notifies the client by invoking the    */
/*  event as a service on the client.                                    */
/*  The invocation structure contains the event data.  The client does   */
/* not send a result to the NSS.  Instead the client calls the API       */
/* function NsTerminateCurrentServer().                                  */
/*************************************************************************/

/*************************************************************************/
/*  This is a generic structure whose fields appear in all events.       */
/*************************************************************************/

typedef struct NsEventHdr
{
    EventTag        eventTag;
} NsEventHdr;

/*************************************************************************/
/*  NS_SERVICE_PIN event - invocation parameters                         */
/*  The NSS invokes this event as a service on the client.               */
/*************************************************************************/

typedef struct NsServicePin
{
    EventTag        eventTag;       /* same as NsEventHdr */
    NodeHandle      nodeHandle;     /* zero if node has not been registered */
    NeuronId        neuronId;
    ProgramId       programId;
    Location        location;
    ChannelHandle   channelHandle;
                                    /* auxiliary information when neuron is */
                                    /* also a special type of object:       */
    ObjectCategory  auxType;        /*  if nonzero, the special object type */
    union {                         /*  if nonzero, the object reference    */
        RouterHandle routerHandle;  /*   used when auxObjectType==CAT_ROUTER*/
    }               auxRef;
} NsServicePin;

/*************************************************************************/
/*  NS_LCA_GENERATED_EVENT event - invocation parameters                 */
/*  The NSS invokes this event as a service on the client.               */
/*************************************************************************/

typedef struct NsLcaGeneratedEvent
{
    EventTag        eventTag;        /* same as NsEventHdr */
    Byte            lcaEventData[1]; /* Variable length data.  */
} NsLcaGeneratedEvent;

/*************************************************************************/
/*  NS_MISSED_EVENT event - invocation parameters                        */
/*************************************************************************/

typedef struct NsMissedEvent
{
    EventTag        eventTag;       /* same as NsEventHdr */
    Bool            unrecoverable;  /* TRUE => full resync required */
                                    /* FALSE => invoke NS_RESYNC_EVENTS */
    NetWord         numberMissed;   /* Number of events missed (0 if unknown */
} NsMissedEvent;


/*************************************************************************/
/*  NS_PING_EVENT event - invocation parameters                          */
/*************************************************************************/

typedef struct NsPingEvent
{
    EventTag        eventTag;       /* same as NsEventHdr */
    NetWord         pingInterval;   /* latest ping interval */
} NsPingEvent;


/****************************************************************************/
/*  NS_CHANGED_PROPETY event - invocation parameters.                       */
/*  The NSS invokes this event as a service on the client.                  */
/*                                                                          */
/*  The invocation structure consists of an event tag for correlating with  */
/*  the event subscription, and options mask, an object reference identify  */
/*  the property and the container object, and the property value whose     */
/*  structure depends on the property.                                      */
/*  Use macro INVALID_CHANGED_PROPERTY_EVENT to determine whether this is a */
/*  "valid" or "invalid" change property event (see NsInvalidProperty).     */
/****************************************************************************/

/*************************************************************/
/* CAT_NODE - Property change events for node properties     */
/*************************************************************/

/* PROP_NODE_ADDRESS change: indicates changes in the node's                */
/* domain/subnet/node address.                                              */
typedef struct NsChangedAddrNode
{
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;        /* Check for INVALID_PROPERTY set */
    NodeRef            objRef;         /* PROP_NODE_ADDRESS */
    AddrNode           value;          /* property value                       */
    Bool               nmAuthentication;    /* True if node has NM auth enabled */
} NsChangedAddrNode;


/* PROP_MT_ADDRESS change:  indicates changes in the node's message tag     */
/* connection address.                                                      */
typedef struct NsChangedAddrMt
{
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;        /* Check for INVALID_PROPERTY set */
    NvMtRef            objRef;         /* PROP_MT_ADDRESS */
    AddrMt             value;          /* property value                       */
} NsChangedAddrMt;


/* PROP_NV_ADDRESS change:  indicates changes in the address for sending    */
/* an nv message to the node.                                               */
typedef struct NsChangedAddrNv
{
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;        /* Check for INVALID_PROPERTY set */
    NvMtRef            objRef;         /* PROP_NV_ADDRESS */
    AddrNv             value;          /* property value                       */
} NsChangedAddrNv;

/* PROP_NV_CONN_INFO,  PROP_ALIAS_CONN_INFO, 
   change:  indicates changes in a connection's       */
/* nv selector or address.                                                             */
typedef struct NsChangedAddrNvConnInfo
{
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;        /* Check for INVALID_PROPERTY set */
    NvMtRef            objRef;         /* PROP_NV_CONN_INFO */
    NvConnInfo         value;
} NsChangedAddrNvConnInfo;


/* PROP_ALIAS_CONN_INFO change:  indicates changes in an alias connection's  */
/* nv selector or address.  Note that this is subscribed to implicitly by    */
/* subscribing to change events for PROP_NV_CONN_INFO.                       */
typedef struct NsChangedAddrAliasConnInfo
{
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;        /* Check for INVALID_PROPERTY set */
    NvMtRef            objRef;         /* PROP_ALIAS_CONN_INFO */
    AliasConnInfo      value;
} NsChangedAddrAliasConnInfo;

/*  PROP_NODE_ATTACHMENT change:  Indicates that node attachment status
    changed - either it has disconnected from the network or its state
    has changed unexpectedly.
*/
typedef struct NsChangedNodeAttachment
{
    EventTag            eventTag;   /* same as NsEventHdr */
    ChangePropertyMask  options;
    NodeRef             objRef;     /* node reference     */
    NodeAttachment      value;      /* Node's new attachment status */
} NsChangedNodeAttachment;

/*  PROP_NODE_INTF_VERSION (or PROP_NODE_INTF_VERSION_V3.23) change:  Indicates that nodes external interface has
    changed - NV added, removed, or type changed.
*/
typedef enum _NodeIntfAction
{
    NodeIntfAction_NV_ADDED             = OBJECT_ADDED,    
    NodeIntfAction_NV_REMOVED           = OBJECT_REMOVED,      
    NodeIntfAction_NV_MODIFIED          = OBJECT_MODIFIED,
    NodeIntfAction_NV_RENAMED,             // LCA GENERATED
    NodeIntfAction_LMOBJ_ADDED,
    NodeIntfAction_LMOBJ_REMOVED,
    NodeIntfAction_LMOBJ_MBR_ADDED,
    NodeIntfAction_LMOBJ_MBR_REMOVED,
    NodeIntfAction_LMOBJ_RENAMED,                  // LCA GENERATED
    NodeIntfAction_LMOBJ_RENAMEDPROGRAMMATICNAME,
    NodeIntfAction_MESSAGETAG_ADDED,
    NodeIntfAction_MESSAGETAG_REMOVED,
    NodeIntfAction_MESSAGETAG_RENAMED,            
} _NodeIntfAction;
typedef NetEnum(_NodeIntfAction) NodeIntfAction;

typedef struct NsChangedNodeIntfVersion
{
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;        /* Check for INVALID_PROPERTY set */
    NodeRef            objRef;         /* node reference                       */
    NsTypedIndex       nvIndex;        /* Index of the NV added, removed, or changed */
    Version            value;          /* The interface version of the node after the change */
    NodeIntfAction     action;         /* The type of change that occurred */
    union   {                       /* Based on 'action', the corresponding data */
        struct {
            NvCount arraySize;
            NvType  nvType;
            NvName  nvName;
        } added;                    /* Data for NV added event */
        struct {
            NvCount arraySize;
        } removed;                  /* Data for NV removed event */
        struct {
            NvCount arraySize;
            NvType  nvType;
            NvName  nvName;
        } modified;                 /* Data for NV modified event */

        struct {
            LmObjIndex objIndex;
            char       objName[LMOBJ_NAME_LEN+1];
            LmObjType  objType;
        } lmObjAdded;
        struct {
            LmObjIndex objIndex;
        } lmObjRemoved;
        struct {
            LmObjIndex objIndex;
            char       objName[LMOBJ_NAME_LEN+1];
        } lmObjRenamed;

        NvLmObj lmObjMemberAdded;
        NvLmObj lmObjMemberRemoved;

        struct {
            NsTypedIndex    dmtIndex;
            NsName          dmtName;
        } dynamicMsgTagAdded;

        struct {
            NsTypedIndex    dmtIndex;
        } dynamicMsgTagRemoved;

        struct {
            NsTypedIndex    dmtIndex;
            NsName          dmtName;
        } dynamicMsgTagRenamed;

    } info;
} NsChangedNodeIntfVersion;

/* PROP_NODE_CONN_VERSION change:  Indicates a connection member has been
   added or removed.
*/
typedef enum _NodeConnAction
{
    NodeConnAction_TARGET_ADDED      = OBJECT_ADDED,   
    NodeConnAction_TARGET_REMOVED    = OBJECT_REMOVED, 
    NodeConnAction_DESC_CHANGED      = OBJECT_MODIFIED,
} _NodeConnAction;
typedef NetEnum(_NodeConnAction) NodeConnAction;

typedef struct NsChangedNodeConnVersion
{
    EventTag            eventTag;       /* same as NsEventHdr                       */
    ChangePropertyMask  options;
    NodeRef             objRef;         /* node reference                           */
    Version             value;          /* The node conn version after the change   */
    NodeConnAction      action;         /* The type of change that occurred         */
    NsTypedIndex        nvMtIndex;      /* Index of the NV or MT added or removed   */
    ConnectionElement   connHub;        /* Hub of connection involved               */
} NsChangedNodeConnVersion;

/*  PROP_NODE_COMMISSION_STATUS change:  Indicates that node is about to be
    updated, has failed to update, or has successfully updated.
*/
typedef struct NsChangedNodeCommissionStatus
{
    EventTag            eventTag;   /* same as NsEventHdr */
    ChangePropertyMask  options;
    NodeRef             objRef;     /* node reference                       */
    CommissionStatus    value;      /* Node's new status */
} NsChangedNodeCommissionStatus;

/*************************************************************/
/* CAT_NSS - Property change events for NSS properties       */
/*************************************************************/

/* PROP_NSS_CHANNEL_VERSION change:  Indicates a channel definition has been
   added or removed.
*/
typedef struct NsChangedNssChannelVersion
{
    EventTag           eventTag;       /* same as NsEventHdr                       */
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                            */
    Version            value;          /* The NSS channel version after the change */
    ObjectAction        action;         /* The type of change that occurred         */
    ChannelHandle      channelHandle;  /* Handle of channel added or removed       */
} NsChangedNssChannelVersion;

/* PROP_NSS_MGMT_MODE change:  Indicates the NSS managment mode has changed
*/
typedef struct NsChangedNssMgmtMode
{
    EventTag           eventTag;       /* same as NsEventHdr                       */
    ChangePropertyMask options;
    NssRef             objRef;         /* NSS reference                            */
    MgmtMode           value;          /* The NSS management mode                  */
} NsChangedNssMgmtMode;

/* PROP_NSS_NODE_VERSION change:  Indicates a node has been registered, added,
   removed, or deregistered.
*/
typedef struct NsChangedNssNodeVersion
{
    EventTag            eventTag;   /* same as NsEventHdr                       */
    ChangePropertyMask  options;
    NssRef              objRef;     /* NSS reference                            */
    DeviceSetVersion    value;      /* The NSS node versions after the change   */
    ObjectAction        action;     /* The type of change that occurred         */
    NodeHandle          nodeHandle; /* Handle of node which changed             */
} NsChangedNssNodeVersion;

/* PROP_NSS_NSI_VERSION change:  Indicates an NSI has been added, removed.
*/
typedef struct NsChangedNssNsiVersion
{
    EventTag            eventTag;   /* same as NsEventHdr                       */
    ChangePropertyMask  options;
    NssRef              objRef;     /* NSS reference                            */
    Version             value;      /* The NSS node versions after the change   */
    ObjectAction        action;     /* The type of change that occurred         */
    NsiHandle           nsiHandle;  /* The handle of the NSI that has changed.  */
    NodeHandle          nodeHandle; /* The node handle associated with the NSI  */
} NsChangedNssNsiVersion;

/* PROP_NSS_ROUTER_VERSION change:  Indicates a router has been registered, added,
   removed, or deregistered.
*/
typedef struct NsChangedNssRouterVersion
{
    EventTag            eventTag;    /* same as NsEventHdr                       */
    ChangePropertyMask  options;
    NssRef              objRef;      /* NSS reference                            */
    DeviceSetVersion    value;       /* The NSS router versions after the change */
    ObjectAction        action;      /* The type of change that occurred         */
    RouterHandle        routerHandle;/* Handle of router which changed           */
} NsChangedNssRouterVersion;

/* PROP_NSS_SUBNET_VERSION change:  Indicates a subnet definition has been
   added or removed.
*/
typedef struct NsChangedNssSubnetVersion
{
    EventTag            eventTag;       /* same as NsEventHdr                       */
    ChangePropertyMask  options;
    NssRef              objRef;         /* NSS reference                            */
    Version             value;          /* The NSS subnet version after the change  */
    ObjectAction        action;         /* The type of change that occurred         */
    DomainHandle        domainHandle;   /* Handle of domain to which subnet is/was defined */
    SubnetId            subnetId;       /* ID of added or removed subnet            */
} NsChangedNssSubnetVersion;


/*************************************************************/
/* CAT_ROUTER - Property change events for router properties */
/*************************************************************/

/*  PROP_ROUTER_ATTACHMENT change:  Indicates that router attachment status
    changed - either it has disconnected from the network or its state
    has changed unexpectedly.
*/
typedef struct NsChangedRouterAttachment
{
    EventTag            eventTag;   /* same as NsEventHdr */
    ChangePropertyMask  options;
    RouterRef           objRef;     /* Router reference     */
    NodeAttachment      value;      /* Router's new attachment status */
} NsChangedRouterAttachment;

/*  PROP_ROUTER_COMMISSION_STATUS change:  Indicates that router is about to be
    updated, has failed to update, or has successfully updated.
*/
typedef struct NsChangedRouterCommissionStatus
{
    EventTag            eventTag;   /* same as NsEventHdr   */
    ChangePropertyMask  options;
    RouterRef           objRef;     /* router reference     */
    CommissionStatus    value;      /* Node's new status    */
} NsChangedRouterCommissionStatus;

/****************************************************************************/
/* The following are generic structures for accessing common fields of the  */
/* various property-changed event structures.  They may be useful for       */
/* initially decoding the type of properties and container objects in an    */
/* event.                                                                   */
/****************************************************************************/

/* Generic structure for all changed-property events */
typedef struct NsChanged
{
    EventTag            eventTag;       /* same as NsEventHdr */
    ChangePropertyMask  options;
    ObjectCategory      cat;            /* object category */
    Property            property;       /* property that is changed */
    /* object-specific handles follows...*/
} NsChanged;

/* Generic structure for all changed-node-property events.                  */
/* Useful for decoding of property-change events for nodes.                 */
typedef struct NsChangedNode
{
    /* eventTag, objRec.cat, and objRef.property align with NsChanged */
    EventTag           eventTag;
    ChangePropertyMask options;
    NodeRef            objRef;
} NsChangedNode;

/* Generic structure for all changed-NSS-property events.                   */
/* Useful for decoding of property-change events for the NSS.               */
typedef struct NsChangedNss
{
    /* eventTag, objRec.cat, and objRef.property align with NsChanged */
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;
    NssRef             objRef;
} NsChangedNss;

/* Generic structure for all changed-nvmt-property events.                  */
/* Useful for decoding of property-change events for nv or mtags.           */
typedef struct NsChangedNvMt
{
    /* eventTag, objRec.cat, and objRef.property align with NsChanged */
    EventTag           eventTag;       /* same as NsEventHdr */
    ChangePropertyMask options;
    NvMtRef            objRef;
} NsChangedNvMt;

/* Generic structure for all changed-router-property events.                */
/* Useful for decoding of property-change events for routers.               */
typedef struct NsChangedRouter
{
    /* eventTag, objRec.cat, and objRef.property align with NsChanged */
    EventTag            eventTag;       /* same as NsEventHdr */
    ChangePropertyMask  options;
    RouterRef           objRef;
} NsChangedRouter;

/* union of all invocation structures */
typedef union NsChangedProperty
{
    NsChanged                       changed;            /* Generic reference for all NsChangedProperty events */
    NsChangedNss                    changedNss;         /* Generic reference for all NSS Prop events */
    NsChangedNode                   changedNode;        /* Generic reference for all Node Prop events */
    NsChangedNvMt                   changedNvMt;        /* Generic reference for all NvMt Prop events */
    NsChangedRouter                 changedRouter;      /* Generic reference for all Router Prop events */
    NsChangedAddrNode               addrNode;           /* PROP_NODE_ADDRESS */
    NsChangedAddrMt                 addrMt;             /* PROP_MT_ADDRESS */
    NsChangedAddrNv                 addrNv;             /* PROP_NV_ADDRESS */
    NsChangedAddrNvConnInfo         addrNvConnInfo;     /* PROP_NV_CONN_INFO */
    NsChangedAddrAliasConnInfo      addrAliasConnInfo;  /* PROP_ALIAS_CONN_INFO */
    NsChangedNodeCommissionStatus   nodeCommissionStatus; /* PROP_NODE_COMMISSION_STATUS */
    NsChangedNodeIntfVersion        nodeIntfVersion;    /* PROP_NODE_INTF_VERSION */
    NsChangedNodeConnVersion        nodeConnVersion;    /* PROP_NODE_CONN_VERSION */
    NsChangedNssSubnetVersion       nssSubnetVersion;   /* PROP_NSS_SUBNET_VERSION */
    NsChangedNssChannelVersion      nssChannelVersion;  /* PROP_NSS_CHANNEL_VERSION */
    NsChangedNssMgmtMode            nssMgmtMode;        /* PROP_NSS_MGMT_MODE */
    NsChangedNssRouterVersion       nssRouterVersion;   /* PROP_NSS_ROUTER_VERSION */
    NsChangedNssNodeVersion         nssNodeVersion;     /* PROP_NSS_NODE_VERSION */
    NsChangedNssNsiVersion          nssNsiVersion;      /* PROP_NSS_NSI_VERSION */
    NsChangedRouterCommissionStatus routerCommissionStatus; /* PROP_ROUTER_COMMISSION_STATUS */
    NsChangedNodeAttachment         nodeAttachment;     /* PROP_NODE_ATTACHMENT */
    NsChangedRouterAttachment       routerAttachment;   /* PROP_ROUTER_ATTACHMENT */
} NsChangedProperty;

/****************************************************************************/
/*  Invalid changed property event invocation parameters.                   */
/*  When a NS_CHANGE_PROPERY event's option mask contains INVALID_PROPERTY, */
/*  the event is an invalid change property event (see macro                */
/*  INVALID_CHANGED_PROPERTY_EVENT).  Note that invalid change property     */
/*  event structures are identical to thier change property counter parts   */
/*  except that they do not contain the property value.                     */
/*                                                                          */
/*  The NSS invokes this event as a service on the client. These events are */
/*  are generated prior to changing certain properties for which the client */
/*  is subscribed, to tell the client that its current data is invalid.     */
/*  After the property has been succefully updated, the client will recieve */
/*  a corresponding NS_CHANGED_PROPERTY event.                              */
/*                                                                          */
/*  A client specifies an interest in invalid NS_CHANGE_PROPERTY events     */
/*  when subscribing to NS_CHANGED_PROPERTY by setting the INVALID_PROPERTY */
/*  value in the options mask.                                              */
/*                                                                          */
/*  The invocation structure consists of an event tag for correlating with  */
/*  the event subscription, the options flags and an object reference       */
/*  identify the property which is temporarily invalid.                     */
/****************************************************************************/

typedef NsChanged NsInvalid;

/* Generic structure for all changed-node-property events.                  */
/* Useful for decoding of property-change events for nodes.                 */
typedef NsChangedNode NsInvalidNode;

/* Generic structure for all changed-nvmt-property events.                  */
/* Useful for decoding of property-change events for nv or mtags.           */
typedef NsChangedNvMt NsInvalidNvMt;

/* PROP_NODE_ADDRESS & PROP_NODE_ADDRESS_V20 invalid:                       */
typedef NsInvalidNode NsInvalidAddrNode;

/* PROP_MT_ADDRESS                                                          */
typedef NsInvalidNvMt NsInvalidAddrMt;

/* PROP_NV_ADDRESS                                                          */
typedef NsInvalidNvMt NsInvalidAddrNv;

/* PROP_NV_CONN_INFO                                                        */
typedef NsInvalidNvMt NsInvalidAddrNvConnInfo;

/* PROP_ALIAS_CONN_INFO                                                     */
typedef NsInvalidNvMt NsInvalidAddrAliasConnInfo;

/* union of all invocation structures */
typedef union NsInvalidProperty
{
    NsInvalidAddrNode           addrNode;           /* PROP_NODE_ADDRESS & PROP_NODE_ADDRESS_V20 */
    NsInvalidAddrMt             addrMt;             /* PROP_MT_ADDRESS  */
    NsInvalidAddrNv             addrNv;             /* PROP_NV_ADDRESS */
    NsInvalidAddrNvConnInfo     addrNvConnInfo;     /* PROP_NV_CONN_INFO */
    NsInvalidAddrAliasConnInfo  addrAliasConnInfo;  /* PROP_ALIAS_CONN_INFO */
} NsInvalidProperty;

/* -------------------------------------------------------------------------
   NS_MISSED_EVENT_NOTIFICATION
   This event is sent by NSS10 Rel 1.02 only, and is obsoleted in NSS for Windows
   and subsequent NSS10.  */

typedef struct NsMissedEventNotification
{
    ServiceCode missedEventServiceCode;     /* The missed event.  May be
                                            * NS_UNKNOWN_SERVICE_CODE to
                                            * indicate that too many types
                                            * of events have been missed
                                            * to keep track of.
                                             */
    Byte        numMissedEvents;            /* The #of times event was missed. */
    Byte        numBytesOfQualifyingData;   /* Qualifying data is used to
                                             * further qualify the type of
                                             * event notification. */
    /* Qualifying data follows */
} NsMissedEventNotification;

/* -------------------------------------------------------------------------
   NS_VALIDATE_DATABASE_PROGRESS_EVENT
   This event is sent during DB validation  */
typedef NetLong DbValidationPhaseId;
typedef NetLong DbValidationStepId;

typedef struct NsValidateDatabaseProgressEvent
{
    EventTag eventTag;              /* same as NsEventHdr */
	NetLong totalNumberOfErrors;
	NetWord totalNumberOfPhases;	// From the service invocation;
	NetWord phaseNumber;		    // Current phase number.  NSS phase # + initial phase number.
	NetWord phasePercentage;
	NetWord nssTotalNumberOfPhases; // Number of NSS phases. 
	NetWord nssPhaseNumber;	        // nss phase number.
    DbValidationPhaseId phaseStringId;
    char                phaseObjectName[32];
    DbValidationStepId  stepStringId;
	char                stepObjectName[32];
    NetLong progressInStep;
} NsValidateDatabaseProgressEvent;


/* -------------------------------------------------------------------------
   NS_NODE_CONFIG_UPDATED_EVENT
   This event is sent by the sweep as the node is updated. This event 
   superceedes property change events that are also generated by the sweep. 
 */

/* This enum describes the type of configuration that was modified on the node.*/
typedef enum _NodeConfigUpdateType
{
    NODE_CONFIG_UPDATE_NODE_ADDRESS,        /* Node's address has changed */
    NODE_CONFIG_UPDATE_NV_ADDED,            /* An NV has been added */
    NODE_CONFIG_UPDATE_NV_REMOVED,          /* An NV has been removed */
    NODE_CONFIG_UPDATE_NV_TYPE_MODIFIED,    /* An NV type has been modified */
    NODE_CONFIG_UPDATE_NV_CONFIGURATION,    /* The NV Config of an NV has been modified */
    NODE_CONFIG_UPDATE_NODE_MAX
} _NodeConfigUpdateType;

typedef NetEnum(_NodeConfigUpdateType) NodeConfigUpdateType;

/* NS_NODE_CONFIG_UPDATED_EVENT change:  Indicates the nodes configuration has 
 * been updated. This event is generated by the sweep.
 */
typedef struct NsNodeConfigUpdateEvent
{
    EventTag                eventTag;   /* same as NsEventHdr                       */
    NodeHandle              nodeHandle;     /* */
    NodeConfigUpdateType    updateType;     /* */
    union
    {
        struct
        {  /* Data for NODE_CONFIG_UPDATE_NODE_ADDRESS  */
            AddrNode  address;            /* Node address                     */
            Bool      nmAuthentication;   /* True if node has NM auth enabled */
        } nodeAddress;

        struct
        {  /* Data for NODE_CONFIG_UPDATE_NV_ADDED  */
            NsTypedIndex nvIndex;  /* Index of the NV added */
            NvCount arraySize;
            NvType  nvType;
            NvName  nvName;
        } nvAdded;

        struct
        {  /* Data for NODE_CONFIG_UPDATE_NV_REMOVED  */
            NsTypedIndex nvIndex;  /* Index of the NV that was removed */
            NvCount arraySize;	
        } nvRemoved;

        struct
        {  /* NODE_CONFIG_UPDATE_NV_TYPE_MODIFIED */
            NsTypedIndex nvIndex;  /* Index of the NV whose type was modified */
            NvCount arraySize;
            NvType  nvType;
            NvName  nvName;
        } nvTypeModified;

        struct
        {  /* NODE_CONFIG_UPDATE_NV_CONFIGURATION  */
            NsTypedIndex nvIndex;  /* Index of the NV whose Config was modified */
            AddrNv       value;    /* PROP_ADDR_NV                       */
        } nvConfig;
    };
} NsNodeConfigUpdateEvent;

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#ifdef _MSC_VER
#pragma pack()
#endif

#endif
