#if !defined(AFX_DLGCHOOSEFORMAT_H__6874F0A4_EDDD_44DF_847A_49C62F0E4C63__INCLUDED_)
#define AFX_DLGCHOOSEFORMAT_H__6874F0A4_EDDD_44DF_847A_49C62F0E4C63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgChooseFormat.h : header file
//

#include "LNSFormatter_res.h"
#include "lcadrf.h"

/////////////////////////////////////////////////////////////////////////////
// DlgChooseFormat dialog

class DlgChooseFormat : public CSysColorDialog
{
// Construction
public:
	typedef enum {
		NO_FORMAT = 0,
		SNVT,
		UNVT,
	} FormatOption; 

	PLdrfFileInfo m_pCatFileInfo;
	CString m_strTypeFileName;
	ProgramId m_progID;
	int m_nScope;
	CString m_strTitle;

	CString m_strCurFormat;
	ProgramId m_curProgID;
	int m_nCurScope;

	DlgChooseFormat(int nCurScope, ProgramId curProgID, LPCTSTR szCurFormat, CWnd* pParent = NULL, LPCTSTR szTitle = NULL);   // standard constructor
	~DlgChooseFormat();
	void BuildUserLists();
	void ReportLdrfError(LPCTSTR szText, TLdrfErrCodes err);
	void RefreshTypes();
	void BuildSnvtLists ();
	void RefreshFormats();
	void PrepareTypeFileInfo();
	void SelectCurFormat();
	void OnChangeType();

// Dialog Data
	//{{AFX_DATA(DlgChooseFormat)
	enum { IDD = IDD_CHOOSE_FORMAT };
	CListBox	m_formats;
	CListBox	m_types;
	CString	m_strUnit;
	int		m_nOption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgChooseFormat)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgChooseFormat)
	virtual BOOL OnInitDialog();
	afx_msg void OnSnvtOnly();
	afx_msg void OnSelchangeTypeFiles();
	afx_msg void OnSelchangeFormats();
	virtual void OnOK();
	afx_msg void OnNoFormat();
	afx_msg void OnSnvt();
	afx_msg void OnUnvt();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCHOOSEFORMAT_H__6874F0A4_EDDD_44DF_847A_49C62F0E4C63__INCLUDED_)
